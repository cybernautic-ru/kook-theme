<?php
/**
 * Template Name: Recipe user
 *
 */
?>
<?php get_header(); ?>
  <div class="os-container top-bar-w">
    <div class="top-bar">
		<ul>
			<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
		</ul>
	</div>
  </div>
	<!-- slider start-->
	<div class="os-container community-mobile-view">
		<?php echo do_shortcode('[community-mobile-slider]');?>
	</div>
	<div class="os-container image-cols community-desktop-view">
		<?php echo do_shortcode('[webz-community-slider]');?>
	</div>
	<!-- slider end -->
	<div class="os-container upage">
		<h1>kookmutsjes COMMUNITY</h1>
		<div class="search-recipes">
			<?php echo do_shortcode('[wd_asp id=7]');?>
		</div>
		<div class="row userpostlist desktop-view">
			<?php
				$currentPage = get_query_var('paged');
			    $administrator = get_users(array('role__in' => 'administrator','fields' => 'ID'));
				$siteadmins = get_users(array('role__in' => 'site_admin','fields' => 'ID'));
				$admins = array_merge($administrator,$siteadmins);
				$args = array(
					'post_type'   => 'osetin_recipe',
					'post_status' => 'publish',
					'author__not_in' => $admins,
					'posts_per_page' => 8,
					'paged' => $currentPage
				);
				$osrecipes = new WP_Query( $args );
				if( $osrecipes->have_posts() ) :
			?>
			<?php
				while( $osrecipes->have_posts() ) :
				$osrecipes->the_post();
			?>
			<div class="col-md-3">
				<div class="user-recipes">
					<a class="featuredimg" href="<?php echo get_permalink(); ?>">
					<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} 
					?>
					</a>
					<div class="upage-info-box">
						<h2><a href="<?php echo get_permalink(); ?>"><?php the_title();?></a></h2>
						<ul class="authorboxs">
							<li><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><span>door</span> <?php the_author_meta( 'display_name' ); ?></a></li>
						</ul>
						<ul class="metaboxs">
							<li>
								<a href="<?php the_permalink(); ?>"><?php _e('Lees meer', 'osetin'); ?></a>
							</li>
							<li>
								<!-- custimize carbojet-->
								<?php if(is_user_logged_in())
								{
									$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
									//var_dump($saved_favorite);
									$redirect = 'false';
									if(!$saved_favorite_post){
									$saved_favorite_post = array();
									}
								}
								else{
								$redirect = 'true';
								$saved_favorite_post = array();
								}?>
								<div class="favorite">
									<?php
									if(in_array(get_the_ID(),$saved_favorite_post)){?>
									<button disabled name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark"></i> Bewaar</button>
									<?php }else{ ?>
									<button name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark-o"></i> Bewaar</button>
									<?php }	?>	
									<!--<span><?php echo get_post_meta(get_the_ID(), '_osetin_vote', true) ?></span>-->
								</div>
							</li>
							<li>
								<?php if(!in_array('like', $hidden_elements_array)){ ?>
								<div class="archive-item-views-count-likes">
									<?php osetin_vote_build_button(get_the_ID(), 'slide-button slide-like-button'); ?>
								</div>
								<?php } ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php
			endwhile;
			?>
			<?php
			else :
			esc_html_e( '', 'text-domain' );
			endif;
			echo "<div class='page-nav-container'>" . paginate_links(array(
					'total' => $osrecipes->max_num_pages,
					'prev_text' => __('<'),
					'next_text' => __('>')
				)) . "</div>";
			?>
		</div>
		<div class="row userpostlist mobile-view">
			<?php
			    $administrator = get_users(array('role__in' => 'administrator','fields' => 'ID'));
				$siteadmins = get_users(array('role__in' => 'site_admin','fields' => 'ID'));
				$admins = array_merge($administrator,$siteadmins);
				$args = array(
					'post_type'   => 'osetin_recipe',
					'post_status' => 'publish',
					'author__not_in' => $admins,
					'paged'=>1,
					'posts_per_page' => 4,
				);
				$osrecipes = new WP_Query( $args );
				if( $osrecipes->have_posts() ) :
			?>
			<?php
				while( $osrecipes->have_posts() ) :
				$osrecipes->the_post();
			?>
			<div class="col-md-3">
				<div class="user-recipes">
					<a class="featuredimg" href="<?php echo get_permalink(); ?>">
					<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} 
					?>
					</a>
					<h2><a href="<?php echo get_permalink(); ?>"><?php the_title();?></a></h2>
					<ul class="authorboxs">
						<li><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><span>door</span> <?php the_author_meta( 'display_name' ); ?></a></li>
					</ul>
					<ul class="metaboxs">
						<li>
							<a href="<?php the_permalink(); ?>"><?php _e('Lees meer', 'osetin'); ?></a>
						</li>
						<li>
							<!-- custimize carbojet-->
							<?php if(is_user_logged_in())
							{
								$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
								//var_dump($saved_favorite);
								$redirect = 'false';
								if(!$saved_favorite_post){
								$saved_favorite_post = array();
								}
							}
							else{
							$redirect = 'true';
							$saved_favorite_post = array();
							}?>
							<div class="favorite">
								<?php
								if(in_array(get_the_ID(),$saved_favorite_post)){?>
								<button disabled name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark"></i> Bewaar</button>
								<?php }else{ ?>
								<button name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark-o"></i> Bewaar</button>
								<?php }	?>	
								<!--<span><?php echo get_post_meta(get_the_ID(), '_osetin_vote', true) ?></span>-->
							</div>
						</li>
						<li>
							<?php if(!in_array('like', $hidden_elements_array)){ ?>
							<div class="archive-item-views-count-likes">
								<?php osetin_vote_build_button(get_the_ID(), 'slide-button slide-like-button'); ?>
							</div>
							<?php } ?>
						</li>
					</ul>
				</div>
			</div>
			<?php
			endwhile;
			?>
			<?php
			else :
			esc_html_e( 'Geen recepten gevonden', 'text-domain' );
			endif;
			?>
		</div>
		<h2 class="popular-head ipad-view"><sup>"</sup>Let food be thy medicine and medicine be thy food.<sup>"</sup></h2>
		<div class="trend-home wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<h2 style="font-size: 30px;color: #333333;text-align: left" class="vc_custom_heading">TRENDING OP <b>Kookmutsjes</b></h2>
					<?php echo do_shortcode('[trendings]');?>
				</div>
			</div>
		</div>
		<div class="row pu-post">
			<div class="col-md-12">
				<h2 class="popular-head desktop-title">"Let food be thy medicine and medicine be thy food."</h2>
				<?php
					$args = array(
						'posts_per_page'=>3,
						'paged'=>1,
						'post_type'=>'osetin_recipe',
						'post_status' => 'publish',
						'author__not_in' => $admins,
						'orderby'=>'post_views_count',
						'order'=>'DESC',
						'meta_query' => array(
							array(
								'key' => 'post_views_count',
							),
						),	
					);
					$topviewedposts = get_posts( $args );
					
					
				?>
				<div class="row community-p-recipes">
					<?php
						if ( count($topviewedposts) >0) {
							$key=0;
							foreach ( $topviewedposts as $postobj){	
								
								if($key==0){
									?>
									<div class="col-md-8 box1">
										<div class="popular-box">
											<img src="<?php echo get_the_post_thumbnail_url($postobj->ID,'full'); ?>" alt="popular" />
											<div class="popular-details">
												<h2><a href="<?php echo get_the_permalink($postobj->ID);?>"><?php echo get_the_title($postobj->ID);?></a></h2>
												<p><a href="<?php echo get_the_permalink($postobj->ID);?>"><?php echo wp_trim_words(get_the_excerpt($postobj->ID),100,null);?></a></p>
											</div>
										</div>
									</div>
									<div class="col-md-4 box2">
									<?php
								}else{
									?>
									<div class="popular-box<?php echo $key;?>">
										<img src="<?php echo get_the_post_thumbnail_url($postobj->ID,'full'); ?>" alt="popular" />
										<div class="popular-details-sm<?php echo $key;?>">
											<h2><a href="<?php echo get_the_permalink($postobj->ID);?>"><?php echo get_the_title($postobj->ID);?></a></h2>
										</div>
									</div>
									<?php
								}
								$key++;
							}
						}
					?>
					</div>					
				</div>
			</div>
		</div>
		<h2 class="popular-head mobile-view">"Let food be thy medicine and medicine be thy food."</h2>
		<div class="row userpostlist mobile-view">
			<?php
			    $administrator = get_users(array('role__in' => 'administrator','fields' => 'ID'));
				$siteadmins = get_users(array('role__in' => 'site_admin','fields' => 'ID'));
				$admins = array_merge($administrator,$siteadmins);
				$args = array(
					'post_type'   => 'osetin_recipe',
					'post_status' => 'publish',
					'author__not_in' => $admins,
					'paged'=>2,
					'posts_per_page' => 4,
				);
				$osrecipes = new WP_Query( $args );
				if( $osrecipes->have_posts() ) :
			?>
			<?php
				while( $osrecipes->have_posts() ) :
				$osrecipes->the_post();
			?>
			<div class="col-md-3">
				<div class="user-recipes">
					<a class="featuredimg" href="<?php echo get_permalink(); ?>">
					<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} 
					?>
					</a>
					<h2><a href="<?php echo get_permalink(); ?>"><?php the_title();?></a></h2>
					<ul class="authorboxs">
						<li><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><span>door</span> <?php the_author_meta( 'display_name' ); ?></a></li>
					</ul>
					<ul class="metaboxs">
						<li>
							<a href="<?php the_permalink(); ?>"><?php _e('Lees meer', 'osetin'); ?></a>
						</li>
						<li>
							<!-- custimize carbojet-->
							<?php if(is_user_logged_in())
							{
								$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
								//var_dump($saved_favorite);
								$redirect = 'false';
								if(!$saved_favorite_post){
								$saved_favorite_post = array();
								}
							}
							else{
							$redirect = 'true';
							$saved_favorite_post = array();
							}?>
							<div class="favorite">
								<?php
								if(in_array(get_the_ID(),$saved_favorite_post)){?>
								<button disabled name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark"></i> Bewaar</button>
								<?php }else{ ?>
								<button name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark-o"></i> Bewaar</button>
								<?php }	?>	
								<!--<span><?php echo get_post_meta(get_the_ID(), '_osetin_vote', true) ?></span>-->
							</div>
						</li>
						<li>
							<?php if(!in_array('like', $hidden_elements_array)){ ?>
							<div class="archive-item-views-count-likes">
								<?php osetin_vote_build_button(get_the_ID(), 'slide-button slide-like-button'); ?>
							</div>
							<?php } ?>
						</li>
					</ul>
				</div>
			</div>
			<?php
			endwhile;
			?>
			<?php
			else :
			esc_html_e( '', 'text-domain' );
			endif;
			?>
		</div>
	</div>
<?php get_footer(); ?>