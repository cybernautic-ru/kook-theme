<?php
/**
 * The template for displaying author pages
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); 
?>
<div class="os-container">
		<div class="myprofile">
			<div class="banner">
			</div>
			<?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));?>
			<?php global $author;  $author = get_user_by( 'slug', get_query_var( 'author_name' ) );?>
			<div class="profile-body">
				<div class="row authorrow">
					<div class="col-md-3 sidebarprofile">
						<div class="user-info-part">
							<div class="profile-img">
								<?php echo do_shortcode('[avatar]'); ?>
								<input type="hidden" name="user_id" value="<?php echo $author->ID;?>">
							</div>
							<div class="prfile-name">
								<p><?php echo $curauth->nickname; ?></p>
								<h2><?php echo $curauth->first_name; ?> <?php echo $curauth->last_name; ?></h2>
								<?php
									$viewfollow = true;
									
									$followactive = '';
									$unfollowactive ='';
									if(is_user_logged_in() ){
										$current_user_id = get_current_user_id();
										$userlogin = true;
										$follow = unserialize( get_user_meta($author->ID,'follow',true) );
										if($author->ID==$current_user_id){
											$viewfollow = false;
										}
									}else{
										$current_user_id = 0;
										$userlogin = false;
										$follow=null;
									}
									//var_dump($follow['followers_list']);
									if(is_array($follow) && in_array($current_user_id,$follow['followers_list'])){
										$unfollowactive = 'active';
									}else{
										$followactive = 'active';
										
									}
									if($viewfollow){
								?>
								<ul class="followingsection">
									<li>
										<?php if($author->ID==11){?>										
											<button name="unfollow" class="unfollow active" type="button" data-unfollow="<?php echo $author->ID;?>" data-userlogin="<?php echo $userlogin;?>" disabled>Unfollow</button>
										<?php }else{?>
										<button name="follow" class="follow <?php echo $followactive;?>" type="button" data-follow="<?php echo $author->ID;?>" data-userlogin="<?php echo $userlogin;?>">Follow</button>
										<button name="unfollow" class="unfollow <?php echo $unfollowactive;?>" type="button" data-unfollow="<?php echo $author->ID;?>" data-userlogin="<?php echo $userlogin;?>">Unfollow</button>
										<?php }?>
									</li>
									<li><button type="button" class="messagepopup" data-toggle="modal" data-target="#myModal">Message</button></li>
									<?php }?>
								</ul>
								<p class="message following-error-message">Je dient eerst ingelogd te zijn <a href="<?php echo get_site_url(); ?>/log-in">Log In</a></p>
							</div>
							<!-- Message Modal -->
							<div id="myModal" class="modal fade messagemodal" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title"><i class="fa fa-envelope"></i> Schrijf een bericht</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											
											<?php echo do_shortcode("[z-form mail='".$author->user_email."']"); ?>
										</div>
										<div class="modal-footer">
											<button type="submit" id="sent-msg" class="btn btn-default"><span>Bericht versturen </span><div class="lds-ellipsis" style="display: none;"><div></div><div></div><div></div><div></div></div></button>
										</div>
									</div>
								</div>
							</div>
							<div class="follwers">
								<ul class="author-desktop-none">
									<li>
										<p>Followers</p>
									</li>
									<li>
										<p>Following</p>
									</li>
									<li>
										<p>Recipes</p>
									</li>
								</ul>
								<?php echo do_shortcode('[follow-follower-posts userid="'.$author->ID.'"]'); ?>
							</div>
						</div>
						<div class="profile-menu">
							<div class="tab desktop-qa">
								<button class="tablinks" onclick="openCity(event, 'myrecipes')" id="defaultOpen"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/fork.png" alt="profile-icon" /> Mijn recepten</button>
								<button class="tablinks" onclick="openCity(event, 'myfavorite')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/tags.png" alt="profile-icon" /> Mijn favorieten</button>
								<button class="tablinks" onclick="openCity(event, 'week')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/wine-menu.png" alt="profile-icon" /> Mijn weekmenu</button>
							</div>
							<div class="tab mobile-qa">
								<button class="tablinks" onclick="openCity(event, 'myrecipes')" id="defaultOpen">Recepten</button>
								<button class="tablinks" onclick="openCity(event, 'myfavorite')">Favorieten</button>
								<button class="tablinks" onclick="openCity(event, 'week')">Weekmenu</button>
							</div>
						</div>
						<div class="najatsocial">
							<ul>
								<li>
						            <?php 
						            	$website = $author->user_url;
						             ?>
									<img src="<?php echo site_url();?>/wp-content/uploads/2019/09/language.png" alt="Profile-icon"/> <span><?php echo ($website) ? $website : 'No Website Link';?></span>
								</li>
								<li>
									<?php
										$insta = get_user_meta($author->ID, 'instagram_profile', true );
									 ?>
									<img src="<?php echo site_url();?>/wp-content/uploads/2019/09/instagram.png" alt="Profile-icon"/> <span><?php echo ($insta) ? $insta : 'No Instagram Link';?></span>
								</li>
								<li>
									<?php
									  $spapchat = get_user_meta( $author->ID, 'snapchat_profile', true );
									 ?>
									<img src="<?php echo site_url();?>/wp-content/uploads/2019/09/snapchat.png" alt="Profile-icon"/> <span><?php echo ($spapchat) ? $spapchat : 'No Snapchat Link';?></span>
								</li>
								<li>
									<?php
										$pint = get_user_meta( $author->ID, 'pinterest_profile', true );
									 ?>
									<img src="<?php echo site_url();?>/wp-content/uploads/2019/09/pinterest.png" alt="Profile-icon"/> <span><?php echo ($pint) ? $pint : 'No Pinterest Link';?></span>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-9 bodyprofile">
						<div class="row">
							<div class="col-md-12">
								<div class="tab-contents">
									<div class="biofor">
										<h2>Biografie</h2>
										<p><?php echo $curauth->description; ?></p>
									</div>
									<div id="myrecipes" class="tabcontent">
										<div class="row">
											<div class="col-md-12 holder">
											<?php  echo do_shortcode('[get-saved-favorites userid="'.$author->ID.'" page="author" saved="0" ]'); ?>
											<?php /* ?>
												<div class="row boxofpost">
													<?php
														if(isset($_GET['author_name'])) :
															$curauth = get_userdatabylogin($author_name);
														else :
															$curauth = get_userdata(intval($author));
														endif;
													?>
													<?php
													if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
													<div class="col-md-4 myrepost">
														<div class="ppost">
															<?php the_post_thumbnail(); ?>
															<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
															<?php the_title(); ?></a>
														</div>
													</div>
													<?php endwhile; else: ?>
														<p><?php _e('Geen recepten gevonden!'); ?></p>
													<?php endif; ?>
												</div>
											<?php */ ?>
											</div>
										</div>
									</div>
									<div id="myfavorite" class="tabcontent">
										<div class="row">
											<div class="col-md-12 holder">
											<?php  echo do_shortcode('[get-saved-favorites userid="'.$author->ID.'" page="author" saved="1"]'); ?>
											</div>
										</div>
									</div>
									<div id="week" class="tabcontent">
										<div class="row">
											<div class="col-md-12">
												<?php echo do_shortcode('[auth_wk_mn_sc userid="'.$author->ID.'"]'); ?>
												<?php echo do_shortcode('[get-group-list-for-weekmenu]');?>
											</div>
										</div>
									</div>
									<script>
										jQuery(document).ready(function() {
											jQuery('#author_msg').submit(function(event) {
												event.preventDefault();
												event.stopPropagation();
												var ajaxurl = "<?php echo admin_url('admin-ajax.php');?>"
												var thisForm = jQuery( this ).serialize();
												console.log(thisForm)
												jQuery.ajax({
													 type : "post",
													 url : ajaxurl,
													 data : {action: "z_author_msg", data : thisForm},
													beforeSend : function(){
															console.log('loading...');
													},
													 success: function(response) {
														
														 //jQuery('#sent-msg').find('span').show();
														 jQuery('#sent-msg').find('.lds-ellipsis').css({'display':'none'});
														 if(response.success){
															jQuery('.form.msg').text(response.data)
															jQuery('.form.msg').addClass('success')
														 }else{
															jQuery('.form.msg').text(response.data)
														 	jQuery('.form.msg').addClass('error')
														 }
													 }
												  })   
												
											});
											jQuery('#sent-msg').click(function(event) {
												event.preventDefault();
												//jQuery(this).find('span').hide();
												jQuery(this).find('.lds-ellipsis').css({'display':'inline-block'});
												jQuery('#author_msg').submit();
											});
										});
										function openCity(evt, cityName) {
										  var i, tabcontent, tablinks;
										  tabcontent = document.getElementsByClassName("tabcontent");
										  for (i = 0; i < tabcontent.length; i++) {
											tabcontent[i].style.display = "none";
										  }
										  tablinks = document.getElementsByClassName("tablinks");
										  for (i = 0; i < tablinks.length; i++) {
											tablinks[i].className = tablinks[i].className.replace(" active", "");
										  }
										  document.getElementById(cityName).style.display = "block";
										  evt.currentTarget.className += " active";
										}
										// Get the element with id="defaultOpen" and click on it
										document.getElementById("defaultOpen").click();
									</script>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>