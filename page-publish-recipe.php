<?php
/**
 * Template Name: Recipe Publisher
 *
 */
?>
<?php 

	function osetin_can_user_post_recipe(){
	  $return = false;
	  if ( ( is_user_logged_in() && current_user_can('read') ) ) {
		$return = true;
	  }
	  return $return;
	} 
	// function to filter every field that gets submitted, but before that check if its array or not
	function osetin_acf_wp_kses_post($data) {
	  if (!is_array($data)) {
		return wp_kses_post($data);
	  }
	  $return = array();
	  foreach ($data as $index => $value) {
		$return[$index] = osetin_acf_wp_kses_post($value);
	  }
	  return $return;
	}
	// allow contributor users upload media
	if ( current_user_can('contributor') && !current_user_can('upload_files') ){ 
	  add_action('get_header', 'allow_contributor_uploads', 1);
	}
	function allow_contributor_uploads() {
	$contributor = get_role('contributor');
	$contributor->add_cap('upload_files');
	}
	// allow contributor users edit categories (in order for them to add ingredients)
	if ( current_user_can('contributor') && !current_user_can('manage_categories') ){ 
	  add_action('get_header', 'allow_contributor_category_editing', 1);
	}
	function allow_contributor_category_editing() {
	 $contributor = get_role('contributor');
	 $contributor->add_cap('manage_categories');
	}
	$new_recipe = true;
	$user_allowed_to_edit_this_post = true;
	if(osetin_can_user_post_recipe()){
		$new_post = array(
		'id' => 'frontend-publisher',
		'post_id'   => 'new_post',
		'post_title'  => true,
		'field_groups' => array(9222),
		'post_content'  => true,
		'submit_value' => __('Submit Recipe', 'osetin'),
		'updated_message' => __("Your recipe has been submitted. We will review it and publish it shortly. In the meantime you can either publish another one using a form below or just continue browsing our website.", 'osetin'),
		'new_post'    => array(
		  'post_type'   => 'osetin_recipe',
		  'post_status' => 'pending'
		)
	  );

	if(get_query_var('recipe_id_to_edit')){
		// EDITING RECIPE
		$new_recipe = false;
		if(!current_user_can('edit_post', get_query_var('recipe_id_to_edit'))){
		  $user_allowed_to_edit_this_post = false;
		}
		$new_post['post_id'] = get_query_var('recipe_id_to_edit');
		$new_post['new_post'] = false;
		$new_post['post_status'] = 'pending';
		$new_post['submit_value'] = __('Update Recipe', 'osetin');
		$new_post['updated_message'] = __("Your recipe has been updated. We will review it and publish it shortly. In the meantime you can either submit/edit another one using a form below or just continue browsing our website.", 'osetin');
	}
	//* Add required acf_form_head() function to head of page
	add_action( 'get_header', 'osetin_do_acf_form_head', 1 );
	function osetin_do_acf_form_head() {
	add_filter('acf/update_value', 'osetin_acf_wp_kses_post', 10, 1);
	acf_form_head();
	}
	}
	function tsm_deregister_admin_styles() {

	  if ( ! ( is_user_logged_in() || current_user_can('publish_posts') ) ) {
		return;
	  }
	  wp_deregister_style( 'wp-admin' );
	}
  get_header();
  ?>

  <?php while ( have_posts() ) : the_post(); ?>
  
	<div class="os-container top-bar-w">
		<div class="top-bar <?php if(!osetin_is_imaged_header(get_the_ID())) echo 'bordered'; ?>">
			<ul>
				<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			</ul>
		</div>
	 </div>

	<?php  
		if(osetin_is_imaged_header(get_the_ID())){
		if(osetin_is_bbpress()){
		$page_bg_image_url = get_template_directory_uri().'/assets/img/patterns/flowers_light.jpg';
		}else{
		$page_bg_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
		} ?>
		<div class="os-container">
			<div class="page-intro-header with-background" style="<?php echo osetin_get_css_prop('background-image', $page_bg_image_url, false, 'background-repeat: repeat; background-position: top left; '); ?>">
			<h2><?php echo osetin_get_the_title(get_the_ID()); ?></h2>
		</div>
		</div>
    <?php
	}
	?>
  
	<div class="os-container">
		<div class="page-w <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
			<div class="page-content">
				<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php 
				if(osetin_is_regular_header(get_the_ID())){
					echo '<h1 class="page-title">'.osetin_get_the_title(get_the_ID()).'</h1>';
				}
				?>
				<?php 
				the_content();
				if ($user_allowed_to_edit_this_post){
				if ( osetin_can_user_post_recipe() ) {
				echo '<div class="frontend-publisher-w">';
				if($new_recipe){
                echo '<h2 class="form-header">'.__('Submit your recipe', 'osetin').'</h2>';
				}else{
                echo '<h2 class="form-header">'.__('Edit your recipe', 'osetin').'</h2>';
				}
				include_once(ABSPATH.'wp-admin/includes/plugin.php');
				if(isset($_POST['submit'])){
              	do_action( 'save_recipe');
              	echo '<p style="color:#0f0;">Your receipe is under review</p>';
              	wp_redirect( site_url());
				}
				?>
				<form method="post" enctype="multipart/form-data">
					<div class="row fr-sub">
						<div class="col-md-8">
							<h5 class="pr-title"><label>Recipe Name <div class="recipe-form required recipe-name">This filed is required</div></label></h5>
							<div class="form-group fr-box">
								<input type="text" name="recipe_name" class="form-control">
							</div>
							
							<div class="gallery-sec fr-box">              
								<input type="hidden" id="rec-upload" name="rec_images" multiple="multiple">
								<input type="file" name="upload_image" multiple="multiple">
								<input type="file" name="temp_upload_image" multiple="multiple">
								<div class="row">
									<div class="col-sm-12 text-center"><h4 class="addimg">Hoofdafbeelding</h4><div class="add-image">+</div></div>
								</div>
								<div class="row preview-images-zone">
								</div>
								<div class="recipe-form required rec-images">This filed is required</div>
							</div>
							
							<h5 class="pr-title"><label>Samenvattingstekst <div class="recipe-form required recipe-desc">This filed is required</div></label></h5>
							<div class="desc-sec fr-box">
								<textarea class="form-control" name="recipe_desc"></textarea>
							</div>
							<h5 class="pr-title"><label>Ingredients<div class="recipe-form required recipe-desc">This filed is required</div></label></h5>
							<div class="recipe-ingredient">
								<div class="form-group fr-box">
									<div class="mobile-view btn btn-info pull-right add-ingred"> + Add Ingredient </div>
									<ul class="stepsss desktop-view">
										<li><div class="btn btn-info pull-right add-ingred"> + Add Ingredient </div></li>
									</ul>
									<table class="table">
										<thead>
											<tr>
												<th><?php _e('NAAM'); ?></th>
												<th><?php _e('HOEVEELHEID'); ?></th>
												<th><?php _e('SPLITSER'); ?> <div class="tooltip"><i class="fa fa-info-circle"></i><span class="tooltiptext">Als je de ingrediëntenlijst wil opsplitsen in meerdere delen, kun je hier bijv. ‘Garnering’ invullen. De ingrediënten die je daarna invult, zullen onder het kopje ‘Garnering getoond worden</span></div></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input class="form-control" type="text" name="ing_name[0]"></td>
												<td><input class="form-control" type="text" name="ing_qnt[0]"></td>
												<td>
												  <input class="form-control separator" type="checkbox" name="ing_splitser[0]">
												  <input type="hidden" class="separator-val" name="separator[]" value="false">
												  <label>Splitsen?</label>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="fr-box step-section">
								<ul class="stepss">
									<li><h5>Stappenplan</h5></li>
									<li><div class="btn btn-info add-step">+ Add Step</div></li>
								</ul>
								<div class="fr-box stepslist">
									<div class="steps">
										<div id="step1" class="step form-group row"><label class="col-md-1 col-form-label"> 1 </label><input class="form-control col-md-11" type="text" name="step[]"></div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="recipe-tools row">
								<div class="col-sm-4 fr-box"><label><h5><?php _e('PREPTIME'); ?></h5><label> <input type="text" name="recipe_preparation_time" class="form-control"></div>
								<div class="col-sm-4 fr-box"><label><h5><?php _e('SERVES'); ?></h5><label> <input type="text" name="recipe_serves" class="form-control"></div>
								<div class="col-sm-4 fr-box"><label><h5><?php _e('DIFFICULTY'); ?></h5><label> 
									<select name="recipe_difficulty" class="form-control">
										<option value="1"><?php _e('Easy'); ?></option>
										<option value="2"><?php _e('Medium'); ?></option>
										<option value="3"><?php _e('Hard'); ?></option>
									</select>
								</div>
							</div>
							<div class="recipe-baking-mold row">
								<div class="form-group fr-box">
									<div class="row">
										<div class="col-md-2">
											<h5 id="bks"><label><?php _e('Bakvorm:'); ?></label></h5>
										</div>                    
										<div class="col-md-10">
											<div class="circle-visible active"><span class="shape"></span>
												<label><input type="text" name="baking_mold_circle_val" class="bakvorm" placeholder="25">CM</label>
											</div>
											<div class="square-visible"><span class="shape"></span>
												<label>
													<input type="text" name="baking_mold_square_box_width" class="bakvorm" placeholder="20"><span>X</span>
													<input type="text" name="baking_mold_square_box_height" class="bakvorm" placeholder="30">CM
												</label>
											</div>
											<input type="hidden" name="baking_mold">
										</div>
									</div>                  
								</div>
							</div>
							<div class="recipe-tax row">
								<div class="form-group fr-box">
									<h5><label class="col-sm-4 col-form-label"><?php _e('CATEGORY'); ?> <div class="recipe-form required cat-list">This filed is required</div></label></h5>
									<div class="cat-dropdown">
									 	<?php $terms = get_terms( 'category'); foreach ($terms as $key => $val) : ?>
									 	<p><input type="checkbox" name="cat[]" value="<?=$val->term_id?>"> <?=$val->name?></p>
									 	<?php endforeach; ?>
									</div>
								</div>
								<h5>Tips</h5>
								<div class="fr-box">
									<textarea class="form-control" name="tips"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="form-footer">
						<input type="hidden" name="post_id" value="<?php echo (isset($_REQUEST['post_id'])) ? $_REQUEST['post_id'] : '';?>">
						<input type="hidden" name="action" value="kkm_recipe_save_post">
						<input type="submit" name="submit" value="Publish Recipe" class="btn btn-primary btn-lg recsubmit"> 
					</div>
					<div class="message-holder"></div>
				</form>
				<?php
				}else{
				if(is_user_logged_in()){
                echo '<h3>'.__('You do not have required permissions to submit recipes', 'osetin').'</h3>';
				}else{
                echo do_shortcode('[userpro template=login login_heading="'.__('Login to submit your recipe', 'osetin').'"]');
				}
				}
				}else{
				echo '<h3>'.__('You do not have required permissions to edit this recipe.', 'osetin').'</h3>';
				}
				?>
			</article>
		</div>

		<?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) { ?>
			<div class="page-sidebar">
				<?php dynamic_sidebar( 'sidebar-index' ); ?>
			</div>
		<?php } ?>
    </div>
	</div>
  <?php endwhile; // end of the loop. ?>
	<script>
	jQuery(document).ready(function($) {
  		var step_count = 1;
  		var ingred_count = 0;
		var elid = new Array();
		/*
  		$('body').on('click','.add-image',function(event){
			console.log('inside')
  			$('input[name=upload_image]').trigger('click')
  		});
		
		$('body').on('change','input[name=upload_image]',function(event) {
			  
  		});
		  */
  		$('#rec-upload').change(function(event) {
			  console.log('inside')
  			//readImage()
  		});

  		$('.separator').click(function(event) {
  			let sep = $(this).closest('td').find('separator-val').val();
  			if(sep == 'false'){
  				$(this).closest('td').find('separator-val').val('true')
  			}else{
  				$(this).closest('td').find('separator-val').val('false')
  			}
  		});

  		$('.add-step').click(function(event) {
  			step_count++;
  			let html = '<div id="step'+step_count+'" class="step form-group row"><label class="col-sm-1 col-form-label"> '+step_count +' </label><input class="form-control col-sm-10" type="text" name="step[]"><div class="col-sm-1"><span class="remove-step"><i class="fa fa-close"></span></div></div>';
  			$(this).closest('.fr-box').find('.steps').append(html);
  		});

		$('body').on('click','.remove-step',function(event){
          $(this).closest('.step').remove();
          step_count--;
		})

  		$('.add-ingred').click(function(event) {
  			ingred_count++;
  			let html = '<tr>'+
						'<td><input class="form-control" type="text" name="ing_name['+ingred_count+']"></td>'+
						'<td><input class="form-control" type="text" name="ing_qnt['+ingred_count+']"></td>'+
						'<td><input class="form-control separator" type="checkbox" name="ing_splitser['+ingred_count+']">'+
						'<input type="hidden" class="separator-val" name="separator['+ingred_count+']" value="false">'+
            '<label>Splitsen?</label>'+
						'</td>'+
						'</tr>';
  			$(this).closest('.fr-box').find('tbody').append(html);
  		});

		$('body').on('click','.remove-media-image',function(){
			arrayindex = $(this).closest('.preview-image').index();
			elid.splice(arrayindex,1);
			$('#rec-upload').val(elid.toString());
        	$(this).closest('.preview-image').remove()
		})
		/*
  		$(document).on('click', '.image-cancel', function() {
	        let no = $(this).data('no');
	        $(".preview-image.preview-show-"+no).remove();
	    });
		
	    var frame,
		  metaBox = $('.fr-sub'), // Your meta box id here
		  button_link = metaBox.find('.add-image');
		  //videoInput = metaBox.find( '.g4l_video' );
		  var output = jQuery(".preview-images-zone");
		  button_link.on( 'click', function( event ){
		    event.preventDefault();
        if ( frame ) {
          frame.open();
          return;
        }  
        frame = wp.media({
          title: 'Select or Upload the Video',
          button: {
              text: 'Use this media'
          },
          multiple: true,
            // Set to true to allow multiple files to be selected
        });

        frame.on( 'select', function() {
          var attachment = frame.state().get('selection').toJSON();
          var $html = '';
          $.each(attachment,function(index, el) {
              elid.push(el.id);
              $html = '<div class="col-sm-3 preview-image preview-show"><div class="remove-media-image"><i class="fa fa-close"></i></div><div class="image-zone"><img id="pro-img" src="' + el.url + '"></div></div>';
              output.append($html);
          });
          console.log(elid);
          $('#rec-upload').val(elid.toString());
        //console.log(attachment);
        });
		    frame.open();
		});
		});
		var num = 4;

		function readImage() {
			if (window.File && window.FileList && window.FileReader) {
				var files = event.target.files; //FileList object
				var output = jQuery(".preview-images-zone");

				for (let i = 0; i < files.length; i++) {
					var file = files[i];
					if (!file.type.match('image')) continue;
					
					var picReader = new FileReader();
					
					picReader.addEventListener('load', function (event) {
						var picFile = event.target;
						var html =  '<div class="col-sm-3 preview-image preview-show-' + num + '">' +
									'<div class="image-cancel" data-no="' + num + '">x</div>' +
									'<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
									'</div>';

						output.prepend(html);
						num = num + 1;
					});

					picReader.readAsDataURL(file);
				}
			} else {
				console.log('Browser not support');
			}
		}
		*/
	})
	</script>
<?php get_footer(); ?>