 <footer class="footer">

    <div class="container">
      <ul class="footer-nav">

        <li class="footer-nav__item">
          <a href="#" class="footer-nav__link">Login</a>
        </li>
        <li class="footer-nav__item">
          <a href="#" class="footer-nav__link">Registreer</a>
        </li>
        <li class="footer-nav__item">
          <a href="#" class="footer-nav__link">Privacy</a>
        </li>
      </ul>

      <div class="social">
        <div class="social__line"></div>
        <div class="social__links">
          <a href="#" class="social__link">
            <span class="social__icon">
            </span>
          </a>
          <a href="#" class="social__link">
            <span class="social__icon">
            </span>
          </a>
          <a href="#" class="social__link">
            <span class="social__icon">
            </span>
          </a>
          <a href="#" class="social__link">
            <span class="social__icon">
            </span>
          </a>
        </div>
        <div class="social__line"></div>
      </div>

      <div class="copyright">
        <p>© Kookmutsjes.com</p>
      </div>
    </div>


  </footer>

  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="<?php echo get_stylesheet_directory_uri()?>/menu/js/vendors.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri()?>/menu/js/bundle.js"></script>
  <?php wp_footer();?>
</body>

</html>