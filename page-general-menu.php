<?php
/**
 * Template Name: Categories Menu Sub
 *
 * @package Neptune
 */

get_header(); ?>

  <?php while ( have_posts() ) : the_post(); ?>


  <div class="os-container top-bar-w">
    <div class="top-bar <?php if(!osetin_is_imaged_header(get_the_ID())) echo 'bordered'; ?>">
		<ul>
			<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			<li class="page-top-title"><h2 id="pagetitles"><?php echo osetin_get_the_title(get_the_ID()); ?></h2></li>
		</ul>
    </div>
  </div>

  <?php  
  if(osetin_is_imaged_header(get_the_ID())){
    if(osetin_is_bbpress()){
      $page_bg_image_url = get_template_directory_uri().'/assets/img/patterns/flowers_light.jpg';
    }else{
      $page_bg_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
    } ?>
    <div class="os-container">
      <div class="page-intro-header with-background" style="<?php echo osetin_get_css_prop('background-image', $page_bg_image_url, false, 'background-repeat: repeat; background-position: top left; '); ?>">
        <h2><?php echo osetin_get_the_title(get_the_ID()); ?></h2>
      </div>
    </div>
    <?php
  }
  ?>

  <div class="os-container">
    <div class="page-w <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
	  <div class="page-content">
	
	 <style>
     .article {
       margin-right: 16px;
       margin-bottom: 6px;
       background-color: #ffffff;
     }

     .article__hero {
       height: 206px;
       width:100%;
       background-size: cover;
       background-repeat: no-repeat;
       padding: 41px 16px 16px;
     }

     .article__title {
        display: -webkit-box;
        display: -ms-flexbox;
        display: inline-flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        padding: 12px 36px;
        background-color: #97C4B8;
        color: #FFFFFF;
     }

     .article__title h2 {
       margin: 0;
       font-family: Cocogoose, serif;
       font-size: 30px;
       font-weight: bold;
       letter-spacing: 0;
       color: #FFFFFF;
     }

     .article__content {
        padding: 9px 16px 16px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
     }

     .article__content p {
       font-size: 11px;
       line-height: 20px;
       letter-spacing: 0;
       color: #585756;
     }

     .article__link {
       margin: 10px auto;
       display:inline-block;
       border: solid 1px #97C4B8;
       padding: 4px 10px;
       min-width: 121px;
       text-align: center;
       font-weight: bold;
       font-size: 11px;
       letter-spacing: 0;
       color: #FFFFFF;
       background-color: #97C4B8;
       -webkit-transition: all .5s ease;
       transition: all .5s ease;
     }

     .article__link:hover {
       color: #97C4B8;
       background-color: #fff;
     }

     @media only screen and (max-width: 1240px) {
      .article__hero {
          max-width:100%;
          width: 66.5vw;
          height: 16.6vw;
          }
      }

     @media only screen and (max-width: 991px) {
          .article__title h2 {
            font-size: 25px;
          }
      }

      @media only screen and (max-width: 767px) {
        .page-w.with-sidebar .page-sidebar {display: none;}
        .article__hero {
    
          width: 92.7vw;
          height: 23.2vw;
          }

          .article__title h2 {
            font-size: 20px;
          }
      }

      @media only screen and (max-width: 576px) {
          .article__title h2 {
            font-size: 16px;
          }
      }
	 /* .image-container {
		top: 220px;
		left: 138px;
		width: 820px;
		height: 122px;
		opacity: 1;
	 }
	 
	 .main-page-l {
		top: 220px;
		left: 138px;
		width: 820px;
		height: 237px;
		background: #FFFFFF 0% 0% no-repeat padding-box;
		opacity: 1;
   } */	 
   
   </style>
    
        <?php /*global $post;    
            $children = get_pages( array( 'child_of' => $post->ID ) );
          if ( is_page() && ($post->post_parent || count( $children ) > 0  )) : 
         ?>
    <?php //while ( have_posts() ) : the_post(); ?>        
   <article class="article">
      <div class="article__hero" style="background: url('https://dev.kookmutsjes.com/wp-content/uploads/2020/01/Track-set-toys.jpg')">
        <div class="article__title">
          <h2><?php 
    $parent_post = get_post($post->ID);          
    $parent_post_title = $parent_post->post_title;
    echo $parent_post_title;
              ?></h2>
        </div>        
      </div>

      <div class="article__content">
         <p>When it comes to throwing a birthday party, there’s a lot of hype over the cake — but it’s just as important to offer tasty finger 
          foods to keep your guests satisfied while they wait for the main event. With these easy, satisfying recipes, any host is sure to pull 
          off a memorable birthday party.</p>
          <a href="<?php the_permalink();?>" class="article__link">Learn more</a>
      </div>
   </article>
          <?php //endwhile;?>
	   <?php endif;*/?>         
<?php
$args = array(
    'post_parent' => $post->ID,
    'post_type' => 'page',
    'child_of' => $post->ID,
    'orderby' => 'menu_order'
    
);

$child_query = new WP_Query( $args );
?>

<?php while ( $child_query->have_posts() ) : $child_query->the_post(); ?>
          
<article class="article">
      <div class="article__hero" style="background-image: url('<?php the_field('hero_image'); ?>')">
        <div class="article__title">
          <h2><?php the_title();?></h2>
        </div>        
      </div>

      <div class="article__content">
          <?php the_field('description'); ?>
          <a href="<?php the_permalink(); ?>" class="article__link">Lees meer</a>
      </div>
   </article>          
          
<?php endwhile; ?>

<?php
wp_reset_postdata();  ?>    
          
          
          
          
          
          
          
          
	<!-- <section class="image-container" style="background-image: url(https://dev.kookmutsjes.com/wp-content/uploads/2020/01/Track-set-toys.jpg)">
	
	<div class="main-page-l"> 
			<div class="hero__title">
				 <h1><?php //the_title();?></h1>
			</div>
		</div>
	
	</section> -->
		  
		<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php 
          if(osetin_is_regular_header(get_the_ID())){
            echo '<h1 class="page-title">'.osetin_get_the_title(get_the_ID()).'</h1>';
          }
          ?>
          <?php $sub_title = osetin_get_field('sub_title');
          if ( ! empty( $sub_title ) ) { ?>
            <h2 class="page-content-sub-title"><?php echo esc_html($sub_title); ?></h2>
          <?php } ?>
          <?php the_content(); ?>
          <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || get_comments_number() ) :
              comments_template();
            endif;
          ?>
        </article>
      </div>
      <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) { ?>

        <div class="page-sidebar">
        
          <?php dynamic_sidebar( 'sidebar-index' ); ?>

        </div>
          
      <?php } ?>


    </div>
  </div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>