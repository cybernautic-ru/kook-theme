<?php
/**
 * Kookmutsjes: Recipes post per page REST API
 *
 */

class Recipe_Index_Post_REST_Controller extends WP_REST_Controller {

	public function register_routes() {
		$namespace = 'recipe-index/v1';
		$path      = 'recipe-posts-paged/(?P<posts_per_page>\d+)/(?P<page_num>\d+)';

		register_rest_route( $namespace, '/' . $path, [
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
			),
		] );

		$path_tags = 'recipe-tags/';

		register_rest_route( $namespace, '/' . $path_tags, [
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'get_tags' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
			),
		] );

	}

	public function get_items_permissions_check( $request ) {
		return true;
	}

	public function get_items( $request ) {
		global $post;
		$category_attr = array();
		$tag_attr      = array();
		//array for response
		$post_array_response = array(
			'no_posts' => false,
			'posts'    => array(),
		);
		//get request parameters (<posts_per_page>, <page_num>)
		$r_args = array(
			'posts_per_page' => $request['posts_per_page'],
			'paged'          => $request['page_num'],
		);
		//get GET request additional parameters
		$params = $request->get_params();
		if ( isset( $params['category'] ) ) {
			$category_attr = explode( ' ', $params['category'] );
		}
		if ( isset( $params['tag'] ) ) {
			$tag_attr = explode( ' ', $params['tag'] );
		}
		if ( isset( $params['user_id'] ) ) {
			$cur_user_id = intval( $params['user_id'] );
		}

		$administrator = get_users( array( 'role__in' => 'administrator', 'fields' => 'ID' ) );
		$siteadmins    = get_users( array( 'role__in' => 'site_admin', 'fields' => 'ID' ) );
		$users_admin   = array_merge( $administrator, $siteadmins );

		$rp_args = array(
			'post_type'      => 'osetin_recipe',
			'posts_per_page' => $r_args['posts_per_page'],
			'paged'          => $r_args['paged'],
			'author__in'     => $users_admin,
		);

		$has_post_flag = true;
		if ( ! empty( $category_attr ) || ! empty( $tag_attr ) ) {
			$in_posts = RecipeCategory::get_posts_cats_tags( $category_attr, $tag_attr );
			if ( empty( $in_posts ) ) {
				$has_post_flag = false;
			}
			$rp_args['post__in'] = $in_posts;
		}

		if ( isset( $_GET['sort'] ) ) {
			if ( $_GET['sort'] == 'new' ) {
				$rp_args['orderby'] = 'date';
				$rp_args['order']   = 'DESC';
			} elseif ( $_GET['sort'] == 'pop' ) {
				$rp_args['meta_key'] = 'wpp_views_total';
				$rp_args['orderby']  = 'meta_value_num';
				$rp_args['order']    = 'DESC';
			}
		} else {
			$rp_args['orderby'] = 'date';
			$rp_args['order']   = 'DESC';
		}
		if ( $has_post_flag ) {
			$rp_query = new WP_Query( $rp_args );
			if ( $rp_query->have_posts() ) {

				while ( $rp_query->have_posts() ) {
					$rp_query->the_post();
					$cur_post = array();

					$cur_post['post_id'] = $post->ID;

					$current_step_class = 'full_full_over';
					if ( ( get_post_format() == 'gallery' ) && osetin_get_field( 'gallery_images', false, false, true ) ) {
						// GALLERY
						$gallery_images = osetin_get_field( 'gallery_images', false, false, true );
						$thumb_name     = osetin_get_archive_thumb_name( $current_step_class );
						if ( isset( $gallery_images[0] ) ) {
							$cur_post['media']['type'] = 'gellery';
							$cur_post['media']['src']  = $gallery_images[0]['sizes'][ $thumb_name ];
						}
					} elseif ( get_post_format() == 'video' && osetin_get_field( 'video_shortcode' ) ) {
						// VIDEO
						$cur_post['media']['type'] = 'video';
						$cur_post['media']['src']  = do_shortcode( osetin_get_field( 'video_shortcode' ) );
					} else {
						// IMAGE
						$cur_post['media']['type'] = 'image';
						$cur_post['media']['src']  = osetin_output_post_thumbnail_url( osetin_get_archive_thumb_name( $current_step_class ), false );
						if ( pathinfo( $cur_post['media']['src'], PATHINFO_EXTENSION ) == 'gif' && osetin_get_field( 'is_gif_media' ) ) {
							$cur_post['media']['type'] = 'gif';
							if ( osetin_get_field( 'preview_image_for_lazy_gif' ) ) {
								$preview_img_src = wp_get_attachment_image_src( osetin_get_field( 'preview_image_for_lazy_gif' ), 'osetin-full-width' );
								if ( ! empty( $preview_img_src[0] ) ) {
									$cur_post['media']['src'] = $preview_img_src[0];
								}
							}
						}
					}

					$cur_post['title'] = $post->post_title;

					$cur_post['author']['img_src'] = get_avatar_url( get_the_author_meta( 'ID' ), [ 'size' => '40' ] );
					$cur_post['author']['name']    = get_the_author_meta( 'display_name' );
					$cur_post['author']['url']     = get_author_posts_url( get_the_author_meta( 'ID' ) );

					$cur_post['date'] = date( 'F j,Y', strtotime( get_the_date() ) );

					$cur_post['comments'] = $post->comment_count;

					$cur_post['permalink'] = get_permalink();

					$cur_post['excerpt'] = osetin_excerpt( 80, false );

					$r_prep_time_field    = osetin_get_field( 'recipe_cooking_time' );
					$cur_post['preptime'] = isset( $r_prep_time_field ) && ! empty( $r_prep_time_field ) ? $r_prep_time_field : osetin_get_field( 'recipe_preparation_time' );

					$r_serves_field     = osetin_get_field( 'recipe_serves' );
					$cur_post['serves'] = isset( $r_serves_field ) && ! empty( $r_serves_field ) ? $r_serves_field : __( 'No Serves', 'neptune-child' );

					$r_difficulty_field = osetin_get_difficulty_string( osetin_get_field( 'recipe_difficulty' ) );
					$cur_post['level']  = isset( $r_difficulty_field ) && ! empty( $r_difficulty_field ) ? $r_difficulty_field : __( 'No Difficulty', 'neptune-child' );

					$cur_post['likes'] = osetin_vote_get_post_vote_data( $post->ID );

					$disabled = false;
					if ( isset( $params['user_id'] ) && isset( $cur_user_id ) && ! empty( $cur_user_id ) ) {
						$saved_favorite_post = unserialize( get_user_meta( $cur_user_id, 'saved_favorite_post', true ) );
						$redirect            = 'false';
						if ( ! $saved_favorite_post ) {
							$saved_favorite_post = array();
						}
					} else {
						$redirect            = 'true';
						$saved_favorite_post = array();
					}
					if ( in_array( $post->ID, $saved_favorite_post ) ) {
						$disabled = true;
					}
					$cur_post['saved']['redirect'] = $redirect;
					$cur_post['saved']['disabled'] = $disabled;


					$post_array_response['posts'][] = $cur_post;
				}
			} else {
				$post_array_response['no_posts'] = true;
			}
		} else {
			$post_array_response['no_posts'] = true;
		}

		wp_reset_postdata();

		//the result is empty
		if ( empty( $post_array_response ) ) {
			return new WP_Error( 'empty_result', 'there are no posts on this page', array( 'status' => 404 ) );
		}

		//return result array in response
		return new WP_REST_Response( $post_array_response, 200 );
	}

	public function get_tags( $request ) {
		$category_attr = array();
		$tag_attr      = array();
		//array for response
		$tag_response = array(
			'no_tags' => false,
			'tags'    => array(),
		);

		//get GET request additional parameters
		$params = $request->get_params();
		if ( isset( $params['category'] ) ) {
			$category_attr = explode( ' ', $params['category'] );
		}
		if ( isset( $params['tag'] ) ) {
			$tag_attr = explode( ' ', $params['tag'] );
		}

		$tag_response['tags'] = RecipeCategory::get_avilable_tags( $category_attr, $tag_attr );

		if ( empty( $tag_response['tags'] ) ) {
			$tag_response['no_tags'] = true;
		}

		//return result array in response
		return new WP_REST_Response( $tag_response, 200 );
	}


}

add_action( 'rest_api_init', function () {
	$posts_paged_rest_controller = new Recipe_Index_Post_REST_Controller();
	$posts_paged_rest_controller->register_routes();
} );