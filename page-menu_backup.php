<?php /*Template Name: Individual Menu Backup*/ 
get_header('menu2');?>
<?php global $post;    
   $children = get_pages( array( 'child_of' => $post->ID ) );
   if ( is_page() && ($post->post_parent || count( $children ) > 0  )) : 
?>
<section class="hero" style="background-image: url('<?php the_field('hero_image'); ?>')">
    <div class="container">
        <div class="hero__title">
            <h1><?php the_title();?></h1>
        </div>
    </div>
</section>
<section class="main">
    <div class="container">
        <div class="breadcrumbs">
			<?php //breadcrumb_menus(); ?>
        </div>
        <div class="main__content">
            <div class="main__text">
                <h1 class="main__title">
                    <?php the_field('title');?>
                </h1>
                <?php the_field('description');?>
            </div>
            <div class="main__menu">
                <div class="menu__box">
                    <h2 class="menu__title">Overige</h2>
					<?php echo do_shortcode('[my_childpages]');?>
					<a href="<?php echo home_url();?>/kookmutsjes-experience" class="btn-menu">Toon meer</a>
                </div>
            </div>
        </div>
        
		
		<?php

// check if the repeater field has rows of data
if (have_rows('listing')):
// loop through the rows of data
    while (have_rows('listing')):
        the_row();

        // display a sub field value
        ?>
		<article class="recipes">
            <h1 class="recipes__title"><?php echo the_sub_field('recipes_menu'); ?></h1>
            <ul class="recipes-galary">
			<?php $myposts = get_sub_field('select_menu');
			if ($myposts):?>
			<?php  foreach ($myposts as $post_object): ?>
			<?php //$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id( $post_object->ID ), 'full' ); ?>
			<?php $featured = wp_get_attachment_image_src( get_post_thumbnail_id( $post_object->ID ), 'full' ); ?>
			<li class="recipes-galary__item <?php if( $i ==1 ){ echo " active "; } ?>">
            <div class="recipes-galary__img" style="background-image: url('<?php echo $featured[0];?>')"></div>
                <p>
                    <?php echo get_the_title($post_object->ID); ?>
                </p>
            </li>
			
			<?php endforeach;?>
			<?php endif;?>
			</ul>




       <?php $myposts = get_sub_field('select_menu');
        if ($myposts):?>
			<div class="recipes__tabs">
			<?php  foreach ($myposts as $post_object): ?>
                <!--loop start-->
                <div class="recipes__tab">
                    <h2 class="recipes__subtitle"><?php echo get_the_title($post_object->ID); ?></h2>
                    <div class="recipe">
						<?php $featureds = wp_get_attachment_image_src( get_post_thumbnail_id( $post_object->ID ), 'full' ); ?>
                        <div class="recipe__img" style="background-image: url('<?php echo $featureds[0];?>')">
                        <a href="<?php echo get_permalink($post_object->ID); ?>" class="btn-recipe" >Naar recept</a>
                        </div>
                        <div class="ingredients">
                            <div class="ingredients__top">
                                <div class="ingredients__title">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20.979" height="20.979" viewBox="0 0 20.979 20.979">
                                        <defs>
                                            <style>
                                                .a {
                                                    fill: #585756;
                                                }

                                            </style>
                                        </defs>
                                        <path class="a" d="M18.357,0H2.622A2.626,2.626,0,0,0,0,2.622V18.357a2.626,2.626,0,0,0,2.622,2.622H18.357a2.626,2.626,0,0,0,2.622-2.622V2.622A2.626,2.626,0,0,0,18.357,0Zm.874,18.357a.875.875,0,0,1-.874.874H2.622a.875.875,0,0,1-.874-.874V2.622a.876.876,0,0,1,.874-.874H18.357a.876.876,0,0,1,.874.874Z" />
                                        <g transform="translate(4.371 4.371)">
                                            <path class="a" d="M108.345,107.208a.9.9,0,0,0-.184-.288l-.131-.1a.663.663,0,0,0-.157-.079.555.555,0,0,0-.157-.052.836.836,0,0,0-.507.052,1.009,1.009,0,0,0-.288.184.9.9,0,0,0-.184.288.824.824,0,0,0,0,.664.9.9,0,0,0,.184.288.919.919,0,0,0,.621.254,1.31,1.31,0,0,0,.175-.017.555.555,0,0,0,.157-.052.663.663,0,0,0,.157-.079l.131-.1a.919.919,0,0,0,.254-.621A.872.872,0,0,0,108.345,107.208Z" transform="translate(-106.667 -106.666)" />
                                        </g>
                                        <g transform="translate(7.867 4.371)">
                                            <path class="a" d="M199.867,106.667h-6.993a.874.874,0,0,0,0,1.748h6.993a.874.874,0,1,0,0-1.748Z" transform="translate(-192 -106.667)" />
                                        </g>
                                        <g transform="translate(4.371 9.62)">
                                            <path class="a" d="M108.4,235.468a.556.556,0,0,0-.052-.157.664.664,0,0,0-.079-.157l-.1-.131a1.009,1.009,0,0,0-.288-.184.874.874,0,0,0-.664,0,1.009,1.009,0,0,0-.288.184l-.1.131a.663.663,0,0,0-.079.157.556.556,0,0,0-.052.157,1.317,1.317,0,0,0-.017.175.874.874,0,1,0,1.748,0A1.332,1.332,0,0,0,108.4,235.468Z" transform="translate(-106.667 -234.773)" />
                                        </g>
                                        <g transform="translate(7.867 9.615)">
                                            <path class="a" d="M199.867,234.667h-6.993a.874.874,0,0,0,0,1.748h6.993a.874.874,0,1,0,0-1.748Z" transform="translate(-192 -234.667)" />
                                        </g>
                                        <g transform="translate(4.371 14.867)">
                                            <path class="a" d="M108.345,363.362a.846.846,0,0,0-1.136-.472.788.788,0,0,0-.472.472.873.873,0,1,0,1.678.332A.734.734,0,0,0,108.345,363.362Z" transform="translate(-106.667 -362.827)" />
                                        </g>
                                        <g transform="translate(7.867 14.86)">
                                            <path class="a" d="M199.867,362.667h-6.993a.874.874,0,1,0,0,1.748h6.993a.874.874,0,1,0,0-1.748Z" transform="translate(-192 -362.667)" />
                                        </g>
                                    </svg>
                                    <h2>Ingrediënten</h2>
                                </div>
                                <div class="ingredients__time">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9">
                                        <defs>
                                            <style>
                                                .a {
                                                    fill: #585756;
                                                }

                                            </style>
                                        </defs>
                                        <g transform="translate(0 0)">
                                            <path class="a" d="M4.5,9A4.5,4.5,0,0,1,1.318,1.318,4.5,4.5,0,0,1,7.682,7.682,4.471,4.471,0,0,1,4.5,9ZM4.5.527A3.973,3.973,0,0,0,1.691,7.309,3.973,3.973,0,1,0,7.309,1.691,3.947,3.947,0,0,0,4.5.527Z" transform="translate(0 0)" />
                                        </g>
                                        <g transform="translate(4.238 1.048)">
                                            <path class="a" d="M241,60.036h.524v.7H241Z" transform="translate(-241 -60.036)" />
                                        </g>
                                        <g transform="translate(6.255 1.88)">
                                            <path class="a" d="M0,0H.7V.524H0Z" transform="translate(0 0.494) rotate(-45)" />
                                        </g>
                                        <g transform="translate(7.253 4.238)">
                                            <path class="a" d="M411.932,241h.7v.524h-.7Z" transform="translate(-411.932 -241)" />
                                        </g>
                                        <g transform="translate(6.255 6.256)">
                                            <path class="a" d="M0,0H.524V.7H0Z" transform="translate(0 0.37) rotate(-45)" />
                                        </g>
                                        <g transform="translate(4.238 7.253)">
                                            <path class="a" d="M241,411.932h.524v.7H241Z" transform="translate(-241 -411.932)" />
                                        </g>
                                        <g transform="translate(1.878 6.256)">
                                            <path class="a" d="M0,0H.7V.524H0Z" transform="translate(0 0.494) rotate(-45)" />
                                        </g>
                                        <g transform="translate(1.048 4.238)">
                                            <path class="a" d="M60.036,241h.7v.524h-.7Z" transform="translate(-60.036 -241)" />
                                        </g>
                                        <g transform="translate(1.879 1.879)">
                                            <path class="a" d="M0,0H.524V.7H0Z" transform="translate(0 0.37) rotate(-45)" />
                                        </g>
                                        <g transform="translate(4.245 2.644)">
                                            <path class="a" d="M243.111,152.219H241v-2.111h.524V151.7h1.587Z" transform="translate(-241 -150.108)" />
                                        </g>
                                    </svg>
                                    <p>
                                        <?php echo get_field('recipe_preparation_time', $post_object->ID ); ?>
                                    </p>
                                </div>
                                <div class="ingredients__time">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="7.404" viewBox="0 0 9 7.404">
                                        <defs>
                                            <style>
                                                .a {
                                                    fill: #585756;
                                                }

                                                .b {
                                                    clip-path: url(#a);
                                                }

                                            </style>
                                            <clipPath id="a">
                                                <rect class="a" width="9" height="7.404" transform="translate(732 730)" />
                                            </clipPath>
                                        </defs>
                                        <g class="b" transform="translate(-732 -730)">
                                            <g transform="translate(732 731.077)">
                                                <g transform="translate(0 4.125)">
                                                    <path class="a" d="M8.972,282.089A.188.188,0,0,0,8.813,282H.188a.188.188,0,0,0-.168.272,1.536,1.536,0,0,0,1.381.854H7.6a1.535,1.535,0,0,0,1.381-.854A.187.187,0,0,0,8.972,282.089Zm-1.373.661H1.4a1.162,1.162,0,0,1-.859-.375H8.458A1.163,1.163,0,0,1,7.6,282.751Z" transform="translate(0 -282)" />
                                                </g>
                                                <g transform="translate(1.66 1.5)">
                                                    <path class="a" d="M80.845,158.8a.191.191,0,0,0-.2-.175,2.993,2.993,0,0,0-2.63,1.995.188.188,0,0,0,.115.239.191.191,0,0,0,.062.01.188.188,0,0,0,.177-.125A2.618,2.618,0,0,1,80.67,159,.188.188,0,0,0,80.845,158.8Z" transform="translate(-78.005 -158.626)" />
                                                </g>
                                                <g transform="translate(0.755 0.75)">
                                                    <path class="a" d="M42.956,126.927a3.75,3.75,0,0,0-7.49,0,.188.188,0,0,0,.178.2h.01a.188.188,0,0,0,.187-.178,3.375,3.375,0,0,1,6.741,0,.188.188,0,1,0,.375-.019Z" transform="translate(-35.466 -123.374)" />
                                                </g>
                                                <g transform="translate(3.937 0)">
                                                    <path class="a" d="M185.625,88.124a.563.563,0,1,0,.563.563A.563.563,0,0,0,185.625,88.124Zm0,.75a.188.188,0,1,1,.188-.188A.188.188,0,0,1,185.625,88.874Z" transform="translate(-185.062 -88.124)" />
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <p>
                                        <?php echo get_field('recipe_serves', $post_object->ID ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="ingredients__columns">

							<style>
							.headline_left{
								float:left;
								width:42%;
								padding-right:6%;
							   /*   margin:1px;*/
								
							   /*padding-left: 1px;*/
							}
							.headline_right{
								float:left;
								width:42%;
								padding-right:6%;
								margin:0;
								
							}
                            .recipe__content {
                                max-width: 946px!important;
                            }
							</style>
							  
                                <div class="ingredients__column">
								<ul class="ingredients__list">
								<?php  
									$i = 0;
									if( have_rows('ingredients', $post_object->ID ) ):  
                                            while( have_rows('ingredients', $post_object->ID ) ): the_row(); 
											$ingredient_name = get_sub_field('ingredient_name'); 
                                            $ingredient_amount = get_sub_field('ingredient_amount');
									if($i % 2 == 0){ //even line
								?>
									<li class="ingredients__item headline_left"><?php echo $ingredient_amount; ?>&nbsp;<?php echo $ingredient_name; ?></li>
								<?php } else { ?>
									<li class="ingredients__item headline_right"><?php echo $ingredient_amount; ?>&nbsp;<?php echo $ingredient_name; ?></li>
								<?php };
								  $i++; endwhile; 
								  endif;
								?>
								 </ul>
								</div>
                             
                            </div>
                        </div>
                    </div>
                    <div class="recipe__content">
                        <h2 class="subtitle">Info</h2>
                        <?php $post_id = $post_object->ID;
							$post_object = get_post( $post_id );
							echo wpautop($post_object->post_content); ?>
                    </div>
                </div>
				<?php endforeach; ?>
            </div>
		</article>
		<?php endif; endwhile; else: endif;?>
</div>
</section>
<?php endif;?>
<?php get_footer('menu2')?>
