<?php
/**
 * The template for displaying all single posts.
 *
 * @package _s
 */

if ( is_user_logged_in() ) {
	//* Add required acf_form_head() function to head of page
	add_action( 'get_header', 'osetin_do_acf_form_head', 1 );
	function osetin_do_acf_form_head() {
		add_filter( 'acf/update_value', 'wp_kses_post', 10, 1 );
		acf_form_head();
	}
}
get_header(); ?>
<?php while ( have_posts() ) : the_post();
	$recipe_sub_title = osetin_get_field( 'sub_title' );
	$layout_type_for_recipe = osetin_get_settings_field( 'layout_type_for_single_recipe', 'half_left_image' );
	$big_header_titled_image = false;
	$recipe_cooking_time = osetin_get_field( 'recipe_cooking_time' );
	$recipe_preparation_time = osetin_get_field( 'recipe_preparation_time' );
	$baking_mold = osetin_get_field( 'baking_mold' );
	$recipe_serves = osetin_get_field( 'recipe_serves' );
	$recipe_difficulty_string = osetin_get_difficulty_string( osetin_get_field( 'recipe_difficulty' ) );
	$cooking_temperature = osetin_get_field( 'recipe_cooking_temperature' );
	$quick_description = osetin_get_field( 'quick_description' );
	$user_add_image = get_post_meta(get_the_ID(),'add_image',true);
	$details_position = osetin_get_settings_field( 'recipe_details_position', 'split' );
	$reviews_info = osetin_recipe_rating_average_and_total( get_the_ID() );
	$previous_post = get_previous_post();
	$next_post = get_next_post();
	setPostViews(get_the_ID());
	$ads_widget_code = osetin_get_field( 'ads_widget_code' );

	if ( $layout_type_for_recipe == 'big_image_titled' ) {
		$custom_image_for_header = osetin_get_field( 'custom_image_for_header', false, false, true );
		if ( is_array( $custom_image_for_header ) ) {
			$big_header_titled_image = $custom_image_for_header[ 'sizes' ][ 'osetin-full-width' ];
		} else {
			$big_header_titled_image = osetin_output_post_thumbnail_url( 'osetin-full-width' );
		}
	}
	osetin_show_featured_recipes_slider(); ?>
    <div class="os-container top-bar-w">
        <div class="top-bar">
			<ul>
				<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			</ul>
		</div>
    </div>
	<div class="os-container main-recipes">
		<div class="row">
			<div class="col-md-8">
				<div class="post-meta-title">
					<h2><?php echo osetin_get_the_title(get_the_ID()); ?></h2>
					<p><a href="<?php site_url(); ?>/author/<?php echo get_the_author(); ?>"><i class="fa fa-user" aria-hidden="true"></i> <?php echo get_the_author(); ?></a></p>
				</div>
				<?php if ( in_array( $layout_type_for_recipe, array( 'half_left_image', 'big_image_titled' ) ) ) { ?>
                    <div class="single-main-media">
						<?php echo output_recipe_media( get_the_ID(), 'large' ); ?>
                    </div>
				<?php } ?>
				<div class="single-content-self">
					<?php the_content(); ?>
                </div>
				<div class="recipe-fields">
					<?php if ( $details_position == 'split' ) { ?>
						<div class="single-meta">
							<ul>
								<?php if ($recipe_cooking_time) : ?>
								   <li class="single-meta-cooking-time">
										<h2>PREPTIME</h2>
										<p><?php echo $recipe_cooking_time; ?></p>
									</li>
								<?php else : ?>
									<li class="single-meta-cooking-time">
										<h2>PREPTIME</h2>
										<p><?php printf( __( '%s', 'osetin' ), $recipe_preparation_time ); ?></p>
									</li>
								<?php endif;  ?>
								
								<?php if ($recipe_serves) : ?>
									<li class="single-meta-serves">
										<h2>SERVES</h2>
										<p><?php printf( __( '%d', 'osetin' ), $recipe_serves ); ?></p>
									</li>
								<?php else : ?>
									<li class="single-meta-serves">
										<h2>SERVES</h2>
										<p>No Serves</p>
									</li>
								<?php endif;  ?>

								<?php if ($recipe_difficulty_string) : ?>
									<li class="single-meta-difficulty">
										<h2><?php echo $recipe_difficulty_string; ?></h2>
										<p>Recipe</p>
									</li>
								<?php else : ?>
									<li class="single-meta-difficulty">
										<h2>No Difficulty</h2>
										<p>Recipe</p>
									</li>
								<?php endif;  ?>
							</ul>
						</div>
					<?php } ?>
					<!-- custom coding carbojet-->
					<?php if(is_user_logged_in()){
					
						$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
						$redirect = 'false';

						if(!$saved_favorite_post){
							$saved_favorite_post = array();
						}

					}else{
						$redirect = 'true';
						$saved_favorite_post = array();
					}?>
					<?php if ( $details_position == 'split' ) { ?>
					<div class="side-meta-box">
						<ul>
							<li class="desktop-view">
								<?php if ($baking_mold) : ?>
									<p>BAKVORM: <span><?php _e($baking_mold); ?></span></p>
								<?php else : ?>
									<p>BAKVORM: geen</p>
								<?php endif;  ?>
							</li>
							<li class="singlesaved">
								<div class="single-favorite">
									<?php
									if(in_array(get_the_ID(),$saved_favorite_post)){?>
										<button disabled name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn default-btn"><i class="fa fa-bookmark"></i>Recept opslaan</button>
									<?php }else{ ?>
										<button name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn default-btn"><i class="fa fa-bookmark-o"></i>Recept opslaan</button>
									<?php }	?>				
								</div>	
							</li>
							<!--<li class="single-meta-share">
								<a href="#" class="post-control-share">
									<i class="fa fa-share-alt" aria-hidden="true"></i>
									<span><?php esc_html_e( 'Delen', 'osetin' ); ?></span>
								</a>
							</li>-->
							<?php if ( function_exists( 'echo_tptn_post_count' ) ) {
								echo '<li class="single-meta-views">';
								echo '<span>' . do_shortcode( '[tptn_views daily="0"]' ) . ' ' . esc_html__( 'Views', 'osetin' ) . '</span>';
								echo '</li>'; ?>
							<?php } ?>
							<li class="single-meta-likes">
								<?php osetin_vote_build_button( $post->ID, 'slide-button slide-like-button' ); ?>
							</li>
							<li class="mobile-view mb-bakvorm">
								<p>BAKVORM: <span><?php _e($baking_mold); ?></span></p>
							</li>
						</ul>
					</div>
					<?php } ?>
				</div>
				<div class="single-recipe-ingredients-nutritions">
					<?php
					// check if ingredients exist
					if ( osetin_have_rows( 'ingredients' ) ) { ?>
						<?php $searchable_ingredients = osetin_get_field( 'searchable_ingredients' ); ?>
						<?php $show_ingredients_on_mobile = osetin_get_field( 'ingredients_open_on_mobile', 'option' ); ?>

                        <div class="single-ingredients <?php echo ( $show_ingredients_on_mobile ) ? 'visible-on-mobile' : 'hidden-on-mobile'; ?>">
                            <div class="close-btn"><i class="os-icon os-icon-plus"></i></div>
                            <h3>
                                <i class="os-icon os-icon-thin-paper-holes-text"></i> <?php esc_html_e( 'ingrediënten', 'osetin' ); ?>
                            </h3>

							<?php if ( osetin_get_field( 'recipe_serves' ) ) { ?>
                                <div class="ingredient-serves">
                                    <div class="ingredient-serves-label"><?php esc_html_e( 'Porties aanpassen', 'osetin' ); ?></div>
                                    <div class="servings-adjuster-control">
                                        <div class="ingredient-serves-decr"><i
                                                    class="os-icon os-icon-basic2-273_remove_delete_minus"></i></div>
                                        <input class="ingredient-serves-num" type="text"
                                               data-initial-service-num="<?php echo get_field( 'recipe_serves' ); ?>"
                                               data-current-serves-num="<?php echo get_field( 'recipe_serves' ); ?>"
                                               value="<?php echo get_field( 'recipe_serves' ); ?>"/>
                                        <div class="ingredient-serves-incr"><i
                                                    class="os-icon os-icon-basic2-272_add_new_plus"></i></div>
                                    </div>
                                </div>
							<?php } ?>
                            <div class="ingredients-table">
								<div class="row mainrow">
								<?php
								// loop through the rows of ingredients
								$ingredientlimit = 0;
								$limitcount = 1;
								$titleexist = false;
								?>
									<div class="col-md-4">
								<?php
								$end=false;
								while ( osetin_have_rows( 'ingredients' ) ) {
									the_row();
									
									if ( get_sub_field( 'separator' ) ) {
										$title = get_sub_field( 'ingredient_name' );
										if($title!='' && $end==true){
											$titleexist = true;																
										?>
											</div>
												<div class="col-md-4">
										<?php
										}
									}else{

										$title = '';										
										if(!$titleexist){
											if($ingredientlimit>=10){
												$ingredientlimit=0;
												?>
													</div>
														<div class="col-md-4">
												<?php
											}

											$ingredientlimit++;
										}
										if(get_sub_field( 'ingredient_name' )!=''){
											$end=true;
										}										
									}
									/*
									if($limitcount<2){
										if($ingredientlimit==10){
											$ingredientlimit=0;
											$limitcount++;
											?>
												</div>
													<div class="col-md-4 col-box">
											<?php
										}
									}
									$ingredientlimit++;
									*/
									$ingredient_link_html = get_sub_field( 'ingredient_name' );
									?>
									<?php if($title!=''){ //if ( get_sub_field( 'separator' ) ) { ?>
										<div class="row">											
                                            <div class="col-md-12 td" style="max-width: 100%;padding-left: 0px;">
                                                <div class="ingredient-heading"><?php echo $title; //the_sub_field( 'ingredient_name' ); ?></div>
											</div>
										</div>
									<?php } else { ?>
									<?php if($ingredient_link_html!=''){?>
										<div class="row">
											<div class="col-md-1 td ingredient-action">
												<span class="ingredient-mark-icon"><i class="os-icon os-icon-circle-o"></i></span>
											</div>
											<div class="col-md-11 td">
												<span class="ingredient-amount"
														data-initial-amount="<?php the_sub_field( 'ingredient_amount' ); ?>"><?php the_sub_field( 'ingredient_amount' ); ?></span>
												<?php												
												if ( $searchable_ingredients) { ?>
													<?php
													$ingredient_obj = get_sub_field( 'ingredient_obj' );
													

													if ( $ingredient_obj ) {
														$ingredient_link_html = '<a href="' . get_term_link( $ingredient_obj ) . '" target="_blank">' . $ingredient_obj->name . '</a>';
													}
													if ( get_sub_field( 'custom_link' ) ) {
														$ingredient_link_html = '<a href="' . get_sub_field( 'custom_link' ) . '" target="_blank">' . $ingredient_obj->name . '</a>';
													}
													if ( $ingredient_obj || $ingredient_link_html ) { ?>
														<span class="ingredient-name"><?php echo $ingredient_link_html; ?></span>
													<?php } ?>
													<?php
												} else { ?>
													<?php
													if ( get_sub_field( 'custom_link' ) ) {
														echo '<span class="ingredient-name"><a href="' . get_sub_field( 'custom_link' ) . '" target="_blank">' . get_sub_field( 'ingredient_name' ) . '</a></span>';
													} else {
														echo '<span class="ingredient-name">' . get_sub_field( 'ingredient_name' ) . '</span>';
													} ?>
													<?php
												} ?>

												<?php
												if ( get_sub_field( 'ingredient_note' ) ) {
													echo '<span class="ingredient-info-icon" onclick="void(0)"><i class="os-icon os-icon-info-round"></i><span class="ingredient-info-popup">' . esc_attr( get_sub_field( 'ingredient_note' ) ) . '</span></span>';
												} ?>
											</div>
										</div>
											<?php } //if end ?>
									<?php } //else end ?>
									
									<?php									
								}
								?>
								<?php if($end){?>
								</div>
								<?php }?>
							</div><!-- row end-->
                            </div>
                        </div>
						<?php
					} ?>

					<?php
					// check if ingredients exist
					if ( osetin_have_rows( 'nutritions' ) ) { ?>
						<?php $show_nutritions_on_mobile = osetin_get_field( 'nutritions_open_on_mobile', 'option' ); ?>

                        <div class="single-nutritions <?php echo ( $show_nutritions_on_mobile ) ? 'visible-on-mobile' : 'hidden-on-mobile'; ?>">
                            <div class="close-btn"><i class="os-icon os-icon-plus"></i></div>
                            <h3>
                                <i class="os-icon os-icon-thin-info"></i> <?php esc_html_e( 'Nutritional information', 'osetin' ); ?>
                            </h3>
                            <div class="single-nutritions-list">
								<?php
								// loop through the rows of nutritions
								while ( osetin_have_rows( 'nutritions' ) ) {
									the_row(); ?>
                                    <div class="single-nutrition">
                                        <div class="single-nutrition-value"><?php the_sub_field( 'nutrition_value' ); ?></div>
                                        <div class="single-nutrition-name"><?php the_sub_field( 'nutrition_name' ); ?></div>
                                    </div>
									<?php
								} ?>
                            </div>
                        </div>
						<?php
					} ?>

                </div>
				<div class="single-content" data-font-change-count="0">
                <div class="cooking-mode-close-btn-w">
                    <a href="#" class="cooking-mode-toggler cooking-mode-close-btn"><i
                                class="os-icon os-icon-thin-close-round"></i></a>
                </div>


				<?php // PRINTABLE INGREDIENTS: ?>


				<?php
				// check if ingredients exist
				if ( osetin_have_rows( 'ingredients' ) ) { ?>
					<?php $searchable_ingredients = osetin_get_field( 'searchable_ingredients' ); ?>

                    <div class="single-print-ingredients">
                        <h2 class="bordered-title"><i class="os-icon os-icon-thin-paper-holes-text"></i>
                            <span><?php esc_html_e( 'Ingredients', 'osetin' ); ?></span></h2>
                        <ul>
							<?php
							// loop through the rows of ingredients
							while ( osetin_have_rows( 'ingredients' ) ) {
								the_row(); ?>
                                <li>
								<?php if ( get_sub_field( 'separator' ) ) { ?>
                                    <h3><?php the_sub_field( 'ingredient_name' ); ?></h3>
								<?php } else { ?>
                                    <div class="print-ingredient">
                                        <span class="ingredient-amount"><?php the_sub_field( 'ingredient_amount' ); ?></span>
										<?php if ( $searchable_ingredients ) { ?>
											<?php
											$ingredient_obj = get_sub_field( 'ingredient_obj' );
											if ( $ingredient_obj ) { ?>
                                                <span class="ingredient-name"><?php echo $ingredient_obj->name; ?></span>
											<?php } ?>
										<?php } else { ?>
                                            <span class="ingredient-name"><?php the_sub_field( 'ingredient_name' ); ?></span>
										<?php } ?>
										<?php if ( get_sub_field( 'ingredient_note' ) ) {
											echo '<div class="ingredient-print-note">' . get_sub_field( 'ingredient_note' ) . '</div>';
										} ?>
                                    </div>
                                    </li>
									<?php
								}
							} ?>
                        </ul>
                    </div>
					<?php
				} ?>

				<?php // END PRINTABLE INGREDIENTS ?>
                
				<?php
				// check if steps exist
				if ( osetin_have_rows( 'steps' ) ) { ?>

                    <div class="single-steps">
                        <h2 class="bordered-title"><span><?php esc_html_e( 'STAPPENPLAN', 'osetin' ); ?></span></h2>
                        <table class="recipe-steps-table lasthide">
							<?php
							$step_number = 0;
							// loop through the rows of steps
							while ( osetin_have_rows( 'steps' ) ) {
								the_row();
								$step_number++;
								?>
                                <tr class="single-step">
                                    <td class="single-step-number">
										<?php $stestilt = get_sub_field( 'step_description' );?>
										<?php if ( !empty($stestilt)) : ?>
										<div class="single-step-number-i">
											<div class="single-step-number-value"><?php echo esc_html( $step_number ) ?></div>
											<div class="single-step-control" wfd-id="236">
												<i class="os-icon os-icon-circle-o"></i>
											</div>
										<?php else : ?>
										<?php echo ""; ?>
										<?php endif; ?>
										<?php if ( get_sub_field( 'step_duration' ) ) { ?>
                                            <div class="single-step-duration"><i class="os-icon os-icon-clock"></i> <?php echo get_sub_field( 'step_duration' ) ?>
                                            </div>
										<?php } ?>
                                    </td>
                                    <td class="single-step-description">
										<?php if ( get_sub_field( 'step_title' ) ) { ?>
                                            <h4 class="single-step-title"><?php echo get_sub_field( 'step_title' ) ?></h4>
										<?php } ?>
                                        <div class="single-step-description-i">
                                            <?php 
											$images = get_sub_field('step_images');
                                            if( $images ): ?>
                                                <ul class="single-step-media">
                                                    <?php foreach( $images as $image ): ?>
                                                        <li>
                                                            <a class="osetin-lightbox-trigger-step-images" href="<?php echo $image['url']; ?>" data-lightbox-thumb-src="<?php echo $image['url']; ?>" data-lightbox-img-src="<?php echo $image['url']; ?>">
                                                                 <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                            </a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
											<?php echo do_shortcode( get_sub_field( 'step_description' ) ); ?>
                                            <div class="step-off-fader"></div>
                                        </div>
                                    </td>
                                </tr>
								<?php
							}
							wp_reset_postdata(); ?>
                        </table>
                    </div>
					<?php
				} ?>
				<div class="recipes-tips mobilesteps">
					<h2>TIPS</h2>
					<div class="tipsnow">
						<p><?php echo get_field( 'Tips' ); ?></p>
					</div>
				</div>
            </div>
			</div>
			<div class="col-md-4 slider-holder">
				<div class="recipes-tips ipad-view">
					<h2>TIPS</h2>
					<div class="tipsnow">
						<p><?php echo get_field( 'Tips' ); ?></p>
					</div>
				</div>
				<?php if($user_add_image): ?>
					<div class="demo">
						<h2>KOOKMUTSJES CREATIES</h2>
						<div class="item">
							<div class="clearfix" style="max-width:474px;">
								<ul id="image-gallery" class="gallery list-unstyled cS-hidden" data-isuserlogin="<?php echo is_user_logged_in();?>" >
									<?php
										foreach($user_add_image as $key=>$thumbnail_id){
											$url =  wp_get_attachment_url( $thumbnail_id );
											?>
												<li data-thumb="<?php echo $url;?>"> 
													<img src="<?php echo $url;?>" />
												</li>
											<?php
										}
									?>
								</ul>
							</div>
						</div>
					</div>
					<?php if ( !is_user_logged_in() ) { ?>
						<div class="user-upload-sec">
							<a href="<?php site_url();?>/log-in"><img src="	<?php site_url(); ?>/wp-content/uploads/2020/01/camera-1.png" alt="add-camera" /></a>
						</div>
					<?php } else { ?>
						<div class="user-upload-sec">
							<button data-postid="<?php echo get_the_ID(); ?>" class="img-upload-btn" data-toggle="modal" data-target="#uploadModal"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/camera-1.png" alt="add-camera" /></button>
						</div>
					<?php } ?>
					<?php else : ?>
						<div class="demo">
							<h2>KOOKMUTSJES CREATIES</h2>
							<?php if ( !is_user_logged_in() ) { ?>
								<div class="not-sliderbutton">
									<a href="<?php site_url();?>/log-in"></a>
								</div>
							<?php } else { ?>
								<div class="not-sliderbutton">
									<button data-postid="<?php echo get_the_ID(); ?>" class="img-upload-btn" data-toggle="modal" data-target="#uploadModal"></button>
								</div>
							<?php } ?>
						</div>
					<?php endif; ?>
				<h2 id="boxheading">SHARE IT ON SOCIAL MEDIA</h2>
				<?php echo do_shortcode('[Sassy_Social_Share]');?>
				<h2 id="boxheading">Gerelateerde RECEPTEN</h2>
				<?php
				$osetin_current_post_id = get_the_ID();
				$related_recipes_args = array(
					'paged'               => 1,
					'posts_per_page'      => 3,
					'post_type'           => 'osetin_recipe',
					'post_status'         => 'publish',
					'ignore_sticky_posts' => 1,
					'post_password'       => '',
					'meta_query'          => array(
						array(
							'key'     => '_thumbnail_id',
							'value'   => 0,
							'type'    => 'NUMERIC',
							'compare' => '>'
						),
					)
				);
				$specific_related_posts = osetin_get_field( 'show_selected_posts_as_relative', false, false, true );
				if ( $specific_related_posts ) {
					// specific related posts were selected, show those
					$related_recipes_args[ 'post__in' ] = $specific_related_posts;
				} else {
					$related_recipes_selecting_by = osetin_get_field( 'related_recipes_selecting_by', 'option', 'random' );
					switch ( $related_recipes_selecting_by ) {
						case 'category':
							$current_post_categories = wp_get_post_categories( $osetin_current_post_id, array( 'fields' => 'all' ) );
							// post has categories
							if ( $current_post_categories ) {
								$category_ids = array();
								foreach ( $current_post_categories as $individual_category ) {
									$category_ids[] = $individual_category->term_id;
								}
								$related_recipes_args[ 'category__in' ] = $category_ids;
							}
							break;
						case 'tag':
							$current_post_tags = wp_get_post_tags( $osetin_current_post_id );
							// post has tags
							if ( $current_post_tags ) {
								$tag_ids = array();
								foreach ( $current_post_tags as $individual_tag ) {
									$tag_ids[] = $individual_tag->term_id;
								}
								$related_recipes_args[ 'tag__in' ] = $tag_ids;
							}
							break;
						default:
							$related_recipes_args[ 'orderby' ] = 'rand';
							break;
					}
					$related_recipes_args[ 'post__not_in' ] = array( $osetin_current_post_id );
				}
				$osetin_related_recipes_query = new WP_Query( $related_recipes_args );

				$related_recipes_bg = osetin_get_field( 'related_posts_background_image', 'option' );
				if ( empty( $related_recipes_bg ) ) {
					$related_recipes_bg = get_template_directory_uri() . '/assets/img/patterns/flowers_light.jpg';
				}
				?>
				<div class="related-rec">
					<ul class="related-recipes">
						<?php while ( $osetin_related_recipes_query->have_posts() ) : $osetin_related_recipes_query->the_post(); ?>

							<li>
								<a href="<?php the_permalink(); ?>" class="fader-activator">
									<figure><?php the_post_thumbnail( 'osetin-medium-square-thumbnail' ); ?><span
												class="image-fader"><span class="hover-icon-w"><i
														class="os-icon os-icon-plus"></i></span></span></figure>
									<span><?php the_title(); ?></span>
								</a>
							</li>

						<?php endwhile;
						wp_reset_postdata();
						?>
					</ul>
				</div>
				<div class="recipes-tips ipad-desktop">
					<h2>TIPS</h2>
					<div class="tipsnow">
						<p><?php echo get_field( 'Tips' ); ?></p>
					</div>
				</div>
				<div class="publish-rec">
					<h2>VOEG ZELF EEN RECEPT TOE</h2>
					<p>Heb jij of wil jij ook een heerlijk recept met ons delen? Dat kan!</p>
					<?php if ( is_user_logged_in() ) { ?>
						<a href="<?php site_url();?>/publish-recipe"><i class="fa fa-plus" aria-hidden="true"></i> Recept toevoegen</a>
					<?php } else { ?>
						<a href="<?php site_url();?>/log-in"><i class="fa fa-plus" aria-hidden="true"></i> Recept toevoegen</a>
					<?php } ?>	
				</div>
			</div>
		</div>
	</div>
    <div class="os-container">
        <div class="single-post-comments-w with-ads">
            <div class="single-post-comments" id="singlePostComments">
				<div id="go-comments"><a href="#reply-title">Write Comments</a></div>
				<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
				?>
            </div>
			<?php if ( is_active_sidebar( 'sidebar-single-comments' ) ) { ?>
                <div class="single-post-comments-sidebar">
					<?php dynamic_sidebar( 'sidebar-single-comments' ); ?>
                </div>
			<?php } ?>
        </div>
    </div>
	<?php endwhile; // end of the loop. ?>
	<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="uploadModalLabel"><i class="vc_tta-icon fa fa-picture-o" aria-hidden="true"></i> Upload Recipe Images</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="dishUpload" action="upload_user_images" enctype="multipart/form-data">
						<input type="hidden" name="c_postId" value="<?php echo get_the_ID(); ?>">
						<!-- <div class="form-group">
							<label for="recipient-name" class="col-form-label">Image Name:<span>(Optional)</span></label>
							<input type="text" class="form-control" name="img_name" id="rec_name">
						</div> -->
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Voer uw receptnaam in" name="img_name" id="rec_name">
							<div class="file-field">
								<div class="btn btn-primary btn-sm float-left waves-effect waves-light">
									<span><i class="fa fa-upload" aria-hidden="true"></i> Kies Recept Afbeeldingen</span>
									<input type="file" name="img" id="rec_img" multiple="multiple">	
								</div>
							</div>
							<label style="display:none;" class="upload-files-count col-form-label"></label>
							<label style="color:red;display:none;" class="img-upload-error col-form-label">Selecteer Recept Afbeelding</label>
						</div>
					</form>
					<!-- <div class="row rec-upimages">
						<div class="col-sm-6 upimg"><div class="df-sec"> + Add </div></div>
						<div class="col-sm-6 upimg">Hello</div>
						
					</div>
					<div class="progBra"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/09/ezgif.com-resize.gif" alt="loader" /></div>
					-->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="imgUpload"><i class="fa fa-paper-plane" aria-hidden="true"></i> Publiceer nu! <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></button>
					
				</div>
			</div>
		</div>
	</div>
	<script>
		jQuery(document).ready(function(){
		//var progBar = jQuery('body').find('.progBra')
		jQuery('body').on('click','.img-upload-btn',function(e){	
			e.preventDefault()
			let oModal = jQuery(this).data('target');
			jQuery(oModal).css({'display' : 'block', 'opacity' : 1 });
		})
		jQuery('body').on('click','.close',function(e){	
			let cModal = jQuery(this).closest('.modal')
			jQuery(cModal).css({'display' : 'none', 'opacity' : 0 });
		})
		// jQuery('.upimg').on('click','.df-sec',function(e){	
		// 	jQuery('#rec_img').click();
		// })
		 jQuery("#rec_img").change(function () {
			 if(jQuery(this).prop('files').length>0){
			 jQuery('.upload-files-count').text(jQuery(this).prop('files').length+' Files for upload').css({'display':'block'});
			 }else{
				jQuery('.upload-files-count').css({'display':'none'});
			 }
		 });

		jQuery('body').on('click','#imgUpload',function(){
           var postID = jQuery('.img-upload-btn').data('postid')
           var postName = jQuery('#rec_name').val().trim()
           var formElem = jQuery(this).closest('#uploadModal').find('#dishUpload');
           var file_data = formElem.find('#rec_img').prop('files');
		   //console.log(formElem.find('.img-upload-error').length)
		   if(jQuery('#image-gallery').data('isuserlogin')!=1){
				window.location.href='https://dev.kookmutsjes.com/log-in/';
				return false;
		   }
		   if(postName==''){
			formElem.find('.img-upload-error').text('image name required').css({'display':'block'});
			return false;
		   }
		   if(file_data.length==0){
				formElem.find('.img-upload-error').text('Selecteer Recept Afbeelding').css({'display':'block'});
			   return false;
			}

		   var form_data = new FormData();
           jQuery.each(file_data,function(key,val){
           		form_data.append('file'+key, val);
           })
           var ajaxAction = formElem.attr('action')
           
           form_data.append('post_id', postID);
           form_data.append('action', ajaxAction);
           form_data.append('rec_name', postName);
           jQuery.ajax({
            url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
            type: 'post',
            contentType: false,
            processData: false,
            data: form_data,
            beforeSend : function(){
              //progBar.show()
              jQuery('.lds-ellipsis').css({'display':'inline-block'})
            },
            success : function(result){
			  jQuery('.lds-ellipsis').hide()
              //progBar.find('span').text('Recipe Images Uploaded Successfully! You are Redirecting.')
              //progBar.addClass('successs')
              console.log(result)
              location.reload();
            },
            error: function(e) {
                console.log(e);
              }
          });
		})
	   })

	// function upimgReadURL(input) {
	// 	console.log(input.files)
 //        if (input.files && input.files[0]) {
 //        	console.log('')
 //            var reader = new FileReader();
 //            var imgtemp;
 //            reader.onload = function (e) {
 //                console.log(e.target.result)
 //                imgtemp = e.target.result
 //                jQuery('.rec-upimages').prepend('<div class="col-sm-6 upimg"><img src="" /></div>')
 //                jQuery('.upimg img').eq(0).attr('src',imgtemp)
 //            }

 //            reader.readAsDataURL(input.files[0]);
 //        }
 //    }
	</script>
	<script>
		$(document).ready(function () {  
           $("#right").click(function () { 
           var leftPos2 = $('.DivDataMain').scrollLeft();
           $(".DivDataMain").animate({scrollLeft: leftPos2 - 250}, 600);
      });   
           });   
	</script>

<?php get_footer(); ?>