	<?php
	/* Template Name: Stories */ 
	get_header(); ?>
  
	<div class="os-container top-bar-w">
		<div class="top-bar <?php if(!osetin_is_imaged_header(get_the_ID())) echo 'bordered'; ?>">
			<ul>
				<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			</ul>
		</div>
	</div>
	<div class="os-container">
		<div class="page-w <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
			<div class="page-content">
				<div class="searchblogs mobile-view">
					<?php echo do_shortcode('[wd_asp id=5]');?>
				</div>
				<?php
				$readmore = "Lees meer";
				$currentPage = get_query_var('paged');
				$post_date = get_the_date( 'F jS Y' );
				$posts = new WP_Query(array(
					'post_type' => 'post',
					'posts_per_page' => 10,
					'cat' => -56,
					'paged' => $currentPage
				));
 
				if ($posts->have_posts()) :
					while ($posts->have_posts()) :
						$posts->the_post();
						echo "<div class='post-wrap'>";
						$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); 
						echo '<img src="' . $feat_image . '" alt="stories-image">';
						echo "<h2>";the_title();echo "</h2>";
						echo "<span class='author-post'>";the_author();echo "</span>"; echo "<span class='post-date'>";echo $post_date;echo "</span>"; echo "<span class='post-cats'>";$categories = get_the_category();
						if ( ! empty( $categories ) ) {
							echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
						}
						echo "</span>";
						echo "<div class='post-contents'>";the_excerpt();echo "</div>";
						echo '<a class="stories-read" href="' . get_permalink() . '">' .$readmore.'</a>';
						echo "</div>";
					endwhile;
				endif;
				
				// Bottom pagination (pagination arguments)
				echo "<div class='page-nav-container'>" . paginate_links(array(
					'total' => $posts->max_num_pages,
					'prev_text' => __('<'),
					'next_text' => __('>')
				)) . "</div>";
				 
				?>
			</div>
			<div class="page-sidebar">
				<div class="searchblogs">
					<?php echo do_shortcode('[wd_asp id=5]');?>
				</div>
				<ul class="popular-post">
					<h2>Populairste stories</h2>
				   <?php
					  query_posts('meta_key=post_views_count&posts_per_page=3&cat=-56&orderby=meta_value_num&
					  order=DESC');
					  $post_dates = get_the_date( 'F jS Y' );
					  if (have_posts()) : while (have_posts()) : the_post();
				   ?>
					<li>
						<div class="recent-post-img">
							<?php	
								$featured_img_url = get_the_post_thumbnail_url('full'); 
								echo '<a href="'.$featured_img_url.'" rel="lightbox">'; 
								the_post_thumbnail('thumbnail');
								echo '</a>';
							?>
						</div>
						<div class="recent-post-des">
							<a href="<?php the_permalink(); ?>"><p class="slider-caption-class"><?php the_title();?></p></a>
							<span class='post-date'><?php  echo $post_dates;?></span>
							<a id="recent-post-link" href="<?php the_permalink(); ?>">Lees meer</a>
						</div>
					</li>
				   <?php
				   endwhile; endif;
				   wp_reset_query();
				   ?>
				</ul>
				<div class="stories-ads">
					<img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/09/Tired-of-Ads.jpg" alt="stories-ads" />
				</div>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>