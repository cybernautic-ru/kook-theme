<?php
/**
 * Template Name: WM
 *
 */
?>
<?php
/**
 * The template for displaying all pages.
 *
 * @package Neptune
 */
get_header(); ?>
	<div class="os-container top-bar-w">
		<div class="top-bar <?php if(!osetin_is_imaged_header(get_the_ID())) echo 'bordered'; ?>">
			<ul>
				<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			</ul>
		</div>
	</div>
	<div class="os-container">
		<div class="page-w <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
			<div class="page-content">
				<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="weekmenupage">
						<?php
						$args = array(
						  'post_type'   => 'weekmenu',
						  'post_status' => 'publish',
						 );
						 
						$weekmenu = new WP_Query( $args );
						if( $weekmenu->have_posts() ) :
						?>
						<?php
						while( $weekmenu->have_posts() ) :
						$weekmenu->the_post();
						?>						
						<ul>
							<li>
								<h2>Vrijdag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('friday_menu_page_image') ): ?>
										<a href="<?php the_field('friday_menu_url'); ?>"><img src="<?php the_field('friday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<h2>Zaterdag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('saturday_menu_page_image') ): ?>
										<a href="<?php the_field('saturday_menu_url'); ?>"><img src="<?php the_field('saturday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<h2>Zondag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('sunday_menu_page_image') ): ?>
										<a href="<?php the_field('sunday_menu_url'); ?>"><img src="<?php the_field('sunday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<h2>Maandag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('monday_menu_page_image') ): ?>
										<a href="<?php the_field('monday_menu_url'); ?>"><img src="<?php the_field('monday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<h2>Dinsdag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('tuesday_menu_page_image') ): ?>
										<a href="<?php the_field('tuesday_menu_url'); ?>"><img src="<?php the_field('tuesday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<h2>Woensdag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('wednesday_menu_page_image') ): ?>
										<a href="<?php the_field('wednesday_menu_url'); ?>"><img src="<?php the_field('wednesday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<h2>Donderdag</h2>
								<div class="weekmenu-img">
									<?php if( get_field('thursday_menu_page_image') ): ?>
										<a href="<?php the_field('thursday_menu_url'); ?>"><img src="<?php the_field('thursday_menu_page_image'); ?>" /></a>
									<?php endif; ?>
								</div>
							</li>
						</ul>
						<?php
						  endwhile;
						  wp_reset_postdata();
						?>	
						<?php
						else :
						  esc_html_e( 'No testimonials in the diving taxonomy!', 'text-domain' );
						endif;
						?>
					</div>
				</article>
			</div>
			<?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) { ?>
			<div class="page-sidebar">
				<?php dynamic_sidebar( 'sidebar-index' ); ?>
			</div>
			<?php } ?>
		</div>
	</div>
<?php get_footer(); ?>