<?php

function hook_footer_popup_template()
{
    if (is_singular('osetin_recipe')) {
?>
        <div class="baking-mold-popup">
            <div class="baking-mold-popup_inner">
                <div class="baking-mold-popup-close_block">
                    <button id="baking-mold-popup-close_btn"><i class="fa fa-times-circle"></i></button>
                </div>
                <div class="bmp-page_main">
                    <h4 class="bmp-page_main-title"><?php _e('Kies je bakvorm', 'neptune-child'); ?></h4>
                    <div class="baking-mold-current">
                        <span class="baking-mold-current-title"><?php _e('Current:', 'neptune-child'); ?></span> <span class="baking-mold-current-size"></span>, <span class="baking-mold-current-type"></span>
                    </div>
                    <div class="bmp-page_main-variants">
						<div class="bmp-page_main-variant" data-variant="circular">
                            <button class="baking-mold-type-button" id="baking-mold-type-cir"><?php _e('Circulaire', 'neptune-child'); ?></button>
							<p><?php _e('Rond', 'neptune-child');?></p>
                        </div>
                        <div class="bmp-page_main-variant" data-variant="rectangular">
                            <button class="baking-mold-type-button" id="baking-mold-type-rec"><?php _e('Rechthoekig', 'neptune-child'); ?></button>
							<p><?php _e('Vierhoek', 'neptune-child');?></p>
                        </div>
                    </div>
                </div>
                <div class="bmp-page_child bmp-page_rectangle">
                    <div class="bmp-page-size_block">
                        <div class="input-container rec-input">
							<div class="input-box">
								<label for="bmp-rectangle-size-length"><?php _e('Lengte', 'neptune-child'); ?></label>
								<input type="text" id="bmp-rectangle-size-length"><span class="baking-mold-size-unit"><?php _e('Cm', 'neptune-child'); ?></span>
							</div>
					   </div>
                        <div class="input-container rec-inputs">
							<div class="input-boxs">
								<label for="bmp-rectangle-size-width"><?php _e('Breedte', 'neptune-child'); ?></label>
								<input type="text" id="bmp-rectangle-size-width"><span class="baking-mold-size-unit"><?php _e('Cm', 'neptune-child'); ?></span>
							</div>
                        </div>
                        <div class="button-container">
                            <button class="baking-mold-apply-size baking-mold-apply-rectangle"><?php _e('Apply', 'neptune-child'); ?></button>
                        </div>
                    </div>
                </div>
                <div class="bmp-page_child bmp-page_circle">
                    <div class="bmp-page-size_block">
                        <div class="input-container">
                            <label for="bmp-rectangle-size-diameter"><?php _e('Diameter', 'neptune-child'); ?></label>
                            <input type="text" id="bmp-rectangle-size-diameter"><span class="baking-mold-size-unit"><?php _e('Cm', 'neptune-child'); ?></span>
                        </div>
                        <div class="button-container">
                            <button class="baking-mold-apply-size baking-mold-apply-circle"><?php _e('Apply', 'neptune-child'); ?></button>
                        </div>
                    </div>
                </div>
                <div class="bmp-reset-button-container">
                    <button class="baking-mold-apply-size baking-mold-reset"><?php _e('Reset', 'neptune-child'); ?></button>
                </div>
            </div>
        </div>
<?php
    }
}
add_action('wp_footer', 'hook_footer_popup_template');
