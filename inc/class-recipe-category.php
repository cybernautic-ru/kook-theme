<?php
/**
 * Class RecipeCategory
 *
 * functionality to work with recipe categories
 */

class RecipeCategory {
	private static $instance;

	public static function get_instance() {
		if ( self::$instance == null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct() {
	}

	/**
	 * Gets categories of recipes
	 *
	 * @return array array(array('name'=> string, 'term_taxonomy_id' => int, 'parent' => int, 'parent_name' => string) ...)
	 */
	private function get_category_request_recipe() {
		global $wpdb;

		return $wpdb->get_results( "SELECT t.name, tt.term_taxonomy_id, tt.parent, (SELECT name FROM wp_terms WHERE wp_terms.term_id = tt.parent) AS parent_name
									FROM wp_terms t
										INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
										INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
										INNER JOIN wp_posts p ON (tr.object_id = p.ID)
									WHERE p.post_type = 'osetin_recipe' AND p.post_status = 'publish' AND tt.taxonomy = 'category'
									GROUP BY tt.term_taxonomy_id
									ORDER BY t.name", ARRAY_A );
	}

	/**
	 * Get recipe categories in array tree
	 *
	 * @return array $tree_arr
	 */
	private function get_category_tree_recipe() {
		$categories_arr = $this->get_category_request_recipe();
		$tree_arr       = array();
		if ( ! empty( $categories_arr ) ) {
			foreach ( $categories_arr as $cat ) {
				if ( ! in_array( $cat['name'], $tree_arr ) ) {
					if ( $cat['parent'] > 0 ) {
						if ( ! in_array( $cat['parent_name'], $tree_arr ) ) {
							$tree_arr[ $cat['parent'] ]['id']   = $cat['parent'];
							$tree_arr[ $cat['parent'] ]['name'] = $cat['parent_name'];
						}
						$tree_arr[ $cat['parent'] ]['child'][ $cat['term_taxonomy_id'] ]['id']   = $cat['term_taxonomy_id'];
						$tree_arr[ $cat['parent'] ]['child'][ $cat['term_taxonomy_id'] ]['name'] = $cat['name'];
					} else {
						$tree_arr[ $cat['term_taxonomy_id'] ]['id']   = $cat['term_taxonomy_id'];
						$tree_arr[ $cat['term_taxonomy_id'] ]['name'] = $cat['name'];
					}
				}
			}
		}

		return $tree_arr;
	}

	/**
	 * Gets template for the parent category block
	 *
	 * @param $_parent_cat
	 *
	 * @return string
	 */
	private function get_rc_template_parent( $_parent_cat, $_checked_arr = array() ) {
		$tmp = '';

		$p_checked = false;
		foreach ( $_parent_cat['child'] as $c_cat ) {
			$checked = false;
			if ( ! empty( $_checked_arr ) ) {
				$checked   = in_array( $c_cat['id'], $_checked_arr );
				$p_checked = $checked ? true : $p_checked;
			}
			$checked_text = $checked ? 'checked' : '';
			$tmp          .= '
			<div class="rb-category_inner rb-category-child_block">
				<div class="form-check-inline rb-category-child_inner">
					<input class="form-check-input rb-category_checker" data-cat="' . $c_cat['id'] . '" type="checkbox" value="' . $c_cat['id'] . '" id="rb-cat-' . $c_cat['id'] . '" ' . $checked_text . '>
					<label class="form-check-label rb-category-child_title" data-cat="' . $c_cat['id'] . '" for="rb-cat-' . $c_cat['id'] . '">
						' . $c_cat['name'] . '
					</label>
				</div>
			</div>';
		}

		if ( ! empty( $_checked_arr ) ) {
			if ( in_array( $_parent_cat['id'], $_checked_arr ) ) {
				$p_checked = true;
			}
		}
		$show_text       = $p_checked ? 'show' : '';
		$a_expanded_text = $p_checked ? "true" : "false";
		$tmp_s           = '
		<div class="rb-category_block rb-category-patent_block">
			<div class="rb-category_inner">
				<div class="rb-category-patent_title" data-toggle="collapse" data-parent-id="' . $_parent_cat['id'] . '" data-target="#rb-cat-parent-' . $_parent_cat['id'] . '" aria-expanded="' . $a_expanded_text . '" aria-controls="rb-cat-parent-' . $_parent_cat['id'] . '">
					<span class="rb-category-patent_title_value">' . $_parent_cat['name'] . '</span><span class="pull-right"><i class="fa fa-plus rb-category-patent_icon"></i></span>
				</div>
				<div id="rb-cat-parent-' . $_parent_cat['id'] . '" class="rb-category-parent_inner collapse ' . $show_text . '" data-parent-id="' . $_parent_cat['id'] . '">';

		$tmp = $tmp_s . $tmp;

		$tmp .= '
				</div>
			</div>
		</div>';

		return $tmp;
	}

	/**
	 * Gets category (does not have parent) template
	 *
	 * @param $_cat_main
	 *
	 * @return string
	 */
	private function get_rc_template_main( $_cat_main, $_checked = false ) {
		$checked_text = $_checked ? 'checked' : '';
		$tmp          = '
		<div class="rb-category_block rb-category-child_block">
			<div class="rb-category_inner">
				<div class="form-check-inline rb-category-child_inner">
					<input class="form-check-input rb-category_checker" type="checkbox" data-cat="' . $_cat_main['id'] . '" value="' . $_cat_main['id'] . '" id="rb-cat-' . $_cat_main['id'] . '" ' . $checked_text . '>
					<label class="form-check-label rb-category-child_title" for="rb-cat-' . $_cat_main['id'] . '" data-cat="' . $_cat_main['id'] . '">
						' . $_cat_main['name'] . '
					</label>
				</div>
			</div>
		</div>';

		return $tmp;
	}

	/**
	 * Gets template of the categories list
	 *
	 * @return string
	 */
	public static function get_template_recipe_categories( $_cat_attr = array() ) {
		$c    = ( new RecipeCategory )->get_category_tree_recipe();
		$cats = ( new RecipeCategory )->get_sorting_parent_first( $c );
		$tmp  = '';
		foreach ( $cats as $cat ) {
			if ( array_key_exists( 'child', $cat ) ) {
				$tmp .= ( new RecipeCategory )->get_rc_template_parent( $cat, $_cat_attr );
			} else {
				$tmp .= ( new RecipeCategory )->get_rc_template_main( $cat, ! empty( $_cat_attr ) ? ( in_array( $cat['id'], $_cat_attr ) ) : false );
			}
		}

		return $tmp;
	}

	/**
	 * Gets sorted array
	 *
	 * @param $_arr
	 *
	 * @return array
	 */
	private function get_sorting_parent_first( $_arr ) {
		$t_arr_AO = new ArrayObject( $_arr );
		$t_arr    = $t_arr_AO->getArrayCopy();
		$p_first  = array();
		$c_second = array();
		foreach ( $t_arr as $t ) {
			if ( array_key_exists( 'child', $t ) ) {
				usort( $t['child'], array( $this, 'sort_cmp' ) );
				$p_first[] = $t;
			} else {
				$c_second[] = $t;
			}
		}
		usort( $p_first, array( $this, 'sort_cmp' ) );
		usort( $c_second, array( $this, 'sort_cmp' ) );

		return array_merge( $p_first, $c_second );
	}

	/**
	 * Sorting string function
	 *
	 * @param $a
	 * @param $b
	 *
	 * @return int|lt
	 */
	public static function sort_cmp( $a, $b ) {
		return strcmp( $a['name'], $b['name'] );
	}

	public static function get_avilable_tags( $_cat_ids, $_tags_ids = array() ) {
		$q_tags = empty( $_tags_ids ) ? ( new RecipeCategory )->get_tags_by_category( $_cat_ids ) : ( new RecipeCategory )->get_tags_by_category_tags( $_cat_ids, $_tags_ids );
		if ( ! empty( $q_tags ) ) {
			$cats_names = ( new RecipeCategory )->get_categories_info( $_cat_ids );
			for ( $j = 0; $j < count( $cats_names ); $j ++ ) {
				$cats_names[ $j ] = trim( strtolower( $cats_names[ $j ] ) );
			}
			for ( $i = 0; $i < count( $q_tags ); $i ++ ) {
				if ( $cats_names !== false && ! empty( $cats_names ) ) {
					if ( in_array( trim( strtolower( $q_tags[ $i ]['name'] ) ), $cats_names ) ) {
						unset( $q_tags[ $i ] );
						continue;
					}
				}
			}
		}

		return array_values( $q_tags );
	}

	private function get_categories_info( $_cat_ids ) {
		if ( ! empty( $_cat_ids ) ) {
			global $wpdb;
			$_cat_ids = array_map( function ( $v ) {
				return "'" . esc_sql( $v ) . "'";
			}, $_cat_ids );
			$_cat_ids = implode( ',', $_cat_ids );
			$result_q = $wpdb->get_results( "SELECT t.name
									FROM wp_terms t
										INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
										INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
										INNER JOIN wp_posts p ON (tr.object_id = p.ID)
									WHERE p.post_type = 'osetin_recipe' AND p.post_status = 'publish' AND tt.taxonomy = 'category' AND tt.term_taxonomy_id IN ($_cat_ids)
									GROUP BY tt.term_taxonomy_id
									ORDER BY t.name", ARRAY_N );

			return ( new RecipeCategory )->get_array_result( $result_q );
		}

		return false;
	}

	private function get_array_result( $q_array, $_n_type = '' ) {
		$return_array = array();
		if ( count( $q_array ) > 0 ) {
			foreach ( $q_array as $q_row ) {
				if ( strlen( $_n_type ) > 0 ) {
					switch ( $_n_type ) {
						case 'int':
							$return_array[] = intval( $q_row[0] );
							break;
						default:
							$return_array[] = $q_row[0];
					}
				} else {
					$return_array[] = $q_row[0];
				}
			}
		}

		return $return_array;
	}

	private function get_tags_by_category( $_cat_ids ) {
		global $wpdb;
		$sql_str_add = "";
		if ( ! empty( $_cat_ids ) ) {
			$_cat_ids    = array_map( function ( $v ) {
				return "'" . esc_sql( $v ) . "'";
			}, $_cat_ids );
			$_cat_ids    = implode( ',', $_cat_ids );
			$sql_str_add = "AND ( tt.term_taxonomy_id IN ($_cat_ids) OR tt.parent IN ($_cat_ids) )";
		}
		$sql_str = "SELECT tt.term_taxonomy_id, t.name, COUNT(p.ID) as total_posts
			FROM wp_terms t
				INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
				INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
				INNER JOIN wp_posts p ON (tr.object_id = p.ID)
			WHERE tt.taxonomy = 'post_tag' AND p.post_type = 'osetin_recipe' AND p.post_status = 'publish' AND p.ID IN (SELECT p.id
				FROM wp_terms t
				INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
				INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
				INNER JOIN wp_posts p ON (tr.object_id = p.ID)
				WHERE p.post_type = 'osetin_recipe' AND p.post_status = 'publish' AND tt.taxonomy = 'category' " . $sql_str_add . ") 
			GROUP BY tt.term_taxonomy_id
			ORDER BY total_posts DESC";

		return $wpdb->get_results( $sql_str, ARRAY_A );
	}

	private function get_tags_by_category_tags( $_in_cats, $_in_tags, $_tags_ex = false ) {
		global $wpdb;

		$posts_cats_tags = ( new RecipeCategory )->get_posts_cats_tags( $_in_cats, $_in_tags );

		if ( ! empty( $posts_cats_tags ) ) {
			$for_sql_in_posts = '';
			$posts_cats_tags  = array_map( function ( $v ) {
				return "'" . esc_sql( $v ) . "'";
			}, $posts_cats_tags );
			$for_sql_in_posts = implode( ',', $posts_cats_tags );
			$for_sql_in_posts = "AND p.ID IN ($for_sql_in_posts)";

			$for_sql_tags_ex = '';
			if ( ! empty( $_in_tags ) ) {
				$for_sql_tags_ex = '';
				if ( $_tags_ex ) {
					$_in_tags        = array_map( function ( $v ) {
						return "'" . esc_sql( $v ) . "'";
					}, $_in_tags );
					$for_sql_in_tags = implode( ',', $_in_tags );
					$for_sql_tags_ex = " AND tt.term_taxonomy_id NOT IN ($for_sql_in_tags)";
				}
			}

			$sql_str = "SELECT tt.term_taxonomy_id, t.name, COUNT(p.ID) as total_posts
			FROM wp_terms t
				INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
				INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
				INNER JOIN wp_posts p ON (tr.object_id = p.ID)
			WHERE tt.taxonomy = 'post_tag' AND p.post_type = 'osetin_recipe' AND p.post_status = 'publish' $for_sql_in_posts $for_sql_tags_ex
			GROUP BY tt.term_taxonomy_id
			ORDER BY total_posts DESC";

			return $wpdb->get_results( $sql_str, ARRAY_A );
		} else {
			if ( ! empty( $_in_tags ) ) {
				$_in_tags        = array_map( function ( $v ) {
					return "'" . esc_sql( $v ) . "'";
				}, $_in_tags );
				$for_sql_in_tags = implode( ',', $_in_tags );
				$for_sql_tags    = " AND tt.term_taxonomy_id IN ($for_sql_in_tags)";

				$sql_str = "SELECT tt.term_taxonomy_id, t.name, 0 as total_posts
					FROM wp_terms t
						INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
						INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
						INNER JOIN wp_posts p ON (tr.object_id = p.ID)
					WHERE tt.taxonomy = 'post_tag' AND p.post_type = 'osetin_recipe' AND p.post_status = 'publish' $for_sql_tags 
					GROUP BY tt.term_taxonomy_id
					ORDER BY total_posts DESC";

				return $wpdb->get_results( $sql_str, ARRAY_A );
			} else {
				return array();
			}
		}

	}

	public static function get_posts_cats_tags( $_in_cats, $_in_tags ) {
		global $wpdb;
		$for_sql_in_cats = '';
		$for_sql_in_tags = '';
		$num_tags        = 0;

		if ( ! empty( $_in_cats ) ) {
			$_in_cats        = array_map( function ( $v ) {
				return "'" . esc_sql( $v ) . "'";
			}, $_in_cats );
			$for_sql_in_cats = implode( ',', $_in_cats );
			$for_sql_in_cats = "AND ( tt.term_taxonomy_id IN ($for_sql_in_cats)  OR tt.parent IN ($for_sql_in_cats) ) ";
		}

		if ( ! empty( $_in_tags ) ) {
			$num_tags        = count( $_in_tags );
			$_in_tags        = array_map( function ( $v ) {
				return "'" . esc_sql( $v ) . "'";
			}, $_in_tags );
			$for_sql_in_tags = implode( ',', $_in_tags );
			$for_sql_in_tags = "AND tt.term_taxonomy_id IN ($for_sql_in_tags)";
		}

		$sql_str = '';
		if ( ! empty( $_in_cats ) && ! empty( $_in_tags ) ) {
			$sql_str = "SELECT p.ID, count(tr.term_taxonomy_id) as total_tags
			FROM wp_terms t
				INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
				INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
				INNER JOIN wp_posts p ON (tr.object_id = p.ID)
			WHERE tt.taxonomy = 'post_tag' 
					AND p.post_type = 'osetin_recipe' 
					AND p.post_status = 'publish' 
					AND p.ID IN (SELECT p.id
									FROM wp_terms t
									INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
									INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
									INNER JOIN wp_posts p ON (tr.object_id = p.ID)
									WHERE p.post_type = 'osetin_recipe' AND p.post_status = 'publish' AND tt.taxonomy = 'category' $for_sql_in_cats)
					$for_sql_in_tags
			GROUP BY p.id 
			HAVING total_tags = $num_tags";
		} elseif ( ! empty( $_in_cats ) && empty( $_in_tags ) ) {
			$sql_str = "SELECT p.id
						FROM wp_terms t
						INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
						INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
						INNER JOIN wp_posts p ON (tr.object_id = p.ID)
						WHERE p.post_type = 'osetin_recipe' AND p.post_status = 'publish' AND tt.taxonomy = 'category' $for_sql_in_cats";
		} elseif ( empty( $_in_cats ) && ! empty( $_in_tags ) ) {
			$sql_str = "SELECT p.id, count(tr.term_taxonomy_id) as total_tags 
						FROM wp_terms t
						INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
						INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
						INNER JOIN wp_posts p ON (tr.object_id = p.ID)
						WHERE p.post_type = 'osetin_recipe' 
							AND p.post_status = 'publish' 
							AND tt.taxonomy = 'post_tag'
							$for_sql_in_tags
						GROUP BY p.id 
						HAVING total_tags = $num_tags";
		}
		$result_q = $wpdb->get_results( $sql_str, ARRAY_N );

		return ( new RecipeCategory )->get_array_result( $result_q, 'int' );
	}
}

RecipeCategory::get_instance();

//TODO: move to functions.php

/* Storing views of different time periods as meta keys */
add_action( 'wpp_post_update_views', 'custom_wpp_update_postviews' );

function custom_wpp_update_postviews( $postid ) {
	// Accuracy:
	//   10  = 1 in 10 visits will update view count. (Recommended for high traffic sites.)
	//   30 = 30% of visits. (Medium traffic websites)
	//   100 = Every visit. Creates many db write operations every request.
	$accuracy = 30;
	if ( function_exists( 'wpp_get_views' ) && ( mt_rand( 0, 100 ) < $accuracy ) ) {
		// Remove or comment out lines that you won't be using!!
		update_post_meta( $postid, 'wpp_views_total', wpp_get_views( $postid ) );
		update_post_meta( $postid, 'wpp_views_daily', wpp_get_views( $postid, 'daily' ) );
		update_post_meta( $postid, 'wpp_views_weekly', wpp_get_views( $postid, 'weekly' ) );
		update_post_meta( $postid, 'wpp_views_monthly', wpp_get_views( $postid, 'monthly' ) );
	}
}