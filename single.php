<?php
/**
 * The template for displaying all single posts.
 *
 * @package Neptune
 */

get_header(); ?>

  <?php while ( have_posts() ) : the_post(); ?>
  <?php osetin_show_featured_recipes_slider(); ?>
  <div class="os-container top-bar-w">
    <div class="top-bar">
		<ul>
			<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
		</ul>
	</div>
  </div>
  <div class="os-container">
    <div class="page-w bordered <?php if ( is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
		<div class="page-content">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php setPostViews(get_the_ID()); ?>
				<?php if(osetin_get_field('hide_title', get_the_ID()) != true){ ?>
				<h1><?php echo osetin_get_the_title(get_the_ID()); ?></h1>
				<?php $sub_title = osetin_get_field('sub_title');
					if ( ! empty( $sub_title ) ) { ?>
					<h2 class="page-content-sub-title"><?php echo esc_html($sub_title); ?></h2>
				<?php } ?>
				<?php } ?>
				<div class="single-post-meta">
					<?php $post_date = get_the_date( 'F jS Y' ); ?>
					<span class='author-post'><?php the_author();?></span> <span class='post-date'><?php echo $post_date;?></span> <span class='post-cats'><?php $categories = get_the_category();
						if ( ! empty( $categories ) ) {
							echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
						}
						?>
					</span>
				</div>
				<div class="single-featured-image-w">
					<?php 
					if(('video' == get_post_format()) && osetin_get_field('video_shortcode')){
					echo do_shortcode(osetin_get_field('video_shortcode')); 
					}elseif(('gallery' == get_post_format()) && osetin_get_field('gallery_images', false, false, true)){
					$gallery_images = osetin_get_field('gallery_images', false, false, true);
					$html = '';
					$html.= '<div class="single-main-media">';
					foreach( $gallery_images as $key => $image ){
					$active_class = ($key == 0) ? 'active' : '';
					$html.= '<div class="single-main-media-image-w has-gallery osetin-lightbox-trigger fader-activator '.$active_class.'" id="singleMainMedia'.$key.'" 
					data-lightbox-caption="'.$image['caption'].'" 
					data-lightbox-img-src="'.$image['sizes']['osetin-full-width'].'" 
					data-lightbox-thumb-src="'.$image['sizes']['osetin-medium-square-thumbnail'].'">
					<span class="image-fader lighter"><span class="hover-icon-w"><i class="os-icon os-icon-plus"></i></span></span>
					<img src="'.$image['sizes']['osetin-full-width'].'"/></div>';
					}
					$html.= '<div class="single-post-gallery-images"><div class="single-post-gallery-images-i">';
					foreach( $gallery_images as $key => $image ){
					$active_class = ($key == 0) ? 'active' : '';
					$html.= '<div class="gallery-image-source '.$active_class.' fader-activator" data-image-id="singleMainMedia'.$key.'">
					<span class="image-fader"><span class="hover-icon-w"><i class="os-icon os-icon-plus"></i></span></span>
					<img src="'.$image['sizes']['osetin-medium-square-thumbnail'].'"/></div>';
					}
					$html.= '</div></div></div>';
					echo $html;
					}else{
					the_post_thumbnail('osetin-full-width');
					} ?>
				</div>
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => '<div class="content-link-pages">', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
				<div class="single-post-tags">
					<i class="os-icon os-icon-tags"></i>
					<?php the_tags('<ul class="post-tags"><li>','</li><li>','</li></ul>'); ?>
				</div>
				<div class="single-post-about-author">
					<div class="author-avatar-w">
						<?php echo get_avatar(get_the_author_meta( 'ID' )); ?>
					</div>
					<div class="author-details">
						<h3 class="author-name"><?php the_author_meta( 'display_name' ); ?></h3>
						<?php if ( get_the_author_meta('description') ) : ?>
						<p><?php the_author_meta('description'); ?></p>
						<?php endif; ?>
						<div class="author-social-links">
							<?php if ( get_the_author_meta('google_profile') ) : ?>
							  <a href="<?php esc_url( the_author_meta('google_profile')); ?>" class="profile-url"><i class="os-icon os-icon-social-googleplus"></i></a>
							<?php endif; ?>
							<?php if ( get_the_author_meta('twitter_profile') ) : ?>
							  <a href="<?php esc_url( the_author_meta('twitter_profile')); ?>" class="profile-url"><i class="os-icon os-icon-social-twitter"></i></a>
							<?php endif; ?>
							<?php if ( get_the_author_meta('facebook_profile') ) : ?>
							  <a href="<?php esc_url( the_author_meta('facebook_profile')); ?>" class="profile-url"><i class="os-icon os-icon-social-facebook"></i></a>
							<?php endif; ?>
							<?php if ( get_the_author_meta('linkedin_profile') ) : ?>
							  <a href="<?php esc_url( the_author_meta('linkedin_profile')); ?>" class="profile-url"><i class="os-icon os-icon-social-linkedin"></i></a>
							<?php endif; ?>
							<?php if ( get_the_author_meta('instagram_profile') ) : ?>
							  <a href="<?php esc_url( the_author_meta('instagram_profile')); ?>" class="profile-url"><i class="os-icon os-icon-social-instagram"></i></a>
							<?php endif; ?>
							<?php if ( get_the_author_meta('flickr_profile') ) : ?>
							  <a href="<?php esc_url( the_author_meta('flickr_profile')); ?>" class="profile-url"><i class="os-icon os-icon-social-flickr"></i></a>
							<?php endif; ?>
							<?php if ( get_the_author_meta('rss_url') ) : ?>
							  <a href="<?php esc_url( the_author_meta('rss_url')); ?>" class="profile-url"><i class="os-icon os-icon-social-rss"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || get_comments_number() ) :
				comments_template();
				endif;
				?>
			</article>
		</div>
		
        <div class="page-sidebar">
			<div class="searchblogs">
				<?php echo do_shortcode('[wd_asp id=5]');?>
			</div>
			<ul class="recent-post">
				<h2>recente stories</h2>
				<?php
				$recent_posts = wp_get_recent_posts(array(
					'numberposts' => 3,
					'cat' => -56,
					'post_status' => 'publish'
				));
				foreach($recent_posts as $post) : ?>
					<li>
						<div class="recent-post-img">
							<?php echo get_the_post_thumbnail($post['ID'], 'full'); ?>
						</div>
						<div class="recent-post-des">
							<a href="<?php echo get_permalink($post['ID']) ?>"><p class="slider-caption-class"><?php echo $post['post_title'] ?></p></a>
							<span class='post-date'><?php  echo date( 'F jS Y', strtotime( $post['post_date'] ) );?></span>
							<a id="recent-post-link" href="<?php echo get_permalink($post['ID']) ?>">Lees meer</a>
						</div>
					</li>
				<?php endforeach; wp_reset_query(); ?>
			</ul>
			<div class="stories-ads">
				<img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/09/Tired-of-Ads.jpg" alt="stories-ads" />
			</div>
			<ul class="popular-post">
				<h2>Populairste stories</h2>
			   <?php
				  query_posts('meta_key=post_views_count&posts_per_page=3&cat=-56&orderby=meta_value_num&
				  order=DESC');
				  $post_dates = get_the_date( 'F jS Y' );
				  if (have_posts()) : while (have_posts()) : the_post();
			   ?>
				<li>
					<div class="recent-post-img">
						<?php	
							$featured_img_url = get_the_post_thumbnail_url('full'); 
							echo '<a href="'.$featured_img_url.'" rel="lightbox">'; 
							the_post_thumbnail('thumbnail');
							echo '</a>';
						?>
					</div>
					<div class="recent-post-des">
						<a href="<?php the_permalink(); ?>"><p class="slider-caption-class"><?php the_title();?></p></a>
						<span class='post-date'><?php  echo $post_dates;?></span>
						<a id="recent-post-link" href="<?php the_permalink(); ?>">Lees meer</a>
					</div>
				</li>
			   <?php
			   endwhile; endif;
			   wp_reset_query();
			   ?>
			</ul>
		</div>
    </div>
</div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>