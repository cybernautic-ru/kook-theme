<?php
/**
 * Template Name: Converter
 *
 * @package Neptune
 */

get_header(); ?>

<?php if(is_page('converter-tool')){
wp_deregister_script('jquery'); 
wp_register_script('jquery', ("https://code.jquery.com/jquery-3.3.1.min.js"), false);
wp_enqueue_script('jquery');
echo '<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>';
echo '<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>';    
echo '<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,700,700i&display=swap" rel="stylesheet">';
echo '<script>
$(document).ready(function() {
  $("#ingr-list").select2({
    placeholder: "Selecteer ingrediënt",  
    });
});
</script>'; 
}
?>

  <?php while ( have_posts() ) : the_post(); ?>


  <div class="os-container top-bar-w">
    <div class="top-bar <?php if(!osetin_is_imaged_header(get_the_ID())) echo 'bordered'; ?>">
		<ul>
			<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			<li class="page-top-title"><h2 id="pagetitles"><?php echo osetin_get_the_title(get_the_ID()); ?></h2></li>
		</ul>
    </div>
  </div>

  <?php  
  if(osetin_is_imaged_header(get_the_ID())){
    if(osetin_is_bbpress()){
      $page_bg_image_url = get_template_directory_uri().'/assets/img/patterns/flowers_light.jpg';
    }else{
      $page_bg_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
    } ?>
	<div class="os-container top-bar-w">
		<div class="top-bar <?php if(!has_post_thumbnail()) echo 'bordered'; ?>">
			<ul>
				<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			</ul>
		</div>
	</div>
    <?php
  }
  ?>

  <div class="os-container">
    <div class="page-w <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
      <div class="page-content">
		<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php /*
          if(osetin_is_regular_header(get_the_ID())){
            echo '<h1 class="page-title">'.osetin_get_the_title(get_the_ID()).'</h1>';
          }
          */?>
          <?php $sub_title = osetin_get_field('sub_title');
          if ( ! empty( $sub_title ) ) { ?>
            <h2 class="page-content-sub-title"><?php echo esc_html($sub_title); ?></h2>
          <?php } ?>
          <?php //the_content(); ?>
          
          <div class="converter-container">
            <div class="left-side">  
              <div class="left-side__text">
                <p> Werk je met eetlepels maar worden de ingrediënten weergegeven in grammen of vice versa? Geen probleem, reken hier de gewenste ingrediënten om naar hetgeen dat jouw voorkeur geniet. Of dat nu eetlepels, theelepels, grammen, cups of oz zijn.</p>
                <p> Om te beginnen, dien je eerst een ingrediënt te selecteren. Vul de hoeveelheid in die je wil omrekenen. Zodra je dit hebt gedaan, zal de converter tool de berekening voor jou uitvoeren. Staat je ingrediënt er niet tussen? Stuur een bericht naar info@kookmutsjes.com. We zullen het z.s.m. toevoegen.</p>
                <p> Theelepel = 5 ml <br>
                    Eetlepel = 15 ml <br>
                    Cup = 240 ml</p>
              </div>
            </div>  
            
            <?php echo do_shortcode('[ingradient-calculator]');?>
              <?php
               // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || get_comments_number() ) :
              comments_template();
              endif;
            ?>
          </div>        
        </article>
      </div>
      <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) { ?>

        <div class="page-sidebar">
        
          <?php dynamic_sidebar( 'sidebar-index' ); ?>

        </div>
          
      <?php } ?>


    </div>
  </div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>