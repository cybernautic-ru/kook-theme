<?php
/**
 * The template for displaying archive pages
 *
 */

get_header(); 

$layout_type_for_index = osetin_get_settings_field('layout_type_for_index');
$archive_title = get_the_archive_title();
$archive_description = get_the_archive_description();
?>
	<div class="os-container top-bar-w">
		<div class="top-bar bordered">
			<ul>
				<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			</ul>
		</div>
	</div>
</div>
          
<div class="os-container">
  <?php global $wp_query; ?>
  <?php echo build_index_posts($layout_type_for_index, 'sidebar-index', $wp_query, false, array('title' => $archive_title, 'description' => $archive_description)); ?>
	<?php 
		previous_posts_link();
		next_posts_link();
	?>
</div>

<?php get_footer(); ?>