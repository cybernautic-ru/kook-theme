<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=no" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title><?php wp_title();?></title>
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/menu/css/vendors.css" />
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/menu/css/styles.css" />
<?php wp_head();?>
</head>

<body>
  <script>
    document.querySelector('body').classList.add('js');
  </script>

  <div class="preloader">
    Birthday Menu
  </div>