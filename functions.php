<?php	
	
	//Style and Script Enqueue within theme
	
	function neptune_child_enqueue_styles() 
	{
		wp_enqueue_media();
		wp_enqueue_style( 'neptune-child-style', get_stylesheet_directory_uri() . '/style.css');
		wp_enqueue_style( 'neptune-child-responsive', get_stylesheet_directory_uri() . '/css/responsive.css');
		wp_enqueue_style( 'neptune-child-styles', get_stylesheet_directory_uri() . '/bootstrap.css');
		wp_enqueue_style( 'neptune-child-styless', get_stylesheet_directory_uri() . '/slide/lightslider.css');
		wp_enqueue_script('kookmutsjes_slider', get_stylesheet_directory_uri() . '/slide/lightslider.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('kookmutsjes_footer', get_stylesheet_directory_uri() . '/public/js/kookmutsjes_footer.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('kookmutsjes_bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.js', array('jquery'), '1.0.0', true);
		wp_localize_script( 'kookmutsjes_footer', 'kookmutsjes_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

        if ( is_singular( 'osetin_recipe' ) ) {
			wp_enqueue_style('km_bakvorm_module_style', get_stylesheet_directory_uri() . '/public/css/bakvorm_module.css', array(), '1.0.0');
            wp_enqueue_script('km_bakvorm_module', get_stylesheet_directory_uri() . '/public/js/bakvorm_module.js', array('jquery'), '1.0.0', true);
        }
        if ( is_page_template( 'recipes-board.php' ) ) {
            wp_enqueue_style( 'recipes_board', get_stylesheet_directory_uri() . '/public/css/recipes-board.css', array(), '1.0.0' );
	        wp_enqueue_script('recipes_board_js', get_stylesheet_directory_uri() . '/public/js/recipes_board.js', array('jquery'), '1.0.0', true);
        }
        
        if ( is_page_template( 'convert-tools.php' ) && is_page('converter-tool') ) {
            wp_enqueue_style( 'cv_tools', get_stylesheet_directory_uri() . '/css/conv.css', array(), '3.4.5' );
        }
		
		if ( is_page_template( 'weekly-menu.php' ) && is_page('week-menu-2') ) {
            wp_enqueue_style( 'menu_w', get_stylesheet_directory_uri() . '/css/wmenu.css', array(), '3.5.5' );
        }
	}
	add_action( 'wp_enqueue_scripts', 'neptune_child_enqueue_styles' );
	
	function z_scripts() 
	{
		wp_enqueue_media();
		wp_enqueue_style( 'select2-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css' );
		wp_enqueue_script( 'select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js' );
	}
	
	add_action( 'wp_enqueue_scripts', 'z_scripts' );
	//Ads code
	$ads_code = array(
	'homepage_mobile' => '<div id="adf-rectangle2" style="width:100%; margin:15px auto; text-align:center;"></div>',
	'recepten_mobile_tablet' => '<div id="adf-rectangle2" style="width:100%; margin:15px auto; text-align:center;"></div>',
	);
	
	//Theme default files
    require_once (get_stylesheet_directory() . '/inc/osetin-magic.php');
	require_once (get_stylesheet_directory() . '/inc/bakvorm-module-popup-template.php');
	require_once (get_stylesheet_directory() . '/inc/class-recipe-category.php');
	require_once (get_stylesheet_directory() . '/inc/funtions-breadcrumbs.php');




	/**
	 * REST API controllers
	 */
	require get_stylesheet_directory() . '/rest/class-recipe-index-post-rest.php';

	//Widget Registration
	if ( function_exists('register_sidebar') ) 
	{
		register_sidebar(array(
		'name' => 'Profile Latest Post',
		'id'   => 'profile-latest-post',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
		register_sidebar(array(
		'name' => 'Profile Latest Recipe',
		'id'   => 'profile-latest-recipe',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
		register_sidebar(array(
		'name' => 'Home Post',
		'id'   => 'home-post',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
		register_sidebar(array(
		'name' => 'Home Video',
		'id'   => 'home-video',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
		register_sidebar(array(
		'name' => 'Post Blog',
		'id'   => 'post-blog',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
		register_sidebar(array(
		'name' => 'Video Blog',
		'id'   => 'video-blog',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
		register_sidebar(array(
		'name' => 'Popular Post',
		'id'   => 'popular-post',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
		));
	}
	
	// Social Login Redirection
	
	function oa_social_login_set_returning_user_redirect_url ($url, $user_data)
	{
		return  get_site_url(null, '/my-profile/'); 
	}
	add_filter('oa_social_login_filter_login_redirect_url', 'oa_social_login_set_returning_user_redirect_url', 10, 2);
	
	//Verify the email address of a new user
	function oa_social_login_restrict_new_user_email ($user_email)
	{
	  //Only users with social network accounts having an email address ending in @gmail.com may register
	  if ( ! preg_match ('/@gmail\.com$/i', trim ($user_email)))
	  {
		trigger_error ('This email address may not be used to register', E_USER_ERROR);
	  }
	  return $user_email;
	}
	
	//This filter is applied to the email addresses of new users
	add_filter('oa_social_login_filter_new_user_email', 'oa_social_login_restrict_new_user_email');
	
	//Use the email address as user_login
	function oa_social_login_set_email_as_user_login ($user_fields)
	{
		if ( ! empty ($user_fields['user_email']))
		{
			if ( ! username_exists ($user_fields['user_email']))
		{
			$user_fields['user_login'] = $user_fields['user_email'];
		}
	  }
	  return $user_fields;
	}
	 
	//This filter is applied to new users
	add_filter('oa_social_login_filter_new_user_fields', 'oa_social_login_set_email_as_user_login');
	
	//Set custom roles for new users
	function oa_social_login_set_new_user_role ($user_role)
	{
		//This is an example for a custom setting with one role
		$user_role = 'subscriber';
		//The new user will be created with this role
		return $user_role;
	}
	 
	//This filter is applied to the roles of new users
	add_filter('oa_social_login_filter_new_user_role', 'oa_social_login_set_new_user_role');
	
	//Admin Week Menu Custom Post
	function admin_week_menu() 
	{
		$labels = array(
		'name'                => _x( 'Week Menu', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Week Menu', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Week Menu', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Week Menu', 'twentythirteen' ),
		'all_items'           => __( 'All Week Menu', 'twentythirteen' ),
		'view_item'           => __( 'View Week Menu', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Week Menu', 'twentythirteen' ),
		'add_new'             => __( 'Add New', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Week Menu', 'twentythirteen' ),
		'update_item'         => __( 'Update Week Menu', 'twentythirteen' ),
		'search_items'        => __( 'Search Week Menu', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		$args = array(
		'label'               => __( 'weekmenu', 'twentythirteen' ),
		'description'         => __( 'Week Menu news and reviews', 'twentythirteen' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'custom-fields'),
		'taxonomies'          => array( 'genres' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
		register_post_type( 'weekmenu', $args );
	}
	add_action( 'init', 'admin_week_menu', 0 );
	
	
	//Udgrade week menu
	function adminup_week_menu() 
	{
		$labels = array(
		'name'                => _x( 'Week Menu', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Week Menu', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Week Menu (N)', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Week Menu', 'twentythirteen' ),
		'all_items'           => __( 'All Week Menu', 'twentythirteen' ),
		'view_item'           => __( 'View Week Menu', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Week Menu', 'twentythirteen' ),
		'add_new'             => __( 'Add New', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Week Menu', 'twentythirteen' ),
		'update_item'         => __( 'Update Week Menu', 'twentythirteen' ),
		'search_items'        => __( 'Search Week Menu', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		$args = array(
		'label'               => __( 'weekmenu', 'twentythirteen' ),
		'description'         => __( 'Week Menu news and reviews', 'twentythirteen' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'custom-fields'),
		'taxonomies'          => array( 'genres' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
		register_post_type( 'weekmenu-upd', $args );
	}
	add_action( 'init', 'adminup_week_menu', 0 );
	
	
	//Home Slider Custom Post
	function slider_recipes() 
	{
		$labels = array(
		'name'                => _x( 'Slider Recipe', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Slider Recipe', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Slider Recipe', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Slider Recipe', 'twentythirteen' ),
		'all_items'           => __( 'All Slider Recipe', 'twentythirteen' ),
		'view_item'           => __( 'View Slider Recipe', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Slider Recipe', 'twentythirteen' ),
		'add_new'             => __( 'Add New', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Slider Recipe', 'twentythirteen' ),
		'update_item'         => __( 'Update Slider Recipe', 'twentythirteen' ),
		'search_items'        => __( 'Search Slider Recipe', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		$args = array(
		'label'               => __( 'slider-recipe', 'twentythirteen' ),
		'description'         => __( 'Slider Recipe news and reviews', 'twentythirteen' ),
		'labels'              => $labels,
		'supports'            => array( 'title'),
		'taxonomies'          => array( 'sliderrecipe' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
		register_post_type( 'sliderecepi', $args );
	}
	add_action( 'init', 'slider_recipes', 0 );
	
	//Home Slider Custom Post
	function home_weektips() 
	{
		$labels = array(
		'name'                => _x( 'Verwenmomentjes', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Verwenmomentjes', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Verwenmomentjes', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Verwenmomentjes', 'twentythirteen' ),
		'all_items'           => __( 'All Verwenmomentjes', 'twentythirteen' ),
		'view_item'           => __( 'View Verwenmomentjes', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Verwenmomentjes', 'twentythirteen' ),
		'add_new'             => __( 'Add New', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Verwenmomentjes', 'twentythirteen' ),
		'update_item'         => __( 'Update Verwenmomentjes', 'twentythirteen' ),
		'search_items'        => __( 'Search Verwenmomentjes', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		$args = array(
		'label'               => __( 'Verwen-momentjes', 'twentythirteen' ),
		'description'         => __( 'Verwenmomentjes news and reviews', 'twentythirteen' ),
		'labels'              => $labels,
		'supports'            => array( 'title'),
		'taxonomies'          => array( 'verwenmomentjes' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
		register_post_type( 'weektips', $args );
	}
	add_action( 'init', 'home_weektips', 0 );
	
	//Admin Week menu post shortcode(desktop)
	function tips_weekmenu() 
	{?>
		<div class="weektips">
			<ul>
				<?php
					$args = array(
					'post_type'   => 'weektips',
					'post_status' => 'publish',
					'posts_per_page' => 4,
					);
					$weekmenu = new WP_Query( $args );
					if( $weekmenu->have_posts() ) :
				?>
				<?php
					while( $weekmenu->have_posts() ) :
					$weekmenu->the_post();
				?>
				<li>
					<?php if( get_field('add_verwenmomentjes') ): ?>
						<a href="<?php the_field('add_link_for_verwenmomentjes'); ?>"><img src="<?php the_field('add_verwenmomentjes'); ?>" /></a>
					<?php endif; ?>
					<h2><?php the_field('add_title_for_verwenmomentjes'); ?></h2>
				</li>
				<?php
				endwhile;
				wp_reset_postdata();
				?>	
				<?php
				else :
				esc_html_e( 'Geen Weekmenu nu!', 'text-domain' );
				endif;
				?>
			</ul>
		</div>
		<?php
	}
	add_shortcode( 'tipweek', 'tips_weekmenu' );
	
	//Admin Week menu post shortcode(desktop)
	function desktop_weekmenu() 
	{?>
		<div class="masonry gutterless desktopweek">
			<?php
				$args = array(
				'post_type'   => 'weekmenu',
				'post_status' => 'publish',
				);
				$weekmenu = new WP_Query( $args );
				if( $weekmenu->have_posts() ) :
			?>
			<?php
				while( $weekmenu->have_posts() ) :
				$weekmenu->the_post();
			?>
			<div class="row">
				<div class="col-md-4 first-part">
					<div class="brick w-b">
						<?php if( get_field('friday_menu') ): ?>
						<a href="<?php the_field('friday_menu_url'); ?>"><img src="<?php the_field('friday_menu'); ?>" /></a>
						<?php endif; ?>
					</div>
					<div class="brick w-b b-top">
						<?php if( get_field('saturday_menu_image') ): ?>
						<a href="<?php the_field('saturday_menu_url'); ?>"><img src="<?php the_field('saturday_menu_image'); ?>" /></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-4 second-part">
					<div class="brick w-cs">
						<?php if( get_field('sunday_menu_image') ): ?>
						<a href="<?php the_field('sunday_menu_url'); ?>"><img src="<?php the_field('sunday_menu_image'); ?>" /></a>
						<?php endif; ?>
					</div>
					<div class="brick w-c">
						<?php if( get_field('monday_menu_image') ): ?>
						<a href="<?php the_field('monday_menu_url'); ?>"><img src="<?php the_field('monday_menu_image'); ?>" /></a>
						<?php endif; ?>
					</div>
					<div class="brick w-c border-none">
						<?php if( get_field('tuesday_menu_image') ): ?>
						<a href="<?php the_field('tuesday_menu_url'); ?>"><img src="<?php the_field('tuesday_menu_image'); ?>" /></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-4 third-part">
					<div class="brick blast w-b">
						<?php if( get_field('wednesday_menu_image') ): ?>
						<a href="<?php the_field('wednesday_menu_url'); ?>"><img src="<?php the_field('wednesday_menu_image'); ?>" /></a>
						<?php endif; ?>
					</div>
					<div class="brick blast w-b b-top">
						<?php if( get_field('thursday_menu_image') ): ?>
						<a href="<?php the_field('thursday_menu_url'); ?>"><img src="<?php the_field('thursday_menu_image'); ?>" /></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php
			endwhile;
			wp_reset_postdata();
			?>	
			<?php
			else :
			esc_html_e( 'Geen Weekmenu nu!', 'text-domain' );
			endif;
			?>
		</div>
		<?php
	}
	add_shortcode( 'week', 'desktop_weekmenu' );
	
	//Admin Week menu post shortcode(mobile)
	function mobile_weekmenu() 
	{?>
		<div class="masonry gutterless mobileweek">
			<?php
				$args = array(
				'post_type'   => 'weekmenu',
				'post_status' => 'publish',
				);
				$weekmenu = new WP_Query( $args );
				if( $weekmenu->have_posts() ) :
			?>
			<?php
				while( $weekmenu->have_posts() ) :
				$weekmenu->the_post();
			?>
			<div class="week-part1">
				<div class="brick b1">
					<?php if( get_field('friday_menu') ): ?>
					<a href="<?php the_field('friday_menu_url'); ?>"><img src="<?php the_field('friday_menu'); ?>" /></a>
					<?php endif; ?>
				</div>
				<div class="brick b2">
					<?php if( get_field('saturday_menu_image') ): ?>
					<a href="<?php the_field('saturday_menu_url'); ?>"><img class="borderimg" src="<?php the_field('saturday_menu_image'); ?>" /></a>
					<?php endif; ?>
				</div>
			</div>
			<div class="week-part2">
				<div class="brick b3">
					<?php if( get_field('sunday_menu_image') ): ?>
					<a href="<?php the_field('sunday_menu_url'); ?>"><img style="height:215px;object-fit: cover;" src="<?php the_field('sunday_menu_image'); ?>" /></a>
					<?php endif; ?>
				</div>
				<div class="brick b4">
					<?php if( get_field('monday_menu_image') ): ?>
					<a href="<?php the_field('monday_menu_url'); ?>"><img style="height:212px;object-fit: cover;" src="<?php the_field('monday_menu_image'); ?>" /></a>
					<?php endif; ?>
				</div>
				<div class="brick b5">
					<?php if( get_field('tuesday_menu_image') ): ?>
					<a href="<?php the_field('tuesday_menu_url'); ?>"><img class="borderimg" style="height:212px;object-fit: cover;" src="<?php the_field('tuesday_menu_image'); ?>" /></a>
					<?php endif; ?>
				</div>
			</div>
			<div class="week-part3">
				<div class="brick b6">
					<?php if( get_field('wednesday_menu_image') ): ?>
					<a href="<?php the_field('wednesday_menu_url'); ?>"><img class="borderimgs" src="<?php the_field('wednesday_menu_image'); ?>" /></a>
					<?php endif; ?>
				</div>
				<div class="brick b7">
					<?php if( get_field('thursday_menu_image') ): ?>
					<a href="<?php the_field('thursday_menu_url'); ?>"><img class="borderimgss" src="<?php the_field('thursday_menu_image'); ?>" /></a>
					<?php endif; ?>
				</div>
			</div>
			<?php
			endwhile;
			wp_reset_postdata();
			?>	
			<?php
			else :
			esc_html_e( 'Geen Weekmenu nu!', 'text-domain' );
			endif;
			?>
		</div>
	<?php
	}
	add_shortcode( 'week-mobile', 'mobile_weekmenu' );

	
	// Post View Count
	function setPostViews($postID) 
	{
		$countKey = 'post_views_count';
		$count = get_post_meta($postID, $countKey, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $countKey);
			add_post_meta($postID, $countKey, '0');
		}
		else
		{
			$count++;
			update_post_meta($postID, $countKey, $count);
		}
	}
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	
	//Video Post Home page
	function home_post_video() 
	{?>
		<div class="row">
			<?php 
			// the query
			$args = array(
				'post_type'      => 'post',
				'cat'            => '56',
				'posts_per_page' => 1
			);
			$query = new WP_Query( $args );?>
			<?php if ( $query->have_posts() ) : ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<div class="col-md-6 home-vd-contents">
					<h3><?php the_title(); ?></h3>
					<p><?php the_excerpt();?></p>
					<a class="video-readmore" href="<?php echo get_permalink();?>">Lees meer</a>
				</div>
				<div class="col-md-6 home-vd-player">
					<?php 
						$homevideo = get_field('video_shortcode');
						if( $homevideo ) 
						{
							echo do_shortcode( $homevideo);
						}
					?>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
			<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	<?php
	}
	add_shortcode( 'home-video-post', 'home_post_video' );
	
	//User social Media
	function user_social_media() {
		$labels = array(
			'name'                => _x( 'User Social', 'Post Type General Name', 'twentythirteen' ),
			'singular_name'       => _x( 'User Social', 'Post Type Singular Name', 'twentythirteen' ),
			'menu_name'           => __( 'User Social', 'twentythirteen' ),
			'parent_item_colon'   => __( 'Parent User Social', 'twentythirteen' ),
			'all_items'           => __( 'All User Social', 'twentythirteen' ),
			'view_item'           => __( 'View User Social', 'twentythirteen' ),
			'add_new_item'        => __( 'Add New User Social', 'twentythirteen' ),
			'add_new'             => __( 'Add New', 'twentythirteen' ),
			'edit_item'           => __( 'Edit User Social', 'twentythirteen' ),
			'update_item'         => __( 'Update User Social', 'twentythirteen' ),
			'search_items'        => __( 'Search User Social', 'twentythirteen' ),
			'not_found'           => __( 'Not Found', 'twentythirteen' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		$args = array(
			'label'               => __( 'social', 'twentythirteen' ),
			'description'         => __( 'User Social news and reviews', 'twentythirteen' ),
			'labels'              => $labels,
			'supports'            => array( 'title',),
			'taxonomies'          => array( 'genres' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( 'social', $args );
	}
	add_action( 'init', 'user_social_media', 0 );
	
	// Images Uplaod by the Users
	function upload_user_images()
	{
		ob_clean();
		$post_id = $_POST['post_id'];
		$post_name = $_POST['rec_name'];
		$att_id = $newId =array();
		$thum = 0;
		$user_add_image = get_post_meta($post_id,'add_image',true);
		foreach($user_add_image as $key=>$thumbnail_id)
		{
			$att_id[] =  $thumbnail_id;          
		}
		foreach ($_FILES as $file) 
		{
			$imgID = km_file_upload($file);
			$newId[] = $imgID;
			$att_id[] =  $imgID;
			$thum = ($thum) ? $thum : $imgID;
		}
		update_field( 'field_5d8cc2b90d30c', $att_id , $post_id ); 
		$pId = wp_insert_post(array(
		  'post_type'    => 'recipe_gallery',
		  'post_title'    => wp_strip_all_tags($post_name ),
		  'post_status'   => 'publish'
		));
		set_post_thumbnail($pId, $thum);  
		update_field( 'field_5d8cc2b90d30c', $newId , $pId ); 
		update_field( 'parent_post_id', $post_id , $pId ); 
		wp_die();
	}
	add_action( 'wp_ajax_upload_user_images', 'upload_user_images' );
	add_action( 'wp_ajax_nopriv_upload_user_images', 'upload_user_images' );

	function km_file_upload($file)
	{
		$moved_file = wp_upload_bits($file["name"], null, file_get_contents($file["tmp_name"]));
		$filename = $moved_file['file'];
		$attachment = array
		(
			'post_mime_type' => $moved_file['type'],
			'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
			'post_content' => '',
			'post_status' => 'inherit',
			'guid' => $moved_file['url']
		);
		$attachment_id = wp_insert_attachment( $attachment, $moved_file['url'] );
		$attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
		wp_update_attachment_metadata( $attachment_id, $attachment_data );
		if( 0 < intval( $attachment_id ) ) 
		{
			return $attachment_id;     
		}
		else
		{
			return 0;
		}
	}

	// Register Taxonomy for saved recipes as favorite
	add_action( 'init', 'km_create_tax' );
	function km_create_tax() 
	{
		register_taxonomy( 
		'recipe_favorites',
		'osetin_recipe',
		array(  
		'hierarchical' => true,  
		'labels' => array(
		'name'              => _x( 'Favorite', 'taxonomy general name', 'twentythirteen' ),
		'singular_name'     => _x( 'Favorite', 'taxonomy singular name', 'twentythirteen' ),
		'search_items'      => __( 'Search Favorite', 'twentythirteen' ),
		'all_items'         => __( 'All Favorite', 'twentythirteen' ),
		'edit_item'         => __( 'Edit Favorite', 'twentythirteen' ),
		'update_item'       => __( 'Update Favorite', 'twentythirteen' ),
		'add_new_item'      => __( 'Add New Favorite', 'twentythirteen' ),
		'new_item_name'     => __( 'New Favorite', 'twentythirteen' ),
		'menu_name'         => __( 'Favorite', 'twentythirteen' )
		),
		'query_var' => true,
		'rewrite' => array(
		'slug' => 'recipe-favorites',
		'with_front' => false 
		)
		) 
		);
	}

	//Function to add or update favorite list
	function update_user_favorit()
	{ 	
		$user_id = $_POST['user_id'];
		$post_id = $_POST['post_id'];
		$favorite = $_POST['favorite'];
		$saved_favorite = unserialize( get_user_meta($user_id,'saved_favorite',true));
		$saved_favorite_post = unserialize( get_user_meta($user_id,'saved_favorite_post',true));
		if(empty($saved_favorite))
		{
			$saved_favorite = array();
		}
		if(empty($saved_favorite_post))
		{
			$saved_favorite_post = array();
		}
		array_push($saved_favorite_post,$post_id);
		foreach($favorite as $term_id)
		{
			if(isset($saved_favorite[$term_id]))
			{
				$posts = $saved_favorite[$term_id];
				array_push($posts,$post_id);
				$saved_favorite[$term_id] = $posts;
			}
			else
			{
				$saved_favorite[$term_id] = array($post_id);
			}
		}
		update_user_meta($user_id,'saved_favorite',serialize($saved_favorite));
		update_user_meta($user_id,'saved_favorite_post',serialize($saved_favorite_post));
		$post_data = get_post($post_id);
		$data = array(
			'kook_notify_user_for'=>$user_id,
			'kook_notify_user_by'=>$user_id,
			'kook_notify_purpose_for'=>'saved favorite',
			'kook_notify_purpose_refer'=>$post_data->ID,
			'kook_notify_post_title'=>$post_data->post_title
		);
		kook_notification_save($data);
		wp_send_json_success ('true') ;
	}
	add_action( 'wp_ajax_update_user_favorit', 'update_user_favorit' );
	add_action( 'wp_ajax_nopriv_update_user_favorit', 'update_user_favorit' );

	//Function to add term list
	function add_new_term()
	{ 
		$term_name = $_POST['term_name'];
		$taxonomy_name = $_POST['taxonomy_name'];
		$cid = wp_insert_term($term_name,$taxonomy_name);
		$term_id = 0;
		if ( ! is_wp_error( $cid ) )
		{
			$term_id =  $cid['term_id'];
		}
		else
		{
		   $term_id =  isset($cid->error_data['term_exists']) ? $cid->error_data['term_exists'] : 0;
		}
		$post_id = "";
		$saved_favorite = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));
		if(empty($saved_favorite))
		{
			$saved_favorite = array();
		}
		$saved_favorite[$term_id]=array();
		update_user_meta(get_current_user_id(),'saved_favorite',serialize($saved_favorite));
		//echo $term_name;
		//	echo $taxonomy_name;
		wp_send_json_success ($term_id) ;
		//	echo $term_id;
	}
	add_action( 'wp_ajax_add_new_term', 'add_new_term' );
	add_action( 'wp_ajax_nopriv_add_new_term', 'add_new_term' );
	
	//Function to hide saved recipes
	function show_hide_saved_recipe()
	{
		$term_id = $_POST['term_id'];
		$status = $_POST['status'];
		$show_hide_saved_recipe = unserialize( get_user_meta(get_current_user_id(),'show_hide_saved_recipe',true));
		if($status=='show')
		{
			if(isset($show_hide_saved_recipe[$term_id])){
				unset($show_hide_saved_recipe[$term_id]);
			}
		}
		else
		{
			$show_hide_saved_recipe[$term_id]=true;
		}
		update_user_meta(get_current_user_id(),'show_hide_saved_recipe',serialize($show_hide_saved_recipe));
		wp_send_json_success ($term_id) ;
	}
	add_action( 'wp_ajax_show_hide_saved_recipe', 'show_hide_saved_recipe' );
	add_action( 'wp_ajax_nopriv_show_hide_saved_recipe', 'show_hide_saved_recipe' );

	//Function to add post to favorite list
	function add_favorite_post()
	{ 
		if(is_user_logged_in())
		{
			$post_id = $_POST['post_id'];
			/*
			$saved_favorite = '';
			$saved_favorite_post = '';
			update_user_meta(get_current_user_id(),'saved_favorite',serialize($saved_favorite));
			update_user_meta(get_current_user_id(),'saved_favorite_post',serialize($saved_favorite_post));
			*/
			$thumbnail = get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'alignleft' ) );
			$title = get_the_title($post_id) ;
			//$checklists = get_terms(array('taxonomy'=>'recipe_favorites','hide_empty'=>false));
			//$checklists = get_terms(array('taxonomy'=>'recipe_favorites','hide_empty'=>false));
			$checklists = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));
			$saved_favorite_keys= array_keys($checklists);
			if($saved_favorite_keys!=null)
			{
				$saved_collections = get_terms( array(
				'taxonomy' => 'recipe_favorites',
				//'number'  =>  -1,
				'include' => $saved_favorite_keys,
				'hide_empty'  => false, 
				'orderby'  => 'include',
				) );
				$htmllist='';	
				foreach($saved_collections as $checklist)
				{
					$htmllist .='<li class="item">
					<label class="checkList_item containers">
					<input class="check-box" type="checkbox" name="favorite[]" value="'.$checklist->term_id.'">
					<span>'.$checklist->name.'</span>
					</label>
					</li>';
				}
			}
			$html='
			<div id="dialogForm" style="display:block">
				<form id="myfavform" class="favlistpop" method="post">
					<div class="popup-header"><h2>Opgeslagen in favorieten!</h2></div>
					<div class="popup-body">
						<div class="popup-sub-title"></div>
							<ul class="collection-items">
								<li class="listbox">
									<ul>
										'.$htmllist.'
									</ul>
									<div class="new-collection">
										<input name="taxonomy_name" type="hidden" value="recipe_favorites">
										<input name="newcollection" type="text" placeholder="Nieuwe groep toevoegen" max-length="200" class="new-collection-name">
										<button name="new-collectikon-btn" class="btn default-btn new-collectikon-btn"><span>+</span></button>
										<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
									</div>
								</li>
								<li class="postbox">
									'.$thumbnail.'
									<span>'.$title.'</span>
								</li>
							</u>	
						</div>
						<div class="popup-footer">
							<input type="hidden" value="'.$post_id.'" name="post_id">
							<input type="hidden" name="action" value="update_user_favorit">
							<input type="hidden" name="user_id" value="'.get_current_user_id().'">
							<button type="submit" class="btn default-btn collection-submit-button" name="collection-submit-button" data-buttonid="'.$post_id.'">Nu opslaan!<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></button>
						</div>
						<div class="close-popup"><i class="fa fa-close"></i>
					</div>
				</form>
			</div>';
			wp_send_json_success ($html) ;
		}
		else
		{
			$html= "no";
			wp_send_json_success ($html) ;
		}
	}
	add_action( 'wp_ajax_add_favorite_post', 'add_favorite_post' );
	add_action( 'wp_ajax_nopriv_add_favorite_post', 'add_favorite_post' );
	
	//Function to delete post to favorite list
	function delete_saved_favorite_post()
	{
		$post_id = $_POST['post_id'];
		$lastpost = $_POST['lastpost'];
		
		$saved_favorite = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));
		$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
		if($lastpost==1){
			$saved_favorite_post = array();
		}else{
			foreach($saved_favorite_post as $key=>$savedpost){
				if($savedpost==$post_id){
					unset($saved_favorite_post[$term_id]);
				}	
			}
		}

		foreach($saved_favorite as $term_id=>$savedposts){
			if($lastpost==1){
				$saved_favorite[$term_id] = array();
			}else{
				foreach ( $savedposts as $key=>$savedpostid ){
					if($post_id==$savedpostid){
						unset($saved_favorite[$term_id][$key]);
					}
				}
			}
		}
		update_user_meta(get_current_user_id(),'saved_favorite',serialize($saved_favorite));
		update_user_meta(get_current_user_id(),'saved_favorite_post',serialize($saved_favorite_post));

		wp_send_json_success (array('status'=>true,'result'=>array($existkey,$saved_favorite)));
	}
	add_action( 'wp_ajax_delete_saved_favorite_post', 'delete_saved_favorite_post' );
	add_action( 'wp_ajax_nopriv_delete_saved_favorite_post', 'delete_saved_favorite_post' );
	
	//Function for saved recipes groups and filter search
	function get_saved_favorite_groups($attr=[])
	{
		$post_per_page = 12;
		if(isset( $attr['userid'])){
			$userid = $attr['userid'];			
		}
		else
		{
			$userid = get_current_user_id();			
		}
		$page = $attr['page'];
		$args = array(
			"paged"            => 1,
			"orderby"          => "post_date",
			"order"            => "DESC",
			"post_type"        => "osetin_recipe",
			"post_status"      => "publish"
		);
		$authorfav = true;
		if(isset($attr['saved']) && $attr['saved']!=''){
			$saved=$attr['saved'];
			if($saved)
			{
				$saved_favorite_post = array();
				$saved_favorite = unserialize( get_user_meta($userid,'saved_favorite',true));
				
				$show_hide_saved_recipe = unserialize( get_user_meta($userid,'show_hide_saved_recipe',true));			

				if(isset( $attr['page']) && $attr['page']=='author'){					
					foreach($show_hide_saved_recipe as $termid=>$status){						
						if(isset($saved_favorite[$termid])){
							unset($saved_favorite[$termid]);
						}
					}
				}
				foreach($saved_favorite as $postids){
					if(count($postids)>0){
						foreach($postids as $postid){
							$saved_favorite_post[] = $postid;
							$authorfav=false;
						}
					}
				}
				
				if(count($saved_favorite_post) > 0){
					$args['post__in'] = $saved_favorite_post;
				}
				$args['posts_per_page'] = 10;
				$post_per_page = 10;
			}
			else
			{
				$authorfav = false;	
				$args['author'] = $userid;
				$args['posts_per_page'] = 12;
				$post_per_page = 12;
			}
		}
		else
		{
			$authorfav = false;
			$args['author'] = $userid;
			$args['posts_per_page'] = 12;
			$post_per_page = 12;
		}
		if($page=='author'){			
			$args['posts_per_page'] = 12;
			$post_per_page = 12;
		}
		if($authorfav){$args=array();}
		?>
		<div class="search-filter-bar">
			<div class="search-bar"><span class="fa fa-search"></span><input placeholder="Zoek recepten" type="text" name="search_saved_post"></div>
			<div class="filter">
				<div class="selected">
					<span class="fa fa-calendar"></span>
					<div class="selected-value" option-id="2">Sorteer recepten</div>
				</div>
				<div class="filter-select">
					<select name="filter_select">
						<option value="1">Sorteer nieuw – oud</option>
						<option value="2">Sorteer oud – nieuw</option>
						<option value="3">Sorteer A – Z</option>
						<option value="4">Sorteer Z – A</option>
					</select>
					<input type="hidden" name="current_user_id" value="<?php echo $userid;?>" >
					<input type="hidden" name="groupid" value="0">
					<input type="hidden" name="saved" value="<?php echo $saved;?>" >
					<input type="hidden" name="page" value="<?php echo $page;?>" >
					<input type="hidden" name="posts_per_page" value="<?php echo $post_per_page;?>" >
					<div class="option" id="1">Sorteer nieuw – oud</div>
					<div class="option" id="2">Sorteer oud – nieuw</div>
					<div class="option" id="3">Sorteer A – Z</div>
					<div class="option" id="4">Sorteer Z – A</div>
				</div>
			</div>
		</div>
		<div class="saved-collection holder-html">
			<div class="masonry-grid">
				<?php 
					$posts_array = array();
					if(count($args)>0){
						$posts_array = get_posts($args);
					}
					
					if(count($posts_array) > 0){
						if($page=='author'){								
							foreach ( $posts_array as $post ){
								?>
								<div class="myrecipes item" id="saved-post-<?php echo $post->ID;?>">									
									<a href="<?php echo get_the_permalink($post->ID); ?>" title="<?php the_title_attribute(array('post'=>$post->ID)); ?>">
										<img src="<?php echo get_the_post_thumbnail_url( $post->ID,'full');?>" rel="<?php echo get_the_title($post->ID);?>" />
									</a>
									<a id="titlerecipe" href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
								</div>
							<?php }								
						}else{
							if($saved){
							foreach ( $posts_array as $post ){?>
							<div class="masonry-item item" id="saved-post-<?php echo $post->ID;?>">
								<?php
								setup_postdata($post);
								$current_step_class = 'full_full';
								include(locate_template('content-for-saved-group.php')); 
								wp_reset_postdata();
								?>
							</div>
						<?php }
							}else{
								foreach ( $posts_array as $post ){
									?>
									<div class="myrecipes item" id="saved-post-<?php echo $post->ID;?>">									
										<a href="<?php echo get_the_permalink($post->ID); ?>" title="<?php the_title_attribute(array('post'=>$post->ID)); ?>">
											<img src="<?php echo get_the_post_thumbnail_url( $post->ID,'full');?>" rel="<?php echo get_the_title($post->ID);?>" />
										</a>
										<a id="titlerecipe" href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
									</div>
								<?php }
							}
						}
					} else{  ?> <div class="no-recipe">No Recipe Found</div> <?php } ?>
			</div>
		<?php 
		
		$args['posts_per_page'] = '-1';
		$posts_array = get_posts($args);
		if(count($posts_array)>10 && $authorfav==false){ ?>
		<div class="get-more">
			<button type="button" name="get_more_recipes">Lees meer
				<div class="lds-ellipsis">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</button>
		</div>
		<?php } ?>
		</div>
		<div id="message-box">
			<div id="success-message">
				<div class="popup-header"><h2>Message</h2></div>
					<div class="popup-body">
						<div class="message">Jouw groep is toegevoegd en opgeslagen.</div>				
					</div>			
				<div class="close-popup"><i class="fa fa-close"></i></div>
			</div>

		</div>
		<div id="err-message-box">
			<div id="err-message">
				<div class="popup-header"><h2>Message</h2></div>
					<div class="popup-body">
						<div class="message">Dit veld kan niet leeg zijn. Voer de groepnaam in. </div>
					</div>			
				<div class="close-popup"><i class="fa fa-close"></i></div>
			</div>

		</div>
	<?php
	}
	add_shortcode( 'get-saved-favorites', 'get_saved_favorite_groups' );

	//Function for saved recipes groups list
	function get_group_list($attr=[])
	{
		if(isset($attr['userid']))
		{
			$userid = $attr['userid'];
		}
		else
		{
			$userid = get_current_user_id();
		}
		$saved_favorite = unserialize( get_user_meta($userid,'saved_favorite',true));
		$show_hide_saved_recipe = unserialize( get_user_meta($userid,'show_hide_saved_recipe',true));
		if(!$saved_favorite)
		{
			$saved_favorite = array();
		}
		if(!$show_hide_saved_recipe)
		{
			$show_hide_saved_recipe = array();
		}
		?>
		<div class="right-block">
			<div class="add-favorites">
				<div class="new-collection">
					<input name="taxonomy_name" type="hidden" value="recipe_favorites">
					<input name="newcollection" type="text" placeholder="Groep toevoegen" max-length="200" class="new-collection-name">
					<button name="new-collectikon-btn" class="btn default-btn new-collectikon-btn"><span>+</span></button><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
				</div>
			</div>				
		</div>
		<div class="favorite-list">
			<ul>
				<?php if(count($saved_favorite)> 0){ ?>
				<?php $looping=0;?>
				<?php $saved_favorite_keys= array_keys($saved_favorite);?>
				<?php 
				$saved_collections = get_terms( array(
				'taxonomy' => 'recipe_favorites',
				'number'  =>  10,
				'include' => $saved_favorite_keys,
				'hide_empty'  => false, 
				'orderby'  => 'include',
				) );
				?>
				<?php foreach($saved_collections as $key=> $term){ 
				$posts = count($saved_favorite[$term->term_id]);
				if(isset($show_hide_saved_recipe[$term->term_id])){
				$visibleicon = '<li><a href="#" class="show-hide-hide">Toon <i class="fa fa-eye-slash"></i></a></li>';
				}else{
				$visibleicon = '<li><a href="#" class="show-hide-show">Verberg <i class="fa fa-eye"></i></a></li>';
				}
				?>
				<li class="extra">
					<div class="fav" data-termid="<?php echo $term->term_id;?>" class="favorite-name">
						<span class="text"><?php echo $term->name;?>(<?php echo $posts;?>)</span>
						<ul class="group-menu">
							<li>
								<a href="#"><i class="fa fa-ellipsis-v"></i></a>
								<ul class="group-sub-menu">
									<li><a href="#" class="delete-this-group">Verwijder <i class="fa fa-trash-o"></i></a></li>
									<?php echo $visibleicon;?>
								</ul>
							</li>
						</ul>
					</div>
				</li>
				<?php $looping++;?>
				<?php } }else{ ?><div class="no-food-collection">No Food Collections Found</div> <?php } ?>
			</ul>
		</div>
		<?php if(count($saved_favorite_keys)>10){?>
		<button id="moregroup" class="group-more" href="#">Lees meer</button>
		<?php }?>
	<?php 
	}
	add_shortcode( 'get-saved-groups', 'get_group_list' );

	//delete favorite group name
	function delete_fav_group()
	{
		$userid = $_POST['userid'];
		$termid = $_POST['termid'];
		$saved_favorite = unserialize( get_user_meta($userid,'saved_favorite',true));
		$status=false;
		foreach($saved_favorite as $k=>$postlist)
		{
			if($k==$termid){
				$status=true;
				unset($saved_favorite[$k]);
			}
		}
		update_user_meta($userid,'saved_favorite',serialize($saved_favorite));
		wp_send_json_success (array("status"=>$status,"data"=>$saved_favorite));
	}
	add_action( 'wp_ajax_delete_fav_group', 'delete_fav_group' );
	add_action( 'wp_ajax_nopriv_delete_fav_group', 'delete_fav_group' );

	//Get Groups in week menu
	function get_group_list_for_weekmenu()
	{
		$saved_favorite = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));

		?>
		<div id="week-menu-popup" class="week-menu-popup" data-weekday="">
			<form class="form" method="post" action="">
				<div class="fav-group-list">
					<ul>
					<?php if(count($saved_favorite) != 0 && is_array($saved_favorite)==true){ ?>
					<?php $saved_favorite_keys= array_keys($saved_favorite);?>
					<?php 
						$saved_collections = get_terms( array(
						'taxonomy' => 'recipe_favorites',
						'number'  =>  50,
						'include' => $saved_favorite_keys,
						'hide_empty'  => false, 
						'orderby'  => 'include',
						) );
					?>
					<?php foreach($saved_collections as $key=> $term){ ?>
					<li>
						<label class="fav-group-list-item" data-termid="<?php echo $term->term_id;?>"><?php echo $term->name;?></label>
					<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
					</li>
					<?php } }else{ ?><div class="no-food-collection">Geen voedselcollecties gevonden!</div> <?php } ?>
					</ul>
					<button id="edit_profile" type="button" name="close-popup"><i class="fa fa-close"></i> Dichtbij</button>
				</div>

			</form>	
		</div>
		<?php 
	}
	add_shortcode( 'get-group-list-for-weekmenu', 'get_group_list_for_weekmenu' );
	
	//Get Saved recipes in week menu
	function get_saved_recipe_on_group()
	{
		$term_id = $_POST['termid'];
		$term_name = $_POST['termname'];
		$weekday = $_POST['weekday'];
		$saved_favorite = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));
		$post_ids = $saved_favorite[$term_id];
		if(count($post_ids)>0){
			$args = array(
				"post__in" => $post_ids,
				"posts_per_page"   => -1,
				"orderby"          => "post_date",
				"order"            => "DESC",
				"post_type"        => "osetin_recipe",
				"post_status"      => "publish"
			);
			$posts_array = get_posts($args);
		}
		?>
		<div class="fav-group-post-list">
			<ul>
				<li>
					<label class="back-to-group" data-termid="<?php echo $term_id;?>"><i class="fa fa-chevron-left"></i><?php echo $term_name;?></label>
				</li>
				<?php if(count($post_ids)>0){?>
					<?php foreach($posts_array as $post){ ?>
					<li>
						<label class="fav-group-post-item" data-postid="<?php echo $post->ID;?>" data-weekend="<?php echo $weekday;?>">
							<div class="post-holder">
								<div class="img-holder" >
								<img src="<?php echo get_the_post_thumbnail_url( $post->ID,'thumbnail');?>" rel="<?php echo get_the_title($post->ID);?>" />
								</div>
								<span class="post-title"><?php echo get_the_title($post->ID);?></span>
							</div>
						</label>	
					</li>
					<?php } ?>
				<?php } ?>
			</ul>
			<button id="edit_profile" type="button" name="save_into_weekmenu"><i class="fa fa-save"></i> Opslaan<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></button>
			<div class="err-msg">Selecteer minimaal één recept.</div>
		</div>
		<?php
		die();
	}
	add_action( 'wp_ajax_get_saved_recipe_on_group', 'get_saved_recipe_on_group' );
	add_action( 'wp_ajax_nopriv_get_saved_recipe_on_group', 'get_saved_recipe_on_group' );
	
	
	//Save recipes into week menu
	function save_recipe_into_weekmenu()
	{
		$weekmenurecipe = $_POST['weekmenurecipe'];
		$weekend = $_POST['weekend'];
		$saved_weekmenu_recipe = unserialize( get_user_meta(get_current_user_id(),'saved_weekmenu_recipe',true));
		
		$currentdate = date('Y-m-d H:i:s');
		$day = date('w',strtotime($currentdate));
		$day = $weekend;
		if(!$saved_weekmenu_recipe){
			$saved_weekmenu_recipe = array();
		}
		$saved_weekmenu_recipe[] = array('date'=>$currentdate,'day'=>$day,'postid'=>$weekmenurecipe);
		
		foreach($saved_weekmenu_recipe as $k=>$row){
			if($row['day']==$weekend){
				$daypostlist[] = $row['postid'];
			}
		}
		$postlist = array_reverse($daypostlist);
		
		/*
		if(isset($saved_weekmenu_recipe[$weekend]))
		{
			$posts = $saved_weekmenu_recipe[$weekend];
			if(is_array($posts))
			{
				$posts[] = $weekmenurecipe;
			}
			else
			{
				$posts = array($weekmenurecipe);
			}
			$saved_weekmenu_recipe[$weekend] = $posts;
		}
		else
		{
			$saved_weekmenu_recipe[$weekend] = array($weekmenurecipe);
		}
		*/
		update_user_meta(get_current_user_id(),'saved_weekmenu_recipe',serialize($saved_weekmenu_recipe));
		
		
		//$postlist = array_reverse($saved_weekmenu_recipe[$weekend]); 

		//$postlist = array_reverse($weekbased[$weekend]); 
		foreach($postlist as $post_id)
		{
			$html .='<li>
				<img src="'.get_the_post_thumbnail_url( $post_id,'thumbnail').'">
				<span class="delete" data-id="'.$post_id.'" data-weekend="'.$weekend.'"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
				<p>'.get_the_title($post_id).'</p>
			</li>';
		}
		if(count($postlist)<=2){
			$html .='<li class="addbutton" data-id="'.$weekend.'"><i class="fa fa-plus" aria-hidden="true"></i></li>';
		}
		wp_send_json_success(array('success'=>true,'html'=>$html,'demo'=>$saved_weekmenu_recipe));
	}
	add_action( 'wp_ajax_save_recipe_into_weekmenu', 'save_recipe_into_weekmenu' );
	add_action( 'wp_ajax_nopriv_save_recipe_into_weekmenu', 'save_recipe_into_weekmenu' );
        
	//delete recipes from week menu
	function delete_recipe_from_weekmenu()
	{
		$weekmenurecipe = $_POST['weekmenurecipe'];
		$weekend = $_POST['weekend'];
		$saved_weekmenu_recipe = unserialize( get_user_meta(get_current_user_id(),'saved_weekmenu_recipe',true));
		foreach($saved_weekmenu_recipe as $k=>$row){
			if($row['day']==$weekend && $row['postid']==$weekmenurecipe){
				unset($saved_weekmenu_recipe[$k]);
				break;
			}
		}
		//$recipeposts = $saved_weekmenu_recipe[$weekend];
		//$key = array_search ($weekmenurecipe, $recipeposts);
		//unset($recipeposts[$key]);
		//reset key order
		$saved_weekmenu_recipe = array_values($saved_weekmenu_recipe);
		
		update_user_meta(get_current_user_id(),'saved_weekmenu_recipe',serialize($saved_weekmenu_recipe));

		foreach($saved_weekmenu_recipe as $k=>$row){
			if($row['day']==$weekend){
				$daypostlist[] = $row;
			}
		}
		$postlist = array_reverse($daypostlist);
		foreach($postlist as $post){
			$post_id = $post['postid'];
			$html .='<li id="liid'.$post_id.'">
			<a href="'.get_permalink($post_id).'">
			<img src="'.get_the_post_thumbnail_url( $post_id,'full').'">
			</a>
			<span class="delete" data-id="'.$post_id.'" data-weekend="'.$weekend.'"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
			<p>'.get_the_title($post_id).'</p>
			</li>';
		}
		if(count($postlist)<3){
			$html .='<li class="addbutton" data-id="'.$weekend.'"><i class="fa fa-plus" aria-hidden="true"></i></li>';
		}
		wp_send_json_success (array('status'=>'true','html'=>$html));
	}
	add_action( 'wp_ajax_delete_recipe_from_weekmenu', 'delete_recipe_from_weekmenu' );
	add_action( 'wp_ajax_nopriv_delete_recipe_from_weekmenu', 'delete_recipe_from_weekmenu' );

	function get_saved_weekmenu_recipe($attr=[])
	{
		$saved_weekmenu_recipe = unserialize( get_user_meta(get_current_user_id(),'saved_weekmenu_recipe',true));
		$html='';
		$weekbased = array();
		foreach($saved_weekmenu_recipe as $k=>$row){
			if($row['day']==$attr['weekday']){
				$daypostlist[] = $row;
			}
		}
		$postlist = array_reverse($daypostlist);

		//$postlist = array_reverse($saved_weekmenu_recipe[$attr['weekday']]); 
		foreach($postlist as $post){?>
		<?php $post_id = $post['postid'];?>
			<li id="liid<?php echo $post_id;?>">
			<a href="<?php echo get_permalink($post_id);?>">
			<img src="<?php echo get_the_post_thumbnail_url( $post_id,'full');?>">
			</a>
			<span class="delete" data-id="<?php echo $post_id;?>" data-weekend="<?php echo $attr['weekday'];?>"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
			<p><?php echo get_the_title($post_id);?></p>
			</li>
		<?php }?>
		<?php if(count($postlist)<3){?>
		<li class="addbutton" data-id="<?php echo $attr['weekday'];?>"><i class="fa fa-plus" aria-hidden="true"></i></li>
		<?php }?>
		<?php
	}
	add_shortcode('weekmenu-recipes','get_saved_weekmenu_recipe');

	//Use a custom CSS file with Social Login
	function oa_social_login_set_custom_css ($css_theme_uri)
	{
	$css_theme_uri = 'http://public.oneallcdn.com/css/api/socialize/themes/buildin/connect/large-v1.css';
	return $css_theme_uri;
	}   
	add_filter('oa_social_login_default_css', 'oa_social_login_set_custom_css');
	add_filter('oa_social_login_widget_css', 'oa_social_login_set_custom_css');
	add_filter('oa_social_login_link_css', 'oa_social_login_set_custom_css');

	//Trending Home Page
	function treading_shortcode() 
	{?>
		<div class="tending">
			<div class="trending-item">
				<ul id="trending-slider" class="trending-slider">
				<?php
					$args = array(
						'post_type'  => 'recipe_gallery',
						'numberposts'=> 6
					);
					$postslist = get_posts( $args );
					$post_count = 1;
					foreach ($postslist as $post) {

						$__postId = get_field('parent_post_id',$post->ID);
						?>
							<li>
								<a href="<?php echo get_permalink($__postId); ?>">
									<?php if($post_count%2): ?>
									<img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" />
									<div class="meta-trading">
										<h3><?php echo $post->post_title; ?></h3>
										<span>Posted By: <?php echo get_the_author_meta('display_name',$post->post_author); ?></span>
										<span>Posted Date: <?php echo get_the_date('F j Y',$post->ID); ?></span>
									</div>
									<?php else : ?>
										<div class="meta-trading">
											<h3><?php echo $post->post_title; ?></h3>
											<span>Posted By: <?php echo get_the_author_meta('display_name',$post->post_author); ?></span>
											<span>Posted Date: <?php echo get_the_date('F j Y',$post->ID); ?></span>
										</div>
										<img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" />
									<?php endif; ?>
								</a>
							</li>						
						<?php
						$post_count++;
					}
				?>				
				</ul>
			</div>
		</div>
	<?php
	}
	add_shortcode( 'trendings', 'treading_shortcode' );

	// Registered Ingradients		
	function register_ingradients()
	{
		$args = array(
			'public'    => true,
			'label'     => __( 'Ingradients', 'twentythirteen' ),
			'menu_icon' => 'dashicons-format-aside',
			'rewrite'   => array( 'slug' => 'ingradients' ),
			'menu_position' => 10,
			'supports'  => array( 'title', 'thumbnail','author', 'comments' ),
		);
		register_post_type( 'ingradients', $args );
	}
	add_action('init', 'register_ingradients');
	
	// Recipe Gallery
	function register_recipe_gallery()
	{
		$args = array(
			'public'    => true,
			'label'     => __( 'Recipe Gallery', 'twentythirteen' ),
			'rewrite'   => array( 'slug' => 'recipe-gallery' ),
			'menu_position' => 10,
			'show_in_menu' => false,
			'supports'  => array( 'title', 'thumbnail','author', 'comments' ),
		);
		register_post_type( 'recipe_gallery', $args );
	}
	add_action('init', 'register_recipe_gallery');

	// Ingradient Functions 
	function the_ingradient_tools($post_id = null)
	{
		$post_id = (isset($_POST['ingradient_id'])) ? $_POST['ingradient_id'] : 0;	
		$acf_groupID = 35085;
		$fields = acf_get_fields($acf_groupID); 
		$toolVals = array();
		$html = '<table>';
		$html .= '<tr><td><input id="toolvals" type="text" value="0" name="gram"></td><th>Gram</th></tr><tr></tr>';
		foreach ($fields as $key => $field) {
			$val = get_field($field['name'],$post_id);
			if(empty($val)){$val = 0;}
			$html .= '<tr><td><input id="toolvals" type="text" value="0" name="'.$field['name'].'"></td><th>'.$field['label'].'</th></tr><tr></tr>';
			$toolVals[$field['name']] = $val;
		} 
		$html .= '</table>';
		if(empty($_POST)) :
			echo $html ;
		else  :
			echo json_encode(array( 'html' => $html, 'values' => $toolVals ));
			wp_die();
		endif ;	
	}
	
	function get_ingradient_tool_rel(){
		return json_encode(array(
		'gram'     =>array('cup'=>'/144','eetlepel_tablespoon' => '/9', 'theelepel_teaspoon' => '/3', 'oz' => '/28.35'),
		'cup' 	   => array( 'gram'=>'*144','eetlepel_tablespoon' => '*16', 'theelepel_teaspoon' => '*48','oz' => '*5.08'),
		'oz' 	   => array( 'gram'=>'*28.35','cup' => '/5.08','eetlepel_tablespoon' => '*3.15', 'theelepel_teaspoon' => '*9.45'),
		'eetlepel_tablespoon' => array( 'gram'=>'*9','cup' => '/16', 'theelepel_teaspoon' => '*3','oz' => '/3.15'),
		'theelepel_teaspoon' => array( 'gram'=>'*3','cup' => '/48', 'eetlepel_tablespoon' => '/3','oz' => '/9.45')
		));
	}

	// Ingradient Calculator Shortcode Function for frontend 
	function ingradient_calculator( $atts ) 
	{
		//ob_start();
		$args = array(
			'post_type'   => 'ingradients',
			'numberposts'=>-1,
		);
		$posts = get_posts($args);
			?>
		<div class="ingr-cont">
			<div class="ingr-header">
				<table>
					<tr>
						<!-- <th><label>Selecteer ingrediënt</label></th> -->
						<td>
							<select id="ingr-list">
							<!-- <option value="0">Selecteer ingrediënt</option> -->
							<option></option>
							<?php foreach ($posts as $key => $post) : ?>
								<option value="<?=esc_attr($post->ID)?>" ><?=$post->post_title?></option>
							<?php endforeach ; ?>
							</select>
						</td>
						</tr>
				</table>
			</div>
			<div class="ingr-body">
				<?php the_ingradient_tools($post->ID); ?>	
			</div>
		</div>
		<script>
			var ingrRel = <?php echo get_ingradient_tool_rel(); ?>;
			var ingrGrams = '';
			jQuery(document).ready(function(){
			//jQuery("#ingr-list").select2();
			
			jQuery('.ingr-cont').on('change','#ingr-list',function(){
				let cr = jQuery( this )
				let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>"
				let data = { action: 'get_ingradient_tools', 'ingradient_id': cr.val() };
				jQuery.post(ajaxurl, data, function(response){
					res = JSON.parse(response)
					cr.closest('.ingr-cont').find('.ingr-body').html(res.html)
					ingrGrams = res.values
					console.log(ingrGrams)
				});
			})
			
			jQuery('.ingr-cont').on('keyup','#toolvals',function(){
				let cr = jQuery( this )
				tVal = cr.val().trim()
				let inputNm = cr.attr('name')
				let relData = cr.data('rel')
				switch(inputNm) {
					case 'cup': 
					cr.closest('.ingr-cont').find('input[name=eetlepel_tablespoon]').val(eval(tVal*16).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=theelepel_teaspoon]').val(eval(tVal*48).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=oz]').val(eval(tVal*5.08).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=gram]').val(eval(tVal*ingrGrams.cup).toFixed(2) )
					break;
					case 'eetlepel_tablespoon' :
					cr.closest('.ingr-cont').find('input[name=cup]').val(eval(tVal/16).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=theelepel_teaspoon]').val(eval(tVal*3).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=oz]').val(eval(tVal/3.15).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=gram]').val(eval(tVal*ingrGrams.eetlepel_tablespoon).toFixed(2) )
					break;
					case 'theelepel_teaspoon' :
					cr.closest('.ingr-cont').find('input[name=eetlepel_tablespoon]').val(eval(tVal/3) )
					cr.closest('.ingr-cont').find('input[name=cup]').val(eval(tVal/48) )
					cr.closest('.ingr-cont').find('input[name=oz]').val(eval(tVal/9.45) )
					cr.closest('.ingr-cont').find('input[name=gram]').val(eval(tVal*ingrGrams.theelepel_teaspoon) )
					break;
					case 'oz' :
					cr.closest('.ingr-cont').find('input[name=eetlepel_tablespoon]').val(eval(tVal*3.15).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=theelepel_teaspoon]').val(eval(tVal*9.45).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=cup]').val(eval(tVal/5.08).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=gram]').val(eval(tVal*ingrGrams.oz).toFixed(2) )
					break;
					case 'gram' :
					cr.closest('.ingr-cont').find('input[name=eetlepel_tablespoon]').val(eval(tVal/ingrGrams.eetlepel_tablespoon).toFixed(2) )
					cr.closest('.ingr-cont').find('input[name=theelepel_teaspoon]').val(eval(tVal/ingrGrams.theelepel_teaspoon).toFixed(2))
					cr.closest('.ingr-cont').find('input[name=oz]').val(eval(tVal/ingrGrams.oz).toFixed(2))
					cr.closest('.ingr-cont').find('input[name=cup]').val(eval(tVal/ingrGrams.cup).toFixed(2))
					break;
					default:
					calsString = ''
					console.log('HELLO')
				}		
			})
			})
		</script>
	<?php //return ob_get_clean();
	}

	// Ingradient Calculator Shortcode for frontend 
	add_shortcode( 'ingradient-calculator', 'ingradient_calculator' );
	add_action( 'wp_ajax_get_ingradient_tools', 'the_ingradient_tools' );
	add_action( 'wp_ajax_nopriv_get_ingradient_tools', 'the_ingradient_tools' );
	function my_login_redirect( $url, $request, $user )
	{
		if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) )
		{
			if( $user->has_cap( 'subscriber')) {
			$url = home_url('/my-profile/');
			} 
			else 
			{
				$url = home_url('/my-profile/');
			}
		}
		return $url;
	}
	add_filter('login_redirect', 'my_login_redirect', 10, 3 );
	
	// Email Template Design 
	add_filter( 'haet_mail_css_desktop', function( $css )
	{
		$css .= '
				body{
					background-color: #F1F1F1;
				}
				.container {
					margin-bottom: 45px !important;
					margin-top: 30px!important;
				}
				.header{
					background: url(https://dev.kookmutsjes.com/wp-content/uploads/2020/02/email-header-1.png);
					background-size: cover;
				}
				.full-width-header-image {
					width: 300px;
					margin: 0px auto;
				}
				.content{
					padding-top:35px !important;
					padding-bottom:35px !important;
				}
				.content a{
					text-decoration:none;
				}
				.mail-footer{
					text-align: center;
				}
				.mail-footer img{
					max-width: 245px;
					margin: 20px auto;
					display: block;
				}
				.mail-footer h2{
					text-align: center !important;
					text-transform: uppercase;
					font-style: normal !important;
					color: #333333 !important;
					font-size: 20px !important;
					margin-bottom: 0px;
				}
				.mail-footer ul{
				    margin: 0px auto;
					padding: 0px;
					max-width: 155px;
					overflow: hidden;
				}
				.mail-footer ul li{
				    list-style: none !important;
					margin-left: 5px !important;
					margin-right: 5px !important;
					float: left;
				}
				.mail-footer ul li img{
					max-width: 25px !important;
					min-width: 25px !important;
				}
				.mail-footer a{}
				.mail-footer p{}
			';
		return $css;
	});

	add_filter( 'haet_mail_css_mobile', function( $css ){
		$css .= '  

			';
		return $css;
	});

	function search_by_title( $where, &$wp_query )
	{
		global $wpdb;
		if ( $search_term = $wp_query->get( 'search_osetin_recipe_title' ) ) 
		{
			$search_term = $wpdb->esc_like($search_term);
			$search_term = ' \'%' . $search_term . '%\'';
			$where .= ' AND ' . $wpdb->posts . '.post_title LIKE '.$search_term;
		}
		return $where;
	}
	add_filter( 'posts_where', 'search_by_title', 10, 2 );

	// Search Recipes
	function search_recipe()
	{
		$posts_array = array();
		$fetchrecord = true;
		$searchStr = $_POST['search_str'];
		$saved = $_POST['saved'];
		$userid = $_POST['userid'];
		$sortby = $_POST['sortby'];
		$page = $_POST['page'];
		
		$args['orderby'] = 'post_date'; 
		$args['order'] = 'DESC'; 
		if(isset($_POST['search_str']) && $searchStr!=''){

			$args["search_osetin_recipe_title"] = $searchStr;
			$args["suppress_filters"] = false;
		}
		$saved_favorite_post = unserialize( get_user_meta($userid,'saved_favorite_post',true));
		if($saved)
		{
			$args["post__in"] = $saved_favorite_post;		
		}
		else
		{
			$args["author"] = $userid;
		}	
		if($sortby==1)
		{
			$args['orderby'] = 'post_date'; 
			$args['order'] = 'ASC'; 
		}
		elseif($sortby==2)
		{
			$args['orderby'] = 'post_date'; 
			$args['order'] = 'DESC'; 
		}
		elseif($sortby==3)
		{
			$args['orderby'] = 'post_title'; 
			$args['order'] = 'ASC'; 
		}
		elseif($sortby==4)
		{
			$args['orderby'] = 'post_title'; 
			$args['order'] = 'DESC'; 
		}
		else
		{
			$args['orderby'] = 'post_date'; 
			$args['order'] = 'DESC'; 
		}
		$args["paged"] = 1;
		if(isset($_POST['posts_per_page']))
		{
			$args["posts_per_page"]   = $_POST['posts_per_page'];
		}
		else
		{			
			if($saved)
			{
				$args["posts_per_page"]   = 10;
			}
			else
			{
				$args["posts_per_page"]   = 12;
			}
		}
		
		$args["post_type"] = "osetin_recipe";
		$args["post_status"] = "publish";
		if(isset($_POST['groupid']) && $_POST['groupid']!=0)
		{	
			$saved_favorite = unserialize( get_user_meta($userid,'saved_favorite',true));
			if(count($saved_favorite[$_POST['groupid']])>0)
			{
				$args["post__in"] = $saved_favorite[$_POST['groupid']];
			}
			else
			{
				unset($args["post__in"]);
				$fetchrecord = false;
			}
		}

		//var_dump($args);
		if($fetchrecord)
		{
			$posts_array = get_posts($args);
		}
		$html='';
		if(!isset($_POST['more']))
		{
			$html ='<div class="masonry-grid">';
		}
		if(count($posts_array)>0)
		{
			if($page=='author')
			{				
				foreach ( $posts_array as $post )
				{
					$html .= '<div class="myrecipes item">
					<a href="'.get_the_permalink($post->ID).'">							
						<img src="'.get_the_post_thumbnail_url( $post->ID,'thumbnail').'" rel="'.get_the_title($post->ID).'" />
						<a id="titlerecipe" href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a>
					</a>
					</div>';
				}
			}
			else
			{
				if($saved)
				{
					foreach ( $posts_array as $post )
					{
						$html .= '<div class="masonry-item item">';
						setup_postdata($post);
						$current_step_class = 'full_full';
						ob_start();
						include locate_template('content-for-saved-group.php'); 
						$html .= ob_get_clean();
						$html .= '</div>';
						wp_reset_postdata();
					}
				}
				else
				{
					foreach ( $posts_array as $post )
					{
						$html .= '<div class="myrecipes item">
							<a href="'.get_the_permalink($post->ID).'">							
								<img src="'.get_the_post_thumbnail_url( $post->ID,'thumbnail').'" rel="'.get_the_title($post->ID).'" />
								<a id="titlerecipe" href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a>
							</a>
						</div>';
					}
				}
			}
		}
		else
		{
			$html= '<div class="masonry-grid"><div class="no-recipe">No Recipe Found</div></div>';
		}

		$args['posts_per_page'] = '-1';
		$postslist = array();
		if(isset($args['paged']))
		{
			unset($args['paged']);
		}
		if($fetchrecord)
		{
			$postslist = get_posts($args);
		}
		$html .='</div>';
		if(count($posts_array)<count($postslist))
		{
			$html .='<div class="get-more">
				<button type="button" name="get_more_recipes">Lees meer
					<div class="lds-ellipsis">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				</button>
			</div>';
		}
		echo $html;
		die();
	}
	add_action( 'wp_ajax_search_recipe', 'search_recipe');
	add_action( 'wp_ajax_nopriv_search_recipe', 'search_recipe');

	/*Function to search related saved recipe*/
	function search_saved_recipe()
	{ 
		$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
		if(count($saved_favorite_post) == 0)
		{
			$html= '<div class="masonry-grid"><div class="no-recipe">No Recipe Found</div></div>';
			echo $html;
		}
		else
		{
			$searchStr = $_POST['str'];
			$mypost = $_POST['mypost'];
			$userid = $_POST['userid'];
			$sortby = $_POST['sortby'];
			if($mypost){
				$args = array(
					"posts_per_page"   => 10,
					"paged"            => 1,
					"orderby"          => "post_date",
					"order"            => "DESC",
					"search_osetin_recipe_title"       => $searchStr,
					"post_type"        => "osetin_recipe",
					"post_status"      => "publish",
					"author"		   => $userid,
					"suppress_filters" => FALSE,
				);
			}
			else
			{
				$args = array(
					"post__in" => $saved_favorite_post,
					"posts_per_page"   => 10,
					"paged"            => 1,
					"orderby"          => "post_date",
					"order"            => "DESC",
					"search_osetin_recipe_title"       => $searchStr,
					"post_type"        => "osetin_recipe",
					"post_status"      => "publish",
					"suppress_filters" => FALSE,
				);
			}
			if($sortby==1)
			{
				$args['orderby'] = 'post_date'; 
				$args['order'] = 'ASC'; 
			}
			elseif($sortby==2)
			{
				$args['orderby'] = 'post_date'; 
				$args['order'] = 'DESC'; 
			}
			elseif($sortby==3)
			{
				$args['orderby'] = 'post_title'; 
				$args['order'] = 'ASC'; 
			}
			elseif($sortby==4)
			{
				$args['orderby'] = 'post_title'; 
				$args['order'] = 'DESC'; 
			}
			$posts_array = get_posts($args);
			$html='';
			if($mypost)
			{
				$html .= '<div class="row boxofpost">';
				foreach ( $posts_array as $post ){
					$html .= '<div class="col-md-4 myrepost item">
						<div class="ppost">							
							<img src="'.get_the_post_thumbnail_url( $post->ID,'thumbnail').'" rel="'.get_the_title($post->ID).'" />
							<a id="titlerecipe" href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a>
						</div>
					</div>';
				 }
				 $html .='</div>';
			}
			else
			{
				$html .= '<div class="masonry-grid">';
				foreach ( $posts_array as $post ){
					$html .= '<div class="masonry-item item">';
					setup_postdata($post);
					$current_step_class = 'full_full';
					ob_start();
					include locate_template('content-for-saved-group.php'); 
					$html .= ob_get_clean();
					$html .= '</div>';
					wp_reset_postdata();
				} 
				$html .= '</div>';
			}	
			echo $html;
		}
		//wp_send_json_success (json_encode($html)) ;
		die();
	}
	add_action( 'wp_ajax_search_saved_recipe', 'search_saved_recipe');
	add_action( 'wp_ajax_nopriv_search_saved_recipe', 'search_saved_recipe');

	// Search recipes by order
	function search_saved_recipe_by_order()
	{ 
		$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
		if(count($saved_favorite_post) == 0)
		{
			$html= '<div class="masonry-grid"><div class="no-recipe">No Recipe Found</div></div>';
			echo $html;
		}
		else
		{
			$searchStr = $_POST['str'];
			$mypost = $_POST['mypost'];
			$userid = $_POST['userid'];
			$sortby = $_POST['sortby'];
			if($mypost)
			{
				$args = array(
					"posts_per_page"   => 10,
					"paged"            => 1,
					"orderby"          => "post_date",
					"order"            => "DESC",
					"search_osetin_recipe_title"       => $searchStr,
					"post_type"        => "osetin_recipe",
					"post_status"      => "publish",
					"author"		   => $userid,
					"suppress_filters" => FALSE,
				);
			}
			else
			{
				$args = array(
					"post__in" => $saved_favorite_post,
					"posts_per_page"   => 10,
					"paged"            => 1,
					"orderby"          => "post_date",
					"order"            => "DESC",
					"search_osetin_recipe_title"       => $searchStr,
					"post_type"        => "osetin_recipe",
					"post_status"      => "publish",
					"suppress_filters" => FALSE,
				);
			}
			if($sortby==1)
			{
				$args['orderby'] = 'post_date'; 
				$args['order'] = 'ASC'; 
			}
			elseif($sortby==2)
			{
				$args['orderby'] = 'post_date'; 
				$args['order'] = 'DESC'; 
			}
			elseif($sortby==3)
			{
				$args['orderby'] = 'post_title'; 
				$args['order'] = 'ASC'; 
			}
			elseif($sortby==4)
			{
				$args['orderby'] = 'post_title'; 
				$args['order'] = 'DESC'; 
			}
			$posts_array = get_posts($args);
			if($mypost)
			{
				$html .= '<div class="row boxofpost">';
				foreach ( $posts_array as $post ){
					$html .= '<div class="col-md-4 myrepost item">
						<div class="ppost">							
							<img src="'.get_the_post_thumbnail_url( $post->ID,'thumbnail').'" rel="'.get_the_title($post->ID).'" />
							<a id="titlerecipe" href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a>
						</div>
					</div>';
				 }
				 $html .='</div>';
			}
			else
			{
				$html.= '<div class="masonry-grid">';
				foreach ( $posts_array as $post ){
					$html.= '<div class="masonry-item item">';							
					setup_postdata($post);
					$current_step_class = 'full_full';
					ob_start();
					include locate_template('content-for-saved-group.php'); 
					$html .= ob_get_clean();
					$html .= '</div>';
					wp_reset_postdata();
				} 
				$html .= '</div>';
			}	
			echo $html;
		}
		//wp_send_json_success (json_encode($posts_array)) ;
		die();
	}
	add_action( 'wp_ajax_search_saved_recipe_by_order', 'search_saved_recipe_by_order');
	add_action( 'wp_ajax_nopriv_search_saved_recipe_by_order', 'search_saved_recipe_by_order');
	
	// Load more group names
	function get_more_group_list()
	{
		$saved_favorite = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));
		$groupListCount= $_POST['count'];
		?>
			<ul>
				<?php $looping=0;?>
				<?php $saved_favorite_keys= array_keys($saved_favorite);?>
				<?php 
					$saved_collections = get_terms( array(
					  'taxonomy' => 'recipe_favorites',
					  'number'  =>  $groupListCount,
					  'include' => $saved_favorite_keys,
					  'hide_empty'  => false, 
					  'orderby'  => 'include',
					) );
				?>
				<?php foreach($saved_collections as $key=> $term){ 
					$posts = count($saved_favorite[$term->term_id]);
				?>
				<li class="extra <?php if($looping==0){echo 'active';}?>">
					<div class="fav"  data-termid="<?php echo $term->term_id;?>" class="favorite-name">
						<span class="text"><?php echo $term->name;?>- <?php echo $posts;?></span>
						<ul class="group-menu">
							<li>
								<a href="#"><i class="fa fa-ellipsis-v"></i></a>
								<ul class="group-sub-menu">
									<li><a href="#" class="delete-this-group">Verwijder <i class="fa fa-trash-o"></i></a></li>
									<li><a href="#" class="show-hide-show">Verberg <i class="fa fa-eye"></i></a></li>									
								</ul>
							</li>
						</ul>
					</div>
				</li>
				<?php $looping++;?>
				<?php }?>
			</ul>
		<?php
		die();
	}
	add_action( 'wp_ajax_get_more_group_list', 'get_more_group_list');
	add_action( 'wp_ajax_nopriv_get_more_group_list', 'get_more_group_list');
	
	//More saved receipe button
	function load_more_saved_recipe()
	{
		if(isset($_POST['user_id']))
		{
			$userid = $_POST['user_id'];
		}
		else
		{
			$userid = get_current_user_id();
		}
		$saved_favorite_post = unserialize( get_user_meta($userid,'saved_favorite_post',true));
		if(count($saved_favorite_post) == 0)
		{
			$html= '<div class="masonry-grid"><div class="no-recipe">No Recipe Found</div></div>';
			echo $html;
		}
		else
		{
			$searchStr = $_POST['str'];
			$countrecipe = $_POST['count'];
			$selectedVal= $_POST['selval'];
			if($selectedVal == 1)
			{
				$orderBy= "post_date";
				$order= "ASC";
			}
			elseif($selectedVal == 2)
			{
				$orderBy= "post_date";
				$order= "DESC";
			}
			elseif($selectedVal == 3)
			{
				$orderBy= "title";
				$order= "ASC";
			}
			elseif($selectedVal == 4)
			{
				$orderBy= "title";
				$order= "DESC";
			}
			else
			{
				$orderBy= "post_date";
				$order= "DESC";
			}
			//$userDetails= get_user_meta($userId);
			if($searchStr)
			{
				$args = array(
					"post__in" => $saved_favorite_post,
					"posts_per_page"   => $countrecipe,
					"paged"            => 1,
					"orderby"          => $orderBy,
					"order"            => $order,
					//"meta_key"         => "post_title",
					"post_title"         => "%s",
					"s"       => $searchStr,
					"post_type"        => "osetin_recipe",
					"post_status"      => "publish"
				);
			}
			else
			{
				$args = array(
					"post__in" => $saved_favorite_post,
					"posts_per_page"   => $countrecipe,
					"paged"            => 1,
					"orderby"          => $orderBy,
					"order"            => $order,
					"post_type"        => "osetin_recipe",
					"post_status"      => "publish"
				);
			}
			$posts_array = get_posts($args);
			$html='';
			$html.= '<div class="masonry-grid">';
			foreach ( $posts_array as $post ){
			$html.= '<div class="masonry-item item">';
			setup_postdata($post);
			$current_step_class = 'full_full';
			ob_start();
			include locate_template('content-for-saved-group.php'); 
			$html .= ob_get_clean();
			$html .= '</div>';
					} 
			$html .= '</div>';
			echo $html;
			wp_reset_postdata();
		}
		//wp_send_json_success (json_encode($html)) ;
		die();
	}
	add_action( 'wp_ajax_load_more_saved_recipe', 'load_more_saved_recipe');
	add_action( 'wp_ajax_nopriv_load_more_saved_recipe', 'load_more_saved_recipe');

	// Related Group Recipes
	function related_group_recipes()
	{ 
		$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite',true));
		//var_dump($saved_favorite_post);
		$groupId= $_POST['groupid'];
		$relatedPostArray= $saved_favorite_post[$groupId];
		if(count($relatedPostArray) == 0)
		{
			$html = '<div class="masonry-grid"><div class="no-recipe">No Recipe Found</div></div>';
			echo $html;
		}
		else
		{
			//$userDetails= get_user_meta($userId);
			$args = array(
				"post__in" => $relatedPostArray,
				"posts_per_page"   => 10,
				"paged"            => 1,
				"orderby"          => "post_date",
				"order"            => "DESC",
				"post_type"        => "osetin_recipe",
				"post_status"      => "publish"
			);
			$posts_array = get_posts($args);
			$html='';
			$html.= '<div class="masonry-grid">';
			foreach ( $posts_array as $post ){
			$html.= '<div class="masonry-item item">';
			setup_postdata($post);
			$current_step_class = 'full_full';
			ob_start();
			include locate_template('content-for-saved-group.php'); 
			$html .= ob_get_clean();
			$html .= '</div>';
					} 
			$html .= '</div>';
			echo $html;
			wp_reset_postdata();
		}
		//wp_send_json_success (json_encode($html)) ;
		die();
	}
	add_action( 'wp_ajax_related_group_recipes', 'related_group_recipes');
	add_action( 'wp_ajax_nopriv_related_group_recipes', 'related_group_recipes');
	
	function getcurrentweekstartenddates(){
		$currentday = date('w');
		$start = $currentday-1;
		$end = 7-$currentday;
		$enddate = date('Y-m-d H:i:s', strtotime('+'.$end.' days'));
		$startdate = date('Y-m-d H:i:s', strtotime('-'.$start.' days'));
		return array('start'=>$startdate,'end'=>$enddate);
	}

	//Ingradient Profile Sidebar
	function get_user_post_ingradient_list() 
	{
		//delete_user_meta(get_current_user_id(),'saved_weekmenu_recipe');
		$saved_weekmenu_recipe = unserialize( get_user_meta(get_current_user_id(),'saved_weekmenu_recipe',true));			
		/*
		$range = getcurrentweekstartenddates();
		
		$weeks =array();
		foreach($saved_weekmenu_recipe as $key=>$row){			
			if(strtotime($row['date'])>=strtotime($range['start']) && strtotime($row['date'])<=strtotime($range['end'])){
				$weeks[$row['day']][] = $row;
			}
		}
		
		$postids =array();
		foreach($weeks as $week){
			$postids[] = $week[count($week)-1]['postid'];
		}
		$postids = array_unique($postids);		
		*/
		$postids =array();
		foreach($saved_weekmenu_recipe as $key=>$row){
			$postids[] = $row['postid'];
		}
		$postids = array_unique($postids);

		?>
		<div class="ingradient-profile">
			<img src="https://dev.kookmutsjes.com/wp-content/uploads/2020/01/image-weekmenu.png" alt="ingradient Image"/>
		<?php
		if(count($postids)>0){
			$args = array(
				'post__in' =>$postids,
				'post_type'  => 'osetin_recipe',
				'posts_per_page' => -1
			);
			$posts = get_posts($args);
			$recipe_ingredient = array();
			?>
				<ul>
			<?php
			foreach($posts as $post)
			{
				//setup_postdata($post);
				$GLOBALS['post'] = $post;
				while ( osetin_have_rows( 'ingredients' ) ) 
				{
					the_row();
					$ingrname = get_sub_field( 'ingredient_name' );
					if (! get_sub_field( 'separator' ) ) {
						if($ingrname!=''){
							if(!in_array($ingrname,$recipe_ingredient)){
								?>
									<li><?php /* ?><span class="ingr-amt"><?php the_sub_field( 'ingredient_amount' ); ?></span><?php */ ?><span class="ingr-name"><?php echo $ingrname;?></span></li>
								<?php						
								$recipe_ingredient[] = $ingrname;
							}
						}
					}
									
				}
			}
			$filename='wp-content/uploads/wp-user-pdf/'.get_current_user_id()."-ingredients.pdf";
			unlink($filename);
			//var_dump($recipe_ingredient);
			$pdf = new PDF();
			// Column headings
			$header = array('Boodschappen');
			// Data loading
			//$data = $pdf->LoadData($recipe_ingredient);
			$pdf->SetFont('Arial','',14);
			
			$pdf->AddPage();
			$pdf->ImprovedTable($header,$recipe_ingredient);
			$pdf->Output($filename,'F');
			?>
				
				</ul>
				

			<?php
		}
		?>
			</div>
			<ul class="download-link">
				<li><a href="<?php echo get_site_url().'/'.$filename;?>" download>Download PDF</a></li>
			</ul>
		<?php
	}
	add_shortcode( 'ingradient-list', 'get_user_post_ingradient_list' );
	
	// Author Week menu
	function author_week_menu($attr=[])
	{
		$userid = $attr['userid'];
		$saved_weekmenu_recipe = unserialize( get_user_meta($userid,'saved_weekmenu_recipe',true));
		$weekdays  = array(
			1=>"Maandag",
			2=>"Dinsdag",
			3=>"Woensdag",
			4=>"Donderdag",
			5=>"Vrijdag",
			6=>"Zaterdag",
			7=>"Zondag",
		);
		$upload_dir = wp_upload_dir();
        $uploadBaseUrl= $upload_dir['baseurl'];
        ?>
		<div class="weekmenupage">																
			<ul>
				<?php		

				foreach($weekdays as $key=>$weekname){

					foreach($saved_weekmenu_recipe as $k=>$row){
						if($row['day']==$key){
							$daypostlist[$key][] = $row;
						}
					}
				}
				foreach($weekdays as $key=>$weekname){
					if(isset($daypostlist[$key]) && !empty($daypostlist[$key])){
						$recent = end($daypostlist[$key]);
						if(!empty($recent)){
                            $post_id = $recent['postid'];
                            $imageUrl = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
                            $permalink= get_permalink( $post_id );
                        }else{
                            $imageUrl = $uploadBaseUrl.'/2019/11/nofound-recipes.png';
						    $permalink= '#';
                        }
					}else{
						$imageUrl = $uploadBaseUrl.'/2019/11/nofound-recipes.png';
						$permalink= '#';
					}
					/*
					if(isset($saved_weekmenu_recipe[$key])){
						$recent = end($saved_weekmenu_recipe[$key]);
                        if(!empty($recent)){
                            $post_id = $recent;
                            $imageUrl = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
                            $permalink= get_permalink( $post_id );
                        }else{
                            $imageUrl = $uploadBaseUrl.'/2019/11/nofound-recipes.png';
						    $permalink= '#';
                        }						
					}else{
						$imageUrl = $uploadBaseUrl.'/2019/11/nofound-recipes.png';
						$permalink= '#';
					}
					*/
					?>
					<li>
						<h2><?php echo $weekname;?></h2>
						<div class="weekmenu-img">
							<a href="<?php echo $permalink;?>">
								<img src="<?php echo $imageUrl;?>">
							</a>
						</div>
					</li>
					<?php
				}?>
			</ul>
		</div>
	<?php
	}
	add_shortcode( 'auth_wk_mn_sc', 'author_week_menu' );

	//question and answer plugin addon code
	function add_question_taxonomy()
	{
		$term_name = $_POST['category_name'];
		$term_slug = str_replace(' ', '-', $term_name);
		wp_insert_term(
			$term_name ,   // the term 
			'dwqa-question_category', // the taxonomy
			array(
				'description' => '',
				'slug'        => $term_slug,
			)
		);
		$html = '<li><a href="'.the_site_url().'/question/category/'.$term_slug.'">'.$term_name.'<a></li>';
		wp_send_json_success( array( 'success' =>true ) );
	}
	add_action( 'wp_ajax_add_question_taxonomy', 'add_question_taxonomy');
	add_action( 'wp_ajax_nopriv_add_question_taxonomy', 'add_question_taxonomy');
	
	// Ingradient from recipes
	function get_ingradient_from_users_recipe()
	{
		$user = get_current_user();
		/*
		$args = array(
			'author'        =>  $current_user->ID,
			"posts_per_page"   => -1,
			"orderby"          => "post_date",
			"order"            => "DESC",
			"post_type"        => "osetin_recipe",
			"post_status"      => "publish"
		);
		$posts_array = get_posts($args);
		*/
	}
	add_shortcode( 'user-ingradients', 'get_ingradient_from_users_recipe' );
	
	// Follow and following 
	function kkm_follow()
	{
		$current_user = get_current_user_id();
		$follow_user = $_POST['follow_user'];
		$follow = unserialize( get_user_meta($current_user,'follow',true));
		//updateing current user
		if(is_array($follow))
		{
			if(!in_array($follow_user,$follow['following_list']))
			{
				$follow['following'] = $follow['following']+1;
				array_push($follow['following_list'],$follow_user);	
			}		
		}
		else
		{
			$follow = array(
				'followers'=>0,
				'following'=>1,
				'followers_list'=>array(),
				'following_list'=>array($follow_user),
			);
		}
		update_user_meta($current_user,'follow',serialize($follow));
		//delete_user_meta($current_user,'follow');
		$follow_user_data = get_userdata($follow_user);
		$data = array(
			'kook_notify_user_for'=>$current_user,
			'kook_notify_user_by'=>$current_user,
			'kook_notify_purpose_for'=>'following',
			'kook_notify_purpose_refer'=>$follow_user,
			'kook_notify_display_name'=>$follow_user_data->display_name
		);
		kook_notification_save($data);
		//updating followed user
		$follow = unserialize( get_user_meta($follow_user,'follow',true));
		if(is_array($follow))
		{
			if(!in_array($current_user,$follow['followers_list'])){
				$follow['followers'] = $follow['followers']+1;
				array_push($follow['followers_list'],$current_user);
			}
		}
		else
		{
			$follow = array(
				'followers'=>1,
				'following'=>0,
				'followers_list'=>array($current_user),
				'following_list'=>array(),
			);
		}
		update_user_meta($follow_user,'follow',serialize($follow));
		//delete_user_meta($follow_user,'follow');
		$current_user_data = get_userdata($current_user);
		$data = array(
			'kook_notify_user_for'=>$follow_user,
			'kook_notify_user_by'=>$current_user,
			'kook_notify_purpose_for'=>'followed',
			'kook_notify_purpose_refer'=>$current_user,
			'kook_notify_display_name'=>$current_user_data->display_name
		);
		kook_notification_save($data);
		wp_send_json_success( array( 'success' =>true,'data'=>$follow ) );
	}
	add_action( 'wp_ajax_kkm_follow', 'kkm_follow');
	add_action( 'wp_ajax_nopriv_kkm_follow', 'kkm_follow');

	function kkm_unfollow()
	{
		$current_user = get_current_user_id();
		$unfollow_user = $_POST['unfollow_user'];
		$follow = unserialize( get_user_meta($current_user,'follow',true));
		
		//updateing current user
		if(is_array($follow)){
			if($follow['following']-1>=0){
				$follow['following'] = $follow['following']-1;
			}else{
				$follow['following']=0;
			}
			$key = array_search($unfollow_user,$follow['following_list']);
			unset($follow['following_list'][$key]);
			//reset keys
			$follow['following_list'] = array_values($follow['following_list']);
		}
		
		update_user_meta($current_user,'follow',serialize($follow));
		//updating followed user
		$follow = unserialize( get_user_meta($unfollow_user,'follow',true));
		if(is_array($follow))
		{
			if($follow['followers']-1>=0){
				$follow['followers'] = $follow['followers']-1;
			}else{
				$follow['followers']=0;
			}
			$key = array_search($current_user,$follow['followers_list']);
			unset($follow['followers_list'][$key]);
			//reset keys
			$follow['followers_list'] = array_values($follow['followers_list']);
		}
		update_user_meta($unfollow_user,'follow',serialize($follow));
		if($unfollow_user==11){
			wp_send_json_success( array( 'success' =>false ,'data'=>$follow,'unfollowid'=>$unfollow_user) );
		}else{
			wp_send_json_success( array( 'success' =>true ,'data'=>$follow,'unfollowid'=>$unfollow_user) );
		}
		
	}
	add_action( 'wp_ajax_kkm_unfollow', 'kkm_unfollow');
	add_action( 'wp_ajax_nopriv_kkm_unfollow', 'kkm_unfollow');

	function kkm_remove_follower()
	{
		$current_user = get_current_user_id();
		$followeruser = $_POST['followeruser'];
		//updating followed user
		$follow = unserialize( get_user_meta($current_user,'follow',true));
		if(is_array($follow))
		{
			if($follow['followers']-1>=0){
				$follow['followers'] = $follow['followers']-1;
			}else{
				$follow['followers']=0;
			}
			$key = array_search($followeruser,$follow['followers_list']);
			unset($follow['followers_list'][$key]);
			//reset keys
			$follow['followers_list'] = array_values($follow['followers_list']);
		}
		update_user_meta($current_user,'follow',serialize($follow));



		$follow = unserialize( get_user_meta($followeruser,'follow',true));
		//updateing current user
		if(is_array($follow)){
			if($follow['following']-1>=0){
				$follow['following'] = $follow['following']-1;
			}else{
				$follow['following']=0;
			}
			$key = array_search($current_user,$follow['following_list']);
			unset($follow['following_list'][$key]);
			//reset keys
			$follow['following_list'] = array_values($follow['following_list']);
		}
		update_user_meta($followeruser,'follow',serialize($follow));
		
		wp_send_json_success( array( 'success' =>true ,'data'=>$follow) );
	}
	add_action( 'wp_ajax_kkm_remove_follower', 'kkm_remove_follower');
	add_action( 'wp_ajax_nopriv_kkm_remove_follower', 'kkm_remove_follower');

	function get_count_follow_follower_posts($attr=[])
	{
		$user_id = $attr['userid'];
		$follow = unserialize( get_user_meta($user_id,'follow',true));
		if(!is_array($follow)){
			$follow = array(
				'followers'=>0,
				'following'=>0
			);
		}
		$args = array(
			'author'        =>  $user_id,
			'posts_per_page' => -1,
			"post_type"        => "osetin_recipe",
			"post_status"      => "publish"
		);
		$posts = get_posts($args);
		?>
		<div class="desktop-fs">
			<ul class="ipad-view">
				<li>
					<p>Followers</p>
				</li>
				<li>
					<p>Following</p>
				</li>
				<li>
					<p>Recepten</p>
				</li>
			</ul>
			<ul class="count-flowr">
				<li>
					<?php if($user_id==11){
						$usercount = count_users();
						$total_users = $usercount['total_users'];
						?>
						<a href="#" id="followers"><?php echo $total_users;?></a>
					<?php }else{?>
					<a href="#" id="followers"><?php if($follow['followers']>=0){echo $follow['followers'];}else{ echo '0';}?></a>
					<?php }?>
				</li>
				<li>
					<a href="#" id="following"><?php if($follow['following']>=0){echo $follow['following'];}else{ echo '0';}?></a>
				</li>
				<li>
					<a href="#" id="my-posts"><?php echo count($posts); ?></a>
				</li>
			</ul>
		</div>
		<div class="mobile-qa">
			<ul class="count-flowr">
				<li>
					<?php if($user_id==11){
						$usercount = count_users();
						$total_users = $usercount['total_users'];
						?>
						<a href="#" id="followers"><?php echo $total_users;?></a>
					<?php }else{?>
						<a href="#" id="followers"><span>Followers</span><?php if($follow['followers']>=0){echo $follow['followers'];}else{ echo '0';}?></a>
					<?php }?>					
				</li>
				<li>
					<a href="#" id="following"><span>Following</span><?php if($follow['following']>=0){echo $follow['following'];}else{ echo '0';}?></a>
				</li>				
			</ul>
		</div>
		<?php
	}
	add_shortcode( 'follow-follower-posts', 'get_count_follow_follower_posts' );

	function get_recent_followers()
	{
		$follow = unserialize( get_user_meta(get_current_user_id(),'follow',true));		
		$followers_list = $follow['followers_list'];
		if(isset($_POST['posts_per_page']))
		{
			$count = $_POST['posts_per_page'];
		}
		else
		{
			$count=5;
		}
		$end = $count;
		if($end>count($followers_list))
		{
			$end = count($followers_list);
		}
		?>
		<h2>Recente followers</h2>
		<ul>
			<?php
			for($i=0;$i<$end;$i++){			
				$userid = $followers_list[$i];
				$userdata = get_userdata($userid);
				?>
				<li>
					<a href="<?php echo esc_url( get_author_posts_url($userdata->ID) ); ?>">
						<img src="<?php echo get_avatar_url($userdata->ID) ?>" alt="<?php echo $userdata->display_name;?>">
						<?php echo $userdata->display_name;?>
					</a>
				</li>
				<?php
			}
			?>
		</ul>
		<?php if(count($followers_list)<=0){ ?>
		<h4>No Recente Followers</h4>
		<?php }?>
		<?php if(count($followers_list)>5){?>
		<div class="get-more">
			<button type="button" name="get_more_friends">Laad meer
				<div class="lds-ellipsis">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</button>
		</div>
		<?php }?>
		<?php
		if(isset($_POST['posts_per_page'])){
			die();
		}
	}
	add_shortcode( 'recent-followers', 'get_recent_followers');
	add_action( 'wp_ajax_get_more_followers', 'get_recent_followers');
	add_action( 'wp_ajax_nopriv_get_more_followers', 'get_recent_followers');
	
	function get_user_followers()
	{
		$follow = unserialize( get_user_meta(get_current_user_id(),'follow',true));
		$followers_list = $follow['followers_list'];
		foreach($followers_list as $userid){
			$userdata = get_userdata($userid);
			$args = array(
				'author'        =>  $userid,
				'posts_per_page' => -1,
				"post_type"        => "osetin_recipe",
				"post_status"      => "publish"
			);
			$posts = count(get_posts($args));
			?>
				<div class="item">
					<ul class="group-menu">
						<li>
							<a href="#"><i class="fa fa-ellipsis-v"></i></a>
							<ul class="group-sub-menu">
								<li><a href="#" class="delete-this-group remove-follower" data-follower='<?php echo $userdata->ID;?>'><i class="fa fa-trash-o"></i> Verwijder</a></li>
							</ul>
						</li>
					</ul>					
					<a href="<?php echo esc_url( get_author_posts_url($userdata->ID) ); ?>">
						<div class="author-image">
							<img src="<?php echo get_avatar_url($userdata->ID) ?>" alt="<?php echo $userdata->display_name;?>">
						</div>
						<div class="author-title"><?php echo $userdata->display_name;?></div>
						<div class="res-count"><?php echo $posts;?> recepten</div>
					</a>
				</div>
			<?php
		}
	}
	add_shortcode( 'get-user-followers', 'get_user_followers');
	
	function get_user_following(){
		$follow = unserialize( get_user_meta(get_current_user_id(),'follow',true));
		$following_list = $follow['following_list'];
		$following_list[] = 11;
		foreach($following_list as $userid){
			$userdata = get_userdata($userid);
			$args = array(
				'author'        =>  $userid,
				'posts_per_page' => -1,
				"post_type"        => "osetin_recipe",
				"post_status"      => "publish"
			);
			$posts = count(get_posts($args));
			?>
				<div class="item">
					<?php if($userid!=11){?>
					<ul class="group-menu">
						<li>
							<a href="#"><i class="fa fa-ellipsis-v"></i></a>
							<ul class="group-sub-menu">
								<li><a href="#" class="hide-following-feed" data-following='<?php echo $userdata->ID;?>' data-status="hide"><i class="fa fa-eye-slash" aria-hidden="true"></i> Unfollow</a></li>
							</ul>
						</li>
					</ul>
					<?php }?>			
					<a href="<?php echo esc_url( get_author_posts_url($userdata->ID) ); ?>">
						<div class="author-image">
							<img src="<?php echo get_avatar_url($userdata->ID) ?>" alt="<?php echo $userdata->display_name;?>">
						</div>
						<div class="author-title"><?php echo $userdata->display_name;?></div>
						<div class="res-count"><?php echo $posts;?> recepten</div>
					</a>
				</div>
			<?php
		}
	}
	add_shortcode( 'get-user-following', 'get_user_following');
	
	/* author recipes in public page */
	add_action( 'pre_get_posts', function ( $athorpost ) {
		if( !is_admin() && $athorpost->is_main_query() && $athorpost->is_author() ) 
		{
			$athorpost->set( 'posts_per_page', 45 );
			$athorpost->set( 'post_type', 'osetin_recipe' );
		}
	});
	
	/* author recipes in public page */
	function cf7_get_author_email($atts){
    $value = '';
    if(get_the_author_meta( 'user_email' )) {
        $value = get_the_author_meta( 'user_email' );
    }
		return $value;
	}
	add_shortcode('CF7_AUTHOR_EMAIL', 'cf7_get_author_email');
	
	/* custom post for notification */		
	function register_notification()
	{
		$args = array(
			'public'    => true,
			'label'     => __( 'Notification', 'twentythirteen' ),
			'menu_icon' => 'dashicons-format-aside',
			'rewrite'   => array( 'slug' => 'kook-notification' ),
			'menu_position' => 10,
			'supports'  => array( 'title', 'thumbnail','author', 'comments' ),
		);
		register_post_type( 'kook-notification', $args );
	}
	add_action('init', 'register_notification');

	function kook_notify_user_by_metabox()
	{
		$html = '<p class="notification-by">';
		$html .= 'Notification By';
		$html .= '</p>';
		$html .= '<input type="text" id="kook_notify_user_by" name="kook_notify_user_by"/>';
		$html .= '<span class="text-danger"></span>';
		echo $html;
	}
	function kook_notify_purpose_for_metabox()
	{
		$html = '<p class="notification-purpose">';
		$html .= 'Purpose For';
		$html .= '</p>';
		$html .= '<input type="text" id="kook_notify_purpose_for" name="kook_notify_purpose_for"/>';
		$html .= '<span class="text-danger"></span>';
		echo $html;
	}
	function kook_notify_purpose_refer_metabox()
	{
		$html = '<p class="notification-purpose">';
		$html .= 'Purpose Refer';
		$html .= '</p>';
		$html .= '<input type="text" id="kook_notify_purpose_refer" name="kook_notify_purpose_refer"/>';
		$html .= '<span class="text-danger"></span>';
		echo $html;
	}
	function notification_add_meta_boxes()
	{		
		add_meta_box('kook-notify-purpose-refer-metabox','Purpose Refer','kook_notify_purpose_refer_metabox','kook-notification','side','low');
		add_meta_box('kook-notify-purpose-for-metabox','Purpose For','kook_notify_purpose_for_metabox','kook-notification','side','low');
		add_meta_box('kook-notify-user-by-metabox','Created By','kook_notify_user_by_metabox','kook-notification','side','low');
	}
	add_action('admin_init','notification_add_meta_boxes');
	function kook_notification_save($data)
	{
		if(isset($data['kook_notify_purpose_for']) && $data['kook_notify_purpose_for']=='new recipe'){
			$post_arg = array(
				//'post_title'    => 'You have created new recipe '.$data['kook_notify_post_title'],
				'post_title'    => 'heeft een nieuw recept geplaatst: '.$data['kook_notify_post_title'],
				'post_type'  => 'kook-notification',
				'post_status'   => 'publish',
				'post_author'   => $data['kook_notify_user_for'],
			);
			//get followers notification
			if($data['kook_notify_user_for']==11){
				$userdetail = get_user_by('id',11);
				$_post_arg = array(
					'post_title'    => $userdetail->display_name.' Posted new recipe '.$data['kook_notify_post_title'],
					'post_type'  => 'kook-notification',
					'post_status'   => 'publish',
					'post_author'   => 11,
				);
				$post_id = wp_insert_post( $_post_arg );
				update_post_meta($post_id,'kook_notify_user_by',$data['kook_notify_user_for']);
				update_post_meta($post_id,'kook_notify_purpose_for',$data['kook_notify_purpose_for']);
				update_post_meta($post_id,'kook_notify_purpose_refer',$data['kook_notify_purpose_refer']);

			}else{
				//non admin
				$follow = unserialize( get_user_meta($data['kook_notify_user_for'],'follow',true));
				$userdetail = get_user_by('id',$data['kook_notify_user_for']);
				if(is_array($follow) && count($follow)>0)
				{
					foreach($follow['followers_list'] as $k=>$followinguserid)
					{
						$_post_arg = array(
							'post_title'    => $userdetail->display_name.' Posted new recipe '.$data['kook_notify_post_title'],
							'post_type'  => 'kook-notification',
							'post_status'   => 'publish',
							'post_author'   => $followinguserid,
						);
						$post_id = wp_insert_post( $_post_arg );
						update_post_meta($post_id,'kook_notify_user_by',$data['kook_notify_user_for']);
						update_post_meta($post_id,'kook_notify_purpose_for',$data['kook_notify_purpose_for']);
						update_post_meta($post_id,'kook_notify_purpose_refer',$data['kook_notify_purpose_refer']);
					}        
				}
			}

		}else{
			if(isset($data['kook_notify_purpose_for']) && $data['kook_notify_purpose_for']=='commented')
			{
				$post_arg = array(
					//'post_title'    => $data['kook_notify_commented_by'].' Er is gereageerd op jouw recept '.$data['kook_notify_post_title'],
					'post_title'    => ' Er is gereageerd op jouw recept '.$data['kook_notify_commented_by'],
					'post_type'  => 'kook-notification',
					'post_status'   => 'publish',
					'post_author'   => $data['kook_notify_user_for'],
				);
				if($data['kook_notify_user_for']==11){
					$post_arg['post_author'] = 11;
				}

			}
			if(isset($data['kook_notify_purpose_for']) && $data['kook_notify_purpose_for']=='following')
			{
				$post_arg = array(
					'post_title'    => 'Je volgt nu: '.$data['kook_notify_display_name'],
					'post_type'  => 'kook-notification',
					'post_status'   => 'publish',
					'post_author'   => $data['kook_notify_user_for'],
				);			
			}
			if(isset($data['kook_notify_purpose_for']) && $data['kook_notify_purpose_for']=='followed')
			{
				$post_arg = array(
					'post_title'    => 'Je wordt nu gevolgd door '.$data['kook_notify_display_name'],
					'post_type'  => 'kook-notification',
					'post_status'   => 'publish',
					'post_author'   => $data['kook_notify_user_for'],
				);
			}
			if(isset($data['kook_notify_purpose_for']) && $data['kook_notify_purpose_for']=='saved favorite')
			{
				$post_arg = array(
					'post_title'    => 'Je hebt het volgende recept opgeslagen: '.$data['kook_notify_post_title'],
					'post_type'  => 'kook-notification',
					'post_status'   => 'publish',
					'post_author'   => $data['kook_notify_user_for'],
				);
			}
			$post_id = wp_insert_post( $post_arg );
			update_post_meta($post_id,'kook_notify_user_by',$data['kook_notify_user_by']);
			update_post_meta($post_id,'kook_notify_purpose_for',$data['kook_notify_purpose_for']);
			update_post_meta($post_id,'kook_notify_purpose_refer',$data['kook_notify_purpose_refer']);
		}
		return true;
	}

	function trigger_on_save_post($new_status, $old_status, $post)
	{
		 if ( ( 'publish' === $new_status && 'publish' !== $old_status ) && 'osetin_recipe' === $post->post_type) {
            $data = array(
				'kook_notify_user_for'=>$post->post_author,
				'kook_notify_user_by'=>$post->post_author,
				'kook_notify_purpose_for'=>'new recipe',
				'kook_notify_purpose_refer'=>$post->ID,
				'kook_notify_post_title'=>$post->post_title
			);
			kook_notification_save($data);	
   		 }
	}
	add_action( 'transition_post_status', 'trigger_on_save_post',10,3 );
	
	function trigger_on_post_comment($comment_id )
	{
		
		$comment = get_comment( $comment_id );
		$post = get_post($comment->comment_post_ID);
		$author = get_user_by('email',$comment->comment_author_email);
		
		$data = array(
			'kook_notify_user_for'=>$post->post_author,
			'kook_notify_user_by'=>$author->ID,
			'kook_notify_purpose_for'=>'commented',
			'kook_notify_purpose_refer'=>$post->ID,
			'kook_notify_post_title'=>$post->post_title,
			'kook_notify_commented_by'=>$author->display_name
		);
		kook_notification_save($data);
	}
	add_action('comment_post','trigger_on_post_comment');

	function get_feed_init()
	{
		
		$args = array(
			//'author'           =>  get_current_user_id(),
			'author__in'           =>  array(get_current_user_id(),11),
			"posts_per_page"   => 5,
			"paged"            => 1,
			"orderby"          => "post_date",
			"order"            => "DESC",
			"post_type"        => "kook-notification",
			"post_status"      => "publish"
		);
		if(isset($_POST['posts_per_page']))
		{
			$args['posts_per_page'] = $_POST['posts_per_page'];
		}
		$feeds = get_posts($args);
		$link = '#';
		?>
		<h2>Feed</h2>
		<ul>
			<?php if(count($feeds)<=0){?>
			<h4>No Feeds</h4>
			<?php }?>
			<?php
			$visible=true;
			$hide_following = unserialize( get_post_meta(get_current_user_id(),'hide_following',true));
			foreach($feeds as $feed)
			{
					
					$kook_notify_purpose_for = get_post_meta($feed->ID,'kook_notify_purpose_for',true);
					$kook_notify_purpose_refer = get_post_meta($feed->ID,'kook_notify_purpose_refer',true);

					$kook_notify_user_by = get_post_meta($feed->ID,'kook_notify_user_by',true);

					if(isset($hide_following[$kook_notify_user_by])){
						if($hide_following[$kook_notify_user_by]){
							$visible=false;
						}else{
							$visible=true;
						}
						
					}else{
						$visible=true;
					}

					if($kook_notify_purpose_for=='following' || $kook_notify_purpose_for=='followed'){
						$link = get_author_posts_url( $kook_notify_purpose_refer );
					}
					if($kook_notify_purpose_for=='new recipe' || $kook_notify_purpose_for=='commented' || $kook_notify_purpose_for=='saved favorite')
					{
						$link = get_permalink($kook_notify_purpose_refer);
						//echo $kook_notify_purpose_for;
					}
					if($visible){
				?>
				<li>
					<a href="<?php echo $link;?>">
						<i class="fa fa-rss-square"></i>
						<?php echo $feed->post_title;?>
					</a>
				</li>
				<?php
					}		
			}
			$args = array(
				'author'           =>  get_current_user_id(),
				"posts_per_page"   => -1,
				"orderby"          => "post_date",
				"order"            => "DESC",
				"post_type"        => "kook-notification",
				"post_status"      => "publish"
			);
			$feeds = get_posts($args);
			?>
		</ul>
		<?php if(count($feeds)>5){?>
			<div class="get-more">
				<button type="button" name="get_more_feeds">Laad meer
					<div class="lds-ellipsis">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				</button>
			</div>
		<?php
		}
		if(isset($_POST['posts_per_page']))
		{
			die();
		}
	}
	add_shortcode('init-feeds', 'get_feed_init');
	add_action( 'wp_ajax_get_more_feeds', 'get_feed_init');
	add_action( 'wp_ajax_nopriv_get_more_feeds', 'get_feed_init');
	
	/* pagination for archive post */
	add_action('pre_get_posts', 'archive_paginations');
	function archive_paginations($query)
	{
		if ( !is_admin() && $query->is_archive() && $query->is_main_query() ) 
		{
			global $wp_query;
			$cat = $wp_query->get_queried_object();
		   $query->set( 'post_type', array( 'osetin_recipe', 'post' ) );
		   $query->set( 'posts_per_page', '8' );
		   $query->set( 'cat', $cat->slug );
		}
		return $query;
	}

	add_shortcode('liked-recipes',function($attr){
		$args = array(
			'post_type' => 'osetin_recipe',
			'meta_query'=> array(
				array(
					'key' =>'_osetin_vote',
					'value'=>0,
					'compare'=>'>',
				),
			),
			'posts_per_page' =>-1,
			/*
			'meta_key' => '_osetin_vote',
			'meta_compare' => '>',
			'meta_value' => 0,
			
			*/
		);
		
		$posts = get_posts($args);
		
		foreach ($posts as $key => $value){
			$vote = osetin_vote_get_post_vote_data($value->ID);
			if($vote['has_voted'])
			{
				echo '<ul>';
				echo '<li class="ingr-name"><a href="'.get_permalink( $value->ID ).'" ><span>'.get_the_title( $value->ID).'</span></a></li>';
				echo '</ul>';
			}
		}
		
	});
	
	add_shortcode('z-form',function($attr)
	{
		$atts = shortcode_atts( array(
			'mail' => 'kumarchandang007@gmail.com',
		), $attr );
		?>
		<form method="post" id="author_msg">
			<div class="form-group">
				<input type="text" class="form-control" name="name" id="exampleFormControlInput1" placeholder="Uw naam">
			</div>
			<div class="form-group">
				<input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="Jouw email">
			</div>
			<div class="form-group">
				<textarea name="msg" cols="40" rows="10" class="form-control" placeholder="Schrijf een bericht"></textarea>
			</div>
			<input type="hidden" name="to_mail" value="<?php echo $attr['mail']; ?>"/>
		</form>
		<div class="form msg">Thank you</div>
		<?php
	}); 

	/*add_action( 'wpcf7_before_send_mail', function($contact_form)
	{
		if($contact_form->id() == 35526){
			global $author;
			$submission = WPCF7_Submission::get_instance();
			$toEmail = $author->user_email;
			$mailProp = $contact_form->get_properties('mail');
			$mailProp['mail']['messages'] = 'hello';
			$mailProp['mail']['recipient'] = $toEmail; 
			$contact_form->set_properties(array('mail' => $mailProp['mail']));
		}
		
	});  */

	add_action("wp_ajax_z_author_msg", "z_author_msg");
	add_action("wp_ajax_nopriv_z_author_msg", "z_author_msg");

	function z_author_msg()
	{
		parse_str($_POST['data'], $postdata);
		$to_email = $postdata['to_mail'];
		$from_email = $postdata['email'];
		$name = $postdata['name'];
		$msg = $postdata['msg'];
		$body = 'Name : '.$name.'<br>';
		$body .= 'Email : '.$from_email.'<br>';
		$body .= 'Message '.$msg;
		$sub = 'User From kookmutsjes';
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$mail = wp_mail($to_email,$sub,$body,$headers);
		if($mail)
		{
			wp_send_json_success('Message sent successfully!');
		}
		else
		{
			wp_send_json_success('Message can not be delivered');
		}
		wp_die();	
	} 

	function z_manage_field_types( $fields ) 
	{
		$fields[] = 'Custom';
		return $fields;
	}
	add_filter( 'wppb_manage_fields_types', 'z_manage_field_types' );

	function z_save_multiple_select2_value( $field, $user_id, $request_data, $form_location )
	{
		if( $field['field'] == 'Custom' )
		{
			if( isset( $request_data[wppb_handle_meta_name( $field['meta-name'] )] ) ) {
				$selected_values = wppb_process_multipl_select2_value($field, $request_data);
				update_user_meta($user_id, sanitize_text_field($field['meta-name']), $request_data[wppb_handle_meta_name( $field['meta-name'] )]);
			}
		}
	}
	add_action( 'wppb_save_form_field', 'z_save_multiple_select2_value', 10, 4 );
	add_action( 'wppb_backend_save_form_field', 'z_save_multiple_select2_value', 10, 4 );

	function z_select2_multiple_handler( $output, $form_location, $field, $user_id, $field_check_errors, $request_data )
	{
		if ( $field['field'] == 'Custom' )
		{
			$arguments = array();
			$arguments['maximumSelectionLength'] = 0;
			$arguments['maximumSelectionSize'] = 0; // Backwards compatibility with Select2 v.3.5.3, loaded by WooCommerce
			if ( isset($field['select2-multiple-tags']) && ('yes' == $field['select2-multiple-tags']) ) 
			{
				$arguments['tags'] = true;
				add_filter('wppb_select2_multiple_options','wppb_select2_multiple_enable_tags', 10, 6 );
			}
			if ( isset($field['select2-multiple-limit']) && ('' != $field['select2-multiple-limit']) && is_numeric($field['select2-multiple-limit']) ) 
			{
				$arguments['maximumSelectionLength'] = $field['select2-multiple-limit'];
				$arguments['maximumSelectionSize'] = $field['select2-multiple-limit']; // Backwards compatibility with Select2 v.3.5.3, loaded by WooCommerce
			}
			$opt = $field;
			$field['labels']= apply_filters( 'wppb_select2_multiple_labels', $field['labels'], $form_location, $field, $user_id, $field_check_errors, $request_data );
			$field['options'] = apply_filters( 'wppb_select2_multiple_options', $field['options'], $form_location, $field, $user_id, $field_check_errors, $request_data );
			$arguments = apply_filters('wppb_select2_multiple_arguments', $arguments, $form_location, $field, $user_id, $field_check_errors, $request_data);
			$item_title = apply_filters( 'wppb_'.$form_location.'_select2_multiple_custom_field_'.$field['id'].'_item_title', wppb_icl_t( 'plugin profile-builder-pro', 'custom_field_'.$field['id'].'_title_translation', esc_attr( $field['field-title'] ) ) );
			$item_description = wppb_icl_t( 'plugin profile-builder-pro', 'custom_field_'.$field['id'].'_description_translation', wp_kses_post( $field['description'] ) );
			$item_option_labels = wppb_icl_t( 'plugin profile-builder-pro', 'custom_field_'.$field['id'].'_option_labels_translation', $field['labels'] );
			$select2_labels = apply_filters( 'wppb_select2_multiple_labels_array', explode( ',', $item_option_labels ) );
			$select2_values = apply_filters( 'wppb_select2_multiple_options_array', explode( ',', $field['options'] ) );
			$extra_attr = apply_filters( 'wppb_extra_attribute', '', $field, $form_location );
			if( $form_location != 'register' )
				$input_value = ( ( wppb_user_meta_exists ( $user_id, $field['meta-name'] ) != null ) ? array_map( 'trim', explode( ',', get_user_meta( $user_id, $field['meta-name'], true ) ) ) : array_map( 'trim', explode( ',', $field['default-options'] ) ) );
			else
				$input_value = ( isset( $field['default-options'] ) ? array_map( 'trim', explode( ',', $field['default-options'] ) ) : array() );
			$input_value = ( isset( $request_data[wppb_sl2_multiple_handle_meta_name( $field['meta-name'] )] ) ? array_map( 'esc_attr', (array)$request_data[wppb_sl2_multiple_handle_meta_name( $field['meta-name'] )] ) : $input_value );
			if ( $form_location != 'back_end' ){
				$error_mark = ( ( $field['required'] == 'Yes' ) ? '<span class="wppb-required" title="'.wppb_required_field_error($field["field-title"]).'">*</span>' : '' );	
				if ( array_key_exists( $field['id'], $field_check_errors ) )
					$error_mark = '<img src="'.WPPB_PLUGIN_URL.'assets/images/pencil_delete.png" title="'.wppb_required_field_error($field["field-title"]).'"/>';
				$val = get_user_meta( $user_id, $field['meta-name'], true );
				$output = '
					<label for="'.esc_attr( $field['meta-name'] ).'">'.$item_title.$error_mark.'</label>
					<input type="text" value="'.$val.'" name="' . esc_attr($field['meta-name'] ) . '"><pre>'.serialize($field['options']).'</pre>';
				if( !empty( $item_description ) )
					$output .= '<span class="wppb-description-delimiter">'.$item_description.'</span>';
			}
			else
			{
				//comment this out for now...I don't think we need it anymore 
				/*if ( class_exists( 'WooCommerce' ) ) {
					// WooCommerce loads an older version of Select2 library which does not support tags
					unset($arguments['tags']);
				}*/
				$item_title = ( ( $field['required'] == 'Yes' ) ? $item_title .' <span class="description">('. __( 'required', 'profile-builder' ) .')</span>' : $item_title );
				$val = get_user_meta( $user_id, $field['meta-name'], true );
				$output = '
					<table class="form-table wppb-select2">
						<tr>
							<th><label for="'. esc_attr( $field['meta-name'] ).'">'.$item_title.'</label></th>
							<td>
								<input type="text" value="'.$val.'" name="' . esc_attr($field['meta-name'] ) . '">
								<span class="description">'.$item_description.'</span>
							</td>
						</tr>
					</table>';
			}
			return apply_filters( 'wppb_'.$form_location.'_select2_multiple_custom_field_'.$field['id'], $output, $form_location, $field, $user_id, $field_check_errors, $request_data, $input_value );
		}
	}
	add_filter( 'wppb_output_form_field_custom', 'z_select2_multiple_handler', 10, 6 );
	add_filter( 'wppb_admin_output_form_field_custom', 'z_select2_multiple_handler', 10, 6 );

	// add_filter('wppb_prepopulated_fields',function($prepopulated_fields){
	//       $prepopulated_fields[] = array( 'field' => 'Text', 'field-title' => __( 'Text', 'profile-builder' ), 'meta-name' => '',	'overwrite-existing' => 'No', 'id' => '20', 'description' => '', 'row-count' => '5', 'allowed-image-extensions' => '.*',	'allowed-upload-extensions' => '.*', 'avatar-size' => '100', 'date-format' => 'mm/dd/yy', 'terms-of-agreement' => '', 'options' => '', 'labels' => '', 'public-key' => '', 'private-key' => '', 'default-value' => '', 'default-option' => '', 'default-options' => '', 'default-content' => '', 'required' => 'No' );  
	// });

	function upload_file()
	{
		ob_clean();
		if ( ! function_exists( 'wp_handle_upload' ) ) 
		{
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		ob_clean();	 
		$uploadedfile = $file; 
		$upload_overrides = array(
			'test_form' => false
		);
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides ); 
		$filename = $movefile['file'];
		$attachment = array(
			'post_mime_type' => $movefile['type'],
			'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content' => '',
			'post_status' => 'inherit',
			'guid' => $movefile['url']
		);
		$attachment_id = wp_insert_attachment( $attachment, $movefile['url'] );
		echo $attachment_id;
		wp_die();
	}

	if (!function_exists('kkm_recipe_save_post')) 
	{
		function kkm_recipe_save_post()
		{
			$post_args = array(
		    'post_title'    => sanitize_text_field( $_POST['recipe_name'] ),
		    'post_content'  => sanitize_text_field( $_POST['recipe_desc'] ),
		    'post_status'   => 'pending',
		    'post_type' 	=> 'osetin_recipe',
		    'post_category' => $_POST['cat']
			);	
			$steps = $_POST['step'];
			$ingrs = $_POST['ing_name'];
			$stp = $ingradients = array();
			if(!empty($steps))
			{
				foreach ($steps as $key => $step) 
				{
					$stp[] = array('step_description' => $step);
				}
			}
			if(!empty($ingrs))
			{
				foreach ($ingrs as $key => $ingr) 
				{
					$ingradients[] = array(
						'ingredient_name' => $ingr,
						'ingredient_amount' => $_POST['ing_qnt'][$key],
						'separator' => $_POST['ing_splitser'][$key]
					);
				}
			}
			$tips = sanitize_text_field( $_POST['tips'] );
			$bakingmold = sanitize_text_field( $_POST['baking_mold'] );
			// echo '<pre>';
			// var_dump($_POST['ing_splitser']);
			// echo '</pre>';
			// return true;
			if(isset($_POST['post_id']) && !empty($_POST['post_id']))
			{
				wp_send_json_success( array( 'success' =>true ,'html'=>'') );
			}
			else
			{
				$post_id =  wp_insert_post( $post_args );
				$_REQUEST['post_id'] = $post_id;
				$gallery = explode(',', $_POST['rec_images']);
				set_post_thumbnail($post_id,$gallery[0]);
				//update_field( 'field_5d8cc2b90d30c', $gallery , $post_id );
				update_field( 'recipe_serves', $_POST['recipe_serves'], $post_id );
				update_field( 'recipe_difficulty', $_POST['recipe_difficulty'], $post_id );
				update_field( 'recipe_preparation_time', $_POST['recipe_preparation_time'], $post_id );
				update_field( 'steps', $stp, $post_id );
				update_field( 'ingredients', $ingradients, $post_id );
				update_field( 'Tips', $tips, $post_id );
				update_field( 'baking_mold', $bakingmold, $post_id );
				$html = '<p class="success"><a href="'.get_permalink($post_id).'">'.$_POST['recipe_name'].' has benn suceessfully added</a></p>';
				wp_send_json_success( array( 'success' =>true ,'html'=>$html) );
			}
		}	
	}
	add_action( 'wp_ajax_kkm_recipe_save_post', 'kkm_recipe_save_post');
	add_action( 'wp_ajax_nopriv_kkm_recipe_save_post', 'kkm_recipe_save_post');

	//add_action( 'save_recipe', 'kkm_recipe_save_post' );
	if(!function_exists('kkm_recent_msg_users'))
	{
		function kkm_recent_msg_users()
		{
			$list_users_chat_with = unserialize(get_user_meta(get_current_user_id(),'list_users_chat_with',true));
			?>
			<div class="user-chat-html-inbox">
				<?php if(is_array($list_users_chat_with) && count($list_users_chat_with)>0){ $user_count = 1; ?>
					<?php foreach($list_users_chat_with as $userdetail){ if($user_count >5) break; ?>
						<?php $chatuser = get_user_by('id',$userdetail['userid']);?>
						<?php if($chatuser){?>
							<ul data-userid="<?php echo $userdetail['userid'];?>" id="<?php echo $userdetail['userid'];?>">
								<li class="chat-user-detail">
									<?php
									$name = ($chatuser->user_firstname!='') ? _($chatuser->user_firstname) : _($chatuser->user_login); ?>
									<a onclick="openCity(event, 'inbox')" href="javascript:void(0);"><i class="fa fa-envelope"></i> Nieuw bericht van <?=$name?></a>
								</li>
							</ul>
						<?php }?>
					<?php $user_count++;  }?>
				<?php }?>
			</div>
		<?php
		}
	}
	add_shortcode( 'recent-messages-users', 'kkm_recent_msg_users' );

	add_action('comment_post', 'kkm_on_add_new_comment', 10, 3);
	function kkm_on_add_new_comment($comment_id) 
	{
		$comment = get_comment($comment_id);		
		$post = get_post($comment->comment_post_ID);		
		$post_id = $post->ID;
		if($post->post_type=='osetin_recipe')
		{
			$cmnt_id = $comment->comment_ID;
			$ftr_image = get_post_thumbnail_id( $post_id );
			$author_id = $comment->comment_author;
			$am = get_the_author_meta('user_login',$author_id);
			$author_name = ($am->user_firstname != '' || $am->user_lastname != '') ? $am->user_firstname.' '.$am->user_lastname : $author_id;
			$_post_args = array(
			  'post_title'    => get_the_title($post_id),
			  'post_type' 	  => 'testimonial',
			  'post_content'  => $comment->comment_content,
			  'post_status'   => 'publish',
			); 
			$is_exists = is_texnomy_by_commnet_id($cmnt_id);
			if($is_exists)
			$_postid = $is_exists;
			else
			$_postid = wp_insert_post( $_post_args );
			update_post_meta( $_postid, '_cite',  '-'.$author_name);
			update_post_meta($_postid, 'commment_id', $cmnt_id);
			update_post_meta( $_postid, '_rotator_id',  '|35128|');
			set_post_thumbnail( $_postid, $ftr_image );	
		}
	}
	
	function is_texnomy_by_commnet_id($cmnt_id)
	{
		$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'testimonial',
		'meta_key'         => 'commment_id',
		'meta_value'       => $cmnt_id
		);
		$posts = get_posts( $args );
		return (!empty($posts)) ? $posts[0]->ID : false;
	}

	function remove_medialibrary_tab($strings) {
        if ( !current_user_can( 'administrator' ) || current_user_can( 'site_admin' )) {
            unset($strings["mediaLibraryTitle"]);
        return $strings;
        }
        else
        {
            return $strings;
        }
    }
	add_filter('media_view_strings','remove_medialibrary_tab');
	function home_mobile_slider(){
		?>
		<ul class="home-mobile-slider" id="home-mobile-slider">
			<?php
			$args = array(
			  'post_type'   => 'sliderecepi',
			  'post_status' => 'publish',
			  'posts_per_page' => 3,
			 );
			 
			$sliderecepi = new WP_Query( $args );
			if( $sliderecepi->have_posts() ) :
			?>
			<?php
			while( $sliderecepi->have_posts() ) :
			$sliderecepi->the_post();
			?>	
			<li>
				<a href="<?php the_field('recipe_url'); ?>">
					
					<?php if( get_field('recipe_image') ): ?>
						<img src="<?php the_field('recipe_image'); ?>" />
					<?php endif; ?>
					<div class="slider-extra">
						<span><i class="vc_tta-icon fa fa-user"></i> <?php the_author(); ?> | <i class="vc_tta-icon fa fa-clock-o" aria-hidden="true"></i> <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time></span>
						<h2><?php the_field('recipe_title'); ?></h2>						
					</div>
				</a>
			</li>
			<?php
			  endwhile;
			  wp_reset_postdata();
			?>	
			<?php
			else :
			  esc_html_e( 'No testimonials in the diving taxonomy!', 'text-domain' );
			endif;
			?>
		</ul>
		<?php
	}
	add_shortcode('home-mobile-slider','home_mobile_slider');

	function webz_community_slider(){
		?>
		<ul class="home-mobile-slider" id="webz-community-slider">
			<?php
			$administrator = get_users(array('role__in' => 'administrator','fields' => 'ID'));
			$siteadmins = get_users(array('role__in' => 'site_admin','fields' => 'ID'));
			$admins = array_merge($administrator,$siteadmins);
			$args = array(
				'post_type'   => 'osetin_recipe',
				'post_status' => 'publish',
				'author__not_in' => $admins,
				'posts_per_page' => 3,
			);

			$sliderecepi = new WP_Query( $args );
			if( $sliderecepi->have_posts() ) :
			?>
			<?php
			while( $sliderecepi->have_posts() ) :
			$sliderecepi->the_post();
			?>	
			<li>
				<a href="<?php echo get_permalink(); ?>">
					<div class="slider-extra">
						
						<h2><?php the_title(); ?></h2>						
					</div>
					<div class="img" style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)">
					
				</a>
			</li>
			<?php
			  endwhile;
			  wp_reset_postdata();
			?>	
			<?php
			else :
			  esc_html_e( 'No testimonials in the diving taxonomy!', 'text-domain' );
			endif;
			?>
		</ul>
		<?php
	}
	add_shortcode('webz-community-slider','webz_community_slider');
	function webz_community_mobile_slider(){
		?>
		<ul class="home-mobile-slider" id="home-mobile-slider">
			<?php
			$administrator = get_users(array('role__in' => 'administrator','fields' => 'ID'));
			$siteadmins = get_users(array('role__in' => 'site_admin','fields' => 'ID'));
			$admins = array_merge($administrator,$siteadmins);
			$args = array(
				'post_type'   => 'osetin_recipe',
				'post_status' => 'publish',
				'author__not_in' => $admins,
				'posts_per_page' => 3,
			);
			 
			$sliderecepi = new WP_Query( $args );
			if( $sliderecepi->have_posts() ) :
			?>
			<?php
			while( $sliderecepi->have_posts() ) :
			$sliderecepi->the_post();
			?>	
			<li>
				<a href="<?php echo get_permalink(); ?>">
					<div class="slider-extra">						
						<h2><?php the_title(); ?></h2>						
					</div>
					<img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" />
				</a>
			</li>
			<?php
			  endwhile;
			  wp_reset_postdata();
			?>	
			<?php
			else :
			  esc_html_e( 'No testimonials in the diving taxonomy!', 'text-domain' );
			endif;
			?>
		</ul>
		<?php
	}
	add_shortcode('community-mobile-slider','webz_community_mobile_slider');

	function search_ingradients_by_title( $where, &$wp_query )
	{
		global $wpdb;
		if ( $search_term = $wp_query->get( 'search_ingradients_title' ) ) 
		{
			$search_term = $wpdb->esc_like($search_term);
			$search_term = ' \'%' . $search_term . '%\'';
			$where .= ' AND ' . $wpdb->posts . '.post_title LIKE '.$search_term;
		}
		return $where;
	}
	add_filter( 'posts_where', 'search_ingradients_by_title', 10, 2 );
	function get_webz_ingridents(){
		$term = $_REQUEST['term'];
		$posts = get_posts( 
			array(
				'search_ingradients_title'=> $term,
				'post_type'   => 'ingradients',
				'orderby'	  => 'post_title',
				'order'		  => 'ASC',
				'post_per_page'=>-1,
				'suppress_filters'=>FALSE,
				)
			);
			$result = array();
		foreach($posts as $post){
			$result[] = array('id'=>$post->ID,'text'=>$post->post_title);
		}
		wp_send_json_success ($result) ;
	}
	add_action( 'wp_ajax_get_webz_ingridents', 'get_webz_ingridents');
	add_action( 'wp_ajax_nopriv_get_webz_ingridents', 'get_webz_ingridents');

	function webz_multi_image_upload(){

		$error='';
		// WordPress environment
		require( dirname(__FILE__) . '/../../../wp-load.php' );

		$wordpress_upload_dir = wp_upload_dir();
		// $wordpress_upload_dir['path'] is the full server path to wp-content/uploads/2017/05, for multisite works good as well
		// $wordpress_upload_dir['url'] the absolute URL to the same folder, actually we do not need it, just to show the link to file
		$i = 1; // number of tries when the file with the same name is already exists
		$profilepicture = $_FILES['upload_image'];
		$new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];
		$new_file_mime = mime_content_type( $profilepicture['tmp_name'] );
		$uploadimageerror = false;

		if( empty( $profilepicture ) ){
			$error = 'File is not selected.';
			$uploadimageerror = true;
		}
		

		if( $profilepicture['error'] ){
			$error = $profilepicture['error'];
			$uploadimageerror = true;
		}
		

		if( $profilepicture['size'] > wp_max_upload_size() ){
			$error = 'It is too large than expected.';
			$uploadimageerror = true;
		}

		if( !in_array( $new_file_mime, get_allowed_mime_types() ) ){
			$error = 'WordPress doesn\'t allow this type of uploads.('.$new_file_mime.')';
			$uploadimageerror = true;
		}

		while( file_exists( $new_file_path ) ) {
			$i++;
			$new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profilepicture['name'];
		}
		// looks like everything is OK
		$upload_id=0;
		if( move_uploaded_file( $profilepicture['tmp_name'], $new_file_path ) ) {

			$upload_id = wp_insert_attachment( array(
			'guid'           => $new_file_path, 
			'post_mime_type' => $new_file_mime,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', $profilepicture['name'] ),
			'post_content'   => '',
			'post_status'    => 'inherit'
			), $new_file_path );
			
			// wp_generate_attachment_metadata() won't work if you do not include this file
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			// Generate and save the attachment metas into the database
			wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
		}
		$html = '<div class="col-sm-3 preview-image preview-show-' . $upload_id . '">
		<div class="image-cancel" data-no="' . $upload_id . '">x</div>
		<div class="image-zone"><img id="pro-img-' . $upload_id . '" src="' . wp_get_attachment_url($upload_id) . '"></div>
		</div>';
		wp_send_json_success(array('success'=>true,'html'=>$html));
	}
	add_action( 'wp_ajax_webz_multi_image_upload', 'webz_multi_image_upload');
	add_action( 'wp_ajax_nopriv_webz_multi_image_upload', 'webz_multi_image_upload');

    add_image_size( 'small-prev-menu', 154, 144, true );
    add_image_size( 'big-prev-menu', 555, 333, true );
	
    add_image_size( 'menu-home-page-image', 650, 800, true );
    //add_image_size( 'big-prev-menu', 555, 333, true );
	
	
	function my_list_child_pages() { 
		global $post;
			if ( is_page() && $post->post_parent )
				$childpages = wp_list_pages( 'sort_column=post_titl&title_li=&child_of=' . $post->post_parent . '&echo=0' );
			else
				$childpages = wp_list_pages( 'sort_column=post_titl&title_li=&child_of=' . $post->ID . '&echo=0' );
			if ( $childpages ) {
				$string = '<ul>' . $childpages . '</ul>';
			}
		return $string;
	}
	add_shortcode('my_childpages', 'my_list_child_pages');
	
	function wp_list_pages_filter($output) {
	  $output = str_replace('page_item', 'page_item menu__item', $output);
	  return $output;
	}
	add_filter('wp_list_pages', 'wp_list_pages_filter');
	


	function breadcrumb_menus() {

	$separator          = '&nbsp; &gt; &nbsp;';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs__list';
    $home_title         = 'Home';
    $custom_taxonomy    = 'product_cat';
       
    global $post,$wp_query;
       
    if ( !is_front_page() ) {
       
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            $post_type = get_post_type();
              
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            $post_type = get_post_type();
              
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $category = get_the_category();
             
            if(!empty($category)) {
              
                $last_category = end(array_values($category));
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
            }
              
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
            }
              
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
            }
              
        } else if ( is_category() ) {
               
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            if( $post->post_parent ){
                $anc = get_post_ancestors( $post->ID );
                $anc = array_reverse($anc);
                   
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                echo $parents;
                   
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
            }
               
        } else if ( is_tag() ) {
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
              
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            global $author;
            $userdata = get_userdata( $author );
               
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            echo '<li>' . 'Error 404' . '</li>';
        }
        echo '</ul>';
    }
}


/*menu breadcrumbs*/

function breadcrumb_nmenus() {

	$separator          = '&nbsp; &gt; &nbsp;';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs__list';
    $home_title         = 'Home';
    $custom_taxonomy    = 'product_cat';
       
    global $post,$wp_query;
       
    if ( !is_front_page() ) {
       
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            $post_type = get_post_type();
              
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            $post_type = get_post_type();
              
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $category = get_the_category();
             
            if(!empty($category)) {
              
                $last_category = end(array_values($category));
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
            }
              
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
            }
              
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
            }
              
        } else if ( is_category() ) {
               
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            if( $post->post_parent ){
                $anc = get_post_ancestors( $post->ID );
                $anc = array_reverse($anc);
                   
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                echo $parents;
                   
                //echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
            }
               
        } else if ( is_tag() ) {
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
              
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            global $author;
            $userdata = get_userdata( $author );
               
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            echo '<li>' . 'Error 404' . '</li>';
        }
        echo '</ul>';
    }
}
	function webz_list_comments($comment,$args,$depth)
	{
		if ( 'div' == $args['style'] ) 
		{
			$tag = 'div';
			$add_below = 'comment';
		} 
		else 
		{
			$tag = 'li';
			$add_below = 'div-comment';
		}
	?>
		<div <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
			<article class="comment-body">
				<div class="comment-author vcard">
					<?php //var_dump($comment);?>
					<span class="avatar">
						<img src="<?php echo get_avatar_url($comment->comment_author_email); ?>" alt="<?php echo $comment->comment_author; ?>">
					</span>
					<b class="fn"><?php echo $comment->comment_author; ?></b>
				</div>
				<div class="comment-metadata">
					<a href="<?php echo esc_url( get_comment_link() );?>">
						<time datetime="2019-08-29T21:20:15+02:00">
							<!--29/08/2019 at 21:20-->
							<?php
								echo get_comment_date( 'F j, Y' ).' at '.get_comment_date( 'H:i' );
							?>
						</time>
					</a>
				</div>
				<div class="comment-content">
					<p><?php comment_text()//echo $comment->comment_content;?></p>
				</div>
				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					
				</div>
			</article>
		</div>
	<?php
}

/* pdf liberady */
require('fpdf182/fpdf.php');
class PDF extends FPDF{
	// Load data
	function LoadData($file)
	{
		// Read file lines
		$lines = file($file);
		$data = array();
		foreach($lines as $line)
			$data[] = explode(';',trim($line));
		return $data;
	}


	// Better table
	function ImprovedTable($header, $data)	{
		// Column widths
		$w = array(160);
		// Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row,'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}
}

