<?php
/**
 * Template Name: Weekle Menu Upgrade
 *
 */
get_header();
?>

<?php if(is_page('week-menu-2')){
wp_deregister_script('jquery'); 
wp_register_script('jquery', ("https://code.jquery.com/jquery-3.3.1.min.js"), false);
wp_enqueue_script('jquery');
echo '<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>';
echo '<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>';    
echo '<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,700,700i&display=swap" rel="stylesheet">';    
echo '<style>

@font-face {
  font-family: "Bison";
  font-weight: bold;
  src: url("fonts/Bison-Bold.woff2") format("woff2"), url("../fonts/Bison-Bold.woff") format("woff");
}

@font-face {
  font-family: "NewsCycle";
  src: url("fonts/NewsCycle.woff2") format("woff2"), url("../fonts/NewsCycle.woff") format("woff");
}

@font-face {
  font-family: "NewsCycle";
  font-weight: bold;
  src: url("fonts/NewsCycle-Bold.woff2") format("woff2"), url("../fonts/NewsCycle-Bold.woff") format("woff");
}
</style>';    
}
?>

<?php while ( have_posts() ) : the_post(); ?>


  <div class="os-container top-bar-w">
    <div class="top-bar <?php if(!osetin_is_imaged_header(get_the_ID())) echo 'bordered'; ?>">
		<ul>
			<li><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></li>
			<li class="page-top-title"><h2 id="pagetitles"><?php echo osetin_get_the_title(get_the_ID()); ?></h2></li>
		</ul>
    </div>
  </div>

  <?php  
  if(osetin_is_imaged_header(get_the_ID())){
    if(osetin_is_bbpress()){
      $page_bg_image_url = get_template_directory_uri().'/assets/img/patterns/flowers_light.jpg';
    }else{
      $page_bg_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
    } ?>
    <div class="os-container">
      <div class="page-intro-header with-background" style="<?php echo osetin_get_css_prop('background-image', $page_bg_image_url, false, 'background-repeat: repeat; background-position: top left; '); ?>">
        <h2><?php echo osetin_get_the_title(get_the_ID()); ?></h2>
      </div>
    </div>
    <?php
  }
  ?>

<section class="hero" style="background-image: url('<?php the_field('menu_hero_page_image'); ?>')">
    <div class="container">
        <div class="hero__title">
            <h1><?php the_title();?></h1>
        </div>
    </div>
</section>

  <div class="os-container">
  <div class="main__content">
            <div class="main__text">
                <h1 class="main__title">
                   <?php the_field('title'); ?>
                </h1>
					<?php the_field('small_description'); ?>
            </div>
        </div>
    <div class="page-w <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) echo 'with-sidebar sidebar-location-right'; ?>">
	  <div class="page-content">
	
	<?php
		$args = array(
			'post_type'   => 'weekmenu-upd',
			'order' => 'ASC',
			'post_status' => 'publish',
		 );
						 
	$weekmenu = new WP_Query( $args ); if( $weekmenu->have_posts() ) : while( $weekmenu->have_posts() ) : $weekmenu->the_post();?>				
    <!----------------------start blocks -------------------->   
    
	<div class="menu-box">
      <div class="menu-box__title">
        <h1><?php the_title();?></h1>
      </div>
		<?php $myposts = get_field('today_name');
        if ($myposts):?>
		<?php  foreach ($myposts as $post_object): ?>
      <div class="recipe-box">
		<div class="recipe__img">
		<?php $featured = wp_get_attachment_image_src( get_post_thumbnail_id( $post_object->ID ), 'full' ); ?>
       <a href="<?php the_permalink($post_object->ID)?>" target="_blank"> <img src='<?php echo $featured[0];?>' alt="dish" ></a>
        </div>
		<?php /* endforeach;?>
		<?php endif; */?>
		
        <div class="recipe__content">
          <div class="recipe__top">
            <h2 class='recipe__subtytle'><?php echo get_the_title($post_object->ID); ?></h2>
            <div class="recipe__icons">
            <div class="recipe__icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9">
                                        <defs>
                                            <style>
                                                .a {
                                                    fill: #585756;
                                                }

                                            </style>
                                        </defs>
                                        <g transform="translate(0 0)">
                                            <path class="a" d="M4.5,9A4.5,4.5,0,0,1,1.318,1.318,4.5,4.5,0,0,1,7.682,7.682,4.471,4.471,0,0,1,4.5,9ZM4.5.527A3.973,3.973,0,0,0,1.691,7.309,3.973,3.973,0,1,0,7.309,1.691,3.947,3.947,0,0,0,4.5.527Z" transform="translate(0 0)"></path>
                                        </g>
                                        <g transform="translate(4.238 1.048)">
                                            <path class="a" d="M241,60.036h.524v.7H241Z" transform="translate(-241 -60.036)"></path>
                                        </g>
                                        <g transform="translate(6.255 1.88)">
                                            <path class="a" d="M0,0H.7V.524H0Z" transform="translate(0 0.494) rotate(-45)"></path>
                                        </g>
                                        <g transform="translate(7.253 4.238)">
                                            <path class="a" d="M411.932,241h.7v.524h-.7Z" transform="translate(-411.932 -241)"></path>
                                        </g>
                                        <g transform="translate(6.255 6.256)">
                                            <path class="a" d="M0,0H.524V.7H0Z" transform="translate(0 0.37) rotate(-45)"></path>
                                        </g>
                                        <g transform="translate(4.238 7.253)">
                                            <path class="a" d="M241,411.932h.524v.7H241Z" transform="translate(-241 -411.932)"></path>
                                        </g>
                                        <g transform="translate(1.878 6.256)">
                                            <path class="a" d="M0,0H.7V.524H0Z" transform="translate(0 0.494) rotate(-45)"></path>
                                        </g>
                                        <g transform="translate(1.048 4.238)">
                                            <path class="a" d="M60.036,241h.7v.524h-.7Z" transform="translate(-60.036 -241)"></path>
                                        </g>
                                        <g transform="translate(1.879 1.879)">
                                            <path class="a" d="M0,0H.524V.7H0Z" transform="translate(0 0.37) rotate(-45)"></path>
                                        </g>
                                        <g transform="translate(4.245 2.644)">
                                            <path class="a" d="M243.111,152.219H241v-2.111h.524V151.7h1.587Z" transform="translate(-241 -150.108)"></path>
                                        </g>
                                        </svg>
                                        <p>&nbsp;&nbsp;<?php echo get_field('recipe_preparation_time', $post_object->ID ); ?></p>
                                      </div>
                                      <div class="recipe__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="7.404" viewBox="0 0 9 7.404">
                                        <defs>
                                            <style>
                                                .a {
                                                    fill: #585756;
                                                }

                                                .b {
                                                    clip-path: url(#a);
                                                }

                                            </style>
                                            <clipPath id="a">
                                                <rect class="a" width="9" height="7.404" transform="translate(732 730)"></rect>
                                            </clipPath>
                                        </defs>
                                        <g class="b" transform="translate(-732 -730)">
                                            <g transform="translate(732 731.077)">
                                                <g transform="translate(0 4.125)">
                                                    <path class="a" d="M8.972,282.089A.188.188,0,0,0,8.813,282H.188a.188.188,0,0,0-.168.272,1.536,1.536,0,0,0,1.381.854H7.6a1.535,1.535,0,0,0,1.381-.854A.187.187,0,0,0,8.972,282.089Zm-1.373.661H1.4a1.162,1.162,0,0,1-.859-.375H8.458A1.163,1.163,0,0,1,7.6,282.751Z" transform="translate(0 -282)"></path>
                                                </g>
                                                <g transform="translate(1.66 1.5)">
                                                    <path class="a" d="M80.845,158.8a.191.191,0,0,0-.2-.175,2.993,2.993,0,0,0-2.63,1.995.188.188,0,0,0,.115.239.191.191,0,0,0,.062.01.188.188,0,0,0,.177-.125A2.618,2.618,0,0,1,80.67,159,.188.188,0,0,0,80.845,158.8Z" transform="translate(-78.005 -158.626)"></path>
                                                </g>
                                                <g transform="translate(0.755 0.75)">
                                                    <path class="a" d="M42.956,126.927a3.75,3.75,0,0,0-7.49,0,.188.188,0,0,0,.178.2h.01a.188.188,0,0,0,.187-.178,3.375,3.375,0,0,1,6.741,0,.188.188,0,1,0,.375-.019Z" transform="translate(-35.466 -123.374)"></path>
                                                </g>
                                                <g transform="translate(3.937 0)">
                                                    <path class="a" d="M185.625,88.124a.563.563,0,1,0,.563.563A.563.563,0,0,0,185.625,88.124Zm0,.75a.188.188,0,1,1,.188-.188A.188.188,0,0,1,185.625,88.874Z" transform="translate(-185.062 -88.124)"></path>
                                                </g>
                                            </g>
                                        </g>
              </svg>
              <p>&nbsp;&nbsp;<?php echo get_field('recipe_serves', $post_object->ID ); ?></p>
            </div>
            </div>
          </div>
          <div class="recipe__text">
            <p><?php $post_id = $post_object->ID;
							$post_object = get_post( $post_id );
							echo wpautop(wp_trim_words($post_object->post_content, 80, '...')); ?>
			      </p>
          </div>
        </div>
      </div>
	  <?php endforeach; endif;?>
    </div>

	<?php
		endwhile;
			wp_reset_postdata();
				else :
				  esc_html_e( 'No configured menu!', 'text-domain' );
				endif;
	?>
    <div class="groceries">
    <h2 class='recipe__subtytle'>groceries list</h2>
    <ul class="groceries__list">
	
	<?php
		global $post;
		$the_query = new WP_Query( array(
			'post_type' => 'osetin_recipe'
			)
		);

		if($the_query->have_posts()){
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
		if( have_rows('ingredients', $post->ID) ):
			while ( have_rows('ingredients') ) : the_row();
				$ingredient_name = get_sub_field('ingredient_name'); 
                $ingredient_amount = get_sub_field('ingredient_amount');
				?>
				<?php if($ingredient_name){?>
				<li><?php echo $ingredient_amount; ?>&nbsp;<?php echo $ingredient_name; ?></li>
				<?php }?>
			<?php endwhile;
				else :
					// no rows found
				endif;
			}
		}
		?>
	
    </ul>
    <a href="javascript:window.print()" class="btn">Print</a>
    </div>
           
    <!----------------------end blocks -------------------->            
	
      </div>
      <?php if ( osetin_is_active_sidebar( 'sidebar-index' ) ) { ?>

        <div class="page-sidebar">
        
          <?php dynamic_sidebar( 'sidebar-index' ); ?>

        </div>
          
      <?php } ?>


    </div>
  </div>

<?php endwhile;  ?>

<?php get_footer(); ?>