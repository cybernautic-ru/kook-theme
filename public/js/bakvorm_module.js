/**
 * JS Module for the converter between different baking mold sizes.
 * @author Serhii Klenachov <serhii.klenachov@gmail.com>
 */

; (function ($) {

    BakingMoldSizeDialog = function (_settings = {}) {        
        this.settings = _settings;
        this.main();
    };

    BakingMoldSizeDialog.prototype = {
        settings: {},
        initialSizeText: '',
        flagUnBlocked: true,
        initServes: 0,
        moldTypes: {
            circular: 'circular',
            rectangular: 'rectangular',
        },
        varsInitial: {
            sizeValue1: 0,
            sizeValue2: 0,
            type: 'circular',
        },
        varsCurrent: {
            sizeValue1: 0,
            sizeValue2: 0,
            type: '',
        },
        varsNeeded: {
            sizeValue1: 0,
            sizeValue2: 0,
            type: '',
        },
        events: {
            click: 'click',
        },
        elements: {
            btnShowPopup: '.main-recipes .recipe-fields .side-meta-box > ul > li:first-child',
            popupWrapper: '.baking-mold-popup',
            popupCloseButton: '#baking-mold-popup-close_btn',
            popupRectangularBtn: '#baking-mold-type-rec',
            popupCircularBtn: '#baking-mold-type-cir',
            popupChildPages: '.bmp-page_child',
            popupRectangularPage: '.bmp-page_rectangle',
            popupCircularPage: '.bmp-page_circle',
            popupVariant: '.bmp-page_main-variant',
            curentInfoSize: '.baking-mold-current-size',
            currentInfoType: '.baking-mold-current-type',
            ingredientsWClass: '.ingredients-table .ingredient-amount',
            applyBtnCircular: '.baking-mold-apply-circle',
            applyBtnRectangular: '.baking-mold-apply-rectangle',
            inValueCircleD: '#bmp-rectangle-size-diameter',
            inValueRecL: '#bmp-rectangle-size-length',
            inValueRecW: '#bmp-rectangle-size-width',
            resetBtn: '.baking-mold-reset',
        },
        textStrings: {
            rectangular: '',
            circular: '',
            cm: 'cm',
            g: 'g',
            ml: 'ml',
        },
        dataAttr: {
            variant: 'data-variant',
            initialAmount: 'data-initial-amount',
            initialSizeAmount: 'data-initial-size-amount',
        },
        ingredientsElements: [],
        
        /**
         * Runs popup init and binds all actions
         */
        main: function () { 
            this.initStrings();
            this.initVars();
            this.saveInitSizeValues();
            this.initPopup();
        },

        addEventHandler: function (_element, _event, _callback, _data = {}) {
            _data._this = this;
            $(_element).on(_event, _data, function (e) { 
                _callback(e);
            });
        },

        initPopup: function () {            
            this.addEventHandler(this.elements.btnShowPopup, this.events.click, this.callbackPopupShow);
            this.addEventHandler(this.elements.popupCloseButton, this.events.click, this.callbackPopupHide);
            this.addEventHandler(this.elements.popupRectangularBtn, this.events.click, this.callbackChildPageShow, {
                size: this.moldTypes.rectangular
            });
            this.addEventHandler(this.elements.popupCircularBtn, this.events.click, this.callbackChildPageShow, {
                size: this.moldTypes.circular
            });
            this.addEventHandler(this.elements.applyBtnCircular, this.events.click, this.callbackApplyCircular);
            this.addEventHandler(this.elements.applyBtnRectangular, this.events.click, this.callbackApplyRectangular);
            this.addEventHandler(this.elements.resetBtn, this.events.click, this.callbackResetInit);
        },

        initStrings: function () { 
            this.textStrings.rectangular = $(this.elements.popupRectangularBtn).text();
            this.textStrings.circular = $(this.elements.popupCircularBtn).text();
        },

        initVars: function () { 
            var modTextValue = $(this.elements.btnShowPopup).children('p').children('span').text().trim();
            if (modTextValue.length <= 0) {
                this.flagUnBlocked = false;
            }
            this.initialSizeText = modTextValue;
            modTextValue = modTextValue.substring(0, modTextValue.toLowerCase().lastIndexOf('cm')).trim();

            if (modTextValue.toLowerCase().indexOf('x') === -1) {
                this.varsInitial.sizeValue1 = parseInt(modTextValue.toLowerCase().trim());
                this.varsInitial.type = this.moldTypes.circular;
            } else {
                this.varsInitial.sizeValue1 = parseInt(modTextValue.substring(0, modTextValue.toLowerCase().indexOf('x')).trim());
                this.varsInitial.sizeValue2 = parseInt(modTextValue.substring(modTextValue.toLowerCase().indexOf('x') + 1).trim());
                this.varsInitial.type = this.moldTypes.rectangular;
            }
            this.varsCurrent = Object.assign({}, this.varsInitial);
            this.updateCurrentInfo();
            this.initServes = $('.ingredient-serves-num').data('initial-service-num');
        },

        callbackPopupShow: function (e) {
            if (e.data._this.flagUnBlocked) {
                $(e.data._this.elements.popupWrapper).addClass('opened');
            }
        },

        callbackPopupHide: function (e) {
            $(e.data._this.elements.popupWrapper).removeClass('opened');
        },

        callbackChildPageShow: function (e) {
            e.data._this.callbackChildPagesHide(e);
            if (e.data.size == e.data._this.moldTypes.rectangular) {
                $(e.data._this.elements.popupRectangularPage).addClass('shown');                
            } else if (e.data.size == e.data._this.moldTypes.circular) {
                $(e.data._this.elements.popupCircularPage).addClass('shown');
            }
            $(e.data._this.elements.popupVariant + '[' + e.data._this.dataAttr.variant + '="' + e.data.size + '"]').addClass('active');
        },

        callbackChildPagesHide: function (e) { 
            $(e.data._this.elements.popupChildPages).removeClass('shown');
            $(e.data._this.elements.popupVariant).removeClass('active');
        },

        updateCurrentInfo: function () {
            var cISize = '';
            var cIType = '';
            if (this.varsCurrent.type == this.moldTypes.circular) {
                cISize = this.varsCurrent.sizeValue1 + this.textStrings.cm;
                cIType = this.textStrings.circular;
            } else if (this.varsCurrent.type == this.moldTypes.rectangular) {
                cISize = this.varsCurrent.sizeValue1 + 'x' + this.varsCurrent.sizeValue2 + this.textStrings.cm;
                cIType = this.textStrings.rectangular;
            }
            $(this.elements.curentInfoSize).text(cISize);
            $(this.elements.currentInfoType).text(cIType);
        },

        saveInitSizeValues: function () { 
            this.ingredientsElements = $(this.elements.ingredientsWClass);
            for (var i = 0; i < this.ingredientsElements.length; i++) {
                $(this.ingredientsElements[i]).attr(this.dataAttr.initialSizeAmount, $(this.ingredientsElements[i]).attr(this.dataAttr.initialAmount));
            }
        },

        updateIngredients: function (e) {            
            var newServes = 0;
            for (var i = 0; i < e.data._this.ingredientsElements.length; i++) {
                var ingredient_initial_amount_text = String($(e.data._this.ingredientsElements[i]).data('initial-size-amount'));
                if (ingredient_initial_amount_text != '') {
                    // extract only the numbers and dot before the first letter
                    var amount_value = ingredient_initial_amount_text.match(/([0-9\.\/]+)[^0-9]*/);
                    if (amount_value != null && typeof amount_value[1] !== 'undefined') {
                        // extract letters from the amount text so we can use it later and join it with the number amount
                        var amount_non_value = ingredient_initial_amount_text.replace(amount_value[1], '');
                        // Fraction                    
                        if (amount_value[1].indexOf("/") > -1) {
                            amount_value[1] = parseFloat(amount_value[1].substring(0, amount_value[1].indexOf("/"))) / parseFloat(amount_value[1].substring(amount_value[1].indexOf("/") + 1));
                        }
                        var initAmount = parseFloat(amount_value[1]);
                        var initial_serves = e.data._this.initServes;
                        var per_amount = parseFloat(amount_value[1]) / parseFloat(initial_serves);
                        var newAmount = Math.round(e.data._this.calculateIngValue(initAmount, e) * 100 + Number.EPSILON) / 100;
                        var newServesC = parseInt(newAmount / per_amount);
                        newServes = newServesC > newServes ? newServesC : newServes;                        
                        $(e.data._this.ingredientsElements[i]).text(newAmount + amount_non_value);
                        $(e.data._this.ingredientsElements[i]).data('initial-amount', newAmount + amount_non_value);                        
                    }
                }
            }
            $('.ingredient-serves-num').data('initial-service-num', newServes);
            $('.ingredient-serves-num').data('current-serves-num', newServes);
            $('.ingredient-serves-num').attr('data-current-serves-num', newServes);
            $('.ingredient-serves-num').attr('data-nitial-service-num', newServes);
            $('.ingredient-serves-num').val(newServes);
        },

        calculateIngValue: function (oldV, e) {           
            var returnV = 0;
            if (e.data._this.varsNeeded.type == e.data._this.moldTypes.circular) {
                if (e.data._this.varsInitial.type == e.data._this.moldTypes.circular) {
                    returnV = oldV * ((Math.PI * Math.pow((parseFloat(e.data._this.varsNeeded.sizeValue1) / 2), 2)) / (Math.PI * Math.pow((parseFloat(e.data._this.varsInitial.sizeValue1) / 2), 2)));
                } else if (e.data._this.varsInitial.type == e.data._this.moldTypes.rectangular) {
                    returnV = oldV * ((Math.PI * Math.pow((parseFloat(e.data._this.varsNeeded.sizeValue1) / 2), 2)) / (parseFloat(e.data._this.varsInitial.sizeValue1) * parseFloat(e.data._this.varsInitial.sizeValue2)));
                }
            } else if (e.data._this.varsNeeded.type == e.data._this.moldTypes.rectangular) {
                if (e.data._this.varsInitial.type == e.data._this.moldTypes.circular) {
                    returnV = oldV * ((parseFloat(e.data._this.varsNeeded.sizeValue1) * parseFloat(e.data._this.varsNeeded.sizeValue2)) / (Math.PI * Math.pow((parseFloat(e.data._this.varsInitial.sizeValue1) / 2), 2)));
                } else if (e.data._this.varsInitial.type == e.data._this.moldTypes.rectangular) {
                    returnV = oldV * ((parseFloat(e.data._this.varsNeeded.sizeValue1) * parseFloat(e.data._this.varsNeeded.sizeValue2)) / (parseFloat(e.data._this.varsInitial.sizeValue1) * parseFloat(e.data._this.varsInitial.sizeValue2)));
                }
            }
            return returnV;
        },

        callbackApplyCircular: function (e) {            
            e.data._this.varsNeeded.type = e.data._this.moldTypes.circular;
            e.data._this.varsNeeded.sizeValue1 = parseFloat($(e.data._this.elements.inValueCircleD).val());            
            if (e.data._this.varsNeeded.sizeValue1 > 0.0) {
                e.data._this.updateIngredients(e);
                e.data._this.varsCurrent = Object.assign({}, e.data._this.varsNeeded);
                e.data._this.updateCurrentInfo();
                e.data._this.updateFEMold(e);
                e.data._this.callbackPopupHide(e);
            }
        },

        callbackApplyRectangular: function (e) {            
            e.data._this.varsNeeded.type = e.data._this.moldTypes.rectangular;
            e.data._this.varsNeeded.sizeValue1 = parseFloat($(e.data._this.elements.inValueRecL).val());
            e.data._this.varsNeeded.sizeValue2 = parseFloat($(e.data._this.elements.inValueRecW).val());            
            if (e.data._this.varsNeeded.sizeValue1 > 0.0 && e.data._this.varsNeeded.sizeValue2 > 0.0) {
                e.data._this.updateIngredients(e);
                e.data._this.varsCurrent = Object.assign({}, e.data._this.varsNeeded);
                e.data._this.updateCurrentInfo();
                e.data._this.updateFEMold(e);
                e.data._this.callbackPopupHide(e);
            }
        },

        updateFEMold: function (e) {
            var cISize = '';            
            if (e.data._this.varsCurrent.type == e.data._this.moldTypes.circular) {
                cISize = e.data._this.varsCurrent.sizeValue1 + ' ' + e.data._this.textStrings.cm.toUpperCase();
            } else if (e.data._this.varsCurrent.type == e.data._this.moldTypes.rectangular) {
                cISize = e.data._this.varsCurrent.sizeValue1 + 'x' + e.data._this.varsCurrent.sizeValue2 + ' ' + e.data._this.textStrings.cm.toUpperCase();                
            }            
            $(e.data._this.elements.btnShowPopup).children('p').children('span').text(cISize);
        },

        callbackResetInit: function (e) { 
            $(e.data._this.elements.btnShowPopup).children('p').children('span').text(e.data._this.initialSizeText);
            for (var i = 0; i < e.data._this.ingredientsElements.length; i++) {
                $(e.data._this.ingredientsElements[i]).data('initial-amount', $(e.data._this.ingredientsElements[i]).attr(e.data._this.dataAttr.initialSizeAmount));
                $(e.data._this.ingredientsElements[i]).text($(e.data._this.ingredientsElements[i]).attr(e.data._this.dataAttr.initialSizeAmount));
                e.data._this.varsCurrent = Object.assign({}, e.data._this.varsInitial);
                e.data._this.varsNeeded = Object.assign({}, e.data._this.varsInitial);
            }
            $('.ingredient-serves-num').data('initial-service-num', e.data._this.initServes);
            $('.ingredient-serves-num').data('current-serves-num', e.data._this.initServes);
            $('.ingredient-serves-num').attr('data-current-serves-num', e.data._this.initServes);
            $('.ingredient-serves-num').attr('data-nitial-service-num', e.data._this.initServes);
            $('.ingredient-serves-num').val(e.data._this.initServes);
            e.data._this.updateCurrentInfo(e);
            e.data._this.callbackPopupHide(e);
        }

    };

    /**
     * Runs when document is loaded
     */
    $(function () {
        new BakingMoldSizeDialog();  
    });

})( jQuery );