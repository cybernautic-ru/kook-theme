;(function($) {
	//$('button[name=get_more_recipes]').append('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
	$('button[name=favorite-model]').append('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
	$('.group-more').append('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
	
    $(function() {
        var paginationBlock = $('.pgntn-page-pagination.pgntn-bottom');
        var parentBlock = $(paginationBlock).closest('.os-container');
        $(parentBlock).append($(paginationBlock));
    });
	$('.responsive-menu-item-link .responsive-menu-subarrow').on('click', function(e){
		//console.log('clicked');
		e.preventDefault();
		var block = $(this);
		console.log(block.closest('li').find( '.back-sub-menu' ).length)
		if ( block.closest('li').find( '.back-sub-menu' ).length === 0) 
		{
			block.closest('li').find('ul.responsive-menu-submenu').eq(0).prepend('<li class="back-sub-menu"><a href="#">< back</a></li>');
		}
		block.closest('li').find('ul.responsive-menu-submenu').eq(0).addClass('active-submenu');
	});
	$('body').on('click','.active-submenu .back-sub-menu', function(e){
		e.preventDefault();
		var block = $(this);
		block.closest('ul.active-submenu').eq(0).removeClass('active-submenu responsive-menu-submenu-open');
		
	});
	
	$(".newsliders li").mouseenter(function() { 
		$(this).css("background-color", "green"); 
		var block= $(this);
		$('.newsliders li').each(function(){
			$(this).css({'width':'260'});
		})
		block.css({'width':'700'});
	});
	$(".newsliders li").mouseleave(function() { 
		$(this).css({'width':'700'});
	});
	
	$('button[name=favorite-model]').on('click', function(e){
		$('#loadingmessage').show();
		e.preventDefault();
		var postId = $(this).data('postid');
		var redirect = $(this).data('redirect');
		if(redirect=='true'){
			window.location.href='https://dev.kookmutsjes.com/';
			return false;
		}
		$('#dialogForm').css({'display':'block !important', 'effect': 'blind', 'duration': '800'});
		
		$.ajax({
			type: "post",
			dataType: "json",
			url: kookmutsjes_ajax_object.ajax_url,
			data: {action: "add_favorite_post",post_id:postId},
			success: function(msg){
				$('#loadingmessage').hide();
				var dataresult= msg.data;
				if(dataresult == "no"){
					window.location.href='https://dev.kookmutsjes.com/log-in/';
				}
				else{
					//console.log(dataresult);
					$('body').append(dataresult);
				}
			},
			error:function(error){
			    console.log(error)
			}
		});
	})

	$('body').on('click','.delete-saved-post-yes',function(){
		var postId = $(this).attr('data-postid');
		var Elem = $('#saved-recipe-block').find('#saved-post-'+$(this).attr('data-postid'))
		var countsavedpost = $('#saved-recipe-block .saved-collection').find('.item').length;	
		
		$('#loadingmessage').show();
		$('#delete-alert-model').hide();
		$.ajax({
			type: "post",
			dataType: "json",
			url: kookmutsjes_ajax_object.ajax_url,
			data: {action: "delete_saved_favorite_post",post_id:postId,lastpost:countsavedpost},
			success: function(responce){
				console.log(responce.data);
				$('#loadingmessage').hide();				
				var dataresult= responce.data;
				if(dataresult.status){
					//$('article.post-'+postId).remove();
					Elem.remove();
				}				
			},
			error:function(error){
			    console.log(error)
			}
		});
	})
	$('body').on('click','.deletepost a', function(e){
		e.preventDefault();

		$('#delete-alert-model button.yes')
		.addClass('delete-saved-post-yes')
		.attr('data-postid',$(this).data('postid'))
		$('#delete-alert-model').show();

		/*
		$('#loadingmessage').show();
		var postId = $(this).data('postid');
		var Elem = $(this);
				
		$.ajax({
			type: "post",
			dataType: "json",
			url: kookmutsjes_ajax_object.ajax_url,
			data: {action: "delete_saved_favorite_post",post_id:postId},
			success: function(msg){
				console.log(msg.data);
				$('#loadingmessage').hide();				
				var dataresult= msg.data;
				if(dataresult=='done'){
					//$('article.post-'+postId).remove();
					$(Elem).closest('article').remove();
				}				
			},
			error:function(error){
			    console.log(error)
			}
		});
		*/
	})


	$('body').on('click', '.close-popup',function(e){
	    
	    $('#dialogForm').fadeOut().remove()
	    $('div#message-box').fadeOut();
		$('div#err-message-box').fadeOut();
	})
	$('body').on('click','button[name=new-collectikon-btn]', function(e){
		e.preventDefault();
		var elem= $(this);
		var taxonomyName = $(this).closest('div.new-collection').find('input[name=taxonomy_name]').val();
		var termName = $(this).closest('div.new-collection').find('input[name=newcollection]').val();
		var groupmenuele = '<ul class="group-menu"><li><a href="#"><i class="fa fa-ellipsis-v"></i></a><ul class="group-sub-menu"><li><a href="#" class="delete-this-group">Delete <i class="fa fa-trash"></i></a></li><li><a href="#" class="show-hide-show">Hide <i class="fa fa-eye"></i></a></li></ul></li></ul>';
		if(termName == "")
		{
			$('div#err-message-box').show();
		}
		else
		{
			//$('#loadingmessage').show();
			elem.closest('.new-collection').find('.lds-ellipsis').css('display','inline-block');
			var profilePage = false;
			if($(this).closest('ul.collection-items').length>0){
				appendEle = $(this).closest('ul.collection-items').find('ul');
			}else if($(this).closest('.right-block').length>=0){
				appendEle = $(this).closest('.profile-sidebar').find('.favorite-list ul');
				/*
				Ele = $(appendEle).find('li:first').clone();

				Ele.attr('href','#').attr('data-termid','0').find('a').text(termName+ '- 0 Recipes')
				*/
				profilePage = true;
			}
			$.ajax({
				type: "post",
				dataType: "json",
				url: kookmutsjes_ajax_object.ajax_url,
				data: {action: "add_new_term",term_name:termName,taxonomy_name:taxonomyName},
				success: function(msg){
					var dataresult= msg.data;
					if(profilePage==true){
						elem.closest('.profile-sidebar').find('.favorite-list>ul').append('<li class="extra"><div class="fav" data-termid="'+dataresult+'" class="favorite-name"><span class="text">'+termName+' - 0 Recipes</span>'+groupmenuele+'</div></li>');
						$('div#message-box').show();
						elem.closest('.new-collection').find('.lds-ellipsis').css('display','none');
					}else{
						elem.closest('.new-collection').find('.lds-ellipsis').css('display','none');
						appendEle.append('<li class="item"><label class="checkList_item checkList_item containers"><input class="check-box" type="checkbox" data-id="'+dataresult+'" name="favorite[]" value="'+dataresult+'"><span>'+termName+'</span></label></li>');
					}
				},
				error:function(error){
					console.log(error)
				}
			});
		}
		
	})
	$('body').on('click','.filter-select .option',function(){		
		$(this).closest('select[name=filter_select]').val($(this).attr('id'))
		$('.selected-value').text($(this).text());
	})
	
	$('body').on('click','.show-hide-show',function(e){
		e.preventDefault();
		$(this).find('.fa').removeClass('fa-eye')
		$(this).find('.fa').addClass('fa-eye-slash')
		$(this).removeClass('show-hide-show').addClass('show-hide-hide')
		var termid = $(this).closest('.fav').data('termid')

		$.ajax({
			type: "post",
			dataType: "json",
			url: kookmutsjes_ajax_object.ajax_url,
			data: {action: "show_hide_saved_recipe",term_id:termid,status:'hide'},
			beforeSend:function(status){
				$('#loadingmessage').show();
			},
			success: function(responce){
				$('#loadingmessage').hide();				
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	
	$('body').on('click','.show-hide-hide',function(e){
		e.preventDefault();
		$(this).find('.fa').addClass('fa-eye')
		$(this).find('.fa').removeClass('fa-eye-slash')
		$(this).removeClass('show-hide-hide').addClass('show-hide-show')

		var termid = $(this).closest('.fav').data('termid')

		$.ajax({
			type: "post",
			dataType: "json",
			url: kookmutsjes_ajax_object.ajax_url,
			data: {action: "show_hide_saved_recipe",term_id:termid,status:'show'},
			beforeSend:function(status){
				$('#loadingmessage').show();
			},
			success: function(responce){
				$('#loadingmessage').hide();				
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','.group-menu>li>a',function(e){
		e.preventDefault();
	})
	$('body').on('click','.group-menu .delete-this-group',function(e){
		e.preventDefault();
		if (!confirm("are you sure want to delete favorite group name")) {
			return false;
		}		
		var termId = $(this).closest('.fav').data('termid');
		var userId = $('input[name=current_user_id]').val();
		$(this).closest('.extra').remove();		
		$.ajax({
			type: "post",
			dataType: "json",
			url: kookmutsjes_ajax_object.ajax_url,
			data: {action: "delete_fav_group",termid:termId,userid:userId},
			success: function(responce){				
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','button[name=collection-submit-button]', function(e){
		e.preventDefault();
		var elem= $(this);
		var favoriteList= $('input.check-box:checked').length;
		if(favoriteList){
			$('.favErrorMsg').remove();
			elem.closest('.popup-footer').find('.collection-submit-button .lds-ellipsis').css({'display':'inline-block'});
			var postId = $(this).data('buttonid')
			var formData = $(this).closest('form#myfavform').serialize()
			var redirectLink = $(location).attr('href');
			$.ajax({
				type: "post",
				dataType: "json",
				url: kookmutsjes_ajax_object.ajax_url,
				data: formData,
				success: function(msg){
					var dataresult= msg.data;
					console.log(dataresult);
					if(dataresult=='true'){
						elem.closest('.popup-footer').find('.collection-submit-button .lds-ellipsis').css({'display':'none'});
						$('#myfavform .popup-footer').after('<div class="succ-msg">Successfully saved to favorites</div>');
						window.location.href=redirectLink;
						//$('.close-popup').trigger('click')
						$('button[data-postid='+postId+']').text('Saved').attr('disabled',true)
					}
				},
				error:function(error){
					console.log(error)
				}
			});
		}
		else{
			if($('div.favErrorMsg').length==0)
			{
				$(this).closest('form#myfavform').append('<div class="favErrorMsg">Selecteer een groep of voeg een nieuwe groep toe</div>')
			}
		}
		
	})


	$('input[name=search_saved_post]').keyup( function(){
		string= $(this).val();		
		var Saved = $(this).closest('.holder').find('input[name=saved]').val();
		var Page = $(this).closest('.holder').find('input[name=page]').val();
		var gid= $(this).closest('.holder').find('input[name=groupid]').val()
		var postData = {
			userid: $('input[name=user_id]').val(),
			sortby: $(this).closest('.holder').find('.selected-value').attr('option-id'),
			search_str:string,
			action: "search_recipe",
			groupid:gid,
			saved: Saved,
			page:Page		
		};
		$.fn.searchFilter(postData,$(this),$(this).closest('.holder').find('.holder-html'));				
	})
	$('body').on('click','.search-filter-bar .filter div.filter-select .option', function(e){		
		$(this).closest('.holder').find('.selected-value').attr('option-id', $(this).attr('id'));
		var gid= $(this).closest('.holder').find('input[name=groupid]').val()
		var Saved = $(this).closest('.holder').find('input[name=saved]').val();
		var Page = $(this).closest('.holder').find('input[name=page]').val();
		var searchStr = $(this).closest('.holder').find('input[name=search_saved_post]').val()
		var postsperpage = $(this).closest('.holder').find('input[name=posts_per_page]').val()
		var postData = {
			userid: $('input[name=user_id]').val(),
			sortby: $(this).attr('id'),
			action: "search_recipe",
			search_str:searchStr,
			groupid:gid,
			saved: Saved,
			posts_per_page:postsperpage,
			page:Page
		};
		$.fn.searchFilter(postData,$(this),$(this).closest('.holder').find('.holder-html'));
	});

	$('body').on('click', 'button[name=get_more_recipes]', function(e){
		var Saved = $(this).closest('.holder').find('input[name=saved]').val();
		var Page = $(this).closest('.holder').find('input[name=page]').val();
		var searchStr = $(this).closest('.holder').find('input[name=search_saved_post]').val()
		var	count = $(this).closest('.holder').find('.item').length+2;
		var gid= $(this).closest('.holder').find('input[name=groupid]').val()
		$(this).closest('.holder').find('input[name=posts_per_page]').val(count)
		var postData = {
			userid: $('input[name=user_id]').val(),
			sortby: $(this).closest('.holder').find('div.selected-value').attr('option-id'),
			search_str:searchStr,
			action: "search_recipe",
			posts_per_page:count,
			groupid:gid,
			saved: Saved,
			page:Page
		};
		$.fn.searchFilter(postData,$(this),$(this).closest('.holder').find('.holder-html'));
	})

	$('body').on('click', '.favorite-list .fav .text', function(e){
		$('.favorite-list li').removeClass('active')
		$(this).closest('li').addClass('active');
		var gid= $(this).closest('.fav').attr('data-termid');
		$(this).closest('.holder').find('input[name=groupid]').val(gid)
		var searchStr = $(this).closest('.holder').find('input[name=search_saved_post]').val()
		var Saved = $(this).closest('.holder').find('input[name=saved]').val();
		var Page = $(this).closest('.holder').find('input[name=page]').val();

		var postData = {
			userid: $('input[name=user_id]').val(),
			sortby: $(this).closest('.holder').find('div.selected-value').attr('option-id'),
			search_str:searchStr,
			action: "search_recipe",
			groupid:gid,
			saved: Saved,
			page:Page
		};
		$.fn.searchFilter(postData,$(this),$(this).closest('.holder').find('.holder-html'));
		
	})

	$.fn.searchFilter = function(postData,elem,holder){
		console.log(postData)
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:postData,
			beforeSend:function(){
				
				if(elem.attr('name')=='get_more_recipes'){
					elem.closest('.get-more').find('button .lds-ellipsis').css({'display':'inline-block'});
				}else{
					$('#loadingmessage').show();
				}				
			},
			success: function(responce){			
				if(elem.attr('name')=='get_more_recipes'){
					elem.closest('.get-more').find('button .lds-ellipsis').css({'display':'none'});
				}else{
					$('#loadingmessage').hide();
				}
				holder.html(responce);			
			},
			error:function(error){
				console.log(error)
			}
		});
	}

	$('body').on('click', 'button[name=get_more_feeds]', function(e){
		elem = $(this)
		list = $(this).closest('.feed').find('li').length+5;

		var postData = {
			action: "get_more_feeds",
			userid: $('input[name=user_id]').val(),
			posts_per_page: list
		};
		//console.log(postData)
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:postData,
			beforeSend:function(){
				elem.closest('.get-more').find('button .lds-ellipsis').css({'display':'inline-block'});						
			},
			success: function(responce){	
				//console.log(responce)		
				elem.closest('.get-more').find('button .lds-ellipsis').css({'display':'none'});
				$('.feed').html(responce)	
			},
			error:function(error){
				console.log(error)
			}
		});		
	})
	$('body').on('click', 'button[name=get_more_friends]', function(e){
		elem = $(this)
		list = $(this).closest('.myfriedns').find('li').length+5;

		var postData = {
			action: "get_more_followers",
			userid: $('input[name=user_id]').val(),
			posts_per_page: list
		};
		//console.log(postData)
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:postData,
			beforeSend:function(){
				elem.closest('.get-more').find('button .lds-ellipsis').css({'display':'inline-block'});						
			},
			success: function(responce){	
				//console.log(responce)		
				elem.closest('.get-more').find('button .lds-ellipsis').css({'display':'none'});
				$('.myfriedns').html(responce)	
			},
			error:function(error){
				console.log(error)
			}
		});		
	})

	
	$('body').on('click', '#moregroup', function(e){
		e.preventDefault();
		var elem= $(this);
		elem.closest('.profile-sidebar').find('.group-more .lds-ellipsis').css({'display':'inline-block'});
		var countgroups= $(this).closest('.profile-sidebar').find('.favorite-list li').length;
		countgroups = countgroups + 2;
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:{action: "get_more_group_list",count:countgroups},
			success: function(msg){
				$('.favorite-list').html(msg);
				//$('#loadingmessage').hide();
				elem.closest('.profile-sidebar').find('.group-more .lds-ellipsis').css({'display':'none'});
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	
	
	

	$('body').on('click','li.addbutton',function(){
		$('#week-menu-popup').addClass('active').data('weekday',$(this).data('id'));
		setTimeout(function(){
			$('#week-menu-popup .fav-group-list').addClass('active')
		},500)
	})

	$('body').on('click','button[name=close-popup]',function(){
		$(this).closest('#week-menu-popup').removeClass('active')
		$(this).closest('.fav-group-list').removeClass('active')
	})
	$('body').on('click','label.fav-group-list-item',function(){
		favelem = $(this).closest('li');
		favelem.find('.lds-ellipsis').show();
		termId = $(this).data('termid');
		weekDay = $(this).closest('.week-menu-popup').data('weekday');
		termName = $(this).text();
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:{action: "get_saved_recipe_on_group",termid:termId,termname:termName,weekday:weekDay},
			success: function(msg){
				$('#week-menu-popup form .fav-group-post-list').remove()
				$('#week-menu-popup form').append(msg);
				setTimeout(function(){
					favelem.find('.lds-ellipsis').hide();
					$('#week-menu-popup form .fav-group-list').addClass('into-submenu')
					$('#week-menu-popup form .fav-group-post-list').addClass('active')
				},100)
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','label.back-to-group',function(){
		$(this).closest('.fav-group-post-list').removeClass('active')
		$('#week-menu-popup .fav-group-list').removeClass('into-submenu')
	})
	
	$('body').on('click','label.fav-group-post-item',function(){
		$('label.fav-group-post-item').removeClass('active');
		$(this).toggleClass('active')
	})
	$('body').on('click','button[name=save_into_weekmenu]',function(){
		activeWeekMenu = $(this).closest('.fav-group-post-list').find('ul li .fav-group-post-item').hasClass('active');
		
		if(activeWeekMenu == true){
			$('button[name="save_into_weekmenu"] .lds-ellipsis').css('display','inline-block');
			$(this).closest('.fav-group-post-list').find('.err-msg').hide();
			
				var weekEnd = $('label.fav-group-post-item.active').data('weekend')
				var postid = $('label.fav-group-post-item.active').data('postid')

			$.ajax({
				type: "post",
				url: kookmutsjes_ajax_object.ajax_url,
				data:{action: "save_recipe_into_weekmenu",weekmenurecipe:postid,weekend:weekEnd},
				success: function(responce){
					console.log(responce)
					result = responce.data
					$('button[name="save_into_weekmenu"] .lds-ellipsis').css('display','none');
					$('#week-day-'+weekEnd).find('ul').html(result.html)
					$('label.fav-group-post-item').removeClass('active')
					$('.fav-group-post-list').removeClass('active')
					$('#week-menu-popup .fav-group-list').removeClass('into-submenu').removeClass('active')
					$('#week-menu-popup').removeClass('active')				
				},
				error:function(error){
					console.log(error)
				}
			});
		}
		else{
			var i = 0;
			function blink(selector){
				$(selector).fadeOut('slow', function(){
					$(this).fadeIn('slow', function(){
						 i++ < 8 && blink(this);
					});
				});
			}

			blink('.fav-group-post-list .err-msg');
		}
	})

	$('body').on('click','.weekname-delete-post-yes',function(){
		weekEnd = $(this).attr('data-weekend')
		postid = $(this).attr('id')
		elem = $('#week-day-'+weekEnd).find('li#liid'+postid)

		$('#delete-alert-model').hide();
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:{action: "delete_recipe_from_weekmenu",weekmenurecipe:postid,weekend:weekEnd},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function(responce){
				result = responce.data;
				$('#loadingmessage').hide();

				if(result.status){
					//$(elem).remove();	
					$('#week-day-'+weekEnd+' ul').html(result.html)				
				}		
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','.modal button.no',function(){
		$('#delete-alert-model').hide();
	})
	$('body').on('click','.weekname span.delete',function(){
		
		$('#delete-alert-model button.yes')
		.addClass('weekname-delete-post-yes')
		.attr('data-weekend',$(this).data('weekend'))
		.attr('id',$(this).data('id'))
		$('#delete-alert-model').show();
		/*
		weekEnd = $(this).data('weekend')
		postid = $(this).data('id')
		elem = $(this).closest('li');
		$.ajax({
			type: "post",
			url: kookmutsjes_ajax_object.ajax_url,
			data:{action: "delete_recipe_from_weekmenu",weekmenurecipe:postid,weekend:weekEnd},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function(msg){
				$('#loadingmessage').hide();
				console.log(msg)
				if(msg.data=='done'){
					$(elem).remove();
				}		
			},
			error:function(error){
				console.log(error)
			}
		});
		*/
		
	})
	$('body').on('click','button[name=get_add_form_category]',function(){
		$('.add-category-form').toggleClass('active')
	})

	$('body').on('click','button.open-answer-form',function(){
		$(this).closest('.dwqa-question-item').find('.list-inside-form').toggleClass('active')
	})
	//question and answer plugin addon script
	$('body').on('click','.add-category-form button[name=add_category]',function(e){
		e.preventDefault();
		var catname = $(this).closest('form').find('input[name=question_category_name]').val()
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'add_question_taxonomy',
				category_name: catname,
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( respond ) {
				console.log(respond)
				result = respond.data;
				if(result.success){
					$('#loadingmessage').hide();
					$('.dwqa-question-category-list ul').append('<li><a href="#">'+catname+'<a></li>')
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	})


	// Vote
	var update_vote = false;
	$( '.dwqa-vote-up' ).on('click', function(e){
		e.preventDefault();
		var t = $(this),
			parent = t.parent(),
			id = parent.data('post'),
			nonce = parent.data('nonce'),
			vote_for = 'question';

		if ( parent.hasClass( 'dwqa-answer-vote' ) ) {
			vote_for = 'answer';
		}

		var data = {
			action: 'dwqa-action-vote',
			vote_for: vote_for,
			nonce: nonce,
			post: id,
			type: 'up'
		}

		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
			data: data,
			beforeSend:function(){
				$('#loadingmessage').show();
			},
            success: function( data ) {
				$('#loadingmessage').hide();
            	console.log(data);
            	if (data.success) {
                    parent.find('.dwqa-vote-count').text(data.data.vote);
                }
            },
			error:function( data ) {
				console.log("error",data);
            	
            },
		});
	});

	$( '.dwqa-vote-down' ).on('click', function(e){
		e.preventDefault();
		var t = $(this),
			parent = t.parent(),
			id = parent.data('post'),
			nonce = parent.data('nonce'),
			vote_for = 'question';

		if ( parent.hasClass( 'dwqa-answer-vote' ) ) {
			vote_for = 'answer';
		}

		var data = {
			action: 'dwqa-action-vote',
			vote_for: vote_for,
			nonce: nonce,
			post: id,
			type: 'down'
		}

		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
            type: 'POST',
            dataType: 'json',
			data: data,
			beforeSend:function(){
				$('#loadingmessage').show();
			},
            success: function( data ) {
				$('#loadingmessage').hide();
				console.log(data);
            	if (data.success) {
                    parent.find('.dwqa-vote-count').text(data.data.vote);
                }
            },
			error:function( data ) {
				console.log("error",data);
            	
            },
		});
	});

	
	//follow and unfollow
	$('body').on('click','button.follow',function(e){
		var followUser = $(this).data('follow')
		if(!$(this).data('userlogin')){
			$('.following-error-message').show();
			return false;
		}
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'kkm_follow',
				follow_user: followUser,
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				console.log(responce)
				result = responce.data
				if(result.success){
					$('#loadingmessage').hide();
					$('button.follow').toggleClass('active')
					$('button.unfollow').toggleClass('active')
					$('ul.count-flowr li a#followers').text(result.data.followers)
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','button.unfollow',function(e){
		var unfollowUser = $(this).data('unfollow')
		if(!$(this).data('userlogin')){
			$('.following-error-message').show();
			return false;
		}
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'kkm_unfollow',
				unfollow_user: unfollowUser,
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				console.log(responce)
				result = responce.data
				if(result.success){		
					$('#loadingmessage').hide();
					$('button.follow').toggleClass('active')
					$('button.unfollow').toggleClass('active')
					$('ul.count-flowr li a#followers').text(result.data.followers)
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','#follower-tab .remove-follower',function(e){
		e.preventDefault();
		var follower = $(this).data('follower')
		var elem = $(this).closest('.item')
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'kkm_remove_follower',
				followeruser: follower,				
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				result = responce.data
				if(result.success){		
					$('#loadingmessage').hide();
					elem.remove();
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','.unfollow-yes',function(e){
		var followingId = $(this).data('following')
		var elem = $('.hide-following-feed[data-following='+followingId+']').closest('.item') 
		$('#delete-alert-model').hide();
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'kkm_unfollow',
				unfollow_user: followingId,
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				result = responce.data
				if(result.success){		
					$('#loadingmessage').hide();
					elem.remove();
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','#following-tab .hide-following-feed',function(e){
		e.preventDefault();

		$('#delete-alert-model button.yes')
		.addClass('unfollow-yes')
		.attr('data-following',$(this).data('following'))
		$('#delete-alert-model').show();

		/*
		var followingId = $(this).data('following')
		var elem = $(this).closest('.item') 
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'kkm_unfollow',
				unfollow_user: followingId,
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				result = responce.data
				if(result.success){		
					$('#loadingmessage').hide();
					elem.remove();
				}
			},
			error:function(error){
				console.log(error)
			}
		});
		*/
	})

	$('body').on('click','a.more-followers',function(e){
		e.preventDefault();
		var startPos = $(this).data('start')
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'get_recent_followers',
				start: startPos,
			},
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				console.log(responce)
				if(responce!=''){
					$('button.more-followers').data('start',startPos+5);
					$('.myfriedns ul').append(responce);
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$('body').on('click','input.recsubmit',function(e){
		e.preventDefault();
		form = $(this).closest('form')
		postData = form.serialize()
		$('.message-holder').html('')
		formStatus = true;
		$('.recipe-form.required').removeClass('active')
		if($('input[name=recipe_name]').val()==''){
			$('.recipe-name.required').addClass('active')
			formStatus = false;
		}
		if($('input[name=rec_images]').val()==''){
			$('.gallery-sec.required').addClass('active')
			formStatus = false;
		}

		if($('textarea[name=recipe_desc]').val()==''){
			$('.recipe-desc.required').addClass('active')
			formStatus = false;
		}
		if($('.cat-dropdown').find('input:checked').length<=0){
			$('.cat-list.required').addClass('active')
			formStatus = false;
		}
		if(!formStatus){
			$('.message-holder').html('<p>Marked Fields are Required</p>')
			return false;
		}


		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: postData,
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				$('#loadingmessage').hide();
				result = responce.data
				if(result.success){
					$('.message-holder').html(result.html)
					$.fn.resetForm(form);
				}

			},
			error:function(error){
				console.log(error)
			}
		});
	})
	$.fn.resetForm = function(form){
		form.trigger('reset')
		form.find('.required').removeClass('active');
		$('.preview-images-zone .preview-image').remove()
		$('.step').remove();
	}
	
	$('body').on('click','.sub-tablinks a',function(e){
		e.preventDefault();
		$('.tab-pane').removeClass('active').addClass('fade')
		$('.tab-pane'+$(this).attr('href')).addClass('active').removeClass('fade')		
	})
	
	$('.sub-tablinks a#default-sub-tab-open').click()
	
	
	$('body').on('change','.checkList_item .check-box',function(){
		if($(this).prop('checked')){
			$(this).closest('ul').find('input.check-box').prop('checked',false)
			$(this).prop('checked',true)
		}
	})

	$('body').on('click','span.shape',function(){
		$('.row div.active').removeClass('active')
		$(this).closest('div').addClass('active')
	})

	$('body').on('keyup','input.bakvorm',function(){
		if($(this).closest('.square-visible').length>0){
			$('input[name=baking_mold]').val($('input[name=baking_mold_square_box_width]').val()+'X'+$('input[name=baking_mold_square_box_height]').val()+'CM')
		}else{
			$('input[name=baking_mold]').val($('input[name=baking_mold_circle_val]').val()+'CM')
		}
	})


	z_sl2m_add_field();
	function z_sl2m_add_field() {
		if (typeof fields == "undefined") {
			return false;
		}
		fields["Custom"] = {
			'show_rows' :   [
				'.row-field-title',
				'.row-meta-name',
				'.row-description',
				'.row-default-options',
				'.row-required',
				'.row-overwrite-existing',
				'.row-options',
				'.row-labels',
				'.row-select2-multiple-limit',
				'.row-select2-multiple-tags',
				'.row-visibility',
				'.row-user-role-visibility',
				'.row-location-visibility'
			]
		};
	}
	$("#home-mobile-slider").lightSlider({
		item:1,
		loop:true,
		auto:true,
		slideMove:1,
		speed:1000,
		pause: 5000,
		pager:false,
		pauseOnHover:true,
		enableDrag:true,
		easing:'linear',
		responsive : [
			{
				breakpoint:800,
				settings: {
					item:1,
					slideMove:1,
					slideMargin:6,
				  }
			}, 
			{
				breakpoint:480,
				settings: {
					item:1,
					slideMove:1
				  }
			}
		]
	});
	$("#webz-community-slider").lightSlider({
		item:1,
		loop:true,
		auto:true,
		slideMove:1,
		speed:1000,
		pause: 5000,
		pager:false,
		pauseOnHover:true,
		enableDrag:true,
		easing:'linear',
		responsive : [
			{
				breakpoint:800,
				settings: {
					item:1,
					slideMove:1,
					slideMargin:6,
				  }
			}, 
			{
				breakpoint:480,
				settings: {
					item:1,
					slideMove:1
				  }
			}
		]
	});
	$('document').ready(function(){
		/*$("#ingr-list").select2({
			
			ajax: {
				url: kookmutsjes_ajax_object.ajax_url,
				dataType: "json",
				data: function (params) {
					
					var queryParameters = {
						term: params.term,
						action:'get_webz_ingridents',
					}
					console.log(queryParameters)
					return queryParameters;
				},
				processResults: function (responce) {
					data = responce.data;
					console.log(data)
					return {
						results: data
					};
				},
				cache: true
			}
			
		});*/
	})
	$('body').on('click','.add-image',function(event){
		//$('input[name=upload_image]').trigger('click')
		$('input[name=upload_image]').trigger('click')
	});
	$.fn.addAttachedImageIds = function(){
		var elid = new Array();
		$('.preview-images-zone .preview-image').each(function(){
			var id = $(this).find('.image-cancel').attr('data-no')
			elid.push(id);
		});
		$('#rec-upload').val(elid.toString());
	}
	$('body').on('click', '.image-cancel', function() {
		postData = {'id':$(this).attr('data-no'),'action':'deleteuser_attached_image'};
		elem = $(this).closest('.preview-image')
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: postData,
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				$('#loadingmessage').hide();
				result = responce.data;
				elem.remove()
				$.fn.addAttachedImageIds()		
			},
			error:function(error){
				console.log(error)
			}
		});
	});
	$('body').on('change','input[name=upload_image]',function(event) {
		formData = new FormData();
		formData.append('upload_image',$(this)[0].files[0])
		formData.append('action','webz_multi_image_upload')
		$.ajax({
			url: kookmutsjes_ajax_object.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
        	processData: false,
			beforeSend:function(){
				$('#loadingmessage').show();
			},
			success: function( responce ) {
				$('#loadingmessage').hide();
				result = responce.data
				console.log(responce)
				$(".preview-images-zone").prepend(result.html)
				$.fn.addAttachedImageIds()
				
			},
			error:function(error){
				console.log(error)
			}
		});
		
	});

	$('body').on('click','.td.ingredient-action',function(){
		if($(this).closest('.row').hasClass('ingredient-off')){
			$(this).closest('.row').removeClass('ingredient-off')
			$(this).find('i').removeClass('os-icon-check').addClass('os-icon-circle-o')
		}else{
			$(this).closest('.row').addClass('ingredient-off')
			$(this).find('i').addClass('os-icon-check').removeClass('os-icon-circle-o')
		}
	})
	$('body').on('click','.ingradient-profile ul li',function(){
		$(this).toggleClass('active')
	})
	$('input.wp-user-avatar-url').change(function(){
		console.log('inside')
	})
	$( "#uploadModal" ).on('shown', function(){
		console.log('work')
		if($('#image-gallery').data('isuserlogin')!=1){
			window.location.href='https://dev.kookmutsjes.com/log-in/';
		}
	})
})(jQuery);