;(function ($) {
    var dynamic = true;
    var elementSelectors = {
        cat_list_wrapper: '.rb-categories-list',
        cat_checkers: '.rb-category_checker',
        cat_checkers_title: '.rb-category-child_title',
        main_bg_loading: 'body div#loadingmessage',
        category_mobile_button: '#rb-mobile-categories-btn',
        sort_select: '#rb-sort-select',
        view_button_list: '#rb-view-btn-list',
        view_button_grid: '#rb-view-btn-grid',
        recipes_wrapper: '#rb-recipes-output',
        view_buttons: '.rb-view-button',
        tag_select: '#rb-tags-select',
        tag_item_template: '#tag-button-item_template',
        tags_box: '.rb-tags_selected-box',
        tag_item_cls: '.rb-tag-item',
        tag_item_close_btn: '.rb-tag-item_close-button',
        tag_apply_btn: '#rb-tags_button-apply',
        cat_mobile_menu_close_btn: '#rb-category-mobile-menu-close-btn',
        rb_items: '#rb-recipes-output .rb-item',
        page_nav_links: '.rb-nav-links',
        template_rb_item: '#recipe-ajax_template',
        block_loading: '.rb-loading-box',
        tags_loading: '.rb-tags-loading-box',
        main_container: '#rb-main',
        template_select_oprion_first: '#tag-option-first_template',
        tag_item_name: '.rb-tag-item_name',
        tag_item_num: '.rb-tag-item_num',
        template_no_posts: '#rb-no-posts_template',
        no_posts_block_class: '.rb-no-posts-block',
        parent_cats_inner_cls: '.rb-category-parent_inner',
        parent_title_cls: '.rb-category-patent_title',
        toggle_select_tags_btn: '.rb-tags-add-tag_button',
        select_tags_block: '.rb-tag-select-button',
        select_tags_btn: '.rb-tags_selector-button',
        main_title: '.rb-title',
    };
    var textVars = {
        category_param_name: 'category',
        sort_param_name: 'sort',
        tag_param_name: 'tag',
        mobile_hide_class: 'rb-mobile-hidden',
        button_open_class: 'rb-btn-open',
        rb_view_one_col: 'rb-items-grid-view',
        view_btn_active: 'active',
        opened_parent: '.show',
        toggle_button_closed_class: '.rb-closed-ts-btn',
        toggle_button_closed_class_name: 'rb-closed-ts-btn',
    };

    var savedPosts = [];
    var postFlag = true;
    var onLoadFlag = true;
    var refreshContentFlag = false;

    function getSearchCategoriesParam() {
        var params = '';
        var returnParams = [];

        var parentCatsArr = $(elementSelectors.parent_cats_inner_cls + textVars.opened_parent);
        if (parentCatsArr.length > 0) {
            for (var pc = 0; pc < parentCatsArr.length; pc++) {
                var checkedChildren = $(parentCatsArr[pc]).find(elementSelectors.cat_checkers + ':checked');
                if (checkedChildren.length == 0) {
                    params += $(parentCatsArr[pc]).data('parent-id');
                    if ((pc + 1) !== parentCatsArr.length) {
                        params += '+';
                    }
                }
            }
        }

        var checkedArr = $(elementSelectors.cat_list_wrapper).find(elementSelectors.cat_checkers + ':checked');
        if (checkedArr.length > 0) {
            var valArr = [];
            for (j = 0; j < checkedArr.length; j++) {
                valArr.push($(checkedArr[j]).val());
            }
            for (j = 0; j < valArr.length; j++) {
                if (j == 0 && params.length > 0) {
                    params += '+';
                }
                if (params.indexOf(valArr[j]) == -1) {
                    params += valArr[j];
                }
                if ((j + 1) !== valArr.length) {
                    params += '+';
                }
            }
        }
        if (params.length > 0) {
            returnParams.push(textVars.category_param_name + '=' + params);
        }

        var tagParams = '';
        var tagItems = $(elementSelectors.tag_item_cls);
        if (tagItems.length > 0) {
            for (k = 0; k < tagItems.length; k++) {
                tagParams += $(tagItems[k]).data('tag-item-id');
                if ((k + 1) !== tagItems.length) {
                    tagParams += '+';
                }
            }
        }
        if (tagParams.length > 0) {
            returnParams.push(textVars.tag_param_name + '=' + tagParams);
        }

        var sortOpt = $(elementSelectors.sort_select).val();
        if (sortOpt.length > 0) {
            returnParams.push(textVars.sort_param_name + '=' + sortOpt);
        }

        var rParams = '';
        if (returnParams.length > 0) {
            rParams = '?';
            for (var i = 0; i < returnParams.length; i++) {
                rParams += returnParams[i];
                if (i !== (returnParams.length - 1)) {
                    rParams += '&';
                }
            }
        }
        return rParams;
    }

    function makeQuery() {
        var curWidth = $(window).width();
        if(curWidth <= 767) {
            if ($(elementSelectors.cat_list_wrapper).hasClass(textVars.button_open_class)) {
                animationCatMenu(false);
            }
        }
        updateTitle();
        var catSearchString = getSearchCategoriesParam();
        console.log(catSearchString);
        if (dynamic && typeof history.pushState !== "undefined") {
            history.pushState({_search: catSearchString}, document.title, window.location.pathname + catSearchString);
            makeGetAction(catSearchString);
        } else if (dynamic) {
            makeGetAction(catSearchString);
        } else {
            window.location.assign(window.location.pathname + catSearchString);
        }
    }

    function makeGetAction(_catSearchString) {
        refreshContentFlag = true;
        $(elementSelectors.recipes_wrapper).html('');
        postFlag = true;
        $(elementSelectors.recipes_wrapper).data('paged-load', 1);
        $(elementSelectors.recipes_wrapper).data('rendered-page', 0);
        //make tag request
        setTimeout(makeTagGet, 100);
        makeGet(_catSearchString);
    }

    function showBGwCallback(_callback, _timeout) {
        $(elementSelectors.main_bg_loading).css({
            "background-color": "rgba(250,250,250,0.8)",
            "position": "fixed",
            "z-index": "999",
            "top": "0",
            "left": "0",
            "right": "0",
            "bottom": "0",
            "display": "flex",
            "justify-content": "center",
            "align-items": "center",
        });
        setTimeout(_callback, _timeout);
    }

    function hideBG() {
        $(elementSelectors.main_bg_loading).css({
            "display": "none",
        });
    }

    function makeGet(_endpointParameters = '') {
        console.log('in make get');
        var endpointBase = '/wp-json/recipe-index/v1/recipe-posts-paged/';
        var endpointParameters = _endpointParameters.length > 0 ? _endpointParameters : getSearchCategoriesParam();
        console.log(endpointParameters);
        var postsPerPage = $(elementSelectors.recipes_wrapper).data('posts-per-page');
        var postsPageLoad = $(elementSelectors.recipes_wrapper).data('paged-load');
        console.log(postsPerPage);
        console.log(postsPageLoad);
        var cur_user_id = parseInt($(elementSelectors.main_container).data('user-id'));
        if (cur_user_id > 0) {
            if (endpointParameters.length == 0) {
                endpointParameters += '?';
            } else {
                endpointParameters += '&';
            }
            endpointParameters += 'user_id=' + cur_user_id;
        }
        $.get(endpointBase + postsPerPage + '/' + postsPageLoad + endpointParameters, function (response) {
            console.log(response);
        }).done(function (response) {
            console.log('in done');
            hidePaggination();
            if (!response.no_posts) {
                console.log('in no_posts = false');
                savePosts(response.posts);
                updateLoadPage();
                if (refreshContentFlag) {
                    console.log('in refresh content');
                    refreshContentFlag = false;
                    onLoadFlag = true;
                    showPosts();
                    makeGet();
                } else {
                    if (onLoadFlag) {
                        console.log('in load flag');
                        onLoadFlag = false;
                        onScroll();
                    } else {
                        console.log('in not onload');
                        var winTopOffset = $(window).scrollTop();
                        var winHeight = $(window).outerHeight();
                        var blockOffset = $(elementSelectors.recipes_wrapper).offset().top;
                        var blockHeight = $(elementSelectors.recipes_wrapper).outerHeight();
                        var blockBottomPosition = blockHeight + blockOffset;
                        if (blockBottomPosition > winTopOffset && blockBottomPosition < (winTopOffset + winHeight)) {
                            console.log('is in bottom');
                            if (isNewPosts()) {
                                showPostsMakeGet();
                            }
                        }
                    }
                }
            } else {
                failPostsGet();
            }
        }).fail(function (response) {
            failPostsGet();
        }).always(function (response) {

        });
    }

    function failPostsGet() {
        postFlag = false;
        hideLoadingBlock();
        hideBG();
        if ($(elementSelectors.rb_items).length == 0 && $(elementSelectors.no_posts_block_class).length == 0) {
            showNoPosts();
        }
    }

    function showNoPosts() {
        $(elementSelectors.recipes_wrapper).append($(elementSelectors.template_no_posts).text());
    }

    $(elementSelectors.cat_checkers).on('change', function () {
        var cat_id = $(this).data("cat");
        uncheckCategory(cat_id);
        showBGwCallback(makeQuery, 500);
    });

    $(elementSelectors.sort_select).on('change', function () {
        showBGwCallback(makeQuery, 500);
    });

    $(elementSelectors.category_mobile_button).on('click', function (e) {
        if (!$(elementSelectors.cat_list_wrapper).hasClass(textVars.button_open_class)) {
            animationCatMenu(true);
        } else {
            animationCatMenu(false);
        }
    });

    function animationCatMenu(_state) {
        if(_state) {
            $(elementSelectors.cat_list_wrapper).addClass(textVars.button_open_class);
            $("#rb-category-sidenav").css({
                "margin-left": "0",
            });
            $('.page-template-recipes-board .all-wrapper').css({
                "margin-left": "100%",
            });
            $(elementSelectors.category_mobile_button).addClass(textVars.button_open_class);
        } else {
            $("#rb-category-sidenav").css({
                "margin-left": "-100%",
            });
            $('.page-template-recipes-board .all-wrapper').css({
                "margin-left": "0",
            });
            setTimeout(function () {
                $(elementSelectors.cat_list_wrapper).removeClass(textVars.button_open_class);
            }, 500);
            $(elementSelectors.category_mobile_button).removeClass(textVars.button_open_class);
        }
    }

    $(elementSelectors.cat_mobile_menu_close_btn).on('click', function () {
        if ($(elementSelectors.cat_list_wrapper).hasClass(textVars.button_open_class)) {
            $("#rb-category-sidenav").css({
                "margin-left": "-100%",
            });
            $('.page-template-recipes-board .all-wrapper').css({
                "margin-left": "0",
            });
            setTimeout(function () {
                $(elementSelectors.cat_list_wrapper).removeClass(textVars.button_open_class);
            }, 500);
            $(elementSelectors.category_mobile_button).removeClass(textVars.button_open_class);
        }
    });

    $(elementSelectors.view_button_list).on('click', function (e) {
        $(elementSelectors.recipes_wrapper).addClass(textVars.rb_view_one_col);
        $(elementSelectors.view_buttons).removeClass(textVars.view_btn_active);
        $(this).addClass(textVars.view_btn_active);
    });

    $(elementSelectors.view_button_grid).on('click', function (e) {
        $(elementSelectors.recipes_wrapper).removeClass(textVars.rb_view_one_col);
        $(elementSelectors.view_buttons).removeClass(textVars.view_btn_active);
        $(this).addClass(textVars.view_btn_active);
    });

    $(elementSelectors.tag_select).on('change', function () {
        var sTagID = parseInt($(this).val());
        if (sTagID > 0) {
            var sTagText = $(this).find(':selected').data('tag-name');
            var sTagNum = $(this).find(':selected').data('tag-num');
            var newTagTmp = getTagItemTemplate(sTagID, sTagText, sTagNum);
            $(elementSelectors.tags_box).append(newTagTmp);
            bindTagItem(sTagID);
            $(elementSelectors.tag_select + ' option[value="' + sTagID + '"]').attr('disabled', 'disabled');
        }
        $(this).select2('close');
        $(this).val(0);
        $(elementSelectors.toggle_select_tags_btn).trigger("click");

        tagChangeAction();
    });

    function tagChangeAction() {
        //make posts query
        showBGwCallback(makeQuery, 300);
        //show tag loader
        showTagLoader();
    }

    function getTagItemTemplate(_tID, _tText, _tNum) {
        var template = $(elementSelectors.tag_item_template).text();
        template = template.replace('{tagID}', _tID);
        template = template.replace('{name}', _tText);
        template = template.replace('{num}', _tNum);
        return template;
    }

    function bindTagItem(_tID) {
        $(elementSelectors.tag_item_cls + '[data-tag-item-id="' + _tID + '"]').find(elementSelectors.tag_item_close_btn).on('click', function () {
            $(this).closest(elementSelectors.tag_item_cls).remove();
            $(elementSelectors.tag_select + ' option[value="' + _tID + '"]').removeAttr('disabled');

            tagChangeAction();
        });
    }

    function bindInitSelected() {
        $(elementSelectors.tag_item_close_btn).on('click', function () {
            var tID = $(this).closest(elementSelectors.tag_item_cls).data('tag-item-id');
            $(elementSelectors.tag_item_cls + '[data-tag-item-id="' + tID + '"]').remove();
            $(elementSelectors.tag_select + ' option[value="' + tID + '"]').removeAttr('disabled');

            tagChangeAction();
        });
    }

    function showTagLoader() {
        $(elementSelectors.tags_loading).css({
            "display": "flex",
        });
    }

    function hideTagLoader() {
        $(elementSelectors.tags_loading).css({
            "display": "none",
        });
    }

    function makeTagGet(_endpointParameters = '') {
        console.log('in make tag get');
        var endpointBase = '/wp-json/recipe-index/v1/recipe-tags/';
        var endpointParameters = _endpointParameters.length > 0 ? _endpointParameters : getSearchCategoriesParam();
        console.log(endpointParameters);
        $.get(endpointBase + endpointParameters, function (response) {
            console.log(response);
        }).done(function (response) {
            console.log('in done');
            if (!response.no_tags) {
                showTags(response.tags);
            } else {
                hideTagLoader();
            }
        }).fail(function (response) {
            hideTagLoader();
        }).always(function (response) {

        });
    }

    function showTags(_tags) {
        var curQueriedTags = $(elementSelectors.tag_item_cls);
        console.log(curQueriedTags);
        var curQueriedTagsIDs = [];
        for (var j = 0; j < curQueriedTags.length; j++) {
            curQueriedTagsIDs.push(parseInt($(curQueriedTags[j]).data('tag-item-id')));
        }
        console.log(curQueriedTagsIDs);

        $(elementSelectors.tag_select).html('');
        $(elementSelectors.tag_select).append($(elementSelectors.template_select_oprion_first).text());

        for (var i = 0; i < _tags.length; i++) {
            var disabledMark = '';
            console.log(_tags[i]);
            if (curQueriedTagsIDs.indexOf(parseInt(_tags[i]['term_taxonomy_id'])) > -1) {
                console.log('in index of');
                disabledMark = 'disabled="disabled"';
                $(elementSelectors.tag_item_cls + '[data-tag-item-id="' + _tags[i]['term_taxonomy_id'] + '"]').find(elementSelectors.tag_item_name).text(_tags[i]['name'].charAt(0).toUpperCase() + _tags[i]['name'].slice(1));
                $(elementSelectors.tag_item_cls + '[data-tag-item-id="' + _tags[i]['term_taxonomy_id'] + '"]').find(elementSelectors.tag_item_num).text(_tags[i]['total_posts']);
            }
            var curOption = '<option value="' + _tags[i]['term_taxonomy_id'] + '" ' +
                'data-tag-name="' + _tags[i]['name'].charAt(0).toUpperCase() + _tags[i]['name'].slice(1) + '"' +
                'data-tag-num="' + _tags[i]['total_posts'] + '"' +
                disabledMark + '>' +
                _tags[i]['name'].charAt(0).toUpperCase() + _tags[i]['name'].slice(1) +
                ' (' + _tags[i]['total_posts'] + ')</option>';
            $(elementSelectors.tag_select).append(curOption);
        }

        hideTagLoader();
    }

    /**
     * Recipe autopopulation
     */
    function savePosts(_posts) {
        console.log('in save posts');
        savedPosts = _posts.slice();
        console.log(savedPosts);
    }

    function updateLoadPage() {
        console.log('in update load page');
        var postsPageLoad = parseInt($(elementSelectors.recipes_wrapper).data('paged-load'));
        $(elementSelectors.recipes_wrapper).data('paged-load', postsPageLoad + 1);
    }

    function updateRenderedPage() {
        console.log('in update rendered page');
        var renderedPage = parseInt($(elementSelectors.recipes_wrapper).data('rendered-page'));
        $(elementSelectors.recipes_wrapper).data('rendered-page', renderedPage + 1);
    }

    function isNewPosts() {
        console.log('in is new');
        var renderedBrandPosts = $(elementSelectors.rb_items);
        var latestRenderedPostIdNum = 0;
        if (renderedBrandPosts.length > 0) {
            var latestRenderedPostId = $(renderedBrandPosts[renderedBrandPosts.length - 1]).attr('id');
            latestRenderedPostIdNum = parseInt(latestRenderedPostId.replace('post-', ''));
        }
        console.log(latestRenderedPostIdNum);
        var renderedPage = parseInt($(elementSelectors.recipes_wrapper).data('rendered-page'));
        var postsPageLoad = parseInt($(elementSelectors.recipes_wrapper).data('paged-load'));
        if (savedPosts.length > 0) {
            console.log('in savedpost > 0');
            console.log(parseInt(savedPosts[savedPosts.length - 1].post_id));
            console.log(postsPageLoad - renderedPage);
            if (latestRenderedPostIdNum != parseInt(savedPosts[savedPosts.length - 1].post_id) && ((postsPageLoad - renderedPage) > 1)) {
                console.log('in if id!=id and loaded - rendered > 1');
                return true;
            }
        }
        return false;
    }

    function showPosts() {
        console.log('in show posts');
        var localPosts = savedPosts.slice();
        localPosts.forEach(function (post) {
            console.log('in foreach');
            var aTemplate = getTemplete(post);
            $(elementSelectors.recipes_wrapper).append(aTemplate);
            bindSaveButton(post.post_id);
            bindLikeButton(post.post_id);
        });
        refreshContentFlag = false;
        updateRenderedPage();

        //hide loaders
        hideBG();
    }

    function getTemplete(_post) {
        var template = $(elementSelectors.template_rb_item).text();
        template = template.replace(/{ID}/g, _post.post_id);
        template = template.replace(/{title}/g, _post.title);
        template = template.replace(/{permalink}/g, _post.permalink);
        template = template.replace(/{date}/g, _post.date);
        template = template.replace(/{authorUrl}/g, _post.author.url);
        template = template.replace(/{authorImage}/g, _post.author.img_src);
        template = template.replace(/{authorName}/g, _post.author.name);
        template = template.replace(/{excerpt}/g, _post.excerpt);
        template = template.replace(/{preptime}/g, _post.preptime);
        template = template.replace(/{serves}/g, _post.serves);
        template = template.replace(/{level}/g, _post.level);
        //comment num
        var commentNum = _post.comments;
        var commentText = commentNum == 0 || commentNum > 1 ? commentNum + ' Comments' : commentNum + ' Comment';
        template = template.replace(/{commentsNumberText}/g, commentText);
        //media
        var postMedia = '';
        if (['gellery', 'image', 'gif'].indexOf(_post.media.type) !== -1) {
            var ifGif = '';
            if (_post.media.type == 'gif') {
                ifGif = '<span class="gif-label"><i class="os-icon os-icon-basic1-082_movie_video_camera"></i><span>GIF</span></span>';
            }
            postMedia = '<a href="' + _post.permalink + '" class="rb-item-media-thumbnail fader-activator" style="background-image:url(' + _post.media.src + '); background-size: cover;">' +
                '<span class="image-fader"><span class="hover-icon-w"><i class="os-icon os-icon-plus"></i></span></span>' +
                ifGif +
                '</a>';
        } else {
            postMedia = _post.media.src;
        }

        template = template.replace(/{media}/g, postMedia);
        //saved
        var textSavedDisabled = _post.saved.disabled == true ? 'disabled' : '';
        var savedIconClassText = _post.saved.disabled == true ? 'fa-bookmark' : 'fa-bookmark-o';
        template = template.replace(/{savedIconClass}/g, savedIconClassText);
        template = template.replace(/{savedDisabled}/g, textSavedDisabled);
        template = template.replace(/{savedRedirect}/g, _post.saved.redirect);
        //likes
        var likesVoteActionText = '';
        var likesVotedClassText = '';
        var likesCountHidenText = '';
        if (!_post.likes.count) {
            likesCountHidenText = 'hidden';
        }
        if (_post.likes.has_voted) {
            likesVoteActionText = 'unvote';
            likesVotedClassText = 'osetin-vote-has-voted';
        } else {
            likesVoteActionText = 'vote';
            likesVotedClassText = 'osetin-vote-not-voted';
        }
        template = template.replace(/{likesVoteAction}/g, likesVoteActionText);
        template = template.replace(/{likesVotedClass}/g, likesVotedClassText);
        template = template.replace(/{likesCountHiden}/g, likesCountHidenText);
        template = template.replace(/{likesCount}/g, _post.likes.count);

        return template;
    }

    function onScroll() {
        var winTopOffset = $(window).scrollTop();
        var winHeight = $(window).outerHeight();
        var blockOffset = $(elementSelectors.recipes_wrapper).offset().top;
        var blockHeight = $(elementSelectors.recipes_wrapper).outerHeight();
        var bottomGap = 800;
        if ((winTopOffset + winHeight) >= (blockOffset + blockHeight - bottomGap) || (winTopOffset == 0 && winHeight > (blockOffset + blockHeight))) {
            console.log('in scroll if');
            if (isNewPosts()) {
                console.log('is new');
                showPostsMakeGet();
            }
        }
    }

    function showPostsMakeGet() {
        showPosts();
        if (postFlag) {
            setTimeout(function () {
                console.log('make next mageGet');
                makeGet();
            }, 200);
        }
    }

    /**
     * End recipe auto population
     */

    function uncheckCategory(_cut_cat, isParent = false) {
        $(elementSelectors.cat_checkers + '[data-cat!="' + _cut_cat + '"]:checked').prop("checked", false);
        if (!isParent) {
            var parentID = $(elementSelectors.cat_checkers + '[data-cat="' + _cut_cat + '"]').closest(elementSelectors.parent_cats_inner_cls).data('parent-id');
            if (parentID != undefined) {
                $(elementSelectors.parent_cats_inner_cls + '.show[data-parent-id!="' + parentID + '"]').closest('.rb-category_inner').find(elementSelectors.parent_title_cls).attr('aria-expanded', 'false');
                $(elementSelectors.parent_cats_inner_cls + '.show[data-parent-id!="' + parentID + '"]').removeClass('show');
            } else {
                $(elementSelectors.parent_cats_inner_cls + '.show[data-parent-id!="' + _cut_cat + '"]').closest('.rb-category_inner').find(elementSelectors.parent_title_cls).attr('aria-expanded', 'false');
                $(elementSelectors.parent_cats_inner_cls + '.show[data-parent-id!="' + _cut_cat + '"]').removeClass('show');
            }
        } else {
            $(elementSelectors.parent_cats_inner_cls + '.show[data-parent-id!="' + _cut_cat + '"]').closest('.rb-category_inner').find(elementSelectors.parent_title_cls).attr('aria-expanded', 'false');
            $(elementSelectors.parent_cats_inner_cls + '.show[data-parent-id!="' + _cut_cat + '"]').removeClass('show');
        }
    }

    function updateTitle() {
        var catTitle = $(elementSelectors.cat_checkers + ':checked').closest('.rb-category-child_inner').find('.rb-category-child_title').text();
        if (!(catTitle != undefined && catTitle != null && catTitle.length > 0)) {
            catTitle = $(elementSelectors.parent_cats_inner_cls + textVars.opened_parent).closest('.rb-category_inner').find('.rb-category-patent_title_value').text();
        }
        if (catTitle.length === 0) {
            catTitle = $(elementSelectors.main_title).data('init-title');
        }
        $(elementSelectors.main_title).text(catTitle.trim());
    }


    function hidePaggination() {
        $(elementSelectors.page_nav_links).hide();
    }

    function hideLoadingBlock() {
        $(elementSelectors.block_loading).css({
            "display": "none",
        });
    }

    function bindSaveButton(_post_id_el) {
        //copied from kookmutsjes_footer.js and changed
        $('#post-' + _post_id_el).find('.favorite button[name=favorite-model]').on('click', function (e) {
            $('#loadingmessage').css({
                "display": "flex",
            });
            e.preventDefault();
            var postId = $(this).data('postid');
            var redirect = $(this).data('redirect');
            if (redirect == 'true') {
                window.location.href = window.location.origin;
                return false;
            }
            $('#dialogForm').css({
                'display': 'block !important',
                'effect': 'blind',
                'duration': '800'
            });

            $.ajax({
                type: "post",
                dataType: "json",
                url: kookmutsjes_ajax_object.ajax_url,
                data: {
                    action: "add_favorite_post",
                    post_id: postId
                },
                success: function (msg) {
                    $('#loadingmessage').hide();
                    var dataresult = msg.data;
                    if (dataresult == "no") {
                        window.location.href = window.location.origin + '/login-and-registration/#SignIn';
                    } else {
                        $('body').append(dataresult);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    }

    function bindLikeButton(_post_id_el) {
        //copied from osetin-feature-like.js
        $('#post-' + _post_id_el).find('.rb-item-views-count-likes .osetin-vote-trigger').on('click', function () {
            var $button = $(this);
            var post_id = $(this).data('post-id');
            var vote_action = $(this).data('vote-action');
            var current_votes_count = $(this).data('votes-count') ? $(this).data('votes-count') : 0;
            var new_votes_count;

            if (vote_action == 'vote') {
                $button.removeClass('osetin-vote-not-voted').addClass('osetin-vote-has-voted');
                new_votes_count = current_votes_count + 1;
                $button.data('votes-count', new_votes_count);
                $button.data('vote-action', 'unvote');
                $button.find('.osetin-vote-count').text(new_votes_count);
                if (new_votes_count > 0) $button.find('.osetin-vote-count').removeClass('hidden');
                $button.find('.osetin-vote-action-label').text($button.data('has-voted-label'));
            } else {
                $button.addClass('osetin-vote-not-voted').removeClass('osetin-vote-has-voted');
                new_votes_count = current_votes_count - 1;
                $button.data('votes-count', new_votes_count);
                $button.data('vote-action', 'vote');
                $button.find('.osetin-vote-count').text(new_votes_count);
                if (new_votes_count === 0) $button.find('.osetin-vote-count').addClass('hidden');
                $button.find('.osetin-vote-action-label').text($button.data('not-voted-label'));
            }
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    "action": "osetin_vote_process_request",
                    "vote_post_id": post_id,
                    "vote_action": vote_action
                },
                dataType: "json",
                success: function (data) {
                    if (data.status == 200) {
                        var count = data.message.count;
                        var has_voted = data.message.has_voted;
                        if (has_voted) {

                        } else {

                        }
                    }
                }
            });
            return false;
        });
    }

    $(elementSelectors.parent_cats_inner_cls).on('shown.bs.collapse', function () {
        var cat_id = $(this).data("parent-id");
        uncheckCategory(cat_id, true);
        showBGwCallback(makeQuery, 500);
    });

    $(elementSelectors.parent_cats_inner_cls).on('hide.bs.collapse', function () {
        var cat_id = $(this).data("parent-id");
        uncheckCategory(cat_id, true);
        showBGwCallback(makeQuery, 500);
    });

    $(elementSelectors.toggle_select_tags_btn).on('click', function () {
        console.log('in add tag click');
        if ($(elementSelectors.toggle_select_tags_btn).closest(elementSelectors.select_tags_block).hasClass(textVars.toggle_button_closed_class_name)) {
            $(elementSelectors.toggle_select_tags_btn).closest(elementSelectors.select_tags_block).find(elementSelectors.select_tags_btn).animate({
                opacity: 1,
            }, 750);
            $(elementSelectors.toggle_select_tags_btn).closest(elementSelectors.select_tags_block).removeClass(textVars.toggle_button_closed_class_name);
            //open
            $(elementSelectors.tag_select).select2('open');
        } else {
            //close
            $(elementSelectors.tag_select).select2('close');
            $(elementSelectors.toggle_select_tags_btn).closest(elementSelectors.select_tags_block).find(elementSelectors.select_tags_btn).animate({
                opacity: 0,
            }, 750);
            $(elementSelectors.toggle_select_tags_btn).closest(elementSelectors.select_tags_block).addClass(textVars.toggle_button_closed_class_name);
        }
    });

    $(document).on('ready', function (e) {
        $(elementSelectors.tag_select).select2({
            allowClear: false,
        });

        bindInitSelected();

        //make pre request for next autopopulation
        makeGet();
        //make action on scroll
        $(window).on('scroll', function (e) {
            onScroll();
        });
    });

})(jQuery);
