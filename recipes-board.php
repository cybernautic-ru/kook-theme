<?php

/**
 * Template Name: Recipes Board
 */
$category_attr           = array();
$tag_attr                = array();
$post_per_page_num_first = 18;
$post_per_page_num       = 18;
if ( isset( $_GET['category'] ) ) {
	$category_attr = explode( ' ', $_GET['category'] );
	for ( $c = 0; $c < count( $category_attr ); $c ++ ) {
		$category_attr[ $c ] = intval( $category_attr[ $c ] );
	}
	$paged_args['category'] = implode( '+', $category_attr );
}
if ( isset( $_GET['tag'] ) ) {
	$tag_attr = explode( ' ', $_GET['tag'] );
}
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$sort_pop_s = '';
$sort_new_s = '';
if ( isset( $_GET['sort'] ) ) {
	if ( $_GET['sort'] == 'new' ) {
		$sort_new_s = 'selected="selected"';
	} elseif ( $_GET['sort'] == 'pop' ) {
		$sort_pop_s = 'selected="selected"';
	}
	$paged_args['sort'] = $_GET['sort'];
} else {
	$sort_new_s = 'selected="selected"';
}

$is_logged_in_text = is_user_logged_in() ? 'data-user-id=' . get_current_user_id() : 'data-user-id=0';

get_header();
?>

<?php // you can put breadcrumbs here
?>
    <div class="os-container rb-container">
        <div class="rb-top-search-row">
            <div class="rb-ajax-search_block">
				<?php echo do_shortcode( '[wd_asp id=8]' ); ?>
            </div>
        </div>
    </div>

    <div class="os-container rb-container" id="rb-main" <?php echo esc_html( $is_logged_in_text ); ?>>
        <div class="rb-row rb-wrapper">
            <div class="col-md-3 rb-sidebar">
                <div class="rb-mobile-categories-btn_block">
                    <button id="rb-mobile-categories-btn"><?php _e( 'Categories', 'neptune-child' ); ?> <span
                                class="pull-right"><i class="fa fa-caret-right"></i><i
                                    class="fa fa-caret-left"></i></span>
                    </button>
                </div>
                <div class="rb-categories-list rb-sidenav <?php echo wp_is_mobile() ? '' : 'rb-sidenav-desktop'; ?>"
                     id="rb-category-sidenav">
                    <!-- mobile title -->
                    <div class="rb-category_block rb-category-patent_block rb-category-mobile-menu-title">
                        <div class="rb-category_inner">
                            <div class="rb-category-patent_title" id="rb-category-mobile-menu-close-btn">
								<?php _e( 'Categories', 'neptune-child' ); ?><span class="pull-right"><i
                                            class="fa fa-times rb-category-patent_icon" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <!-- mobile title -->
					<?php echo \RecipeCategory::get_template_recipe_categories( $category_attr ); ?>
                </div>
            </div>
            <div class="col-md-9 rb-main">
                <div class="rb-header-wrapper">
                    <div class="rb-header-wrapper-right">
                        <div class="rb-title_block">
                            <h1 class="rb-title" data-init-title="<?php the_title(); ?>"><?php the_title(); ?></h1>
                        </div>

                        <div class="rb-search-tags_block">
							<?php
							$a_tags          = \RecipeCategory::get_avilable_tags( $category_attr, $tag_attr );
							$options_output  = '';
							$selected_output = '';

							$current_available_tags = array();
							foreach ( $a_tags as $a_tag ) {
								$disabled_item = '';
								if ( in_array( $a_tag['term_taxonomy_id'], $tag_attr ) ) {
									$current_available_tags[] = intval( $a_tag['term_taxonomy_id'] );
									$disabled_item            = 'disabled="disabled"';
									$selected_output          .= '<div class="rb-tag-item" data-tag-item-id="' . esc_attr( $a_tag['term_taxonomy_id'] ) . '">
													<span class="rb-tag-item_name">' . esc_html( ucfirst( $a_tag['name'] ) ) . '</span> (<span class="rb-tag-item_num">' . esc_html( $a_tag['total_posts'] ) . '</span>)
													<button class="rb-tag-item_close-button"><i class="fa fa-times"></i></button>
												</div>';
								}
								$options_output .= '<option value="' . esc_attr( $a_tag['term_taxonomy_id'] ) . '" data-tag-name="' . esc_attr( ucfirst( $a_tag['name'] ) ) . '" data-tag-num="' . esc_attr( ucfirst( $a_tag['total_posts'] ) ) . '" ' . $disabled_item . '>' . esc_html( ucfirst( $a_tag['name'] ) ) . ' (' . esc_html( $a_tag['total_posts'] ) . ')</option>';
							}

							if ( ! empty( $a_tags ) ) : ?>
                                <div class="rb-tags_selected-box">
                                    <div class="rb-tag-title">
                                        <span class="rb-tag-title-text"><?php _e( 'Voe tags toe:', 'neptune-child' ); ?></span>
                                    </div>
									<?php echo $selected_output; ?>
                                    <div class="rb-tag-select-button rb-closed-ts-btn">
                                        <div class="rb-tags-add-tag_toggle">
                                            <button class="rb-tags-add-tag_button"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <div class="rb-tags_selector-button">
                                            <select id="rb-tags-select"
                                                    data-placeholder="<?php esc_attr_e( 'Select a tag', 'neptune-child' ); ?>">
												<?php get_template_part( 'templates/tag/tag-select', 'option-first' ); ?>
												<?php echo $options_output; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="rb-tags-loading-box">
                                    <div class="lds-ellipsis">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>

							<?php endif; ?>
                        </div>
                    </div>

                    <div class="rb-header-wrapper-left">
                        <div class="rb-sort_block">
                            <select id="rb-sort-select">
                                <option value="new" <?php echo esc_html( $sort_new_s ); ?>><?php _e( 'Sort: Newest - Old', 'neptune-child' ); ?></option>
                                <option value="pop" <?php echo esc_html( $sort_pop_s ); ?>><?php _e( 'Sort: Popular', 'neptune-child' ); ?></option>
                            </select>
                        </div>

                        <div class="rb-view-select_block">
                            <button id="rb-view-btn-grid" class="rb-view-button active"><span
                                        class="rb-view-button_icon"><i class="fa fa-th-large"
                                                                       aria-hidden="true"></i></span><span
                                        class="rb-view-button_text"><?php _e( 'Grid', 'neptune-child' ); ?></span>
                            </button>
                            <button id="rb-view-btn-list" class="rb-view-button"><span class="rb-view-button_icon"><i
                                            class="fa fa-bars" aria-hidden="true"></i></span><span
                                        class="rb-view-button_text"><?php _e( 'List', 'neptune-child' ); ?></span>
                            </button>
                        </div>
                    </div>
                </div>

                <div id="rb-recipes-output" class="rb-items"
                     data-posts-per-page="<?php echo esc_attr( $post_per_page_num ); ?>"
                     data-paged="<?php echo $paged; ?>" data-paged-load="<?php echo( $paged + 1 ); ?>"
                     data-rendered-page="<?php echo $paged; ?>">
					<?php
					$administrator = get_users( array( 'role__in' => 'administrator', 'fields' => 'ID' ) );
					$siteadmins    = get_users( array( 'role__in' => 'site_admin', 'fields' => 'ID' ) );
					$users_admin   = array_merge( $administrator, $siteadmins );

					$rp_args = array(
						'post_type'      => 'osetin_recipe',
						'posts_per_page' => $post_per_page_num_first,
						'paged'          => $paged,
						'author__in'     => $users_admin,
					);

					$has_post_flag = true;
					if ( ! empty( $category_attr ) || ! empty( $current_available_tags ) ) {
						$in_posts = RecipeCategory::get_posts_cats_tags( $category_attr, $current_available_tags );
						if ( empty( $in_posts ) ) {
							$has_post_flag = false;
						}
						$rp_args['post__in'] = $in_posts;
					}

					if ( isset( $_GET['sort'] ) ) {
						if ( $_GET['sort'] == 'new' ) {
							$rp_args['orderby'] = 'date';
							$rp_args['order']   = 'DESC';
						} elseif ( $_GET['sort'] == 'pop' ) {
							$rp_args['meta_key'] = 'wpp_views_total';
							$rp_args['orderby']  = 'meta_value_num';
							$rp_args['order']    = 'DESC';
						}
					} else {
						$rp_args['orderby'] = 'date';
						$rp_args['order']   = 'DESC';
					}
					if ( $has_post_flag ) {
						$rp_query = new WP_Query( $rp_args );
						if ( $rp_query->have_posts() ) {
							while ( $rp_query->have_posts() ) {
								$rp_query->the_post();
								get_template_part( 'templates/content/content', 'item-recipe-board' );
							}
						} else {
							get_template_part( 'templates/content/content', 'no-posts-recipe-board' );
						}
					} else {
						get_template_part( 'templates/content/content', 'no-posts-recipe-board' );
					} ?>
                </div>

                <div class="rb-paged-navigation">
                    <div class="rb-loading-box">
                        <div class="lds-ellipsis">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="rb-nav-links">
						<?php
						echo paginate_links( array(
							'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
							'total'        => $rp_query->max_num_pages,
							'current'      => max( 1, get_query_var( 'paged' ) ),
							'format'       => 'page/%#%/',
							'show_all'     => false,
							'type'         => 'plain',
							'end_size'     => 2,
							'mid_size'     => 1,
							'prev_next'    => true,
							'prev_text'    => sprintf(
								'<span class="nav-prev-text">%s</span>',
								__( '<', 'twentynineteen' )
							),
							'next_text'    => sprintf(
								'<span class="nav-next-text">%s</span>',
								__( '>', 'twentynineteen' )
							),
							'add_args'     => $paged_args,
							'add_fragment' => '',
						) );
						?>
                    </div>
                </div>
				<?php wp_reset_query(); ?>

                <!-- Template for Recipe -->
                <script type="text/x-tmpl" id="recipe-ajax_template">
				<?php get_template_part( 'templates/ajax/ajax-item', 'recipe-board' ); ?>


                </script>

                <!-- Template for tags -->
                <script type="text/x-tmpl" id="tag-button-item_template">
				<?php get_template_part( 'templates/tag/tag-button-item', 'recipe-board' ); ?>


                </script>

                <!-- Template for tags select first option-->
                <script type="text/x-tmpl" id="tag-option-first_template">
				<?php get_template_part( 'templates/tag/tag-select', 'option-first' ); ?>


                </script>

                <!-- Template for no posts -->
                <script type="text/x-tmpl" id="rb-no-posts_template">
				<?php get_template_part( 'templates/content/content', 'no-posts-recipe-board' ); ?>


                </script>
            </div>
        </div>
    </div>

<?php get_footer(); ?>