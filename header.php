<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php if( function_exists( 'wp_site_icon' )){ ?>
		<?php wp_site_icon(); ?>
		<?php } ?>
		<?php wp_head(); ?>
		<?php if ( is_singular( 'osetin_recipe' ) ) { ?>
		<?php  echo osetin_generate_recipe_rich_snippet(); ?>
		<?php } ?>
		<script>
			jQuery(document).ready(function() {
				jQuery("#content-slider").lightSlider({
					loop:true,
					keyPress:true
				});
				jQuery('#image-gallery').lightSlider({
					currentPagerPosition:'left',
					gallery:true,
					item:1,
					thumbItem:4,
					slideMargin: 0,
					speed:1000,
					auto:false,
					loop:true,
					onSliderLoad: function() {
						jQuery('#image-gallery').removeClass('cS-hidden');
					}
					
				});
				
			});
		</script>
		<script>
			 jQuery(document).ready(function() {
				jQuery("#trending-slider").lightSlider({
					item:3,
					loop:true,
					auto:true,
					slideMove:1,
					easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
					speed:1000,
					enableDrag:true,
					responsive : [
						{
							breakpoint:800,
							settings: {
								item:2,
								slideMove:1,
								slideMargin:6,
							  }
						}, 
						{
							breakpoint:480,
							settings: {
								item:1,
								slideMove:1
							  }
						}
					]
				});
			});
		</script>
	</head>
	<?php 
	$is_parallax_background = osetin_get_settings_field('parallax_background');
	$background_image_size = osetin_get_settings_field('background_image_size');
	$background_contain = ($background_image_size == 'small') ? '' : 'background-size: contain;';
	if(is_category())
	{
		$cat_id =  get_query_var('cat');
		$body_bg_image = osetin_get_field('category_page_background_image', "category_{$cat_id}", false, true);
		if(empty($body_bg_image))
		{
			$body_bg_image = osetin_get_field('background_image_option', 'option', false, true);
		}
		}else
		{
			$body_bg_image = osetin_get_settings_field('background_image', false, false, false, true);
		}
		if(!empty($body_bg_image) && is_array($body_bg_image))
		{
			$size = 'osetin-for-background';
			$body_bg_image_url = $body_bg_image['sizes'][ $size ];
			$body_bg_image_width = $body_bg_image['sizes'][ $size . '-width' ];
			$body_bg_image_height = $body_bg_image['sizes'][ $size . '-height' ];
			if($body_bg_image_width) $body_bg_image_width = str_replace('px', '', $body_bg_image_width);
			if($body_bg_image_height) $body_bg_image_height = str_replace('px', '', $body_bg_image_height);
		}
		else
		{
		  $body_bg_image_url = false;
		}
		$default_logo = get_template_directory_uri().'/assets/img/neptune-logo.png';
		if(osetin_get_field('enable_custom_header_settings') === true)
		{
			$top_menu_version_type = osetin_get_field('top_menu_type');
			$logo_image_url = osetin_get_field('logo_image');
			$logo_image_width = osetin_get_field('logo_image_width');
		}
		else
		{
			$top_menu_version_type = osetin_get_field('top_menu_type_option', 'option', 'version_1');
			$logo_image_url = osetin_get_field('logo_image_option', 'option', $default_logo);
			$logo_image_width = osetin_get_field('logo_image_width_option', 'option', '210');
		}
		
		$top_menu_bg_color =  osetin_get_field('top_menu_background_color', 'option');
		$top_menu_bg_color_type =  osetin_get_field('top_menu_background_color_type', 'option', 'light');
		$fixed_menu_logo_image_url = osetin_get_field('fixed_header_logo', 'option', $logo_image_url);
		$fixed_menu_logo_image_width = osetin_get_field('fixed_header_logo_width', 'option', '210');
		$mobile_logo_image_url = osetin_get_field('mobile_logo_image', 'option', $logo_image_url);
		$mobile_logo_image_width = osetin_get_field('mobile_logo_image_width', 'option', '210');

	?>
	<body <?php body_class(); ?> style="<?php echo osetin_get_css_prop('background-color', osetin_get_field('background_color_option', 'option')); ?><?php if(($is_parallax_background != 'yes') && $body_bg_image_url) echo osetin_get_css_prop('background-image', $body_bg_image_url, false, 'background-repeat: repeat; background-position: top center;'.$background_contain); ?>">
		<?php if(osetin_get_field('custom_css_styles', 'option')){ ?>
			<?php echo '<style type="text/css">'.osetin_get_field('custom_css_styles', 'option').'</style>'; ?>
		<?php } ?>
		<?php if(($is_parallax_background == 'yes') && $body_bg_image_url){ ?>
		<div class="os-parallax" data-width="<?php echo esc_attr($body_bg_image_width); ?>" data-height="<?php echo esc_attr($body_bg_image_height); ?>"><img src="<?php echo esc_attr($body_bg_image_url); ?>" alt=""></div>
		<?php } ?>
			<div class="all-wrapper with-animations">
				<div class="main-header-part">
					<div class="os-container section-topbar">
						<?php if (is_user_logged_in()) : ?>
							<?php if (is_page(35185)) : ?>
								<div class="mobile-bar">
									<div class="row">
										<div class="mobile-lg profile-logo-header">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php site_url();?>/wp-content/uploads/2020/01/Asset-1.png" alt="mobile-logo" /></a>
										</div>
										<div class="mobile-profile-menu profile-menu-header">
											<ul>
												<li><a href="<?php echo wp_logout_url( home_url() ); ?>"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/logout.png" alt="mobile-my-profile" /></a></li>
											</ul>
										</div>
									</div>
									<!-- Modal -->
									<div class="modal fade mobilesearch" id="exampleModals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h4>Search Recipes</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<?php echo do_shortcode('[wd_asp id=5]');?>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php else : ?>
							<div class="mobile-bar">
								<div class="row">
									<div class="mobile-lg">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php site_url();?>/wp-content/uploads/2020/01/mobile-logo.png" alt="mobile-logo" /></a>
									</div>
									<div class="mobile-profile-menu">
										<ul>
											<li><a href="<?php site_url();?>/publish-recipe"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/plus-1.png" alt="mobile-publish-recipe" /></a></li>
											<li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/search.png" alt="mobile-logout" /></button></li>
											<li><a href="<?php site_url();?>/my-profile"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/user.png" alt="mobile-my-profile" /></a></li>
										</ul>
									</div>
								</div>
								<!-- Modal -->
								<div class="modal fade mobilesearch" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4>Search Recipes</h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<?php echo do_shortcode('[wd_asp id=5]');?>
											</div>
										</div>
								  </div>
								</div>
								<div class="row">
									<div class="m-menu">
										<ul>
											<li><a href="<?php site_url();?>/recepten-categories/">Recepten</a></li>
											<li><a href="<?php site_url();?>/stories/">Stories</a></li>
											<li><a href="<?php site_url();?>/converter-tool/">Tools</a></li>
											<li><a href="<?php site_url();?>/community/">Community</a></li>
										</ul>
									</div>
								</div>
							</div>
						<?php endif;  ?>
						<?php else : ?>
						<div class="mobile-bar">
							<div class="row">
								<div class="mobile-lg not-logged-lg">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php site_url();?>/wp-content/uploads/2020/01/Asset-1.png" alt="mobile-logo" /></a>
								</div>
								<div class="mobile-profile-menu not-logged-mu">
									<ul>
										<li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModals"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/search.png" alt="mobile-logout" /></button></li>
										<li><a href="<?php site_url();?>/log-in"><img src="<?php site_url(); ?>/wp-content/uploads/2020/01/user.png" alt="mobile-my-profile" /></a></li>
									</ul>
								</div>
							</div>
							<!-- Modal -->
							<div class="modal fade mobilesearch" id="exampleModals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4>Search Recipes</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<?php echo do_shortcode('[wd_asp id=5]');?>
										</div>
									</div>
							  </div>
							</div>
							<div class="row">
								<div class="m-menu">
									<ul>
										<li><a href="<?php site_url();?>/recepten-categories/">Recepten</a></li>
										<li><a href="<?php site_url();?>/stories/">Stories</a></li>
										<li><a href="<?php site_url();?>/converter-tool/">Tools</a></li>
										<li><a href="<?php site_url();?>/community/">Community</a></li>
									</ul>
								</div>
							</div>
						</div>
						<?php endif;  ?>
						<div class="row desktop-version">
							<div class="col-md-3 top-social-media desktopsocial">
								<ul class="bar-social">
									<li><a href="https://www.facebook.com/groups/Kookmutsjes/" target="_blank"><i class="os-icon os-icon-social-facebook"></i></a></li>
									<li><a href="https://www.instagram.com/kookmutsjes/" target="_blank"><i class="os-icon os-icon-social-instagram"></i></a></li>
									<li><a href="https://www.youtube.com/channel/UCEoMwp-m-fRk4_Ec6LkSgfA" target="_blank"><i class="os-icon os-icon-social-youtube"></i></a></li>
									<li><a href="https://nl.pinterest.com/kookmutsjes/" target="_blank"><i class="os-icon os-icon-social-pinterest"></i></a></li>
								</ul>
							</div>
							<div class="col-md-4 search-section">
								<div class="searchtopbar">
									<?php echo do_shortcode('[wd_asp id=2]');?>
								</div>
							</div>
							<div class="col-md-5 usermenus">
								<div class="accessbar">
									<ul class="usermenu">
										<?php if (is_user_logged_in()) : ?>
										<li class="rec-publish"><a href="<?php site_url();?>/publish-recipe"><img src="<?php site_url();?>/wp-content/uploads/2020/01/recipe.png"> <span><?php _e('Recept Delen', 'osetin'); ?></span></a></li>
										<li class="rec-profile"><a href="<?php site_url();?>/my-profile"><img src="<?php site_url();?>/wp-content/uploads/2020/01/kookmutsjes-user.png"> <span><?php _e('Mijn Profiel', 'osetin'); ?></span></a></li>
										<li class="rec-logout"><a href="<?php echo wp_logout_url( home_url() ); ?>"><img src="<?php site_url();?>/wp-content/uploads/2020/01/kookmutsjes-switch.png"> <span><?php _e('Uitloggen', 'osetin'); ?></span></a></li>
										<?php else : ?>
											<li class="rec-publish"><a href="<?php site_url();?>/log-in"><img src="<?php site_url();?>/wp-content/uploads/2020/01/recipe.png"> <span><?php _e('Recept Delen', 'osetin'); ?></span></a></li>
											<li class="rec-register"><a href="<?php site_url();?>/register/"><img src="<?php site_url();?>/wp-content/uploads/2020/01/add-user.png"> <span><?php _e('Aanmelden', 'osetin'); ?></span></a></li>
											<li class="rec-login"><a href="<?php site_url();?>/log-in"><img src="<?php site_url();?>/wp-content/uploads/2020/01/user-1.png"> <span><?php _e('Log in', 'osetin'); ?></span></a></li>
										<?php endif;  ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="ipadsection">
							<div class="ipad-logo">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php site_url();?>/wp-content/uploads/2020/01/Asset-1.png" alt="mobile-logo" /></a>
							</div>
							<div class="usermenus">
								<div class="accessbar">
									<ul class="usermenu">
										<?php if (is_user_logged_in()) : ?>
										<li class="rec-publish"><a href="<?php site_url();?>/publish-recipe"><img src="<?php site_url();?>/wp-content/uploads/2020/01/recipe.png"> <span><?php _e('Recept Delen', 'osetin'); ?></span></a></li>
										<li class="rec-profile"><a href="<?php site_url();?>/my-profile"><img src="<?php site_url();?>/wp-content/uploads/2020/01/kookmutsjes-user.png"> <span><?php _e('Mijn Profiel', 'osetin'); ?></span></a></li>
										<li class="rec-logout"><a href="<?php echo wp_logout_url( home_url() ); ?>"><img src="<?php site_url();?>/wp-content/uploads/2020/01/kookmutsjes-switch.png"> <span><?php _e('Uitloggen', 'osetin'); ?></span></a></li>
										<?php else : ?>
											<li class="rec-publish"><a href="<?php site_url();?>/log-in"><img src="<?php site_url();?>/wp-content/uploads/2020/01/recipe.png"> <span><?php _e('Recept Delen', 'osetin'); ?></span></a></li>
											<li class="rec-register"><a href="<?php site_url();?>/register/"><img src="<?php site_url();?>/wp-content/uploads/2020/01/kookmutsjes-registration.png"> <span><?php _e('Aanmelden', 'osetin'); ?></span></a></li>
											<li class="rec-login"><a href="<?php site_url();?>/log-in"><img src="<?php site_url();?>/wp-content/uploads/2020/01/kookmutsjes-login.png"> <span><?php _e('Log in', 'osetin'); ?></span></a></li>
										<?php endif;  ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="os-container main-header-w main-header-<?php echo $top_menu_version_type; ?>">
							<div class="main-header color-scheme-<?php echo $top_menu_bg_color_type; ?> " style="<?php echo osetin_get_css_prop('background-color', 	$top_menu_bg_color); ?>">
								<?php if($top_menu_version_type != 'version_2'){ ?>
								<div class="logo" style="width: <?php echo $logo_image_width; ?>px;">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
										<img src="<?php echo $logo_image_url; ?>" alt="">
									</a>
								</div>
								<?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'header-menu', 'container_class' => 'top-menu menu-activated-on-hover', 'fallback_cb' => false ) ); ?>
								<?php }else{ ?>
								<div class="top-logo-w">
									<div class="social-trigger-w">
										<div class="social-trigger">
											<i class="os-icon os-icon-thin-heart"></i>
										</div>
										<?php osetin_social_share_icons('header'); ?>
									</div>
									<div class="logo" style="width: <?php echo $logo_image_width; ?>px;">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
											<img src="<?php echo $logo_image_url; ?>" alt="">
										</a>
									</div>
								</div>
								<div class="top-menu-w">
									<?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'header-menu', 'container_class' => 'top-menu 	menu-activated-on-hover', 'fallback_cb' => false ) ); ?>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php if(osetin_get_field('use_fixed_header', 'option')){ ?>
						<div class="fixed-header-w color-scheme-<?php echo osetin_get_field('fixed_menu_bar_background_color_type', 'option', 'light'); ?>" style="<?php echo osetin_get_css_prop('background-color', osetin_get_field('fixed_menu_bar_background_color', 'option')); ?>">
							<div class="os-container">
								<div class="fixed-header-i">
									<div class="fixed-logo-w" style="width: <?php echo $fixed_menu_logo_image_width; ?>px;">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
										<img src="<?php echo $fixed_menu_logo_image_url; ?>" alt="">
										</a>
									</div>
									<?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'fixed-header-menu', 'container_class' => 'fixed-top-menu-w menu-activated-on-hover', 'fallback_cb' => false ) ); ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="mobile-header-w ipadsection">
							<div class="mobile-header-menu-w menu-activated-on-click color-scheme-<?php echo osetin_get_field('mobile_header_background_color_type', 'option', 'dark'); ?>" style="<?php echo osetin_get_css_prop('background-color', osetin_get_field('mobile_header_background_color', 'option')); ?>">
								<?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'mobile-header-menu', 'container' => '', 'fallback_cb' => false ) ); ?>
							</div>
							<div class="mobile-header">
								<div class="mobile-menu-toggler">
								  <i class="os-icon os-icon-thin-hamburger"></i>
								</div>
								<div class="mobile-logo" style="width: <?php echo $mobile_logo_image_width; ?>px;">
								  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $mobile_logo_image_url; ?>" alt=""></a>
								</div>
								<div class="ipad-flat-menu">
									<ul>
										<li><a href="<?php site_url();?>/recepten-categories/">Recepten</a></li>
										<li><a href="<?php site_url();?>/stories/">Stories</a></li>
										<li><a href="<?php site_url();?>/converter-tool/">Tools</a></li>
										<li><a href="<?php site_url();?>/community/">Community</a></li>
									</ul>
								</div>
								<div class="mobile-menu-search-toggler">
								  <i class="os-icon os-icon-thin-search"></i>
								</div>
							</div>
						</div>
					</div>
				</div>