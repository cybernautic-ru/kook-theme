<?php

/**
 * Template part for recipe board items - ajax requested
 */

?>

<?php

/**
 * Template part for recipe board items
 */

$current_step_class = 'full_full_over';
?>
<article class="rb-item any" id="post-{ID}">
    <div class="rb-item-i">

        <div class="rb-item-first-row-info">

            <div class="rb-item-media_block">
                <div class="rb-item-media">
                    {media}
                </div>

                <div class="rb-item-media-buttons_block">
                    <div class="rb-item-meta">

                        <div class="rb-item-rating-and-read-more">
                            <a class="rb-item-read-more-btn" href="{permalink}"><?php _e('Lees meer', 'osetin'); ?></a>
                        </div>

                        <div class="rb-item-favorite">
                            <div class="favorite">
                                <button {savedDisabled} name="favorite-model" data-postid="{ID}" data-exe="false" data-redirect="{savedRedirect}" class="btn"><i class="fa {savedIconClass}"></i> <?php _e('Bewaard', 'neptune-child'); ?></button>
                            </div>
                        </div>

                        <div class="rb-item-views-count-likes">
                            <a href="#" class="slide-button slide-like-button osetin-vote-trigger {likesVotedClass}" data-has-voted-label="<?php _e('Heb ik gemaakt', ''); ?>" data-not-voted-label="<?php _e('Heb ik gemaakt', ''); ?>" data-post-id="{ID}" data-vote-action="{likesVoteAction}" data-votes-count="{likesCount}">
                                <span class="slide-button-i">
                                    <i class="fa fa-heart"></i>
                                    <span class="slide-button-label osetin-vote-action-label">
                                        <?php echo esc_html__("Heb ik gemaakt", "osetin"); ?>
                                    </span>
                                    <span class="slide-button-sub-label osetin-vote-count {likesCountHiden}">
                                        {likesCount}
                                    </span>
                                </span>
                            </a>
                        </div>

                    </div>
                </div>

            </div>

            <div class="rb-item-content_block">

                <header class="rb-item-header">
                    <h3 class="rb-item-title"><a href="{permalink}" rel="bookmark">{title}</a></h3>

                    <div class="rb-item-post-info">
                        <div class="date-comment">
                            <div class="rb-item-date-posted">{date}</div>
                        </div>

                        <div class="rb-item-comments">
                            <a href="{permalink}#singlePostComments"><i class="fa fa-comment-o"></i>
                                <span>{commentsNumberText}</span></a>
                        </div>

                        <div class="rb-item-post-info-author">
                            <a class="recipe-author" href="{authorUrl}">door {authorName}</a>
                        </div>
                    </div>
                </header>

                <div class="rb-item-content-text">
                    {excerpt}
                </div>

                <div class="rb-item-author-meta">
                    <div class="date-comment">
                        <div class="rb-item-date-posted">{date}</div>
                    </div>

                    <div class="rb-item-comments">
                        <a href="{permalink}#singlePostComments">
                            <i class="fa fa-comment-o"></i>
                            <span>{commentsNumberText}</span>
                        </a>
                    </div>
                </div>

            </div>

            <div class="rb-item-recipe-info_block">

                <div class="rb-item-recipe-info-i">
                    <div class="rb-item-recipe-info-i_title"><span><?php _e('Preptime', 'neptune-child'); ?></span></div>
                    <div class="rb-item-recipe-info-i_value"><span>{preptime}</span></div>
                </div>

                <div class="rb-item-recipe-info-i">
                    <div class="rb-item-recipe-info-i_title"><span><?php _e('Serves', 'neptune-child'); ?></span></div>
                    <div class="rb-item-recipe-info-i_value"><span>{serves}</span></div>
                </div>

                <div class="rb-item-recipe-info-i">
                    <div class="rb-item-recipe-info-i_title"><span>{level}</span></div>
                    <div class="rb-item-recipe-info-i_value"><span><?php _e('Recipe', 'neptune-child'); ?></span></div>
                </div>

            </div>

        </div>

        <div class="rb-item-mobile-only-description_block">
            {excerpt}
        </div>

        <div class="rb-item-content">
            <div class="rb-item-meta">

                <div class="rb-item-rating-and-read-more">
                    <a class="rb-item-read-more-btn" href="{permalink}"><?php _e('Lees meer', 'osetin'); ?></a>
                </div>

                <div class="rb-item-favorite">
                    <div class="favorite">
                        <button {savedDisabled} name="favorite-model" data-postid="{ID}" data-exe="false" data-redirect="{savedRedirect}" class="btn"><i class="fa {savedIconClass}"></i> <?php _e('Bewaard', 'neptune-child'); ?></button>
                    </div>
                </div>

                <div class="rb-item-views-count-likes">
                    <a href="#" class="slide-button slide-like-button osetin-vote-trigger {likesVotedClass}" data-has-voted-label="<?php _e('Heb ik gemaakt', ''); ?>" data-not-voted-label="<?php _e('Heb ik gemaakt', ''); ?>" data-post-id="{ID}" data-vote-action="{likesVoteAction}" data-votes-count="{likesCount}">
                        <span class="slide-button-i">
                            <i class="fa fa-heart"></i>
                            <span class="slide-button-label osetin-vote-action-label">
                                <?php echo esc_html__("Heb ik gemaakt", "osetin"); ?>
                            </span>
                            <span class="slide-button-sub-label osetin-vote-count {likesCountHiden}">
                                {likesCount}
                            </span>
                        </span>
                    </a>
                </div>

            </div>
        </div>

    </div>

</article>