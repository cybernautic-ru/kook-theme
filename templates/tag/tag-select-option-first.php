<?php

/**
 * Template part for tag select first option
 */

?>
<option value="0" disabled="disabled" selected="selected"><?php esc_html_e('Select a tag', 'neptune-child'); ?></option>