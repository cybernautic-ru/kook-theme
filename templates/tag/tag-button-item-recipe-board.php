<?php

/**
 * Template part for tag items on the recipe board
 */

?>

<div class="rb-tag-item" data-tag-item-id="{tagID}">
    <span class="rb-tag-item_name">{name}</span> (<span class="rb-tag-item_num">{num}</span>)
    <button class="rb-tag-item_close-button"><i class="fa fa-times"></i></button>
</div>