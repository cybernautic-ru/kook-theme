<?php

/**
 * Template part for no recipe board message
 */

?>
<div class="rb-no-posts-block">
    <span><?php _e('There is no posts in your query.', 'neptune-child');?></span>
</div>
