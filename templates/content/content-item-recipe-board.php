<?php

/**
 * Template part for recipe board items
 */

$current_step_class = 'full_full_over';
?>
<article id="post-<?php echo esc_attr( $post->ID ); ?>" <?php post_class( 'rb-item any ' ); ?>>
    <div class="rb-item-i">

        <div class="rb-item-first-row-info">

            <div class="rb-item-media_block">
                <div class="rb-item-media">
					<?php
					// ------ MEDIA
					if ( ( get_post_format() == 'gallery' ) && osetin_get_field( 'gallery_images', false, false, true ) && ! isset( $only_image ) ) {
						// GALLERY
						$gallery_images = osetin_get_field( 'gallery_images', false, false, true );
						$thumb_name     = osetin_get_archive_thumb_name( $current_step_class );
						if ( isset( $gallery_images[0] ) ) {
							?>
                            <a href="<?php the_permalink(); ?>" class="rb-item-media-thumbnail fader-activator"
                               style="background-image:url(<?php echo $gallery_images[0]['sizes'][ $thumb_name ]; ?>); background-size: cover;">
                                <span class="image-fader"><span class="hover-icon-w"><i
                                                class="os-icon os-icon-plus"></i></span></span>
                            </a>
							<?php
						}
						foreach ( $gallery_images as $key => $image ) {
							$active_class = ( $key == 0 ) ? 'active' : '';
							echo '<div class="gallery-image-source ' . $active_class . '" data-gallery-image-url="' . $image['sizes'][ $thumb_name ] . '"></div>';
						}
						echo '<div class="gallery-image-previous"><i class="os-icon os-icon-angle-left"></i></div>';
						echo '<div class="gallery-image-next"><i class="os-icon os-icon-angle-right"></i></div>';
					} elseif ( get_post_format() == 'video' && osetin_get_field( 'video_shortcode' ) && ! isset( $only_image ) ) {
						// VIDEO
						echo do_shortcode( osetin_get_field( 'video_shortcode' ) );
					} else {
						?>
						<?php
						$gif_html = '';

						$item_bg_image = osetin_output_post_thumbnail_url( osetin_get_archive_thumb_name( $current_step_class ), false );

						if ( pathinfo( $item_bg_image, PATHINFO_EXTENSION ) == 'gif' && osetin_get_field( 'is_gif_media' ) ) {
							$gif_html = '<span class="gif-label"><i class="os-icon os-icon-basic1-082_movie_video_camera"></i><span>' . __( 'GIF', 'sun' ) . '</span></span>';
							if ( osetin_get_field( 'preview_image_for_lazy_gif' ) ) {
								$preview_img_src = wp_get_attachment_image_src( osetin_get_field( 'preview_image_for_lazy_gif' ), 'osetin-full-width' );
								if ( ! empty( $preview_img_src[0] ) ) {
									$item_bg_image = $preview_img_src[0];
								}
							}
						}
						?>
                        <a href="<?php the_permalink(); ?>" class="rb-item-media-thumbnail fader-activator"
                           style="background-image:url(<?php echo $item_bg_image; ?>); background-size: cover;">
                            <span class="image-fader"><span class="hover-icon-w"><i
                                            class="os-icon os-icon-plus"></i></span></span>
							<?php echo $gif_html; ?>
                        </a>
					<?php } ?>
                </div>

                <div class="rb-item-media-buttons_block">
                    <div class="rb-item-meta">

                        <div class="rb-item-rating-and-read-more">
                            <a class="rb-item-read-more-btn"
                               href="<?php the_permalink(); ?>"><?php _e( 'Lees meer', 'osetin' ); ?></a>
                        </div>

						<?php if ( is_user_logged_in() ) {
							$saved_favorite_post = unserialize( get_user_meta( get_current_user_id(), 'saved_favorite_post', true ) );
							$redirect            = 'false';
							if ( ! $saved_favorite_post ) {
								$saved_favorite_post = array();
							}
						} else {
							$redirect            = 'true';
							$saved_favorite_post = array();
						} ?>
                        <div class="rb-item-favorite">
                            <div class="favorite">
								<?php
								if ( in_array( get_the_ID(), $saved_favorite_post ) ) {
									?>
                                    <button disabled name="favorite-model" data-postid="<?php echo get_the_ID(); ?>"
                                            data-exe="false" data-redirect="<?php echo $redirect; ?>" class="btn"><i
                                                class="fa fa-bookmark"></i> <?php _e( 'Bewaard', 'neptune-child' ); ?>
                                    </button>
								<?php } else { ?>
                                    <button name="favorite-model" data-postid="<?php echo get_the_ID(); ?>"
                                            data-exe="false" data-redirect="<?php echo $redirect; ?>" class="btn"><i
                                                class="fa fa-bookmark-o"></i> <?php _e( 'Bewaard', 'neptune-child' ); ?>
                                    </button>
								<?php } ?>
                            </div>
                        </div>

                        <div class="rb-item-views-count-likes">
							<?php osetin_vote_build_button( get_the_ID(), 'slide-button slide-like-button' ); ?>
                        </div>

                    </div>
                </div>

            </div>

            <div class="rb-item-content_block">

                <header class="rb-item-header">
					<?php the_title( sprintf( '<h3 class="rb-item-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

                    <div class="rb-item-post-info">
                        <div class="date-comment">
                            <div class="rb-item-date-posted"><?php echo date( 'F j,Y', strtotime( get_the_date() ) ); ?></div>
                        </div>

                        <div class="rb-item-comments">
                            <a href="<?php the_permalink() ?>#singlePostComments"><i class="fa fa-comment-o"></i>
                                <span><?php comments_number( '0 ' . __( 'Comments', 'osetin' ), '1 ' . __( 'Comment', 'osetin' ), '% ' . __( 'Comments', 'osetin' ) ); ?></span></a>
                        </div>

                        <div class="rb-item-post-info-author">
                            <a class="recipe-author
"
                               href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                                door <?php the_author_meta( 'display_name' ); ?></a>
                        </div>
                    </div>
                </header>

                <div class="rb-item-content-text">
					<?php echo osetin_excerpt( 80, false ); ?>
                </div>

                <div class="rb-item-author-meta">
                    <div class="date-comment">
                        <div class="rb-item-date-posted"><?php echo date( 'F j,Y', strtotime( get_the_date() ) ); ?></div>
                    </div>

                    <div class="rb-item-comments">
                        <a href="<?php the_permalink() ?>#singlePostComments"><i class="fa fa-comment-o"></i>
                            <span><?php comments_number( '0 ' . __( 'Comments', 'osetin' ), '1 ' . __( 'Comment', 'osetin' ), '% ' . __( 'Comments', 'osetin' ) ); ?></span></a>
                    </div>
                </div>

            </div>

            <div class="rb-item-recipe-info_block">

                <div class="rb-item-recipe-info-i">
					<?php
					$r_prep_time_field = osetin_get_field( 'recipe_cooking_time' );
					$prep_time         = isset( $r_prep_time_field ) && ! empty( $r_prep_time_field ) ? $r_prep_time_field : osetin_get_field( 'recipe_preparation_time' ); ?>
                    <div class="rb-item-recipe-info-i_title"><span><?php _e( 'Preptime', 'neptune-child' ); ?></span>
                    </div>
                    <div class="rb-item-recipe-info-i_value"><span><?php echo esc_html( $prep_time ); ?></span></div>
                </div>

                <div class="rb-item-recipe-info-i">
					<?php
					$r_serves_field = osetin_get_field( 'recipe_serves' );
					$r_serves       = isset( $r_serves_field ) && ! empty( $r_serves_field ) ? $r_serves_field : __( 'No Serves', 'neptune-child' ); ?>
                    <div class="rb-item-recipe-info-i_title"><span><?php _e( 'Serves', 'neptune-child' ); ?></span>
                    </div>
                    <div class="rb-item-recipe-info-i_value"><span><?php echo esc_html( $r_serves ); ?></span></div>
                </div>

                <div class="rb-item-recipe-info-i">
					<?php
					$r_difficulty_field = osetin_get_difficulty_string( osetin_get_field( 'recipe_difficulty' ) );
					$r_difficulty       = isset( $r_difficulty_field ) && ! empty( $r_difficulty_field ) ? $r_difficulty_field : __( 'No Difficulty', 'neptune-child' ); ?>
                    <div class="rb-item-recipe-info-i_title"><span><?php echo esc_html( $r_difficulty ); ?></span></div>
                    <div class="rb-item-recipe-info-i_value"><span><?php _e( 'Recipe', 'neptune-child' ); ?></span>
                    </div>
                </div>

            </div>

        </div>

        <div class="rb-item-mobile-only-description_block">
			<?php echo osetin_excerpt( 80, false ); ?>
        </div>

        <div class="rb-item-content">
            <div class="rb-item-meta">

                <div class="rb-item-rating-and-read-more">
                    <a class="rb-item-read-more-btn"
                       href="<?php the_permalink(); ?>"><?php _e( 'Lees meer', 'osetin' ); ?></a>
                </div>

				<?php if ( is_user_logged_in() ) {
					$saved_favorite_post = unserialize( get_user_meta( get_current_user_id(), 'saved_favorite_post', true ) );
					$redirect            = 'false';
					if ( ! $saved_favorite_post ) {
						$saved_favorite_post = array();
					}
				} else {
					$redirect            = 'true';
					$saved_favorite_post = array();
				} ?>
                <div class="rb-item-favorite">
                    <div class="favorite">
						<?php
						if ( in_array( get_the_ID(), $saved_favorite_post ) ) {
							?>
                            <button disabled name="favorite-model" data-postid="<?php echo get_the_ID(); ?>"
                                    data-exe="false" data-redirect="<?php echo $redirect; ?>" class="btn"><i
                                        class="fa fa-bookmark"></i> <?php _e( 'Bewaard', 'neptune-child' ); ?>
                            </button>
						<?php } else { ?>
                            <button name="favorite-model" data-postid="<?php echo get_the_ID(); ?>" data-exe="false"
                                    data-redirect="<?php echo $redirect; ?>" class="btn"><i
                                        class="fa fa-bookmark-o"></i> <?php _e( 'Bewaard', 'neptune-child' ); ?>
                            </button>
						<?php } ?>
                    </div>
                </div>

                <div class="rb-item-views-count-likes">
					<?php osetin_vote_build_button( get_the_ID(), 'slide-button slide-like-button' ); ?>
                </div>

            </div>
        </div>

    </div>

</article>