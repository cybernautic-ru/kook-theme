(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

$(document).ready(function () {
  // Don't touch
  objectFitImages();
  svg4everybody(); // End don't touch

  require('./target-blank.js');

  require('./menu.js');

  require('./recipes.js');
}); // remove preloader

$(window).on('load', function () {
  $('.preloader').fadeOut();
});

},{"./menu.js":2,"./recipes.js":3,"./target-blank.js":4}],2:[function(require,module,exports){
"use strict";

(function () {// Code here
})();

},{}],3:[function(require,module,exports){
"use strict";

(function () {
  $('.recipes').each(function () {
    var tabTabs = $(this).find('.recipes-galary__item');
    var tabItems = $(this).find('.recipes__tab');
    tabTabs.each(function (i) {
      $(this).click(function () {
        // if ($(this).hasClass('active')) {
        //   $(this).removeClass('active');
        //   tabItems.slideUp();
        //   return;
        // }

        $(this).addClass('active');
        tabTabs.not(this).removeClass('active');
        $(tabItems[i]).fadeIn();
        tabItems.not(tabItems[i]).hide();
      });
    });
  });
})();

},{}],4:[function(require,module,exports){
"use strict";

// add target blank to external links
var siteUrl = window.location.hostname;
$('a[href*="//"]:not([href*="' + siteUrl + '"])').attr({
  target: '_blank',
  rel: 'noopener noreferrer'
});

},{}]},{},[1])

//# sourceMappingURL=bundle.js.map
