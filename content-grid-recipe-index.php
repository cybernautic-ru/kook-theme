<?php
if(!isset($layout_type_for_index)){
	global $layout_type_for_index;
}
if(!isset($current_step_class)){
	global $current_step_class;
}
if(!isset($limit)){
	global $limit;
}
if(!isset($hidden_elements_array)){
	$hidden_elements_array = osetin_get_hidden_elements_array();
}
?>
<article <?php post_class('archive-item any '.$current_step_class); ?>>
	<div class="archive-item-i">
		<div class="extra-styling-box"></div>
		<?php if(!in_array('share', $hidden_elements_array)){ ?>
			<div class="archive-item-share-w active">
				<div class="archive-item-share-trigger">
					<div class="archive-item-share-plus"><i class="os-icon os-icon-plus"></i></div>
					<div class="archive-item-share-label"><?php esc_html_e('Share', 'osetin'); ?></div>
					<div class="archive-item-share-icons">
						<?php osetin_get_sharing_icons(); ?>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="archive-item-media">
			<?php
			// ------ MEDIA
			if((get_post_format() == 'gallery') && osetin_get_field('gallery_images', false, false, true) && !isset($only_image)){
				// GALLERY
				$gallery_images = osetin_get_field('gallery_images', false, false, true);
				$thumb_name = osetin_get_archive_thumb_name($current_step_class);
				if(isset($gallery_images[0]))
				{?>
					<a href="<?php the_permalink(); ?>" class="archive-item-media-thumbnail fader-activator" style="background-image:url(<?php echo $gallery_images[0]['sizes'][$thumb_name]; ?>); background-size: cover;">
						<span class="image-fader"><span class="hover-icon-w"><i class="os-icon os-icon-plus"></i></span></span>
					</a>
					<?php
				}
				foreach( $gallery_images as $key => $image ){
					$active_class = ($key == 0) ? 'active' : '';
					echo '<div class="gallery-image-source '.$active_class.'" data-gallery-image-url="'.$image['sizes'][$thumb_name].'"></div>';
				}
				echo '<div class="gallery-image-previous"><i class="os-icon os-icon-angle-left"></i></div>';
				echo '<div class="gallery-image-next"><i class="os-icon os-icon-angle-right"></i></div>';
			}
			elseif(get_post_format() == 'video' && osetin_get_field('video_shortcode') && !isset($only_image))
			{
				// VIDEO
				echo do_shortcode(osetin_get_field('video_shortcode'));
			}else{?>
				<?php
				$gif_html = '';
				if($current_step_class == 'hero'){
					$custom_image_for_header = osetin_get_field('custom_image_for_header', false, false, true);
					if(is_array($custom_image_for_header)){
						$item_bg_image = $custom_image_for_header['sizes']['osetin-full-width'];
					}else{
						$item_bg_image = osetin_output_post_thumbnail_url('osetin-full-width');
					}
				}else{
					$item_bg_image = osetin_output_post_thumbnail_url(osetin_get_archive_thumb_name($current_step_class), false);
				}
				if(pathinfo($item_bg_image, PATHINFO_EXTENSION) == 'gif' && osetin_get_field('is_gif_media')){
					$gif_html = '<span class="gif-label"><i class="os-icon os-icon-basic1-082_movie_video_camera"></i><span>'.__('GIF', 'sun').'</span></span>';
					if(osetin_get_field('preview_image_for_lazy_gif'))
					{
						$preview_img_src = wp_get_attachment_image_src(osetin_get_field('preview_image_for_lazy_gif'), 'osetin-full-width');
						if(!empty($preview_img_src[0])){
							$item_bg_image = $preview_img_src[0];
						}
					}
				}
				?>
				<a href="<?php the_permalink(); ?>" class="archive-item-media-thumbnail fader-activator" style="background-image:url(<?php echo $item_bg_image; ?>); background-size: cover;">
					<span class="image-fader"><span class="hover-icon-w"><i class="os-icon os-icon-plus"></i></span></span>
					<?php echo $gif_html; ?>
				</a>
			<?php } ?>
		</div>

		<div class="archive-item-content">
			<?php if(!in_array('title', $hidden_elements_array)){ ?>
				<header class="archive-item-header">
					<ul>
						<li><?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?></li>
						<li><a class="recipe-author" href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><img class="author-avt" src="<?php echo get_avatar_url(get_the_author_meta( 'ID' ), ['size' => '40']); ?>" alt="inbox-avatar"> door <?php the_author_meta( 'display_name' ); ?></a></li>
					</ul>
				</header>
			<?php } ?>

				<div class="archive-item-content-text">
					<?php echo osetin_excerpt(100, false); ?>
				</div>
            
			<?php if($current_step_class != 'full_third'){ ?>
				<?php if(array_diff(array('author_avatar', 'author_name', 'date', 'like'), $hidden_elements_array)){ ?>
					<div class="archive-item-author-meta">
						<ul class="metaul">
							<li>
								<?php if(in_array('author_avatar', $hidden_elements_array)){?>
									<div class="author-avatar-w authorimg">
										<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_avatar(get_the_author_meta( 'ID' )); ?></a>
									</div>
								<?php } ?>
							</li>
							<?php /*if(array_diff(array('author_name', 'date'), $hidden_elements_array)){*/ ?>
							<?php /*if(in_array('author_name', $hidden_elements_array)){*/ ?>
							<!--<li>
								<div class="author-name">
									<a href="<?php //echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php the_author_meta( 'display_name' ); ?></a>
								</div>
							</li> -->
							<?php /*}*/ ?>
							<li>
								<div class="date-comment">
									<?php /*if(!in_array('date', $hidden_elements_array)){*/ ?>
									<div class="archive-item-date-posted"><?php echo date('F j,Y',strtotime(get_the_date())); ?></div>
									<?php /* }*/ ?>
								</div>
							</li>
							<li class="post-comnts">
								<!-- custom coding carbojet-->
								<div class="archive-item-comments">
									<a href="<?php the_permalink() ?>#singlePostComments"><i class="fa fa-comment-o"></i> <span><?php comments_number( __('Comment', 'osetin'), '1 '.__('Comment', 'osetin'), '% ' ); ?> Comments</span></a>
								</div>
								<?php /*}*/ ?>
							</li>
						</ul>
					</div>
				<?php } ?>
			<?php } ?>

			<?php if($current_step_class == 'full_third'){  echo '</div><div class="clear"></div><div>'; } ?>
			<?php if(array_diff(array('rating', 'read_more', 'comments', 'views'), $hidden_elements_array)){ ?>
			<div class="archive-item-deep-meta">
				<ul class="recipe-meta">
					<li>
						<?php if(array_diff(array('rating', 'read_more'), $hidden_elements_array)){ ?>
							<div class="archive-item-rating-and-read-more">
								<?php
								if(!in_array('rating', $hidden_elements_array)){
									$reviews_info = osetin_recipe_rating_average_and_total(get_the_ID());
									if($reviews_info){
										if($reviews_info->avg_rating)
										{
											echo '<div class="archive-item-rating">';
											$stars_html = osetin_build_stars($reviews_info->avg_rating);
											echo '<div class="review-summary-average">'.$stars_html.'</div>';
											if($reviews_info->total_reviews) echo '<a href="'.get_permalink().'#osetinRecipeReviews" class="review-summary-total">'.$reviews_info->total_reviews.'</a>';
											echo '</div>';
										}
									}
								}
								?>
								<?php if(!in_array('read_more', $hidden_elements_array) && $current_step_class != 'full_third'){ ?>
									<div class="archive-item-read-more-btn">
										<div class="read-more-link"><a href="<?php the_permalink(); ?>"><?php _e('Lees meer', 'osetin'); ?></a></div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</li>
					<li>
						<!-- custimize carbojet-->
						<?php if(is_user_logged_in())
						{
							$saved_favorite_post = unserialize( get_user_meta(get_current_user_id(),'saved_favorite_post',true));
							//var_dump($saved_favorite);
							$redirect = 'false';
							if(!$saved_favorite_post){
								$saved_favorite_post = array();
							}
						}
						else{
							$redirect = 'true';
							$saved_favorite_post = array();
						}?>
						<div class="favorite">
							<?php
							if(in_array(get_the_ID(),$saved_favorite_post)){?>
								<button disabled name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark"></i> Bewaard</button>
							<?php }else{ ?>
								<button name="favorite-model" data-postid="<?php echo get_the_ID();?>" data-exe="false" data-redirect="<?php echo $redirect;?>" class="btn"><i class="fa fa-bookmark-o"></i> Bewaar</button>
							<?php }	?>
							<!--<span><?php echo get_post_meta(get_the_ID(), '_osetin_vote', true) ?></span>-->
						</div>
					</li>
					<li>
						<?php if(!in_array('like', $hidden_elements_array)){ ?>
							<div class="archive-item-views-count-likes">
								<?php osetin_vote_build_button(get_the_ID(), 'slide-button slide-like-button'); ?>
							</div>
						<?php } ?>
					</li>
					<?php /* if(!in_array('comments', $hidden_elements_array)){ ?>
							<div class="archive-item-comments">
							  <a href="<?php the_permalink() ?>#singlePostComments"><i class="os-icon os-icon-thin-comment"></i> <span><?php comments_number( __('Comment', 'osetin'), '1 '.__('Comment', 'osetin'), '% '.__('Comments', 'osetin') ); ?></span></a>
							</div>
							<?php } */?>
					<?php if(!in_array('views', $hidden_elements_array)){ ?>
						<?php if(function_exists('echo_tptn_post_count')){
							echo '<div class="archive-item-views-count">';
							echo '<i class="os-icon os-icon-eye"></i> <span>'. do_shortcode('[tptn_views daily="0"]'). ' ' . esc_html__('Views', 'osetin') .'</span>';
							echo '</div>'; ?>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
		</div>
	<?php } ?>
		<div class="clear"></div>
		<?php  /* edit_post_link( esc_html__( 'Edit', 'osetin' ), '<span class="edit-link">', '</span>' );*/ ?>
	</div>
    <div class="rb-1-col-view-add">
        <h3 class="rb-1-col-view-add-title">
            <i class="os-icon os-icon-thin-paper-holes-text"></i> <?php esc_html_e( 'Ingredients', 'osetin' ); ?>
        </h3>
        <table class="ingredients-table">
	        <?php $searchable_ingredients = osetin_get_field( 'searchable_ingredients' ); ?>
		    <?php
		    // loop through the rows of ingredients
		    while ( osetin_have_rows( 'ingredients' ) ) {
			    the_row();

			    ?>
                <tr>
				    <?php if ( get_sub_field( 'separator' ) ) { ?>
                        <td></td>
                        <td>
                            <div class="ingredient-heading"><?php the_sub_field( 'ingredient_name' ); ?></div>
                        </td>
				    <?php } else { ?>
                        <td class="ingredient-action">
                            <span class="ingredient-mark-icon"><i class="os-icon os-icon-circle-o"></i></span>
                        </td>
                        <td>
                                                <span class="ingredient-amount"
                                                      data-initial-amount="<?php the_sub_field( 'ingredient_amount' ); ?>"><?php the_sub_field( 'ingredient_amount' ); ?></span>
						    <?php
						    if ( $searchable_ingredients ) { ?>
							    <?php
							    $ingredient_obj = get_sub_field( 'ingredient_obj' );
							    $ingredient_link_html = get_sub_field( 'ingredient_name' );

							    if ( $ingredient_obj ) {
								    $ingredient_link_html = '<a href="' . get_term_link( $ingredient_obj ) . '" target="_blank">' . $ingredient_obj->name . '</a>';
							    }
							    if ( get_sub_field( 'custom_link' ) ) {
								    $ingredient_link_html = '<a href="' . get_sub_field( 'custom_link' ) . '" target="_blank">' . $ingredient_obj->name . '</a>';
							    }
							    if ( $ingredient_obj || $ingredient_link_html ) { ?>
                                    <span class="ingredient-name"><?php echo $ingredient_link_html; ?></span>
							    <?php } ?>
							    <?php
						    } else { ?>
							    <?php
							    if ( get_sub_field( 'custom_link' ) ) {
								    echo '<span class="ingredient-name"><a href="' . get_sub_field( 'custom_link' ) . '" target="_blank">' . get_sub_field( 'ingredient_name' ) . '</a></span>';
							    } else {
								    echo '<span class="ingredient-name">' . get_sub_field( 'ingredient_name' ) . '</span>';
							    } ?>
							    <?php
						    } ?>

						    <?php
						    if ( get_sub_field( 'ingredient_note' ) ) {
							    echo '<span class="ingredient-info-icon" onclick="void(0)"><i class="os-icon os-icon-info-round"></i><span class="ingredient-info-popup">' . esc_attr( get_sub_field( 'ingredient_note' ) ) . '</span></span>';
						    } ?>
                        </td>
				    <?php } ?>
                </tr>
			    <?php
		    }
		    wp_reset_postdata();
		    wp_reset_query();
		    ?>
        </table>
    </div>
</article>