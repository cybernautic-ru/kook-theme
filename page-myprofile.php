<?php /* Template Name: My Profile */ ?>
	<?php
	/**
	 * The template for displaying all pages.
	 *
	 * @package Neptune
	 */

	get_header(); ?>
	<div class="os-container">
		<div class="myprofile">
			<div class="banner">
				<div class="mobile-qa">
					<?php echo do_shortcode('[notification-box userid="'.get_current_user_id().'"]');?>
					<a id="p-publish" href="<?php site_url();?>/publish-recipe"><i class="fa fa-plus" aria-hidden="true"></i></a>
					<div class="mobile-ppic">
						<?php echo do_shortcode('[avatar_upload]'); ?>
						<div class="follwers">
							<?php echo do_shortcode('[follow-follower-posts userid="'.get_current_user_id().'"]'); ?>
						</div>
					</div>
				</div>
				<div class="desktop-qa">
					<?php echo do_shortcode('[notification-box userid="'.get_current_user_id().'"]');?>
				</div>
			</div>
			<div class="profile-body-top mobile-qas">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6 siderow">
								<div class="recipe-new-post">
									<?php dynamic_sidebar('Profile Latest Recipe'); ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="recipe-new-post-title">
									<p>NIEUWSTE RECEPT!</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6 siderow">
								<div class="story-new-post">
									<?php dynamic_sidebar('Profile Latest Post'); ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="story-new-post-title">
									<p>NIEUWSTE STORY!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="profile-menu mobile-qa">
				<ul class="mobile-qa">
					<div class="tab">
						<li><button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Profiel</button></li>
						<li><button class="tablinks" onclick="openCity(event, 'Paris')">Recepten</button></li>
						<li><button class="tablinks" onclick="openCity(event, 'saved-recipe-block')">Favorieten</button></li>
						<li><button class="tablinks" onclick="openCity(event, 'week')">Weekmenu</button></li>
						<li><button class="tablinks" onclick="openCity(event, 'inbox')">Inbox</button></li>
					</div>
				</ul>
			</div>
			<div class="profile-body ipad-profile">
				<div class="row">
					<div class="col-md-4">
						<div class="user-info-part">
							<div class="profile-img">
								<?php echo do_shortcode('[avatar_upload]'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6 frec">
								<div class="siderow">
									<div class="recipe-new-post">
										<?php dynamic_sidebar('Profile Latest Recipe'); ?>
									</div>
									<div class="recipe-new-post-title">
										<p>NIEUWSTE RECEPT!</p>
									</div>
								</div>
							</div>
							<div class="col-md-6 lrec">
								<div class="siderow">
									<div class="story-new-post">
										<?php dynamic_sidebar('Profile Latest Post'); ?>
									</div>
									<div class="story-new-post-title">
										<p>NIEUWSTE STORY!</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="profile-menu">
						<div class="tab">
							<li><button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Profiel</button></li>
							<li><button class="tablinks" onclick="openCity(event, 'Paris')">Recepten</button></li>
							<li><button class="tablinks" onclick="openCity(event, 'saved-recipe-block')">Favorieten</button></li>
							<li><button class="tablinks" onclick="openCity(event, 'week')">Weekmenu</button></li>
							<li><button class="tablinks" onclick="openCity(event, 'inbox')">Inbox</button></li>
							<li><button class="tablinks" onclick="openCity(event, 'follow-section')">Followers</button></li>
						</div>
					</div>
				</div>
			</div>
			
			<div class="profile-body ipad-desk">
				<div class="row">
					<div class="col-md-3 sidebarprofile idek">
						<div class="user-info-part desktop-qa">
							<div class="profile-img">
								<?php echo do_shortcode('[avatar_upload]'); ?>
							</div>
							<div class="prfile-name">
								<?php if ( is_user_logged_in() ) { ?>
								<?php global $current_user; get_currentuserinfo(); ?>
								<p><?php echo '' . $current_user->user_login . "";?></p>
								<h2><?php echo '' . $current_user->user_firstname . ""; ?> <?php echo '' . $current_user->user_lastname . ""; ?></h2>
								<?php } else {   ?> 
								<?php } ?>
							</div>
							<div class="follwers">
								<?php echo do_shortcode('[follow-follower-posts userid="'.get_current_user_id().'"]'); ?>
							</div>
						</div>
						<div class="profile-menu desktop-qa">
							<div class="tab">
								<button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/user.png" alt="profile-icon" /> Mijn profiel</button>
								<button class="tablinks" onclick="openCity(event, 'Paris')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/fork.png" alt="profile-icon" /> Mijn recepten</button>
								<button class="tablinks" onclick="openCity(event, 'saved-recipe-block')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/tags.png" alt="profile-icon" /> Mijn favorieten</button>
								<button class="tablinks" onclick="openCity(event, 'week')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/wine-menu.png" alt="profile-icon" /> Mijn weekmenu</button>
								<button class="tablinks" onclick="openCity(event, 'inbox')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/inbox.png" alt="profile-icon" /> Mijn inbox</button>
								<button class="tablinks" onclick="openCity(event, 'follow-section')"><img src="https://dev.kookmutsjes.com/wp-content/uploads/2019/12/followers-1.png" alt="profile-icon" /> Followers</button>
							</div>
						</div>
					</div>
					
					<div class="col-md-9 bodyprofile">
						<div class="profile-body-top desktop-qa idek">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-6 siderow">
											<div class="recipe-new-post">
												<?php dynamic_sidebar('Profile Latest Recipe'); ?>
											</div>
										</div>
										<div class="col-md-6">
											<div class="recipe-new-post-title">
												<p>NIEUWSTE RECEPT!</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-6 siderow">
											<div class="story-new-post">
												<?php dynamic_sidebar('Profile Latest Post'); ?>
											</div>
										</div>
										<div class="col-md-6">
											<div class="story-new-post-title">
												<p>NIEUWSTE STORY!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="tab-contents">

									<div id="London" class="tabcontent">
										<div class="row">
											<div class="col-md-8 removepaddings">
												<div class="removepadding">
													<div class="mobile-qa"><h2 id="medit">Edit Profiel</h2></div>
													<?php echo do_shortcode('[wppb-edit-profile]'); ?>
												</div>
												<?php  /*?>
												<div class="mobile-profile-pic">
													<h2>Change Profile Image</h2>
													<?php echo do_shortcode('[avatar_upload]'); ?>
												</div>
												<?php */ ?>
											</div>
											<div class="col-md-4">
												<div class="profile-sidebar">
													<div class="feed">
														<?php echo do_shortcode('[init-feeds]');?>
													</div>
													<div class="myfriedns">
														<?php echo do_shortcode('[recent-followers]'); ?>
													</div>
													<div class="inbox">
														<h2>Inbox</h2>
														<?php echo do_shortcode( '[recent-messages-users]' ); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div id="Paris" class="tabcontent holder">
										<div class="row">
											<div class="col-md-12 removepadding holder">
											<?php  echo do_shortcode('[get-saved-favorites userid="'.get_current_user_id().'" saved="0" page="profile"]'); ?>
												<?php
												//delete_user_meta(get_current_user_id(),'saved_favorite');
												//	delete_user_meta(get_current_user_id(),'saved_favorite_post');
												
												?>
											</div>
										</div>
									</div>
									
									<div id="saved-recipe-block" class="tabcontent">
										<div class="row holder">
											<div class="profile-sidebar mobile-qa">
												<div class="saved-side">
													<h2>Jouw groepen</h2>
													<?php  echo do_shortcode('[get-saved-groups]'); ?>
												</div>
											</div>
											<div class="col-md-8 removepadding">												
												<?php  echo do_shortcode('[get-saved-favorites userid="'.get_current_user_id().'" saved="1" page="profile"]'); ?>
												
											</div>											
											<div class="col-md-4">
												<div class="profile-sidebar">
													<div class="saved-side desktop-qa">
														<h2>Jouw groepen</h2>
														<?php  echo do_shortcode('[get-saved-groups]'); ?>
													</div>
													<div class="saved-sides">
														<img src="<?php site_url();?>/wp-content/uploads/2019/12/ingre-profile-1.png" alt="Liked Recipes" />
														<div class="likedrecipes">
															<?php  echo do_shortcode('[liked-recipes]'); ?>
														</div>
													</div>
												</div>
											</div>
											
										</div>
									</div>
									
									<div id="week" class="tabcontent">
										<div class="row">
											<div class="col-md-8 removepadding">
												<h2 id="weektitle">STEL HIER JE WEEKMENU SAMEN</h2>
												<div class="weekname" id="week-day-1">
													<h1>MAANDAG</h1>
													<ul>
														<?php echo do_shortcode('[weekmenu-recipes weekday="1"]');?>
													</ul>
												</div>
												<div class="weekname" id="week-day-2">
													<h1>Dinsdag</h1>
													<ul>
													<?php echo do_shortcode('[weekmenu-recipes weekday="2"]');?>
													</ul>
												</div>
												<div class="weekname" id="week-day-3">
													<h1>WOENSDAG</h1>
													<ul>
													<?php echo do_shortcode('[weekmenu-recipes weekday="3"]');?>
													</ul>
												</div>
												<div class="weekname" id="week-day-4">
													<h1>DONDERDAG</h1>
													<ul>
													<?php echo do_shortcode('[weekmenu-recipes weekday="4"]');?>
													</ul>
												</div>
												<div class="weekname" id="week-day-5">
													<h1>VRIJDAG</h1>
													<ul>
													<?php echo do_shortcode('[weekmenu-recipes weekday="5"]');?>
													</ul>
												</div>
												<div class="weekname" id="week-day-6">
													<h1>ZATERDAG</h1>
													<ul>
													<?php echo do_shortcode('[weekmenu-recipes weekday="6"]');?>
													</ul>
												</div>
												<div class="weekname" id="week-day-7">
													<h1>ZONDAG</h1>
													<ul>
													<?php echo do_shortcode('[weekmenu-recipes weekday="7"]');?>
													</ul>
												</div>
												<?php echo do_shortcode('[get-group-list-for-weekmenu]');?>
											</div>

											<div class="col-md-4">
												<div class="profile-sidebar weekside">
													<?php  echo do_shortcode('[ingradient-list]'); ?>
												</div>
											</div>
										</div>
									</div>
									
									<div id="inbox" class="tabcontent">
										<div class="row">
											<?php echo do_shortcode('[webz-message]');?>
										</div>
									</div>
									
									<div id="follow-section" class="tabcontent">
										<div class="row">
											<div class="col-md-12 removepadding followerstab">
												<ul class="nav nav-tabs">
													<li class="sub-tablinks active"><a id="default-sub-tab-open" data-toggle="tab" href="#follower-tab">Followers</a></li>
													<li  class="sub-tablinks"><a data-toggle="tab" href="#following-tab">Following</a></li>
												</ul>
												<div class="tab-content">
													<div id="follower-tab" class="tab-pane active">
														<?php echo do_shortcode('[get-user-followers]');?>
													</div>
													<div id="following-tab" class="tab-pane fade">
														<?php echo do_shortcode('[get-user-following]');?>
													</div>
												</div>
											</div>
										</div>
									</div>

									<script>
										function openCity(evt, cityName) {
										  var i, tabcontent, tablinks;
										  tabcontent = document.getElementsByClassName("tabcontent");
										  for (i = 0; i < tabcontent.length; i++) {
											tabcontent[i].style.display = "none";
											tabcontent[i].className = "tabcontent";
										  }
										  tablinks = document.getElementsByClassName("tablinks");
										  for (i = 0; i < tablinks.length; i++) {
											tablinks[i].className = tablinks[i].className.replace(" active", "");
										  }
										  document.getElementById(cityName).style.display = "block";
										  document.getElementById(cityName).className += " active";

										  evt.currentTarget.className += " active";
										}
										// Get the element with id="defaultOpen" and click on it
										document.getElementById("defaultOpen").click();									

									</script>
									<script>
										document.addEventListener('DOMContentLoaded', () => {
											let myBtns=document.querySelectorAll('.sub-tablinks');
											myBtns.forEach(function(btn) {
												btn.addEventListener('click', () => {
												  myBtns.forEach(b => b.classList.remove('active'));
												  btn.classList.add('active');
												});
										 
											});

										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="delete-alert-model" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">				
				<div class="modal-body">
					<p>Weet je zeker dat je dit wil verwijderen?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary yes" data-dismiss="modal">Ja</button>
					<button type="button" class="btn btn-primary no" data-dismiss="modal">Nee</button>
				</div>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>